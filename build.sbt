// See README.md for license details.

def scalacOptionsVersion(scalaVersion: String): Seq[String] = {
  Seq() ++ {
    // If we're building with Scala > 2.11, enable the compile option
    //  switch to support our anonymous Bundle definitions:
    //  https://github.com/scala/bug/issues/10047
    CrossVersion.partialVersion(scalaVersion) match {
      case Some((2, scalaMajor: Long)) if scalaMajor < 12 => Seq()
      case _ => Seq("-Xsource:2.11")
    }
  }
}

def javacOptionsVersion(scalaVersion: String): Seq[String] = {
  Seq() ++ {
    // Scala 2.12 requires Java 8. We continue to generate
    //  Java 7 compatible code for Scala 2.11
    //  for compatibility with old clients.
    CrossVersion.partialVersion(scalaVersion) match {
      case Some((2, scalaMajor: Long)) if scalaMajor < 12 =>
        Seq("-source", "1.7", "-target", "1.7")
      case _ =>
        Seq("-source", "1.8", "-target", "1.8")
    }
  }
}

scalacOptions ++= scalacOptionsVersion(scalaVersion.value)

javacOptions ++= javacOptionsVersion(scalaVersion.value)

resolvers ++= Seq(
  Resolver.sonatypeRepo("snapshots"),
  Resolver.sonatypeRepo("releases")
)

addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full)

// Provide a managed dependency on X if -DXVersion="" is supplied on the command line.
val defaultVersions = Seq(
  "chisel-iotesters" -> "1.4.1+",
  "chiseltest"  -> "0.2.1+"
  )

libraryDependencies ++= defaultVersions.map { case (dep, ver) =>
  "edu.berkeley.cs" %% dep % sys.props.getOrElse(dep + "Version", ver) }


lazy val hsceval = (project in file("."))
    .settings(
      version := "0.0.1-SNAPSHOT",
      organization := "cowlibry",
      scalaVersion := "2.12.10",
      publishLocal := {},
      publish := {},
      packagedArtifacts := Map.empty,
      libraryDependencies += "edu.berkeley.cs" %% "chisel3" % "3.3.+",
      libraryDependencies += "edu.berkeley.cs" %% "chisel-iotesters" % "1.4.1+",
      libraryDependencies += "edu.berkeley.cs" %% "chiseltest" % "0.2.1+",
      scalacOptions ++= scalacOptionsVersion(scalaVersion.value),
      javacOptions ++= javacOptionsVersion(scalaVersion.value)
    )
    .dependsOn(common % "test->test;compile->compile", aubrac % "test->test;compile->compile", ferrandaise % "test->test;compile->compile", salers % "test->test;compile->compile", manseng % "test->test;compile->compile")
    .aggregate(common, aubrac, salers, ferrandaise, manseng)

lazy val common = (project in file("cores/common"))
    .settings(
      version := "0.0.1-SNAPSHOT",
      organization := "cowlibry",
      scalaVersion := "2.12.10",
      publishLocal := {},
      publish := {},
      packagedArtifacts := Map.empty,
      libraryDependencies += "edu.berkeley.cs" %% "chisel3" % "3.3.+",
      libraryDependencies += "edu.berkeley.cs" %% "chisel-iotesters" % "1.4.1+",
      libraryDependencies += "edu.berkeley.cs" %% "chiseltest" % "0.2.1+",
      scalacOptions ++= scalacOptionsVersion(scalaVersion.value),
      javacOptions ++= javacOptionsVersion(scalaVersion.value)
    )

lazy val aubrac = (project in file("cores/core/aubrac"))
    .settings(
      version := "0.0.1-SNAPSHOT",
      organization := "cowlibry",
      scalaVersion := "2.12.10",
      publishLocal := {},
      publish := {},
      packagedArtifacts := Map.empty,
      libraryDependencies += "edu.berkeley.cs" %% "chisel3" % "3.3.+",
      libraryDependencies += "edu.berkeley.cs" %% "chisel-iotesters" % "1.4.1+",
      libraryDependencies += "edu.berkeley.cs" %% "chiseltest" % "0.2.1+",
      scalacOptions ++= scalacOptionsVersion(scalaVersion.value),
      javacOptions ++= javacOptionsVersion(scalaVersion.value)
    )
    .dependsOn(common % "test->test;compile->compile", manseng % "test->test;compile->compile")
    .aggregate(common, manseng)

lazy val ferrandaise = (project in file("cores/core/ferrandaise"))
    .settings(
      version := "0.0.1-SNAPSHOT",
      organization := "cowlibry",
      scalaVersion := "2.12.10",
      publishLocal := {},
      publish := {},
      packagedArtifacts := Map.empty,
      libraryDependencies += "edu.berkeley.cs" %% "chisel3" % "3.3.+",
      libraryDependencies += "edu.berkeley.cs" %% "chisel-iotesters" % "1.4.1+",
      libraryDependencies += "edu.berkeley.cs" %% "chiseltest" % "0.2.1+",
      scalacOptions ++= scalacOptionsVersion(scalaVersion.value),
      javacOptions ++= javacOptionsVersion(scalaVersion.value)
    )
    .dependsOn(common % "test->test;compile->compile", manseng % "test->test;compile->compile", aubrac % "test->test;compile->compile")
    .aggregate(common, manseng, aubrac)

lazy val salers = (project in file("cores/core/salers"))
    .settings(
      version := "0.0.1-SNAPSHOT",
      organization := "cowlibry",
      scalaVersion := "2.12.10",
      publishLocal := {},
      publish := {},
      packagedArtifacts := Map.empty,
      libraryDependencies += "edu.berkeley.cs" %% "chisel3" % "3.3.+",
      libraryDependencies += "edu.berkeley.cs" %% "chisel-iotesters" % "1.4.1+",
      libraryDependencies += "edu.berkeley.cs" %% "chiseltest" % "0.2.1+",
      scalacOptions ++= scalacOptionsVersion(scalaVersion.value),
      javacOptions ++= javacOptionsVersion(scalaVersion.value)
    )
    .dependsOn(common % "test->test;compile->compile", manseng % "test->test;compile->compile", aubrac % "test->test;compile->compile", ferrandaise % "test->test;compile->compile")
    .aggregate(common, manseng, aubrac, ferrandaise)

lazy val manseng = (project in file("cores/mem/manseng"))
    .settings(
      version := "0.0.1-SNAPSHOT",
      organization := "cowlibry",
      scalaVersion := "2.12.10",
      publishLocal := {},
      publish := {},
      packagedArtifacts := Map.empty,
      libraryDependencies += "edu.berkeley.cs" %% "chisel3" % "3.3.+",
      libraryDependencies += "edu.berkeley.cs" %% "chisel-iotesters" % "1.4.1+",
      libraryDependencies += "edu.berkeley.cs" %% "chiseltest" % "0.2.1+",
      scalacOptions ++= scalacOptionsVersion(scalaVersion.value),
      javacOptions ++= javacOptionsVersion(scalaVersion.value)
    )
    .dependsOn(common % "test->test;compile->compile")
    .aggregate(common)
