/*
 * File: Dome_l1i.scala
 * Project: aubrac
 * Created Date: Thursday September 3rd 2020
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 24th November 2020 11:50:40 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2020 INRIA
 */
package eval.aubrac

import java.io._

import scala._
import scala.collection.mutable.ListBuffer

import chisel3._
import chisel3.util._
import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

import cowlibry.core.aubrac.{AubracTop, AubracParams}


trait AubracL1IParams extends AubracParams {
  // ******************************
  //       GLOBAL PARAMETERS
  // ******************************
  def debug = true
  def pcBoot: Int = 0x40
  def useDome: Boolean = false
  def useDomeConstFlush: Boolean = true
  def nDomeFlushCycle: Int = 10

  // ******************************
  //            PIPELINE
  // ******************************
  // ------------------------------
  //             FETCH
  // ------------------------------
  def nFetchInstr: Int = 2
  def useIMemSeq: Boolean = false
  def useIf1Stage: Boolean = false
  def nFetchFifoDepth: Int = 4

  def useNlp: Boolean = false
  def nBhtSet: Int = 8
  def nBhtSetEntry: Int = 128
  def nBhtBit: Int = 2
  def nBtbLine: Int = 16
  def nBtbTagBit: Int = 10

  // ------------------------------
  //           EXECUTION
  // ------------------------------
  def useMulDiv: Boolean = true
  def useFencei: Boolean = true
  def useMemStage: Boolean = true
  def useBranchReg: Boolean = true

  // ******************************
  //           L1I CACHE
  // ******************************
  def nL1ISetReadPort: Int = 1
  def nL1ISetWritePort: Int = 1
  def nL1IWordByte: Int = 16
  def slctL1IPolicy: String = "BitPLRU"
  def nL1ISubCache: Int = 1
  def nL1ISet: Int = 8
  def nL1ILine: Int = 2
  def nL1IWord: Int = 4

  // ******************************
  //           L1D CACHE
  // ******************************
  def nL1DSetReadPort: Int = 1
  def nL1DSetWritePort: Int = 1
  def nL1DWordByte: Int = 16
  def slctL1DPolicy: String = "BitPLRU"
  // def slctL1DPolicy: String = "LRU"
  def nL1DSubCache: Int = 1
  def nL1DSet: Int = 8
  def nL1DLine: Int = 2
  def nL1DWord: Int = 2
}

object AubracUL1ICfg extends AubracL1IParams {
  override val useDome: Boolean = false
}

object AubracPL1ICfg extends AubracL1IParams {
  override val useDome: Boolean = true
}

object AubracUL1I extends App {
  chisel3.Driver.execute(args, () => new AubracTop(AubracUL1ICfg))
}

object AubracPL1I extends App {
  chisel3.Driver.execute(args, () => new AubracTop(AubracPL1ICfg))
}
