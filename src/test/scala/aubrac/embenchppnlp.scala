/*
 * File: embenchpnlp copy.scala
 * Project: aubrac
 * Created Date: Wednesday September 23rd 2020
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 5th November 2020 4:32:04 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2020 INRIA
 */
package eval.aubrac

import java.io._

import scala._
import scala.collection.mutable.ListBuffer

import chisel3._
import chisel3.util._
import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

import cowlibry.core.aubrac.{Aubrac, AubracTop, AubracParams}


object AubracCfgPPNLP extends AubracParams {
  // ******************************
  //       GLOBAL PARAMETERS
  // ******************************
  def debug = true
  def pcBoot: Int = 0x40
  def useDome: Boolean = true
  def useDomeConstFlush: Boolean = true
  def nDomeFlushCycle: Int = 10

  // ******************************
  //            PIPELINE
  // ******************************
  // ------------------------------
  //             FETCH
  // ------------------------------
  def nFetchInstr: Int = 2
  def useIMemSeq: Boolean = false
  def useIf1Stage: Boolean = false
  def nFetchFifoDepth: Int = 4

  def useNlp: Boolean = true
  def nBhtSet: Int = 8
  def nBhtSetEntry: Int = 128
  def nBhtBit: Int = 2
  def nBtbLine: Int = 16
  def nBtbTagBit: Int = 16

  // ------------------------------
  //           EXECUTION
  // ------------------------------
  def useMulDiv: Boolean = true
  def useFencei: Boolean = true
  def useMemStage: Boolean = true
  def useBranchReg: Boolean = true

  // ******************************
  //           L1I CACHE
  // ******************************
  def nL1ISetReadPort: Int = 1
  def nL1ISetWritePort: Int = 1
  def nL1IWordByte: Int = 16
  def slctL1IPolicy: String = "BitPLRU"
  def nL1ISubCache: Int = 1
  def nL1ISet: Int = 16
  def nL1ILine: Int = 4
  def nL1IWord: Int = 4

  // ******************************
  //           L1D CACHE
  // ******************************
  def nL1DSetReadPort: Int = 1
  def nL1DSetWritePort: Int = 1
  def nL1DWordByte: Int = 16
  def slctL1DPolicy: String = "BitPLRU"
  def nL1DSubCache: Int = 1
  def nL1DSet: Int = 16
  def nL1DLine: Int = 4
  def nL1DWord: Int = 4
}

object AubracEmbenchPPNLP extends App {
  chisel3.Driver.execute(args, () => new AubracTop(AubracCfgPPNLP))
}

object AubracSynthPPNLP extends App {
  chisel3.Driver.execute(args, () => new Aubrac(AubracCfgPPNLP))
}
