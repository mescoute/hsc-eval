with import <nixpkgs> {};

let
     pkgs_verilator4016 = import (builtins.fetchGit {
         # Descriptive name to make the store path easier to identify                
         name = "my-old-verilator";                                                 
         url = "https://github.com/nixos/nixpkgs-channels/";                       
         ref = "refs/heads/nixpkgs-unstable";                     
         rev = "84f318e323989435d5dd54b4038b0af728f20c85";                                           
     }) {};                                                                           
in

# Make a new "derivation" that represents our shell
stdenv.mkDerivation {
  name = "chisel";

  # The packages in the `buildInputs` list will be added to the PATH in our shell
  buildInputs = [
    pkgs_verilator4016.verilator
    pkgs_verilator4016.sbt
  ];
}