PRJ_DIR=`pwd`
TEST_DIR=verilator
LOG_DIR=${TEST_DIR}/log

all: embench-eval sec-eval fpga

# Test for toolchain existence, install if needed
TC_TEST=$(shell command -v riscv32-inria-elf-gcc 2>/dev/null)
ifeq ($(TC_TEST), )
.PHONY: toolchain
toolchain:
	@echo "Toolchain installation... This operation may take a few minutes."
	nix-env -i -f ./toolchain/riscv32-inria-elf.nix
	@echo "Toolchain installation completed"
else
.PHONY: toolchain
toolchain:
	@echo "Toolchain OK"
endif

sec-eval-cores:
	@./verilator/security_eval/build_cores.sh

sec-eval-apps: toolchain
	@./eval/hsc_evaluator/build_apps.sh

sec-eval: sec-eval-cores sec-eval-apps
	@./verilator/security_eval/build_evals.sh

embench-eval-cores:
	@./verilator/embench/build_cores.sh

embench-eval-apps: toolchain
	@./eval/embench/build_apps.sh

embench-eval-apps-smt: toolchain
	@./eval/embench_smt/build_apps.sh

embench-eval: embench-eval-cores embench-eval-apps embench-eval-apps-smt
	@./verilator/embench/build_evals.sh

fpga:
	@./verilator/fpga/build_fpga.sh


# clean:
# 	rm -rf ${TEST_DIR}
# 	rm -rf output
# 	rm -rf target
