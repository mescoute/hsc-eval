#!/bin/bash

PRJ_DIR=`pwd`
TEST_DIR=${PRJ_DIR}/verilator
GEN_DIR=${PRJ_DIR}/gen
VCD_DIR=${TEST_DIR}/vcd
LOG_DIR=${TEST_DIR}/log
EXE_DIR=${TEST_DIR}/exe

CFG=$1
DESIGN_NAME=FerrandaiseDbg
TESTER_NAME=ferrandaise-dbg

KEEP_EXE=true

TEST_FILE="${GEN_DIR}/core/ferrandaise/scripts/${TESTER_NAME}${CFG}.test"

sbt "test:runMain cowlibry.core.ferrandaise.${DESIGN_NAME}${CFG} --target-dir verilator/src --top-name ${DESIGN_NAME}"
verilator -Wno-WIDTH -cc ${TEST_DIR}/src/${DESIGN_NAME}.v ${TEST_DIR}/src/initram.sv --exe --Mdir ${TEST_DIR}/obj_dir --build ${GEN_DIR}/core/ferrandaise/src/test/scala/top/${TESTER_NAME}.cpp --trace

mkdir -p ${VCD_DIR}/${TESTER_NAME}${CFG}
mkdir -p ${LOG_DIR}
rm -f ${LOG_DIR}/${TESTER_NAME}${CFG}.log

{ while IFS= read -r line
do
  test=`echo "$line" | awk '{print $1;}'`
  trigger=`echo "$line" | awk '{print $2;}'`
  ${TEST_DIR}/obj_dir/V${DESIGN_NAME} $trigger sw/isa-tests/hex/asm-self/${test}.hex ${VCD_DIR}/${TESTER_NAME}${CFG}/${test}.vcd
done < "${TEST_FILE}"
} | tee ${LOG_DIR}/${TESTER_NAME}${CFG}.log

if [ "${KEEP_EXE}" ]
then
  mkdir -p ${EXE_DIR}
  cp ${TEST_DIR}/obj_dir/V${DESIGN_NAME} ${EXE_DIR}/V${DESIGN_NAME}${CFG}
fi
