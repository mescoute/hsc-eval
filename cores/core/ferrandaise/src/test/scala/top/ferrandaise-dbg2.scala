package cowlibry.core.ferrandaise

import chisel3._
import chisel3.util._


object FerrandaiseDbg2Cfg extends FerrandaiseParams {
  // ******************************
  //       GLOBAL PARAMETERS
  // ******************************
  override def debug: Boolean = true
  override def pcBoot: Int = 0x1000
  override def useDome: Boolean = false
  override def useDomeConstFlush: Boolean = true
  override def nDomeFlushCycle: Int = 10

  // ******************************
  //            PIPELINE
  // ******************************
  // ------------------------------
  //            FRONT-END
  // ------------------------------
  override def nFetchInstr: Int = 2
  override def useIMemSeq: Boolean = false
  override def useIf1Stage: Boolean = false
  override def nFetchFifoDepth: Int = 8

  override def useNlp: Boolean = true
  override def nBhtSet: Int = 8
  override def nBhtSetEntry: Int = 128
  override def nBhtBit: Int = 2
  override def nBtbLine: Int = 16
  override def nBtbTagBit: Int = 10

  // ------------------------------
  //           BACK-END
  // ------------------------------
  override def nBackPort: Int = 2
  override def nAlu: Int = 2
  override def nMulDiv: Int = 1
  override def useFencei: Boolean = true
  override def useBranchReg: Boolean = true

  // ******************************
  //           L1I CACHE
  // ******************************
  def nL1ISetReadPort: Int = 1
  def nL1ISetWritePort: Int = 1
  def nL1IWordByte: Int = 16
  def slctL1IPolicy: String = "BitPLRU"
  def nL1ISubCache: Int = 1
  def nL1ISet: Int = 8
  def nL1ILine: Int = 4
  def nL1IWord: Int = 8

  // ******************************
  //           L1D CACHE
  // ******************************
  def nL1DSetReadPort: Int = 1
  def nL1DSetWritePort: Int = 1
  def nL1DWordByte: Int = 16
  def slctL1DPolicy: String = "BitPLRU"
  def nL1DSubCache: Int = 1
  def nL1DSet: Int = 8
  def nL1DLine: Int = 4
  def nL1DWord: Int = 4
}

object FerrandaiseDbg2 extends App {
  chisel3.Driver.execute(args, () => new FerrandaiseTop(FerrandaiseDbg2Cfg))
}
