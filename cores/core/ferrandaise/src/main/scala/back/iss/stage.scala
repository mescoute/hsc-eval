package cowlibry.core.ferrandaise.back

import chisel3._
import chisel3.util._

import cowlibry.common.isa._
import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._
import cowlibry.core.aubrac.back.{ReadGprIO, ExStageBus}
import cowlibry.core.aubrac.back.{EXUNIT, OP}


class IssStage(p: BackParams) extends Module {
  val io = IO(new Bundle {
    val b_hart = Vec(p.nHart, new RsrcBus(p.nHart, p.nDome, 1))
    val b_backport = Vec(p.nBackPort, new RsrcBus(p.nHart, p.nDome, 1))

    val i_lock = Input(Vec(p.nBackPort, Bool()))
    val o_lock = Output(Vec(p.nBackPort, Bool()))
    val i_flush = Input(Vec(p.nHart, Bool()))

    val o_br_next = Output(Vec(p.nHart, new BranchBus(32)))
    val i_end = Input(Vec(p.nHart, Bool()))
    val i_pend = Input(Vec(p.nHart, Bool()))

    val i_valid = Input(Vec(p.nBackPort, Bool()))
    val i_bus =  Input(Vec(p.nBackPort, new IssStageBus(p)))

    val i_rs_wait = Input(Vec(p.nBackPort * 2, Bool()))
    val b_rs = Vec(p.nBackPort * 2, Flipped(new ReadGprIO(p)))
    val i_rmu_wait = if (p.useDome) Some(Input(Vec(p.nHart, Bool()))) else None

    val b_rsrc_alu = Vec(p.nAlu, new RsrcBus(1, p.nDome, p.nAlu))
    val i_free_alu = Input(Vec(p.nAlu, Bool()))
    val b_rsrc_muldiv = if (p.nMulDiv > 0) Some(Vec(p.nMulDiv, new RsrcBus(1, p.nDome, p.nMulDiv))) else None
    val i_free_muldiv = if (p.nMulDiv > 0) Some(Input(Vec(p.nMulDiv, Bool()))) else None

    val o_valid = Output(Vec(p.nBackPort, Bool()))
    val o_bus =  Output(Vec(p.nBackPort, new ExStageBus(p)))
  })

  val w_valid = Wire(Vec(p.nBackPort, Bool()))
  val w_flush = Wire(Vec(p.nBackPort, Bool()))
  val w_lock = Wire(Vec(p.nBackPort, Bool()))
  val w_wait = Wire(Vec(p.nBackPort, Bool()))

  // ******************************
  //           UOP STATUS
  // ******************************
  val reg_done = RegInit(VecInit(Seq.fill(p.nBackPort)(false.B)))
  val w_i_valid = Wire(Vec(p.nBackPort, Bool()))
  for (bp <- 0 until p.nBackPort) {
    w_i_valid(bp) := io.i_valid(bp) & ~reg_done(bp)
  }

  // ******************************
  //          NEXT BRANCH
  // ******************************
  for (h <- 0 until p.nHart) {
    io.o_br_next(h).valid := false.B
    io.o_br_next(h).pc := io.i_bus(0).info.pc

    for (bp <- 0 until p.nBackPort) {
      when (w_i_valid(p.nBackPort - 1 - bp) & (h.U === io.i_bus(p.nBackPort - 1 - bp).info.hart)) {
        io.o_br_next(h).valid := true.B
        io.o_br_next(h).pc := io.i_bus(p.nBackPort - 1 - bp).info.pc
      }
    }
  }

  // ******************************
  //              END
  // ******************************
  val w_end = Wire(Vec(p.nBackPort, Bool()))

  for (bp <- 0 until p.nBackPort) {
    w_end(bp) := io.i_end(io.i_bus(bp).info.hart)
  }

  for (bp0 <- 0 until p.nBackPort) {
    for (bp1 <- (bp0 + 1) until p.nBackPort) {
      when (w_i_valid(bp0) & w_i_valid(bp1) & io.i_bus(bp0).info.end & (io.i_bus(bp0).info.hart === io.i_bus(bp1).info.hart)) {
        w_end(bp1) := true.B
      }
    }
  }

  // ******************************
  //        HAS PREDECESSOR
  // ******************************
  val w_has_pred = Wire(Vec(p.nBackPort, Bool()))

  for (bp <- 0 until p.nBackPort) {
    w_has_pred(bp) := false.B
  }

  for (bp0 <- 0 until p.nBackPort) {
    for (bp1 <- (bp0 + 1) until p.nBackPort) {
      when (w_i_valid(bp0) & (io.i_bus(bp0).info.hart === io.i_bus(bp1).info.hart)) {
        w_has_pred(bp1) := true.B
      }
    }
  }

  // ******************************
  //       WAIT EMPTY PIPELINE
  // ******************************
  val w_empty_wait = Wire(Vec(p.nBackPort, Bool()))

  for (bp <- 0 until p.nBackPort) {
    w_empty_wait(bp) := w_i_valid(bp) & io.i_bus(bp).info.empty & (io.i_pend(io.i_bus(bp).info.hart) | w_has_pred(bp))
  }

  // ******************************
  //          GPR ACCESSES
  // ******************************
  for (bp <- 0 until p.nBackPort) {
    io.b_rs(bp * 2).hart := io.i_bus(bp).info.hart
    io.b_rs(bp * 2).addr := io.i_bus(bp).data.rs1
    io.b_rs(bp * 2 + 1).hart := io.i_bus(bp).info.hart
    io.b_rs(bp * 2 + 1).addr := io.i_bus(bp).data.rs2
  }

  // ******************************
  //           DISPATCHER
  // ******************************
  val dispatcher = Module(new Dispatcher(p))
  for (bp <- 0 until p.nBackPort) {
    dispatcher.io.i_req(bp) := w_i_valid(bp)
    dispatcher.io.i_hart(bp) := io.i_bus(bp).info.hart
    dispatcher.io.i_dome(bp) := io.b_backport(bp).dome
    dispatcher.io.i_unit(bp) := io.i_bus(bp).ex.unit
  }

  dispatcher.io.b_rsrc_alu <> io.b_rsrc_alu
  dispatcher.io.i_free_alu := io.i_free_alu
  if (p.nMulDiv > 0) dispatcher.io.b_rsrc_muldiv.get <> io.b_rsrc_muldiv.get
  if (p.nMulDiv > 0) dispatcher.io.i_free_muldiv.get := io.i_free_muldiv.get

  // ******************************
  //     IN-STAGE DEPENDENCIES
  // ******************************
  val w_depend_in = Wire(Vec(p.nBackPort, Bool()))

  for (bp <- 0 until p.nBackPort) {
    w_depend_in(bp) := false.B
  }

  for (bp0 <- 0 until p.nBackPort) {
    for (bp1 <- (bp0 + 1) until p.nBackPort) {
      when (w_i_valid(bp0) & io.i_bus(bp0).wb.en) {
        when (w_i_valid(bp1) & (io.i_bus(bp0).info.hart === io.i_bus(bp1).info.hart)) {
          when (io.i_bus(bp1).data.s1_is_reg & (io.i_bus(bp1).data.rs1 === io.i_bus(bp0).wb.addr)) {
            w_depend_in(bp1) := true.B
          }
          when ((io.i_bus(bp1).data.s1_is_reg | io.i_bus(bp1).data.s2_is_reg) & (io.i_bus(bp1).data.rs2 === io.i_bus(bp0).wb.addr)) {
            w_depend_in(bp1) := true.B
          }
        }
      }
    }
  }

  // ******************************
  //    SOURCE AND DEPENDENCIES
  // ******************************
  val w_depend_rs = Wire(Vec(p.nBackPort, Bool()))
  val w_s1 = Wire(Vec(p.nBackPort, UInt(32.W)))
  val w_s2 = Wire(Vec(p.nBackPort, UInt(32.W)))
  val w_s3 = Wire(Vec(p.nBackPort, UInt(32.W)))

  for (bp <- 0 until p.nBackPort) {
    w_depend_rs(bp) := false.B

    // ------------------------------
    //              S1
    // ------------------------------
    when (w_i_valid(bp) & (io.i_bus(bp).data.s1_is_reg === OP.REG.U) & io.i_rs_wait(bp * 2)) {
      w_depend_rs(bp) := true.B
    }
    w_s1(bp) := Mux(io.i_bus(bp).data.s1_is_reg, io.b_rs(bp * 2).data, io.i_bus(bp).data.s1)

    // ------------------------------
    //              S2
    // ------------------------------
    when (w_i_valid(bp) & (io.i_bus(bp).data.s2_is_reg === OP.REG.U) & io.i_rs_wait(bp * 2 + 1)) {
      w_depend_rs(bp) := true.B
    }
    w_s2(bp) := Mux(io.i_bus(bp).data.s2_is_reg, io.b_rs(bp * 2 + 1).data, io.i_bus(bp).data.s2)

    // ------------------------------
    //              S3
    // ------------------------------
    when (w_i_valid(bp) & io.i_bus(bp).data.s3_is_reg === OP.REG.U & io.i_rs_wait(bp * 2 + 1)) {
      w_depend_rs(bp) := true.B
    }
    w_s3(bp) := Mux(io.i_bus(bp).data.s3_is_reg, io.b_rs(bp * 2 + 1).data, io.i_bus(bp).data.s3)
  }

  // ******************************
  //           SERIALIZE
  // ******************************
  val w_ser = Wire(Vec(p.nBackPort, Bool()))

  for (bp <- 0 until p.nBackPort) {
    w_ser(bp) := w_i_valid(bp) & io.i_bus(bp).info.ser & w_has_pred(bp)
  }

  for (bp0 <- 0 until p.nBackPort) {
    for (bp1 <- (bp0 + 1) until p.nBackPort) {
      when (w_i_valid(bp0) & io.i_bus(bp0).info.ser & (io.i_bus(bp0).info.hart === io.i_bus(bp1).info.hart)) {
        w_ser(bp1) := true.B
      }
    }
  }

  // ******************************
  //              RMU
  // ******************************
  // ------------------------------
  //              CSR
  // ------------------------------
  val w_rmu_csr = Wire(Vec(p.nBackPort, Bool()))

  for (bp <- 0 until p.nBackPort) {
    w_rmu_csr(bp) := w_i_valid(bp) & io.i_bus(bp).csr.write & ((io.i_bus(bp).data.s3(11,0) === DOME.CSRNEXTID.U) | (io.i_bus(bp).data.s3(11,0) === DOME.CSRNEXTPC.U) | (io.i_bus(bp).data.s3(11,0) === DOME.CSRNEXTCAP.U))
  }

  // ------------------------------
  //          DEPENDENCIES
  // ------------------------------
  val w_rmu_depend = Wire(Vec(p.nBackPort, Bool()))

  for (bp <- 0 until p.nBackPort) {
    if (p.useDome) w_rmu_depend(bp) := w_i_valid(bp) & (io.i_bus(bp).ex.unit === EXUNIT.RMU.U) & io.i_rmu_wait.get(io.i_bus(bp).info.hart) else w_rmu_depend(bp) := false.B
  }

  for (bp0 <- 0 until p.nBackPort) {
    for (bp1 <- (bp0 + 1) until p.nBackPort) {
      when (w_i_valid(bp0) & w_rmu_csr(bp0) & (io.i_bus(bp1).ex.unit === EXUNIT.RMU.U) & (io.i_bus(bp0).info.hart === io.i_bus(bp1).info.hart)) {
        w_rmu_depend(bp1) := true.B
      }
    }
  }

  // ******************************
  //          MANAGE STAGE
  // ******************************
  // ------------------------------
  //          SPREAD LOCK
  // ------------------------------
  val w_h_lock = Wire(Vec(p.nHart, Bool()))
  val w_h_wait = Wire(Vec(p.nHart, Vec(p.nBackPort + 1, Bool())))

  for (h <- 0 until p.nHart) {
    w_h_wait(h)(0) := false.B
  }

  for (h <- 0 until p.nHart) {
    for (bp <- 0 until p.nBackPort) {
      when (h.U === io.i_bus(bp).info.hart) {
        w_h_wait(h)(bp + 1) := w_h_wait(h)(bp) | w_h_lock(h) | w_end(bp) | dispatcher.io.o_miss(bp) | w_depend_in(bp) | w_depend_rs(bp) | w_ser(bp) | w_rmu_depend(bp) | w_empty_wait(bp)
      }.otherwise {
        w_h_wait(h)(bp + 1) := w_h_wait(h)(bp)
      }
    }
  }

  // ------------------------------
  //         BACK PORT STATE
  // ------------------------------
  for (bp <- 0 until p.nBackPort) {
    w_flush(bp) := false.B
    w_wait(bp) := false.B
    for (h <- 0 until p.nHart) {
      when (h.U === io.i_bus(bp).info.hart) {
        w_flush(bp) := io.i_flush(h) | io.b_hart(h).flush
        w_wait(bp) := w_h_wait(h)(bp + 1)
      }
    }
  }

  // ******************************
  //            REGISTERS
  // ******************************
  val reg_valid = RegInit(VecInit(Seq.fill(p.nBackPort)(false.B)))
  val reg_bus = Reg(Vec(p.nBackPort, new ExStageBus(p)))

  // ------------------------------
  //              DONE
  // ------------------------------
  for (h <- 0 until p.nHart) {
    for (bp <- 0 until p.nBackPort) {
      when ((h.U === io.i_bus(bp).info.hart) & w_h_wait(h)(p.nBackPort)) {
        reg_done(bp) := reg_done(bp) | (io.i_valid(bp) & ~w_wait(bp) & ~w_flush(bp))
      }.elsewhen (h.U === io.i_bus(bp).info.hart) {
        reg_done(bp) := false.B
      }
    }
  }

  // ------------------------------
  //             LOCK
  // ------------------------------
  for (bp <- 0 until p.nBackPort) {
    w_lock(bp) := reg_valid(bp) & io.i_lock(bp)
  }

  for (h <- 0 until p.nHart) {
    w_h_lock(h) := false.B
    for (bp <- 0 until p.nBackPort) {
      when ((h.U === reg_bus(bp).info.hart) & w_lock(bp)) {
        w_h_lock(h) := true.B
      }
    }
  }

  // ------------------------------
  //               BUS
  // ------------------------------
  for (bp <- 0 until p.nBackPort) {
    w_valid(bp) := w_i_valid(bp) & ~w_flush(bp) & ~w_wait(bp)
  }

  for (bp <- 0 until p.nBackPort) {
    when (io.b_hart(reg_bus(bp).info.hart).flush | ~w_lock(bp)) {
      reg_valid(bp) := w_valid(bp)

      reg_bus(bp).info := io.i_bus(bp).info
      reg_bus(bp).ex.unit := io.i_bus(bp).ex.unit
      reg_bus(bp).ex.port := dispatcher.io.o_port(bp)
      reg_bus(bp).ex.uop := io.i_bus(bp).ex.uop
      reg_bus(bp).mem := io.i_bus(bp).mem
      reg_bus(bp).csr := io.i_bus(bp).csr
      reg_bus(bp).wb := io.i_bus(bp).wb

      reg_bus(bp).data.s1 := w_s1(bp)
      reg_bus(bp).data.s2 := w_s2(bp)
      reg_bus(bp).data.s3 := w_s3(bp)
    }
  }

  for (bp <- 0 until p.nBackPort) {
    io.o_lock(bp) := false.B
    for (h <- 0 until p.nHart) {
      when (h.U === io.i_bus(bp).info.hart) {
        io.o_lock(bp) := w_h_wait(h)(p.nBackPort)
      }
    }
  }

  // ******************************
  //            OUTPUTS
  // ******************************
  // ------------------------------
  //               BUS
  // ------------------------------
  io.o_valid := reg_valid
  io.o_bus := reg_bus

  // ------------------------------
  //              FREE
  // ------------------------------
  for (h <- 0 until p.nHart) {
    val w_free = Wire(Vec(p.nBackPort, Bool()))

    for (bp <- 0 until p.nBackPort) {
      when (h.U === reg_bus(bp).info.hart) {
        w_free(bp) := ~reg_valid(bp)
      }.otherwise {
        w_free(bp) := true.B
      }
    }

    io.b_hart(h).free := w_free.asUInt.andR
  }

  for (bp <- 0 until p.nBackPort) {
    io.b_backport(bp).free := ~reg_valid(bp)
  }
}

object IssStage extends App {
  chisel3.Driver.execute(args, () => new IssStage(BackDefault))
}
