package cowlibry.core.ferrandaise.back

import chisel3._
import chisel3.util._

import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._
import cowlibry.core.aubrac.back.{EXUNIT}

class SlctPort(nDome: Int, nUnit: Int) extends Module {
  val io = IO(new Bundle {
    val i_req = Input(Bool())
    val i_dome = Input(UInt(log2Ceil(nDome).W))

    val b_rsrc = Vec(nUnit, new RsrcBus(1, nDome, nUnit))
    val i_free = Input(Vec(nUnit, Bool()))

    val o_av = Output(Bool())
    val o_port = Output(UInt(log2Ceil(nUnit).W))
    val o_free = Output(Vec(nUnit, Bool()))
  })

  // ******************************
  //             USE
  // ******************************
  val w_use = Wire(Vec(nUnit, Bool()))

  for (u <- 0 until nUnit) {
    w_use(u) := io.i_free(u) & io.b_rsrc(u).valid & ~io.b_rsrc(u).flush & (io.i_dome === io.b_rsrc(u).dome)
  }

  // ******************************
  //             LOGIC
  // ******************************
  val w_av = w_use.asUInt.orR
  val w_port = PriorityEncoder(w_use.asUInt)
  val w_free = Wire(Vec(nUnit, Bool()))


  for (u <- 0 until nUnit) {
    w_free(u) := Mux(io.i_req & w_av & (u.U === w_port), false.B, io.i_free(u))
  }

  // ******************************
  //             OUTPUT
  // ******************************
  io.o_av := w_av
  io.o_port := w_port
  io.o_free := w_free

  for (u <- 0 until nUnit) {
    io.b_rsrc(u).free := true.B
  }
}

class SlctNPort(nDome: Int, nBackPort: Int, nUnit: Int) extends Module {
  val io = IO(new Bundle {
    val i_req = Input(Vec(nBackPort, Bool()))
    val i_dome = Input(Vec(nBackPort, UInt(log2Ceil(nDome).W)))

    val b_rsrc = Vec(nUnit, new RsrcBus(1, nDome, nUnit))
    val i_free = Input(Vec(nUnit, Bool()))

    val o_av = Output(Vec(nBackPort, Bool()))
    val o_port = Output(Vec(nBackPort, UInt(log2Ceil(nUnit).W)))
  })

  val slct = Seq.fill(nBackPort) {Module(new SlctPort(nDome, nUnit))}
  slct(0).io.i_req := io.i_req(0)
  slct(0).io.i_dome := io.i_dome(0)
  slct(0).io.b_rsrc <> io.b_rsrc
  slct(0).io.i_free := io.i_free

  for (bp <- 1 until nBackPort) {
    slct(bp).io.i_req := io.i_req(bp)
    slct(bp).io.i_dome := io.i_dome(bp)
    slct(bp).io.b_rsrc <> io.b_rsrc
    slct(bp).io.i_free := slct(bp - 1).io.o_free
  }

  for (bp <- 0 until nBackPort) {
    io.o_av(bp):= slct(bp).io.o_av
    io.o_port(bp):= slct(bp).io.o_port
  }
}

class ReqUnit(p: DispatcherParams) extends Module {
  val io = IO(new Bundle {
    val i_req = Input(Vec(p.nBackPort, Bool()))
    val i_unit = Input(Vec(p.nBackPort, UInt(EXUNIT.NBIT.W)))

    val o_alu = Output(Vec(p.nBackPort, Bool()))
    val o_bru = Output(Vec(p.nBackPort, Bool()))
    val o_muldiv = if (p.nMulDiv > 0) Some(Output(Vec(p.nBackPort, Bool()))) else None
  })

  for (bp <- 0 until p.nBackPort) {
    io.o_alu(bp) := io.i_req(bp) & (io.i_unit(bp) === EXUNIT.ALU.U)
    io.o_bru(bp) := io.i_req(bp) & (io.i_unit(bp) === EXUNIT.BRU.U)
    if (p.nMulDiv > 0) io.o_muldiv.get(bp) := io.i_req(bp) & (io.i_unit(bp) === EXUNIT.MULDIV.U)
  }
}

class Dispatcher(p: DispatcherParams) extends Module {
  val io = IO(new Bundle {
    val i_req = Input(Vec(p.nBackPort, Bool()))
    val i_hart = Input(Vec(p.nBackPort, UInt(log2Ceil(p.nHart).W)))
    val i_dome = Input(Vec(p.nBackPort, UInt(log2Ceil(p.nDome).W)))
    val i_unit = Input(Vec(p.nBackPort, UInt(EXUNIT.NBIT.W)))

    val b_rsrc_alu = Vec(p.nAlu, new RsrcBus(1, p.nDome, p.nAlu))
    val i_free_alu = Input(Vec(p.nAlu, Bool()))
    val b_rsrc_muldiv = if (p.nMulDiv > 0) Some(Vec(p.nMulDiv, new RsrcBus(1, p.nDome, p.nMulDiv))) else None
    val i_free_muldiv = if (p.nMulDiv > 0) Some(Input(Vec(p.nMulDiv, Bool()))) else None

    val o_miss = Output(Vec(p.nBackPort, Bool()))
    val o_port = Output(Vec(p.nBackPort, UInt(log2Ceil(p.nBackPort).W)))
  })

  val req = Module(new ReqUnit(p))
  req.io.i_req := io.i_req
  req.io.i_unit := io.i_unit

  for (bp <- 0 until p.nBackPort) {
    io.o_miss(bp) := false.B
    io.o_port(bp) := 0.U
  }

  // ******************************
  //              ALU
  // ******************************
  val slct_alu = Module(new SlctNPort(p.nDome, p.nBackPort, p.nAlu))
  slct_alu.io.i_req := req.io.o_alu
  slct_alu.io.i_dome := io.i_dome
  slct_alu.io.b_rsrc <> io.b_rsrc_alu
  slct_alu.io.i_free := io.i_free_alu

  if (p.nAlu != p.nBackPort) {
    for (bp <- 0 until p.nBackPort) {
      when(io.i_unit(bp) === EXUNIT.ALU.U) {
        io.o_miss(bp) := io.i_req(bp) & ~slct_alu.io.o_av(bp)
        io.o_port(bp) := slct_alu.io.o_port(bp)
      }
    }
  }


  // ******************************
  //              BRU
  // ******************************
  val w_bru_av = Wire(Vec(p.nHart, Vec(p.nBackPort, Bool())))
  for (h <- 0 until p.nHart) {
    w_bru_av(h)(0) := true.B

    for (bp <- 1 until p.nBackPort) {
      when (h.U === io.i_hart(bp - 1)) {
        w_bru_av(h)(bp) := w_bru_av(h)(bp - 1) & ~req.io.o_bru(bp - 1)
      }.otherwise {
        w_bru_av(h)(bp) := w_bru_av(h)(bp - 1)
      }
    }
  }

  for (bp <- 0 until p.nBackPort) {
    when (io.i_unit(bp) === EXUNIT.BRU.U) {
      io.o_miss(bp) := io.i_req(bp) & ~w_bru_av(io.i_hart(bp))(bp)
    }
  }

  // ******************************
  //            MULDIV
  // ******************************
  val slct_muldiv = if (p.nMulDiv > 0) Some(Module(new SlctNPort(p.nDome, p.nBackPort, p.nMulDiv))) else None

  if (p.nMulDiv > 0) {
    slct_muldiv.get.io.i_req := req.io.o_muldiv.get
    slct_muldiv.get.io.i_dome := io.i_dome
    slct_muldiv.get.io.b_rsrc <> io.b_rsrc_muldiv.get
    slct_muldiv.get.io.i_free := io.i_free_muldiv.get

    if (p.nMulDiv != p.nBackPort) {
      for (bp <- 0 until p.nBackPort) {
        when (io.i_unit(bp) === EXUNIT.MULDIV.U) {
          io.o_miss(bp) := io.i_req(bp) & ~slct_muldiv.get.io.o_av(bp)
          io.o_port(bp) := slct_muldiv.get.io.o_port(bp)
        }
      }
    }
  }
}

object SlctPort extends App {
  chisel3.Driver.execute(args, () => new SlctPort(2, 4))
}

object SlctNPort extends App {
  chisel3.Driver.execute(args, () => new SlctNPort(2, 4, 4))
}

object ReqUnit extends App {
  chisel3.Driver.execute(args, () => new ReqUnit(DispatcherDefault))
}

object Dispatcher extends App {
  chisel3.Driver.execute(args, () => new Dispatcher(DispatcherDefault))
}
