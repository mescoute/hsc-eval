package cowlibry.core.ferrandaise.back

import chisel3._
import chisel3.util._

import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._
import cowlibry.core.aubrac.back.{Decoder, SlctImm, SlctSource, CsrIdBus}
import cowlibry.core.aubrac.back.{EXUNIT, OP}


class IdUnit(p: BackParams) extends Module {
  val io = IO(new Bundle {
    val b_hart = Vec(p.nHart, new RsrcBus(p.nHart, p.nDome, 1))
    val b_backport = new RsrcBus(p.nHart, p.nDome, 1)

    val i_flush = Input(Vec(p.nHart, Bool()))
    val i_lock = Input(Bool())
    val i_wait = Input(Bool())
    val o_lock = Output(Bool())

    val b_csr = Flipped(new CsrIdBus(p.nHart, p.useDome, p.useMulDiv))

    val i_valid = Input(Bool())
    val i_bus = Input(new BackPortBus(p.nHart, 32, 32))

    val o_valid = Output(Bool())
    val o_bus = Output(new IssStageBus(p))
  })

  val w_lock = Wire(Bool())
  val w_flush = Wire(Bool())

  w_flush := io.i_flush(io.i_bus.hart) | io.b_hart(io.i_bus.hart).flush

  // ******************************
  //            DECODER
  // ******************************
  val decoder = Module(new Decoder(p))
  decoder.io.i_instr := io.i_bus.instr

  // ******************************
  //             IMM
  // ******************************
  val slct_imm = Module(new SlctImm())
  slct_imm.io.i_instr := io.i_bus.instr
  slct_imm.io.i_imm_type := decoder.io.o_data.immtype

  // ******************************
  //             SOURCE
  // ******************************
  // ------------------------------
  //               S1
  // ------------------------------
  val slct_s1 = Module(new SlctSource(false))
  slct_s1.io.i_src_type := decoder.io.o_data.s1type
  slct_s1.io.i_imm := slct_imm.io.o_val
  slct_s1.io.i_pc := io.i_bus.pc
  slct_s1.io.i_csr := decoder.io.o_data.csr

  // ------------------------------
  //               S2
  // ------------------------------
  val slct_s2 = Module(new SlctSource(false))
  slct_s2.io.i_src_type := decoder.io.o_data.s2type
  slct_s2.io.i_imm := slct_imm.io.o_val
  slct_s2.io.i_pc := io.i_bus.pc
  slct_s2.io.i_csr := decoder.io.o_data.csr

  // ------------------------------
  //               S3
  // ------------------------------
  val slct_s3 = Module(new SlctSource(false))
  slct_s3.io.i_src_type := decoder.io.o_data.s3type
  slct_s3.io.i_imm := slct_imm.io.o_val
  slct_s3.io.i_pc := io.i_bus.pc
  slct_s3.io.i_csr := decoder.io.o_data.csr

  // ******************************
  //          CAPABILITIES
  // ******************************
  val w_cap_valid = Wire(Bool())
  w_cap_valid := true.B

  if (p.useDome) {
    if (p.nMulDiv > 0) {
      when ((decoder.io.o_ex.unit === EXUNIT.MULDIV.U) & ~io.b_csr.cap_muldiv.get(io.i_bus.hart)) {
        w_cap_valid := false.B
      }
    }
  }

  // ******************************
  //           REGISTERS
  // ******************************
  val w_reg_flush = Wire(Bool())
  val reg_valid = RegInit(0.B)
  val reg_bus =  Reg(new IssStageBus(p))

  w_lock := io.i_lock & reg_valid

  w_reg_flush := io.i_flush(reg_bus.info.hart) | io.b_hart(reg_bus.info.hart).flush

  when (~w_lock | w_reg_flush) {
    reg_valid := io.i_valid & decoder.io.o_valid & ~w_flush & ~io.i_wait & w_cap_valid

    reg_bus.info := decoder.io.o_info
    reg_bus.info.hart := io.i_bus.hart
    reg_bus.info.pc := io.i_bus.pc

    reg_bus.ex := decoder.io.o_ex
    reg_bus.mem := decoder.io.o_mem
    reg_bus.csr := decoder.io.o_csr
    reg_bus.wb := decoder.io.o_wb

    reg_bus.data.s1 := slct_s1.io.o_val
    reg_bus.data.s2 := slct_s2.io.o_val
    reg_bus.data.s3 := slct_s3.io.o_val
    reg_bus.data.rs1 := decoder.io.o_data.rs1
    reg_bus.data.rs2 := decoder.io.o_data.rs2
    reg_bus.data.s1_is_reg := (decoder.io.o_data.s1type === OP.REG.U)
    reg_bus.data.s2_is_reg := (decoder.io.o_data.s2type === OP.REG.U)
    reg_bus.data.s3_is_reg := (decoder.io.o_data.s3type === OP.REG.U)
  }

  // ******************************
  //            OUTPUTS
  // ******************************
  io.o_lock := w_lock & ~w_reg_flush

  io.o_valid := reg_valid
  io.o_bus := reg_bus

  // ******************************
  //              FREE
  // ******************************
  for (h <- 0 until p.nHart) {
    when (h.U === reg_bus.info.hart) {
      io.b_hart(h).free := ~reg_valid
    }.otherwise {
      io.b_hart(h).free := true.B
    }
  }

  io.b_backport.free := ~reg_valid
}

class IdStage(p: BackParams) extends Module {
  val io = IO(new Bundle {
    val b_hart = Vec(p.nHart, new RsrcBus(p.nHart, p.nDome, 1))
    val b_backport = Vec(p.nBackPort, new RsrcBus(p.nHart, p.nDome, 1))

    val i_lock = Input(Vec(p.nBackPort, Bool()))
    val i_flush = Input(Vec(p.nHart, Bool()))
    val o_lock = Output(Vec(p.nBackPort, Bool()))

    val b_csr = Flipped(new CsrIdBus(p.nHart, p.useDome, p.useMulDiv))

    val i_valid = Input(Vec(p.nBackPort, Bool()))
    val i_bus = Input(Vec(p.nBackPort, new BackPortBus(p.nHart, 32, 32)))

    val o_valid = Output(Vec(p.nBackPort, Bool()))
    val o_bus =  Output(Vec(p.nBackPort, new IssStageBus(p)))
  })

  val unit = Seq.fill(p.nBackPort) {Module(new IdUnit(p))}

  // ******************************
  //              WAIT
  // ******************************
  val w_h_wait = Wire(Vec(p.nHart, Bool()))
  for (h <- 0 until p.nHart) {
    w_h_wait(h) := false.B
    for (bp <- 0 until p.nBackPort) {
      when ((h.U === io.i_bus(bp).hart) & unit(bp).io.o_lock) {
        w_h_wait(h) := true.B
      }
    }
  }

  // ******************************
  //              UNIT
  // ******************************
  for (bp <- 0 until p.nBackPort) {
    unit(bp).io.b_hart <> io.b_hart
    unit(bp).io.b_backport <> io.b_backport(bp)

    unit(bp).io.i_lock := io.i_lock(bp)
    unit(bp).io.i_flush := io.i_flush
    unit(bp).io.i_wait := w_h_wait(io.i_bus(bp).hart)

    unit(bp).io.b_csr <> io.b_csr

    unit(bp).io.i_valid := io.i_valid(bp)
    unit(bp).io.i_bus := io.i_bus(bp)

    io.o_valid(bp) := unit(bp).io.o_valid
    io.o_bus(bp) := unit(bp).io.o_bus
  }

  // ******************************
  //              LOCK
  // ******************************
  for (bp <- 0 until p.nBackPort) {
    io.o_lock(bp) := w_h_wait(io.i_bus(bp).hart)
  }

  // ******************************
  //              FREE
  // ******************************
  for (h <- 0 until p.nHart) {
    val w_free = Wire(Vec(p.nBackPort, Bool()))

    for (bp <- 0 until p.nBackPort) {
      w_free(bp) := unit(bp).io.b_hart(h).free
    }

    io.b_hart(h).free := w_free.asUInt.andR
  }
}

object IdUnit extends App {
  chisel3.Driver.execute(args, () => new IdUnit(BackDefault))
}

object IdStage extends App {
  chisel3.Driver.execute(args, () => new IdStage(BackDefault))
}
