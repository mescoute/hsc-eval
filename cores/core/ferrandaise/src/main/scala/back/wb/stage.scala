package cowlibry.core.ferrandaise.back

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._
import cowlibry.core.aubrac.back.{MEM, CSRUOP}
import cowlibry.core.aubrac.back.{WbStage => WbUnit}
import cowlibry.core.aubrac.back.{WbStageBus, WriteGprIO, CsrWbBus, CsrWriteBus, BypassBus}
import cowlibry.core.aubrac.rmu._


class WbStage(p: BackParams, p_dmem: PmbParams) extends Module {
  val io = IO(new Bundle {
    val o_lock = Output(Vec(p.nBackPort, Bool()))

    val o_end = Output(Vec(p.nHart, Bool()))
    val o_pend = Output(Vec(p.nHart, Bool()))

    val i_valid = Input(Vec(p.nBackPort, Bool()))
    val i_bus = Input(Vec(p.nBackPort, new WbStageBus(p)))

    val b_dmem = Vec(p.nBackPort, new PmbAckIO(p_dmem))
    val b_rmu = if (p.useDome) Some(Vec(p.nHart, Flipped(new RmuAckIO()))) else None
    val o_rmu_wait = if (p.useDome) Some(Output(Vec(p.nHart, Bool()))) else None

    val o_bypass = Output(Vec(p.nBackPort, new BypassBus(p.nHart)))
    val b_csr = Vec(p.nHart, Flipped(new CsrWriteBus(p)))
    val b_rd = Vec(p.nGprWrite, Flipped(new WriteGprIO(p)))

    val b_csr_info = Flipped(new CsrWbBus(p.nHart, p.nBackPort))
  })

  val unit = Seq.fill(p.nBackPort) {Module(new WbUnit(p, p_dmem))}

  // ******************************
  //          MANAGE HART
  // ******************************
  val w_h_wait = Wire(Vec(p.nHart, Bool()))

  for (h <- 0 until p.nHart) {
    w_h_wait(h) := false.B
    for (bp <- 0 until p.nBackPort) {
      when((h.U === io.i_bus(bp).info.hart) & unit(bp).io.o_lock) {
        w_h_wait(h) := true.B
      }
    }
  }

  for (bp <- 0 until p.nBackPort) {
    io.o_lock(bp) := w_h_wait(io.i_bus(bp).info.hart)
  }

  // ******************************
  //              END
  // ******************************
  for (h <- 0 until p.nHart) {
    io.o_end(h) := false.B
  }

  for (bp <- 0 until p.nBackPort) {
    when (io.i_valid(bp) & io.i_bus(bp).info.end) {
      io.o_end(io.i_bus(bp).info.hart) := true.B
    }
  }

  // ******************************
  //              PEND
  // ******************************
  for (h <- 0 until p.nHart) {
    io.o_pend(h) := false.B
  }

  for (bp <- 0 until p.nBackPort) {
    when (io.i_valid(bp)) {
      io.o_pend(io.i_bus(bp).info.hart) := true.B
    }
  }

  // ******************************
  //        RMU DEPENDENCIES
  // ******************************
  if (p.useDome) {
    for (h <- 0 until p.nHart) {
      io.o_rmu_wait.get(h) := false.B
      for (bp <- 0 until p.nBackPort) {
        when (unit(bp).io.o_rmu_wait.get & (h.U === io.i_bus(bp).info.hart)) {
          io.o_rmu_wait.get(h) := true.B
        }
      }
    }
  }

  // ******************************
  //              UNIT
  // ******************************
  for (bp <- 0 until p.nBackPort) {
    unit(bp).io.i_wait.get := w_h_wait(io.i_bus(bp).info.hart)

    unit(bp).io.i_valid := io.i_valid(bp)
    unit(bp).io.i_bus := io.i_bus(bp)

    unit(bp).io.b_dmem <> io.b_dmem(bp)
    if (p.useDome) unit(bp).io.b_rmu.get <> io.b_rmu.get(io.i_bus(bp).info.hart)

    io.o_bypass(bp) := unit(bp).io.o_bypass
    unit(bp).io.b_csr <> io.b_csr(io.i_bus(bp).info.hart)
    io.b_rd(bp) := unit(bp).io.b_rd
  }

  // ******************************
  //              RMU
  // ******************************
  if (p.useDome) {
    for (h <- 0 until p.nHart) {
      val w_rmu_ready = Wire(Vec(p.nBackPort, Bool()))

      for (bp <- 0 until p.nBackPort) {
        when (h.U === io.i_bus(bp).info.hart) {
          w_rmu_ready(bp) := unit(bp).io.b_rmu.get.ready
        }.otherwise {
          w_rmu_ready(bp) := false.B
        }
      }

      io.b_rmu.get(h).ready := w_rmu_ready.asUInt.orR
    }
  }

  // ******************************
  //              CSR
  // ******************************
  // ------------------------------
  //             WRITE
  // ------------------------------
  for (h <- 0 until p.nHart) {
    io.b_csr(h).valid := false.B
    io.b_csr(h).addr := 0.U
    io.b_csr(h).uop := CSRUOP.X.U
    io.b_csr(h).data := 0.U
    io.b_csr(h).mask := 0.U

    for (bp <- 0 until p.nBackPort) {
      when (unit(bp).io.b_csr.valid & (h.U === io.i_bus(bp).info.hart)) {
        io.b_csr(h) <> unit(bp).io.b_csr
      }
    }
  }

  // ------------------------------
  //           INFORMATION
  // ------------------------------
  for (h <- 0 until p.nHart) {
    for (bp <- 0 until p.nBackPort) {
      io.b_csr_info.instret(h)(bp) := unit(bp).io.b_csr_info.instret(h)(0)
    }
  }
}

object WbStage extends App {
  chisel3.Driver.execute(args, () => new WbStage(BackDefault, PmbCfg0))
}
