package cowlibry.core.ferrandaise.back

import chisel3._
import chisel3.util._

import cowlibry.core.aubrac.back.{InfoBus, DataBus, ExCtrlBus, MemCtrlBus, CsrCtrlBus, WbCtrlBus}


// ******************************
//           DATA BUSES
// ******************************
class PrevDataBus extends DataBus {
  val rs1 = UInt(5.W)
  val rs2 = UInt(5.W)
  val s1_is_reg = Bool()
  val s2_is_reg = Bool()
  val s3_is_reg = Bool()
}

// ******************************
//          STAGE BUS
// ******************************
class IssStageBus(p: BackParams) extends Bundle {
  val info = new InfoBus(p.nHart)

  val ex = new ExCtrlBus(p.nBackPort)
  val mem = new MemCtrlBus()
  val csr = new CsrCtrlBus()
  val wb = new WbCtrlBus()

  val data = new PrevDataBus()

  override def cloneType = (new IssStageBus(p)).asInstanceOf[this.type]
}
