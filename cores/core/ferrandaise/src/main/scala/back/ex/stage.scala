package cowlibry.core.ferrandaise.back

import chisel3._
import chisel3.util._

import cowlibry.common.isa._
import cowlibry.common.rsrc._
import cowlibry.common.tools._
import cowlibry.common.mem.pmb.{SIZE}
import cowlibry.common.mem.flush._
import cowlibry.core.aubrac.tools._
import cowlibry.core.aubrac.back.{Alu, Bru, MulDiv, Mamu}
import cowlibry.core.aubrac.back.{BypassBus, ExStageBus, MemStageBus, CsrExBus}
import cowlibry.core.aubrac.back.{EXUNIT, MEM}
import cowlibry.core.aubrac.rmu.{RmuReqIO}


class ExStage(p: BackParams) extends Module {
  val io = IO(new Bundle {
    val b_hart = Vec(p.nHart, new RsrcBus(p.nHart, p.nDome, 1))
    val b_backport = Vec(p.nBackPort, new RsrcBus(p.nHart, p.nDome, 1))

    val i_lock = Input(Vec(p.nBackPort, Bool()))
    val o_lock = Output(Vec(p.nBackPort, Bool()))

    val o_end = Output(Vec(p.nHart, Bool()))
    val o_pend = Output(Vec(p.nHart, Bool()))

    val i_valid = Input(Vec(p.nBackPort, Bool()))
    val i_bus =  Input(Vec(p.nBackPort, new ExStageBus(p)))
    val i_br_next = Input(Vec(p.nHart, new BranchBus(32)))

    val b_rsrc_alu = Vec(p.nAlu, new RsrcBus(1, p.nDome, p.nAlu))
    val o_free_alu = Output(Vec(p.nAlu, Bool()))
    val b_rsrc_muldiv = if (p.nMulDiv > 0) Some(Vec(p.nMulDiv, new RsrcBus(1, p.nDome, p.nMulDiv))) else None
    val o_free_muldiv = if (p.nMulDiv > 0) Some(Output(Vec(p.nMulDiv, Bool()))) else None

    val o_bypass = Output(Vec(p.nBackPort, new BypassBus(p.nHart)))
    val o_br_new = Output(Vec(p.nHart, new BranchBus(32)))
    val o_br_info = Output(Vec(p.nHart, new BranchInfoBus(32)))
    val o_rmu_wait = if (p.useDome) Some(Output(Vec(p.nHart, Bool()))) else None
    val b_csr_info = Flipped(new CsrExBus(p.nHart, p.nBackPort))

    val b_rmu = if (p.useDome) Some(Vec(p.nHart, Flipped(new RmuReqIO()))) else None
    val b_lsu = Vec(p.nBackPort, Flipped(new LsuReqIO(p.nHart)))
    val b_flush = if (p.useMamu) Some(Vec(p.nHart, new FlushIO(p.nHart, p.nDome, 32))) else None

    val o_valid = Output(Vec(p.nBackPort, Bool()))
    val o_bus =  Output(Vec(p.nBackPort, new MemStageBus(p)))
  })

  val w_br_new_flush = Wire(Vec(p.nHart, Bool()))

  val w_h_flush = Wire(Vec(p.nHart, Vec(p.nBackPort, Bool())))
  val w_h_wait = Wire(Vec(p.nHart, Vec(p.nBackPort, Bool())))
  val w_h_lock = Wire(Vec(p.nHart, Vec(p.nBackPort, Bool())))

  val w_valid = Wire(Vec(p.nBackPort, Bool()))
  val w_lock = Wire(Vec(p.nBackPort, Bool()))
  val w_flush = Wire(Vec(p.nBackPort, Bool()))
  val w_wait = Wire(Vec(p.nBackPort, Bool()))

  // ******************************
  //           UOP STATUS
  // ******************************
  val reg_done = RegInit(VecInit(Seq.fill(p.nBackPort)(false.B)))
  val w_i_valid = Wire(Vec(p.nBackPort, Bool()))
  for (bp <- 0 until p.nBackPort) {
    w_i_valid(bp) := io.i_valid(bp) & ~reg_done(bp) & ~w_br_new_flush(io.i_bus(bp).info.hart)
  }

  // ****************************************
  //    SELECT CORRECT CURRENT NEXT BRANCH
  // ****************************************
  val w_h_br_next = Wire(Vec(p.nHart, Vec(p.nBackPort, new BranchBus(32))))
  val w_br_next = Wire(Vec(p.nBackPort, new BranchBus(32)))
  for (h <- 0 until p.nHart) {
    w_h_br_next(h)(p.nBackPort - 1) := io.i_br_next(h)
    for (bp <- 1 until p.nBackPort) {
      when (io.i_valid(p.nBackPort - bp) & (h.U === io.i_bus(p.nBackPort - bp).info.hart)) {
        w_h_br_next(h)(p.nBackPort - bp - 1).valid := true.B
        w_h_br_next(h)(p.nBackPort - bp - 1).pc := io.i_bus(p.nBackPort - bp).info.pc
      }.otherwise {
        w_h_br_next(h)(p.nBackPort - bp - 1) := w_h_br_next(h)(p.nBackPort - bp)
      }
    }
  }

  for (bp <- 0 until p.nBackPort) {
    w_br_next(bp) := w_h_br_next(io.i_bus(bp).info.hart)(bp)
  }

  // ******************************
  //        EXECUTION UNITS
  // ******************************
  val w_end = Wire(Vec(p.nBackPort, Bool()))
  val w_res = Wire(Vec(p.nBackPort, UInt(32.W)))
  val w_br_new = Wire(Vec(p.nBackPort, new BranchBus(32)))

  for (bp <- 0 until p.nBackPort) {
    w_end(bp) := false.B
    w_res(bp) := 0.U
    w_br_new(bp).valid := false.B
    w_br_new(bp).pc := 0.U
  }

  // ------------------------------
  //             ALU
  // ------------------------------
  val alu = Seq.fill(p.nAlu) {Module(new Alu())}

  for (a <- 0 until p.nAlu) {
    io.o_free_alu(a) := true.B

    alu(a).io.i_valid := false.B
    alu(a).io.i_uop := io.i_bus(a).ex.uop
    alu(a).io.i_s1 := io.i_bus(a).data.s1
    alu(a).io.i_s2 := io.i_bus(a).data.s2

    if (p.nAlu == p.nBackPort) {
      when (w_i_valid(a) & (io.i_bus(a).ex.unit === EXUNIT.ALU.U)) {
        alu(a).io.i_valid := true.B

        w_end(a) := alu(a).io.o_end
        w_res(a) := alu(a).io.o_res
        w_br_new(a).valid := false.B
        w_br_new(a).pc := 0.U
      }
    } else {
      for (bp <- 0 until p.nBackPort) {
        when (w_i_valid(bp) & (io.i_bus(bp).ex.unit === EXUNIT.ALU.U) & (io.i_bus(bp).ex.port === a.U)) {
          alu(a).io.i_valid := true.B
          alu(a).io.i_uop := io.i_bus(bp).ex.uop
          alu(a).io.i_s1 := io.i_bus(bp).data.s1
          alu(a).io.i_s2 := io.i_bus(bp).data.s2

          io.o_free_alu(a) := ~w_wait(bp) & alu(a).io.o_end

          w_end(bp) := alu(a).io.o_end
          w_res(bp) := alu(a).io.o_res
          w_br_new(bp).valid := false.B
          w_br_new(bp).pc := 0.U
        }
      }
    }
  }

  // ------------------------------
  //             BRU
  // ------------------------------

  val bru = Seq.fill(p.nHart) {Module(new Bru())}
  for (b <- 0 until p.nHart) {
    bru(b).io.i_lock := false.B
    bru(b).io.i_valid := false.B
    bru(b).io.i_uop := io.i_bus(0).ex.uop
    bru(b).io.i_pc := io.i_bus(0).info.pc
    bru(b).io.i_br_next := w_br_next(0)
    bru(b).io.i_s1 := io.i_bus(0).data.s1
    bru(b).io.i_s2 := io.i_bus(0).data.s2
    bru(b).io.i_s3 := io.i_bus(0).data.s3

    for (bp <- 0 until p.nBackPort) {
      when (w_i_valid(bp) & (io.i_bus(bp).ex.unit === EXUNIT.BRU.U) & (io.i_bus(bp).info.hart === b.U)) {
        bru(b).io.i_lock := w_wait(bp)
        bru(b).io.i_valid := true.B
        bru(b).io.i_uop := io.i_bus(bp).ex.uop
        bru(b).io.i_pc := io.i_bus(bp).info.pc
        bru(b).io.i_br_next := w_br_next(bp)
        bru(b).io.i_s1 := io.i_bus(bp).data.s1
        bru(b).io.i_s2 := io.i_bus(bp).data.s2
        bru(b).io.i_s3 := io.i_bus(bp).data.s3

        w_end(bp) := bru(b).io.o_end
        w_res(bp) := bru(b).io.o_res
        w_br_new(bp).valid := bru(b).io.o_br_new.valid
        w_br_new(bp).pc := bru(b).io.o_br_new.pc
      }
    }
  }

  // ------------------------------
  //             MULDIV
  // ------------------------------
  val muldiv = if (p.nMulDiv > 0) Some(Seq.fill(p.nMulDiv) {Module(new MulDiv())}) else None
  if (p.nMulDiv > 0) {
    for (m <- 0 until p.nMulDiv) {
      io.o_free_muldiv.get(m) := true.B

      muldiv.get(m).io.i_flush := w_flush(m)
      muldiv.get(m).io.i_lock := w_wait(m)
      muldiv.get(m).io.i_valid := false.B
      muldiv.get(m).io.i_uop := io.i_bus(m).ex.uop
      muldiv.get(m).io.i_s1 := io.i_bus(m).data.s1
      muldiv.get(m).io.i_s2 := io.i_bus(m).data.s2

      if (p.nMulDiv == p.nBackPort) {
        when (w_i_valid(m) & (io.i_bus(m).ex.unit === EXUNIT.MULDIV.U)) {
          muldiv.get(m).io.i_flush := w_flush(m)
          muldiv.get(m).io.i_lock := w_wait(m)
          muldiv.get(m).io.i_valid := true.B
          muldiv.get(m).io.i_uop := io.i_bus(m).ex.uop
          muldiv.get(m).io.i_s1 := io.i_bus(m).data.s1
          muldiv.get(m).io.i_s2 := io.i_bus(m).data.s2

          w_end(m) := muldiv.get(m).io.o_end
          w_res(m) := muldiv.get(m).io.o_res
          w_br_new(m).valid := false.B
          w_br_new(m).pc := 0.U
        }
      } else {
        for (bp <- 0 until p.nBackPort) {
          when (w_i_valid(bp) & (io.i_bus(bp).ex.unit === EXUNIT.MULDIV.U) & (io.i_bus(bp).ex.port === m.U)) {
            muldiv.get(m).io.i_flush := w_flush(bp)
            muldiv.get(m).io.i_lock := w_wait(bp)
            muldiv.get(m).io.i_valid := true.B
            muldiv.get(m).io.i_uop := io.i_bus(bp).ex.uop
            muldiv.get(m).io.i_s1 := io.i_bus(bp).data.s1
            muldiv.get(m).io.i_s2 := io.i_bus(bp).data.s2

            io.o_free_muldiv.get(m) := ~w_wait(bp) & muldiv.get(m).io.o_end

            w_end(bp) := muldiv.get(m).io.o_end
            w_res(bp) := muldiv.get(m).io.o_res
            w_br_new(bp).valid := false.B
            w_br_new(bp).pc := 0.U
          }
        }
      }
    }
  }

  // ------------------------------
  //             RMU
  // ------------------------------
  if (p.useDome) {
    for (h <- 0 until p.nHart) {
      io.b_rmu.get(h).valid := false.B
      io.b_rmu.get(h).uop := io.i_bus(0).ex.uop

      for (bp <- 0 until p.nBackPort) {
        when (w_i_valid(bp) & (io.i_bus(bp).ex.unit === EXUNIT.RMU.U) & (io.i_bus(bp).info.hart === h.U)) {
          io.b_rmu.get(h).valid := ~w_flush(bp) & ~w_wait(bp)
          io.b_rmu.get(h).uop := io.i_bus(bp).ex.uop

          w_end(bp) := io.b_rmu.get(h).ready
          w_res(bp) := 0.U
          w_br_new(bp).valid := false.B
          w_br_new(bp).pc := 0.U
        }
      }
    }
  }

  // ------------------------------
  //             MAMU
  // ------------------------------
  val mamu = if (p.useMamu) Some(Seq.fill(p.nHart) {Module(new Mamu(p.nHart, p.nDome))}) else None

  if (p.useMamu) {
    for (h <- 0 until p.nHart) {
      mamu.get(h).io.b_flush <> io.b_flush.get(h)

      mamu.get(h).io.i_lock := w_wait(0)
      mamu.get(h).io.i_valid := false.B
      mamu.get(h).io.i_dome := io.b_hart(io.i_bus(0).info.hart).dome
      mamu.get(h).io.i_hart := io.i_bus(0).info.hart
      mamu.get(h).io.i_uop := io.i_bus(0).ex.uop
      mamu.get(h).io.i_pc := io.i_bus(0).info.pc

      for (bp <- 0 until p.nBackPort) {
        when (w_i_valid(bp) & (io.i_bus(bp).ex.unit === EXUNIT.MAMU.U) & (io.i_bus(bp).info.hart === h.U)) {
          mamu.get(h).io.i_lock := w_wait(bp)
          mamu.get(h).io.i_valid := true.B
          mamu.get(h).io.i_dome := io.b_hart(io.i_bus(bp).info.hart).dome
          mamu.get(h).io.i_hart := io.i_bus(bp).info.hart
          mamu.get(h).io.i_uop := io.i_bus(bp).ex.uop
          mamu.get(h).io.i_pc := io.i_bus(bp).info.pc

          w_end(bp) := mamu.get(h).io.o_end
          w_res(bp) := 0.U
          w_br_new(bp) := mamu.get(h).io.o_br_new
        }
      }
    }
  }

  // ******************************
  //        LOAD STORE UNIT
  // ******************************
  val w_lsu_wait = Wire(Vec(p.nBackPort, Bool()))
  for (bp <- 0 until p.nBackPort) {
    w_lsu_wait(bp) := Mux(w_i_valid(bp) & (io.i_bus(bp).mem.load | io.i_bus(bp).mem.store), ~io.b_lsu(bp).ready, false.B)

    io.b_lsu(bp).valid := w_i_valid(bp) & ~w_flush(bp) & ~w_wait(bp) & (io.i_bus(bp).mem.load | io.i_bus(bp).mem.store)
    io.b_lsu(bp).hart := io.i_bus(bp).info.hart
    io.b_lsu(bp).rw := io.i_bus(bp).mem.store
    io.b_lsu(bp).size := SIZE.B0.U
    io.b_lsu(bp).addr := w_res(bp)
    io.b_lsu(bp).wdata := io.i_bus(bp).data.s3

    switch (io.i_bus(bp).mem.size) {
      is (MEM.B.U, MEM.BU.U) {
        io.b_lsu(bp).size := SIZE.B1.U
      }
      is (MEM.H.U, MEM.HU.U) {
        io.b_lsu(bp).size := SIZE.B2.U
      }
      is (MEM.W.U) {
        io.b_lsu(bp).size := SIZE.B4.U
      }
    }
  }

  // ******************************
  //             BRANCH
  // ******************************
  // ------------------------------
  //              NEW
  // ------------------------------

  val w_h_br_new = Wire(Vec(p.nHart, Vec(p.nBackPort, new BranchBus(32))))
  for (h <- 0 until p.nHart) {
    when (h.U === io.i_bus(p.nBackPort - 1).info.hart) {
      w_h_br_new(h)(p.nBackPort - 1) := w_br_new(p.nBackPort - 1)
    }.otherwise {
      w_h_br_new(h)(p.nBackPort - 1).valid := false.B
      w_h_br_new(h)(p.nBackPort - 1).pc := 0.U
    }
    for (bp <- 1 until p.nBackPort) {
      when ((h.U === io.i_bus(p.nBackPort - 1 - bp).info.hart) & w_br_new(p.nBackPort - 1 - bp).valid) {
        w_h_br_new(h)(p.nBackPort - 1 - bp) := w_br_new(p.nBackPort - 1 - bp)
      }.otherwise {
        w_h_br_new(h)(p.nBackPort - 1 - bp) := w_h_br_new(h)(p.nBackPort - bp)
      }
    }
  }

  val init_br_new = Wire(Vec(p.nHart, new BranchBus(32)))
  for (h <- 0 until p.nHart) {
    init_br_new(h).valid := false.B
    init_br_new(h).pc := DontCare
  }

  val reg_br_new = RegInit(init_br_new)

  for (h <- 0 until p.nHart) {
    if (p.useBranchReg) {
      reg_br_new(h) := w_h_br_new(h)(0)

      w_br_new_flush(h) := reg_br_new(h).valid
      io.o_br_new(h) := reg_br_new(h)

      if (p.useMamu) {
        io.o_br_new(h).valid := reg_br_new(h).valid | (mamu.get(h).io.b_flush.valid & mamu.get(h).io.b_flush.apply)
        io.o_br_new(h).pc := reg_br_new(h).pc
      } else {
        io.o_br_new(h) := reg_br_new(h)
      }
    } else {
      w_br_new_flush(h) := false.B
      io.o_br_new(h) := w_h_br_new(h)(0)

      if (p.useMamu) {
        io.o_br_new(h).valid := w_h_br_new(h)(0).valid | (mamu.get(h).io.b_flush.valid & mamu.get(h).io.b_flush.apply)
        io.o_br_new(h).pc := w_h_br_new(h)(0).pc
      } else {
        io.o_br_new(h) := w_h_br_new(h)(0)
      }
    }
  }

  // ------------------------------
  //             INFO
  // ------------------------------
  if (p.useBranchReg) {
    val init_info = Wire(Vec(p.nHart, new BranchInfoBus(32)))
    for (h <- 0 until p.nHart) {
      init_info(h).valid := false.B
      init_info(h).pc := DontCare
      init_info(h).br := DontCare
      init_info(h).taken := DontCare
      init_info(h).jmp := DontCare
      init_info(h).target := DontCare
    }

    val reg_info = RegInit(init_info)

    for (h <- 0 until p.nHart) {
      reg_info(h) := bru(h).io.o_br_info
      io.o_br_info(h) := reg_info(h)
    }

  } else {
    for (h <- 0 until p.nHart) {
      io.o_br_info(h) := bru(h).io.o_br_info
    }
  }

  // ******************************
  //          MANAGE HART
  // ******************************
  // ------------------------------
  //             FLUSH
  // ------------------------------
  for (h <- 0 until p.nHart) {
    w_h_flush(h)(0) := io.b_hart(h).flush | w_br_new_flush(h)
    for (bp <- 1 until p.nBackPort) {
      when (h.U === io.i_bus(bp - 1).info.hart) {
        w_h_flush(h)(bp) := w_h_flush(h)(bp - 1) | w_br_new(bp - 1).valid
      }.otherwise {
        w_h_flush(h)(bp) := w_h_flush(h)(bp - 1)
      }
    }
  }

  for (bp <- 0 until p.nBackPort) {
    w_flush(bp) := w_h_flush(io.i_bus(bp).info.hart)(bp)
  }

  // ------------------------------
  //             WAIT
  // ------------------------------
  for (h <- 0 until p.nHart) {
    for (bp <- 0 until p.nBackPort) {
      when (h.U === io.i_bus(bp).info.hart) {
        if (bp == 0) w_h_wait(h)(bp) := w_lock(bp) else w_h_wait(h)(bp) := w_h_wait(h)(bp - 1) | w_lock(bp)
      }.otherwise {
        if (bp == 0) w_h_wait(h)(bp) := false.B else w_h_wait(h)(bp) := w_h_wait(h)(bp - 1)
      }
    }

    for (bp <- 1 until p.nBackPort) {
      when (h.U === io.i_bus(bp - 1).info.hart) {
        when ((~w_end(bp - 1) & w_i_valid(bp - 1)) | w_lsu_wait(bp - 1)) {
          w_h_wait(h)(bp) := true.B
        }
      }
    }
  }

  for (bp <- 0 until p.nBackPort) {
    w_wait(bp) := w_h_wait(io.i_bus(bp).info.hart)(bp)
  }

  // ------------------------------
  //             LOCK
  // ------------------------------
  for (h <- 0 until p.nHart) {
    for (bp <- 0 until p.nBackPort) {
      when (h.U === io.i_bus(bp).info.hart) {
        w_h_lock(h)(bp) := ~w_h_flush(h)(bp) & (w_h_wait(h)(bp) | ((~w_end(bp) & w_i_valid(bp)) | w_lsu_wait(bp)))
      }.otherwise {
        w_h_lock(h)(bp) := ~w_h_flush(h)(bp) & w_h_wait(h)(bp)
      }
    }
  }

  for (bp <- 0 until p.nBackPort) {
    io.o_lock(bp) := w_h_lock(io.i_bus(bp).info.hart)(bp)
  }

  // ******************************
  //       BYPASS CONNECTIONS
  // ******************************
  for (bp <- 0 until p.nBackPort) {
    io.o_bypass(bp).hart := io.i_bus(bp).info.hart
    io.o_bypass(bp).en := w_i_valid(bp) & ~w_flush(bp) & io.i_bus(bp).wb.en
    io.o_bypass(bp).ready := ~(io.i_bus(bp).mem.load | io.i_bus(bp).csr.read | io.i_bus(bp).wb.rmu) & w_end(bp)
    io.o_bypass(bp).addr := io.i_bus(bp).wb.addr
    io.o_bypass(bp).data := w_res(bp)
  }

  // ******************************
  //        RMU DEPENDENCIES
  // ******************************
  if (p.useDome) {
    for (h <- 0 until p.nHart) {
      io.o_rmu_wait.get(h) := false.B
      for (bp <- 0 until p.nBackPort) {
        when (h.U === io.i_bus(bp).info.hart) {
          when (w_i_valid(bp) & io.i_bus(bp).csr.write & ((io.i_bus(bp).data.s3(11,0) === DOME.CSRNEXTID.U) | (io.i_bus(bp).data.s3(11,0) === DOME.CSRNEXTPC.U) | (io.i_bus(bp).data.s3(11,0) === DOME.CSRNEXTCAP.U))) {
            io.o_rmu_wait.get(h) := true.B
          }
        }
      }
    }
  }

  // ******************************
  //              END
  // ******************************
  for (h <- 0 until p.nHart) {
    io.o_end(h) := false.B
  }

  for (bp <- 0 until p.nBackPort) {
    when (w_i_valid(bp) & io.i_bus(bp).info.end) {
      io.o_end(io.i_bus(bp).info.hart) := true.B
    }
  }

  // ******************************
  //              PEND
  // ******************************
  for (h <- 0 until p.nHart) {
    io.o_pend(h) := false.B
  }

  for (bp <- 0 until p.nBackPort) {
    when (w_i_valid(bp)) {
      io.o_pend(io.i_bus(bp).info.hart) := true.B
    }
  }

  // ******************************
  //              CSR
  // ******************************
  for (h <- 0 until p.nHart) {
    for (bp <- 0 until p.nBackPort) {
      io.b_csr_info.branch(h)(bp) := w_i_valid(bp) & (h.U === io.i_bus(bp).info.hart) & ~w_flush(bp) & ~w_wait(bp) & (io.i_bus(bp).ex.unit === EXUNIT.BRU.U)
      io.b_csr_info.mispred(h)(bp) := (h.U === io.i_bus(bp).info.hart) & w_br_new(bp).valid
    }
  }

  // ******************************
  //             OUTPUT
  // ******************************
  val w_reg_flush = Wire(Vec(p.nBackPort, Bool()))
  val reg_valid = RegInit(VecInit(Seq.fill(p.nBackPort)(0.B)))
  val reg_bus = Reg(Vec(p.nBackPort, new MemStageBus(p)))

  // ------------------------------
  //              DONE
  // ------------------------------
  for (h <- 0 until p.nHart) {
    for (bp <- 0 until p.nBackPort) {
      when (~reg_done(bp)) {
        reg_done(bp) := io.i_valid(bp) & w_end(bp) & ~w_wait(bp) & ~w_lsu_wait(bp) & w_h_lock(io.i_bus(bp).info.hart)(p.nBackPort - 1)
      }.otherwise {
        reg_done(bp) := w_h_lock(io.i_bus(bp).info.hart)(p.nBackPort - 1)
      }
    }
  }

  // ------------------------------
  //             LOCK
  // ------------------------------
  val w_reg_lock = Wire(Vec(p.nHart, Vec(p.nBackPort, Bool())))

  for (h <- 0 until p.nHart) {
    for (bp <- 0 until p.nBackPort) {
      when (h.U === reg_bus(p.nBackPort - 1 - bp).info.hart) {
        if (bp == 0) {
          w_reg_lock(h)(p.nBackPort - 1) := reg_valid(p.nBackPort - 1) & io.i_lock(p.nBackPort - 1)
        } else  {
          w_reg_lock(h)(p.nBackPort - 1 - bp) := w_reg_lock(h)(p.nBackPort - bp) | (reg_valid(p.nBackPort - 1 - bp) & io.i_lock(p.nBackPort - 1 - bp))
        }
      }.otherwise {
        if (bp == 0) {
          w_reg_lock(h)(p.nBackPort - 1) := false.B
        } else {
          w_reg_lock(h)(p.nBackPort - 1 - bp) := w_reg_lock(h)(p.nBackPort - bp)
        }
      }
    }
  }

  for (bp <- 0 until p.nBackPort) {
    w_lock(bp) := w_reg_lock(reg_bus(bp).info.hart)(bp)
  }

  // ------------------------------
  //               BUS
  // ------------------------------
  for (bp <- 0 until p.nBackPort) {
    w_valid(bp) := w_i_valid(bp) & ~w_flush(bp) & ~w_wait(bp) & w_end(bp)
    w_reg_flush(bp) := io.b_hart(reg_bus(bp).info.hart).flush
  }

  for (bp <- 0 until p.nBackPort) {
    when (w_reg_flush(bp) | ~w_lock(bp)) {
      reg_valid(bp) := w_valid(bp)
      reg_bus(bp).info := io.i_bus(bp).info
      reg_bus(bp).mem := io.i_bus(bp).mem
      reg_bus(bp).csr := io.i_bus(bp).csr
      reg_bus(bp).wb := io.i_bus(bp).wb
      reg_bus(bp).data.s3 := io.i_bus(bp).data.s3
      reg_bus(bp).data.res := w_res(bp)
    }
  }

  io.o_valid := reg_valid
  io.o_bus :=  reg_bus

  // ******************************
  //              FREE
  // ******************************
  for (h <- 0 until p.nHart) {
    val w_free = Wire(Vec(p.nBackPort, Bool()))

    for (bp <- 0 until p.nBackPort) {
      when (h.U === reg_bus(bp).info.hart) {
        w_free(bp) := ~reg_valid(bp)
      }.otherwise {
        w_free(bp) := true.B
      }
    }

    io.b_hart(h).free := w_free.asUInt.andR
  }

  for (bp <- 0 until p.nBackPort) {
    io.b_backport(bp).free := ~reg_valid(bp)
  }

  for (a <- 0 until p.nAlu) {
    io.b_rsrc_alu(a).free := true.B
  }

  if (p.nMulDiv > 0) {
    for (m <- 0 until p.nMulDiv) {
      io.b_rsrc_muldiv.get(m).free := muldiv.get(m).io.o_free
    }
  }
}

object ExStage extends App {
  chisel3.Driver.execute(args, () => new ExStage(BackDefault))
}
