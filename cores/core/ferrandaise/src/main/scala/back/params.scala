package cowlibry.core.ferrandaise.back

import chisel3._
import chisel3.util._
import scala.math._

import cowlibry.core.aubrac.back.{BackParams => AubracBackParams, GprParams}

trait DispatcherParams {
  def nHart: Int
  def nDome: Int

  def nBackPort: Int
  def nAlu: Int
  def nMulDiv: Int
}

trait BackParams extends AubracBackParams
                  with DispatcherParams{
  override def debug: Boolean
  override def pcBoot: Int
  override def nHart: Int
  override def useDome: Boolean
  override def nDome: Int

  override def nBackPort: Int
  override def nAlu: Int
  override def nMulDiv: Int
  override def useMulDiv: Boolean = (nMulDiv > 0)
  override def useFencei: Boolean
  override def useMamu: Boolean = useFencei
  override def useMemStage: Boolean = true
  override def useBranchReg: Boolean

  override def nGprRead: Int = nBackPort * 2
  override def nGprWrite: Int = nBackPort
  override def nGprBypass: Int = nBackPort * 3
}
