package cowlibry.core.ferrandaise.back

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._
import cowlibry.common.mem.flush._
import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._
import cowlibry.core.aubrac.back.{Gpr, Bypass, Csr}
import cowlibry.core.aubrac.back.{CsrBus, CsrRmuBus}
import cowlibry.core.aubrac.rmu._


class Back(p: BackParams, p_dmem: PmbParams) extends Module {
  val io = IO(new Bundle {
    val b_hart = Vec(p.nHart, new RsrcBus(p.nHart, p.nDome, 1))
    val b_backport = Vec(p.nBackPort, new RsrcBus(p.nHart, p.nDome, 1))

    val b_unit_alu = Vec(p.nAlu, new RsrcBus(1, p.nDome, p.nAlu))
    val b_unit_muldiv = if (p.nMulDiv > 0) Some(Vec(p.nMulDiv, new RsrcBus(1, p.nDome, p.nMulDiv))) else None

    val o_lock = Output(Vec(p.nBackPort, Bool()))
    val o_en = Output(Vec(p.nHart, Bool()))

    val i_valid = Input(Vec(p.nBackPort, Bool()))
    val i_port = Input(Vec(p.nBackPort, new BackPortBus(p.nHart, 32, 32)))

    val b_dmem = Vec(p.nHart, new PmbIO(p_dmem))
    val i_br_next = Input(Vec(p.nHart, new BranchBus(32)))
    val o_br_new = Output(Vec(p.nHart, new BranchBus(32)))
    val o_br_info = Output(Vec(p.nHart, new BranchInfoBus(32)))
    val b_rmu = if (p.useDome) Some(Vec(p.nHart, Flipped(new RmuIO()))) else None
    val b_rmu_csr = if (p.useDome) Some(Vec(p.nHart, new CsrRmuBus())) else None
    val b_flush = if (p.useMamu) Some(Vec(p.nHart, new FlushIO(p.nHart, p.nDome, 32))) else None

    val o_dbg_gpr = if (p.debug) Some(Output(Vec(p.nHart, Vec(32, UInt(32.W))))) else None
    val o_dbg_csr = if (p.debug) Some(Output(Vec(p.nHart, new CsrBus()))) else None
  })

  val id = Module(new IdStage(p))
  val iss = Module(new IssStage(p))
  val ex = Module(new ExStage(p))
  val lsu = Module(new Lsu(p, p_dmem))
  val mem = Module(new MemStage(p, p_dmem))
  val csr = Module(new Csr(p))
  val wb = Module(new WbStage(p, p_dmem))

  val gpr = Module(new Gpr(p))
  val bypass = Module(new Bypass(p))

  // ******************************
  //             FLUSH
  // ******************************
  val w_flush = Wire(Vec(p.nHart, Bool()))

  for (h <- 0 until p.nHart) {
    w_flush(h) := ex.io.o_br_new(h).valid
  }

  // ******************************
  //            ID STAGE
  // ******************************
  id.io.b_hart <> io.b_hart
  id.io.b_backport <> io.b_backport
  id.io.i_lock := iss.io.o_lock
  id.io.i_flush := w_flush
  id.io.b_csr <> csr.io.b_id
  id.io.i_valid := io.i_valid
  id.io.i_bus := io.i_port

  // ******************************
  //            ISS STAGE
  // ******************************
  // ------------------------------
  //              END
  // ------------------------------
  val w_end = Wire(Vec(p.nHart, Bool()))

  for (h <- 0 until p.nHart) {
    w_end(h) := ex.io.o_end(h) | mem.io.o_end(h) | wb.io.o_end(h)
  }
  // ------------------------------
  //             PEND
  // ------------------------------
  val w_pend = Wire(Vec(p.nHart, Bool()))

  for (h <- 0 until p.nHart) {
    w_pend(h) := ex.io.o_pend(h) | mem.io.o_pend(h) | wb.io.o_pend(h)
  }

  // ------------------------------
  //             CONNECT
  // ------------------------------
  iss.io.b_hart <> io.b_hart
  iss.io.b_backport <> io.b_backport
  iss.io.i_lock := ex.io.o_lock
  iss.io.i_flush := w_flush
  iss.io.i_end := w_end
  iss.io.i_pend := w_pend

  iss.io.i_valid := id.io.o_valid
  iss.io.i_bus := id.io.o_bus

  iss.io.i_rs_wait := bypass.io.o_wait
  if (p.useDome) {
    for (h <- 0 until p.nHart) {
      iss.io.i_rmu_wait.get(h) := ex.io.o_rmu_wait.get(h) | mem.io.o_rmu_wait.get(h) | wb.io.o_rmu_wait.get(h)
    }
  }

  iss.io.b_rsrc_alu <> io.b_unit_alu
  iss.io.i_free_alu := ex.io.o_free_alu
  if (p.nMulDiv > 0) iss.io.b_rsrc_muldiv.get <> io.b_unit_muldiv.get
  if (p.nMulDiv > 0) iss.io.i_free_muldiv.get := ex.io.o_free_muldiv.get

  // ******************************
  //         GPR AND BYPASS
  // ******************************
  for (bp <- 0 until p.nBackPort) {
    gpr.io.b_rport(bp * 2) <> iss.io.b_rs(bp * 2)
    gpr.io.b_rport(bp * 2 + 1) <> iss.io.b_rs(bp * 2 + 1)
    gpr.io.b_wport(bp) := wb.io.b_rd(bp)

    bypass.io.b_rs(bp * 2) <> iss.io.b_rs(bp * 2)
    bypass.io.i_rs(bp * 2) := gpr.io.b_rport(bp * 2).data
    bypass.io.b_rs(bp * 2 + 1) <> iss.io.b_rs(bp * 2 + 1)
    bypass.io.i_rs(bp * 2 + 1) := gpr.io.b_rport(bp * 2 + 1).data
    bypass.io.i_bypass(bp) := wb.io.o_bypass(bp)
    bypass.io.i_bypass(bp + p.nBackPort) := mem.io.o_bypass(bp)
    bypass.io.i_bypass(bp + p.nBackPort * 2) := ex.io.o_bypass(bp)
  }

  // ******************************
  //            EX STAGE
  // ******************************
  // ------------------------------
  //          NEXT BRANCH
  // ------------------------------
  val w_br_next = Wire(Vec(p.nHart, new BranchBus(32)))

  for (h <- 0 until p.nHart) {
    w_br_next(h) := Mux(iss.io.o_br_next(h).valid, iss.io.o_br_next(h), io.i_br_next(h))

    /**w_br_next(h) := io.i_br_next(h)

    for (bp <- 0 until p.nBackPort) {
      when(id.io.o_valid(p.nBackPort - 1 - bp) & (h.U === id.io.o_bus(p.nBackPort - 1 - bp).info.hart)) {
        w_br_next(h).valid := true.B
        w_br_next(h).pc := id.io.o_bus(p.nBackPort - 1 - bp).info.pc
      }
    }*/
  }

  // ------------------------------
  //             CONNECT
  // ------------------------------
  ex.io.b_hart <> io.b_hart
  ex.io.b_backport <> io.b_backport
  ex.io.i_lock := mem.io.o_lock

  ex.io.i_valid := iss.io.o_valid
  ex.io.i_bus := iss.io.o_bus
  ex.io.i_br_next := w_br_next

  ex.io.b_rsrc_alu <> io.b_unit_alu
  if (p.nMulDiv > 0) ex.io.b_rsrc_muldiv.get <> io.b_unit_muldiv.get

  if (p.useDome) {
    for (h <- 0 until p.nHart) {
      ex.io.b_rmu.get(h) <> io.b_rmu.get(h).req
    }
  }
  if (p.useMamu) ex.io.b_flush.get <> io.b_flush.get

  // ******************************
  //       LSU AND MEM STAGE
  // ******************************
  lsu.io.b_hart <> io.b_hart
  lsu.io.b_backport <> io.b_backport

  lsu.io.b_req <> ex.io.b_lsu
  lsu.io.b_ack <> wb.io.b_dmem
  lsu.io.b_dmem <> io.b_dmem

  mem.io.b_hart <> io.b_hart
  mem.io.b_backport <> io.b_backport
  mem.io.i_lock := wb.io.o_lock

  mem.io.i_valid := ex.io.o_valid
  mem.io.i_bus := ex.io.o_bus

  // ******************************
  //              CSR
  // ******************************
  csr.io.b_hart <> io.b_hart
  csr.io.b_read <> mem.io.b_csr
  csr.io.b_write <> wb.io.b_csr
  csr.io.b_ex <> ex.io.b_csr_info
  csr.io.b_wb <> wb.io.b_csr_info
  if (p.useDome) csr.io.b_rmu.get <> io.b_rmu_csr.get
  io.o_en := csr.io.o_en

  // ******************************
  //            WB STAGE
  // ******************************
  wb.io.i_valid := mem.io.o_valid
  wb.io.i_bus := mem.io.o_bus

  if (p.useDome) {
    for (h <- 0 until p.nHart) {
      wb.io.b_rmu.get(h) <> io.b_rmu.get(h).ack
    }
  }

  // ******************************
  //              I/O
  // ******************************
  io.o_lock := id.io.o_lock
  io.o_br_new := ex.io.o_br_new
  io.o_br_info := ex.io.o_br_info

  if (p.debug) io.o_dbg_gpr.get := gpr.io.o_dbg_gpr.get
  if (p.debug) io.o_dbg_csr.get := csr.io.o_dbg_csr.get

  // ******************************
  //              FREE
  // ******************************
  for (h <- 0 until p.nHart) {
    io.b_hart(h).free := id.io.b_hart(h).free & iss.io.b_hart(h).free & ex.io.b_hart(h).free & lsu.io.b_hart(h).free & mem.io.b_hart(h).free
  }

  for (bp <- 0 until p.nBackPort) {
    io.b_backport(bp).free := id.io.b_backport(bp).free & iss.io.b_backport(bp).free & ex.io.b_backport(bp).free & lsu.io.b_backport(bp).free & mem.io.b_backport(bp).free
  }

  for (a <- 0 until p.nAlu) {
    io.b_unit_alu(a).free := ex.io.b_rsrc_alu(a).free
  }

  for (m <- 0 until p.nMulDiv) {
    io.b_unit_muldiv.get(m).free := ex.io.b_rsrc_muldiv.get(m).free
  }
}

object Back extends App {
  chisel3.Driver.execute(args, () => new Back(BackDefault, PmbCfg0))
}
