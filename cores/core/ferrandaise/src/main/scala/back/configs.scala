package cowlibry.core.ferrandaise.back

import chisel3._
import scala.math._


object DispatcherDefault extends DispatcherParams{
  def nHart: Int = 2
  def nDome: Int = 2

  def nBackPort: Int = 2
  def nAlu: Int = 2
  def nMulDiv: Int = 2
}

object BackDefault extends BackParams {
  override def debug: Boolean = false
  override def pcBoot: Int = 0x40
  override def nHart: Int = 1
  override def useDome: Boolean = false
  override def nDome: Int = 1

  override def nBackPort: Int = 2
  override def nAlu: Int = 2
  override def nMulDiv: Int = 1
  override def useFencei: Boolean = true
  override def useBranchReg: Boolean = true
}
