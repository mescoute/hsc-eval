package cowlibry.core.ferrandaise.back

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._
import cowlibry.common.tools._
import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._


class LsuReqIO (nHart: Int) extends Bundle {
  val ready = Output(Bool())
  val valid = Input(Bool())
  val hart = Input(UInt(log2Ceil(nHart).W))
  val rw = Input(Bool())
  val size = Input(UInt(SIZE.NBIT.W))
  val addr = Input(UInt(32.W))
  val wdata = Input(UInt(32.W))

  override def cloneType = (new LsuReqIO(nHart)).asInstanceOf[this.type]
}

class LsuReqBus(nHart: Int, nBackPort: Int) extends Bundle {
  val hart = UInt(log2Ceil(nHart).W)
  val backport = UInt(log2Ceil(nBackPort).W)
  val rw = Bool()
  val size = UInt(SIZE.NBIT.W)
  val addr = UInt(32.W)
  val wdata = UInt(32.W)

  override def cloneType = (new LsuReqBus(nHart, nBackPort)).asInstanceOf[this.type]
}

class LsuMemBus(nHart: Int) extends Bundle {
  val hart = UInt(log2Ceil(nHart).W)
  val rw = Bool()

  override def cloneType = (new LsuMemBus(nHart)).asInstanceOf[this.type]
}

class LsuAckBus(nBackPort: Int) extends Bundle {
  val ready = Bool()
  val valid = Bool()
  val backport = UInt(log2Ceil(nBackPort).W)
  val rw = Bool()
  val rdata = UInt(32.W)

  override def cloneType = (new LsuAckBus(nBackPort)).asInstanceOf[this.type]
}

class Lsu (p: BackParams, p_dmem: PmbParams) extends Module {
  val io = IO(new Bundle {
    val b_hart = Vec(p.nHart, new RsrcBus(p.nHart, p.nDome, 1))
    val b_backport = Vec(p.nBackPort, new RsrcBus(p.nHart, p.nDome, 1))

    val b_req = Vec(p.nBackPort, new LsuReqIO(p.nHart))
    val b_ack = Vec(p.nBackPort, Flipped(new PmbAckIO(p_dmem)))

    val b_dmem = Vec(p.nHart, new PmbIO(p_dmem))
  })

  // ******************************
  //             INPUT
  // ******************************
  val fifo_req = Seq.fill(p.nHart) {Module(new Fifo(3, new LsuReqBus(p.nHart, p.nBackPort), p.nBackPort * 2, p.nBackPort, 1))}
  val fifo_bp = Seq.fill(p.nBackPort) {Module(new Fifo(2, new LsuMemBus(p.nHart), 3, 1, 1))}

  for (h <- 0 until p.nHart) {
    fifo_req(h).io.i_flush := false.B
    fifo_req(h).io.i_ren(0) := false.B

    for (bp <- 0 until p.nBackPort) {
      fifo_req(h).io.i_wen(bp) := io.b_req(bp).valid & (h.U === io.b_req(bp).hart) & ~fifo_bp(bp).io.o_full(0)
      fifo_req(h).io.i_data(bp).hart := io.b_req(bp).hart
      fifo_req(h).io.i_data(bp).backport := bp.U
      fifo_req(h).io.i_data(bp).rw := io.b_req(bp).rw
      fifo_req(h).io.i_data(bp).size := io.b_req(bp).size
      fifo_req(h).io.i_data(bp).addr := io.b_req(bp).addr
      fifo_req(h).io.i_data(bp).wdata := io.b_req(bp).wdata
    }
  }

  for (bp <- 0 until p.nBackPort) {
    fifo_bp(bp).io.i_flush := false.B
    fifo_bp(bp).io.i_wen(0) := false.B
    fifo_bp(bp).io.i_data(0).hart := io.b_req(bp).hart
    fifo_bp(bp).io.i_data(0).rw := io.b_req(bp).rw
    fifo_bp(bp).io.i_ren(0) := false.B

    for (h <- 0 until p.nHart) {
      when (h.U === io.b_req(bp).hart) {
        fifo_bp(bp).io.i_wen(0) := io.b_req(bp).valid & ~fifo_req(h).io.o_full(bp)
      }
    }
  }

  for (bp <- 0 until p.nBackPort) {
    io.b_req(bp).ready := false.B

    for (h <- 0 until p.nHart) {
      when (h.U === io.b_req(bp).hart) {
        io.b_req(bp).ready := ~fifo_req(h).io.o_full(bp) & ~fifo_bp(bp).io.o_full(0)
      }
    }
  }

  // ******************************
  //            MEMORY
  // ******************************
  val fifo_dmem = Seq.fill(p.nHart) {Module(new Fifo(2, UInt(log2Ceil(p.nBackPort).W), p.nBackPort, 1, 1))}

  // ------------------------------
  //            REQUEST
  // ------------------------------
  for (h <- 0 until p.nHart) {
    fifo_req(h).io.i_ren(0) := io.b_dmem(h).req.ready(0) & ~fifo_dmem(h).io.o_full(0)

    io.b_dmem(h).req.valid := ~fifo_req(h).io.o_empty(0) & ~fifo_dmem(h).io.o_full(0)
    io.b_dmem(h).req.hart := fifo_req(h).io.o_data(0).hart
    io.b_dmem(h).req.dome := io.b_hart(h).dome
    io.b_dmem(h).req.rw := fifo_req(h).io.o_data(0).rw
    io.b_dmem(h).req.size := fifo_req(h).io.o_data(0).size
    io.b_dmem(h).req.addr := fifo_req(h).io.o_data(0).addr
    io.b_dmem(h).req.wdata := fifo_req(h).io.o_data(0).wdata
  }

  // ------------------------------
  //             FIFO
  // ------------------------------
  for (h <- 0 until p.nHart) {
    fifo_dmem(h).io.i_flush := false.B
    fifo_dmem(h).io.i_wen(0) := ~fifo_req(h).io.o_empty(0) & io.b_dmem(h).req.ready(0)
    fifo_dmem(h).io.i_data(0) := fifo_req(h).io.o_data(0).backport
    fifo_dmem(h).io.i_ren(0) := false.B
  }

  // ******************************
  //         ACKNOWLEDGEMENT
  // ******************************
  // ------------------------------
  //             DATA
  // ------------------------------
  val w_ack_bus = Wire(Vec(p.nHart, new LsuAckBus(p.nBackPort)))

  val reg_valid = RegInit(VecInit(Seq.fill(p.nBackPort)(false.B)))
  val reg_rdata = Reg(Vec(p.nBackPort, UInt(32.W)))

  for (h <- 0 until p.nHart) {
    io.b_dmem(h).ack.ready(0) := w_ack_bus(h).ready
  }

  for (h <- 0 until p.nHart) {
    w_ack_bus(h).ready := false.B
    w_ack_bus(h).valid := false.B
    w_ack_bus(h).backport := fifo_dmem(h).io.o_data(0)
    w_ack_bus(h).rw := fifo_bp(0).io.o_data(0).rw
    w_ack_bus(h).rdata := io.b_dmem(h).ack.rdata
  }

  for (h <- 0 until p.nHart) {
    for (bp <- 0 until p.nBackPort) {
      when (~fifo_dmem(h).io.o_empty(0) & (bp.U === fifo_dmem(h).io.o_data(0))) {
        when (~fifo_bp(bp).io.o_empty(0) & (h.U === fifo_bp(bp).io.o_data(0).hart)) {
          fifo_bp(bp).io.i_ren(0) := w_ack_bus(h).ready & io.b_dmem(h).ack.valid
          fifo_dmem(h).io.i_ren(0) := w_ack_bus(h).ready & io.b_dmem(h).ack.valid

          w_ack_bus(h).valid := io.b_dmem(h).ack.valid
          w_ack_bus(h).rw := fifo_bp(bp).io.o_data(0).rw
        }
      }
    }
  }

  for (bp <- 0 until p.nBackPort) {
    when (io.b_ack(bp).ready(0)) {
      reg_valid(bp) := false.B
    }

    io.b_ack(bp).valid := reg_valid(bp)
    io.b_ack(bp).rdata := reg_rdata(bp)
    io.b_ack(bp).dome := io.b_backport(bp).dome
    io.b_ack(bp).hart := 0.U
  }

  for (h <- 0 until p.nHart) {
    for (bp <- 0 until p.nBackPort) {
      when (w_ack_bus(h).valid & (bp.U === w_ack_bus(h).backport)) {
        when (reg_valid(bp)) {
          when (io.b_ack(bp).ready(0)) {
            w_ack_bus(h).ready := ~w_ack_bus(h).rw
            reg_valid(bp) := true.B
            reg_rdata(bp) := w_ack_bus(h).rdata
          }.otherwise {
            w_ack_bus(h).ready := false.B
          }
        }.otherwise {
          reg_rdata(bp) := w_ack_bus(h).rdata
          io.b_ack(bp).valid := true.B
          io.b_ack(bp).rdata := w_ack_bus(h).rdata

          when (io.b_ack(bp).ready(0)) {
            w_ack_bus(h).ready := true.B
          }.otherwise {
            w_ack_bus(h).ready := ~w_ack_bus(h).rw
            reg_valid(bp) := true.B
          }
        }
      }
    }
  }

  // ******************************
  //             FREE
  // ******************************

  for (h <- 0 until p.nHart) {
    io.b_hart(h).free := fifo_req(h).io.o_empty(0) & fifo_dmem(h).io.o_empty(0)
  }

  for (bp <- 0 until p.nBackPort) {
    io.b_backport(bp).free := fifo_bp(bp).io.o_empty(0) & ~reg_valid(bp)
  }
}

object Lsu extends App {
  chisel3.Driver.execute(args, () => new Lsu(BackDefault, PmbCfg0))
}
