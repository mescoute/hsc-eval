package cowlibry.core.ferrandaise.back

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._
import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._
import cowlibry.core.aubrac.back.{MemStage => MemUnit}
import cowlibry.core.aubrac.back.{BypassBus, CsrReadBus, MemStageBus, WbStageBus}


class MemStage(p: BackParams, p_dmem: PmbParams) extends Module {
  val io = IO(new Bundle {
    val b_hart = Vec(p.nHart, new RsrcBus(p.nHart, p.nDome, 1))
    val b_backport = Vec(p.nBackPort, new RsrcBus(p.nHart, p.nDome, 1))

    val i_lock = Input(Vec(p.nBackPort, Bool()))
    val o_lock = Output(Vec(p.nBackPort, Bool()))

    val o_end = Output(Vec(p.nHart, Bool()))
    val o_pend = Output(Vec(p.nHart, Bool()))

    val i_valid = Input(Vec(p.nBackPort, Bool()))
    val i_bus = Input(Vec(p.nBackPort, new MemStageBus(p)))

    val b_csr = Vec(p.nHart, Flipped(new CsrReadBus(p)))
    val o_bypass = Output(Vec(p.nBackPort, new BypassBus(p.nHart)))
    val o_rmu_wait = if (p.useDome) Some(Output(Vec(p.nHart, Bool()))) else None

    val o_valid = Output(Vec(p.nBackPort, Bool()))
    val o_bus = Output(Vec(p.nBackPort, new WbStageBus(p)))
  })

  val unit = Seq.fill(p.nBackPort) {Module(new MemUnit(false, p, p_dmem))}

  // ******************************
  //          MANAGE HART
  // ******************************
  val w_h_wait = Wire(Vec(p.nHart, Bool()))

  for (h <- 0 until p.nHart) {
    w_h_wait(h) := false.B
    for (bp <- 0 until p.nBackPort) {
      when ((h.U === io.i_bus(bp).info.hart) & unit(bp).io.o_lock) {
        w_h_wait(h) := true.B
      }
    }
  }

  for (bp <- 0 until p.nBackPort) {
    io.o_lock(bp) := w_h_wait(io.i_bus(bp).info.hart)
  }

  // ******************************
  //              END
  // ******************************
  for (h <- 0 until p.nHart) {
    io.o_end(h) := false.B
  }

  for (bp <- 0 until p.nBackPort) {
    when (io.i_valid(bp) & io.i_bus(bp).info.end) {
      io.o_end(io.i_bus(bp).info.hart) := true.B
    }
  }

  // ******************************
  //              PEND
  // ******************************
  for (h <- 0 until p.nHart) {
    io.o_pend(h) := false.B
  }

  for (bp <- 0 until p.nBackPort) {
    when (io.i_valid(bp)) {
      io.o_pend(io.i_bus(bp).info.hart) := true.B
    }
  }

  // ******************************
  //        RMU DEPENDENCIES
  // ******************************
  if (p.useDome) {
    for (h <- 0 until p.nHart) {
      io.o_rmu_wait.get(h) := false.B
      for (bp <- 0 until p.nBackPort) {
        when (unit(bp).io.o_rmu_wait.get & (h.U === io.i_bus(bp).info.hart)) {
          io.o_rmu_wait.get(h) := true.B
        }
      }
    }
  }

  // ******************************
  //              UNIT
  // ******************************
  for (bp <- 0 until p.nBackPort) {
    unit(bp).io.b_hart <> io.b_hart
    unit(bp).io.b_backport <> io.b_backport(bp)

    unit(bp).io.i_lock := io.i_lock(bp)
    unit(bp).io.i_wait.get := w_h_wait(io.i_bus(bp).info.hart)

    unit(bp).io.i_valid := io.i_valid(bp)
    unit(bp).io.i_bus := io.i_bus(bp)

    unit(bp).io.b_csr <> io.b_csr(io.i_bus(bp).info.hart)
    io.o_bypass(bp) := unit(bp).io.o_bypass

    io.o_valid(bp) := unit(bp).io.o_valid
    io.o_bus(bp) := unit(bp).io.o_bus
  }

  // ******************************
  //              CSR
  // ******************************
  for (h <- 0 until p.nHart) {
    io.b_csr(h).valid := false.B
    io.b_csr(h).addr := 0.U

    for (bp <- 0 until p.nBackPort) {
      when (unit(bp).io.b_csr.valid & (h.U === io.i_bus(bp).info.hart)) {
        io.b_csr(h) <> unit(bp).io.b_csr
      }
    }
  }
}

object MemStage extends App {
  chisel3.Driver.execute(args, () => new MemStage(BackDefault, PmbCfg0))
}
