package cowlibry.core.ferrandaise.rmu

import chisel3._
import chisel3.util._

import cowlibry.common.tools._
import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._


class NoRmu (p: RmuParams) extends Module {
  val io = IO(new Bundle {
    val b_dome = Vec(p.nDome, Flipped(new DomeBus(p.nHart)))
    val b_hart = Vec(p.nHart, Flipped(new RsrcBus(p.nHart, p.nDome, 1)))
    val b_backport = Vec(p.nBackPort, Flipped(new RsrcBus(p.nHart, p.nDome, 1)))

    val b_unit_alu = Vec(p.nAlu, Flipped(new RsrcBus(1, p.nDome, p.nAlu)))
    val b_unit_muldiv = if (p.nMulDiv > 0) Some(Vec(p.nMulDiv, Flipped(new RsrcBus(1, p.nDome, p.nMulDiv)))) else None
  })

  for (d <- 0 until p.nDome) {
    io.b_dome(d).valid := true.B
    io.b_dome(d).flush := false.B
    io.b_dome(d).weight := 0.U
  }

  for (ha <- 0 until p.nHart) {
    io.b_hart(ha).valid := true.B
    io.b_hart(ha).flush := false.B
    io.b_hart(ha).hart := ha.U
    io.b_hart(ha).dome := 0.U
    io.b_hart(ha).port := 0.U
  }

  for (bp <- 0 until p.nBackPort) {
    io.b_backport(bp).valid := true.B
    io.b_backport(bp).flush := false.B
    io.b_backport(bp).hart := 0.U
    io.b_backport(bp).dome := 0.U
    io.b_backport(bp).port := 0.U
  }

  for (a <- 0 until p.nAlu) {
    io.b_unit_alu(a).valid := true.B
    io.b_unit_alu(a).flush := false.B
    io.b_unit_alu(a).hart := 0.U
    io.b_unit_alu(a).dome := 0.U
    io.b_unit_alu(a).port := 0.U
  }

  if (p.nMulDiv > 0) {
    for (m <- 0 until p.nMulDiv) {
      io.b_unit_muldiv.get(m).valid := true.B
      io.b_unit_muldiv.get(m).flush := false.B
      io.b_unit_muldiv.get(m).hart := 0.U
      io.b_unit_muldiv.get(m).dome := 0.U
      io.b_unit_muldiv.get(m).port := 0.U
    }
  }
}

object NoRmu extends App {
  chisel3.Driver.execute(args, () => new NoRmu(RmuDefault))
}
