package cowlibry.core.ferrandaise.rmu

import chisel3._
import chisel3.util._
import scala.math._

import cowlibry.core.aubrac.rmu.{RmuParams => AubracRmuParams}


trait RmuParams extends AubracRmuParams {
  def nHart: Int
  def nDome: Int
  def useDomeConstFlush: Boolean
  def nDomeFlushCycle: Int

  def nBackPort: Int
  def nAlu: Int
  def nMulDiv: Int
}
