package cowlibry.core.ferrandaise

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._
import cowlibry.common.mem.flush._
import cowlibry.common.rsrc._
import cowlibry.core.aubrac.front._
import cowlibry.core.aubrac.rmu.{Rmu}
import cowlibry.core.aubrac.back.{CsrBus}
import cowlibry.core.ferrandaise.back._
import cowlibry.core.ferrandaise.rmu._


class Pipeline(p: PipelineParams) extends Module {
  // ******************************
  //          IMEM PARAMS
  // ******************************
  var p_imem = new PmbIntf(p.nHart, p.nDome, false, 32, 32 * p.nFetchInstr)

  // ******************************
  //          DMEM PARAMS
  // ******************************
  var p_dmem = new PmbIntf(p.nHart, p.nDome, false, 32, 32)

  val io = IO(new Bundle {
    val b_dome = Flipped(new DomeBus(p.nHart))
    val b_hart = Flipped(new RsrcBus(p.nHart, p.nDome, p.nHart))

    val b_imem = new PmbIO(p_imem)
    val b_dmem = new PmbIO(p_dmem)

    val b_flush = if (p.useMamu) Some(new FlushIO(p.nHart, p.nDome, 32)) else None

    val o_dbg_gpr = if (p.debug) Some(Output(Vec(32, UInt(32.W)))) else None
    val o_dbg_csr = if (p.debug) Some(Output(new CsrBus())) else None
  })

  val front = Module(new Front(p, p_imem))
  val back = Module(new Back(p, p_dmem))
  val rmu = if (p.useDome) Some(Module(new Rmu(p))) else None
  val normu = if (!p.useDome) Some(Module(new NoRmu(p))) else None

  // ******************************
  //             FRONT
  // ******************************
  if (p.useDome) {
    front.io.b_hart <> rmu.get.io.b_hart
    front.io.i_br_boot.get := rmu.get.io.o_br_boot
  } else {
    front.io.b_hart <> normu.get.io.b_hart(0)
  }
  front.io.i_br_new := back.io.o_br_new(0)
  if (p.useNlp) front.io.i_br_info.get := back.io.o_br_info(0)
  front.io.b_imem <> io.b_imem
  for (bp <- 0 until p.nBackPort) {
    front.io.i_ready(bp) := ~back.io.o_lock(bp)
  }


  // ******************************
  //             BACK
  // ******************************
  if (p.useDome) {
    back.io.b_hart(0) <> rmu.get.io.b_hart
    for (bp <- 0 until p.nBackPort) back.io.b_backport(bp) <> rmu.get.io.b_hart
    for (a <- 0 until p.nAlu) back.io.b_unit_alu(a) <> rmu.get.io.b_hart
    for (md <- 0 until p.nMulDiv) back.io.b_unit_muldiv.get(md) <> rmu.get.io.b_hart
  } else {
    back.io.b_hart <> normu.get.io.b_hart
    back.io.b_backport <> normu.get.io.b_backport
    back.io.b_unit_alu <> normu.get.io.b_unit_alu
    if (p.nMulDiv > 0) back.io.b_unit_muldiv.get <> normu.get.io.b_unit_muldiv.get
  }

  back.io.i_valid := front.io.o_valid
  for (bp <- 0 until p.nBackPort) {
    back.io.i_port(bp).hart := 0.U
    back.io.i_port(bp).pc := front.io.o_fetch(bp).pc
    back.io.i_port(bp).instr := front.io.o_fetch(bp).instr
  }
  back.io.i_br_next(0) := front.io.o_br_next
  back.io.b_dmem(0) <> io.b_dmem
  if (p.useMamu) back.io.b_flush.get(0) <> io.b_flush.get

  // ******************************
  //              I/O
  // ******************************
  if (p.useDome) {
    io.b_dome <> rmu.get.io.b_dome
    io.b_hart <> rmu.get.io.b_hart
  } else {
    io.b_dome <> normu.get.io.b_dome(0)
    io.b_hart <> normu.get.io.b_hart(0)
  }

  if (p.debug) io.o_dbg_gpr.get := back.io.o_dbg_gpr.get(0)
  if (p.debug) io.o_dbg_csr.get := back.io.o_dbg_csr.get(0)

  // ******************************
  //              RMU
  // ******************************
  if (p.useDome) {
    rmu.get.io.b_port <> back.io.b_rmu.get(0)
    rmu.get.io.b_csr <> back.io.b_rmu_csr.get(0)
    rmu.get.io.b_hart.free := front.io.b_hart.free & back.io.b_hart(0).free & io.b_hart.free
  }
}

object Pipeline extends App {
  chisel3.Driver.execute(args, () => new Pipeline(PipelineDefault))
}
