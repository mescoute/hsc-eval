package cowlibry.core.ferrandaise

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._
import cowlibry.mem.manseng._
import cowlibry.core.aubrac.back.{CsrBus}


class Ferrandaise(p: FerrandaiseParams) extends Module {
  // ******************************
  //           PARAMETERS
  // ******************************
  // ------------------------------
  //             IMEM
  // ------------------------------
  var p_imem = new PmbIntf(1, 1, false, 32, p.nL1IWordByte * 8)

  // ------------------------------
  //            ICACHE
  // ------------------------------
  val p_l1i = new MansengIntf(
    p.debug,                  // debug
    32,                       // nAddrBit
    1,                        // nHart
    if (p.useMamu) 1 else 0,  // nFlush
    1,                        // nDome
    false,                    // multiPrevDome
    true,                     // usePartWay
    false,                    // useMpuReg
    4 * p.nFetchInstr,        // nPrevWordByte
    p.nL1IWordByte,           // nNextWordByte
    1,                        // nPrevMem
    1,                        // nPrevReqFifoDepth
    false,                    // usePrevReqReg
    false,                    // usePrevReadReg
    2,                        // nNextReqFifoDepth
    2,                        // nNextMemFifoDepth
    2,                        // nNextWriteFifoDepth
    p.nL1ISetReadPort,        // nSetReadPort
    p.nL1ISetWritePort,       // nSetWritePort
    p.slctL1IPolicy,          // slctPolicy
    p.nL1ISubCache,           // nSubCache
    p.nL1ISet,                // nSet
    p.nL1ILine,               // nLine
    p.nL1IWord                // nWord
  )

  // ------------------------------
  //             DMEM
  // ------------------------------
  var p_dmem = new PmbIntf(1, 1, false, 32, p.nL1DWordByte * 8)

  // ------------------------------
  //            DCACHE
  // ------------------------------
  val p_l1d = new MansengIntf(
    p.debug,            // debug
    32,                 // nAddrBit
    1,                  // nHart
    0,                  // nFlush
    1,                  // nDome
    false,              // multiPrevDome
    true,               // usePartWay
    false,              // useMpuReg
    4,                  // nPrevWordByte
    p.nL1DWordByte,     // nNextWordByte
    1,                  // nPrevMem
    1,                  // nPrevReqFifoDepth
    false,              // usePrevReqReg
    false,              // usePrevReadReg
    2,                  // nNextReqFifoDepth
    2,                  // nNextMemFifoDepth
    2,                  // nNextWriteFifoDepth
    p.nL1DSetReadPort,  // nSetReadPort
    p.nL1DSetWritePort, // nSetWritePort
    p.slctL1DPolicy,    // slctPolicy
    p.nL1DSubCache,     // nSubCache
    p.nL1DSet,          // nSet
    p.nL1DLine,         // nLine
    p.nL1DWord          // nWord
  )

  // ******************************
  //              I/O
  // ******************************
  val io = IO(new Bundle {
    val b_imem = new PmbIO(p_imem)
    val b_dmem = new PmbIO(p_dmem)

    val o_dbg_gpr = if (p.debug) Some(Output(Vec(32, UInt(32.W)))) else None
    val o_dbg_csr = if (p.debug) Some(Output(new CsrBus())) else None
  })

  val pipe = Module(new Pipeline(p))
  val l1icache = Module(new Manseng(p_l1i))
  val l1dcache = Module(new Manseng(p_l1d))

  // ******************************
  //             MODULES
  // ******************************
  // ------------------------------
  //           L1I CACHE
  // ------------------------------
  l1icache.io.i_hier_flush(0) := true.B
  l1icache.io.b_dome(0) <> pipe.io.b_dome
  l1icache.io.b_hart(0) <> pipe.io.b_hart
  l1icache.io.b_prevmem(0) <> pipe.io.b_imem
  l1icache.io.b_nextmem <> io.b_imem
  // ------------------------------
  //           L1D CACHE
  // ------------------------------
  l1dcache.io.i_hier_flush(0) := true.B
  l1dcache.io.b_dome(0) <> pipe.io.b_dome
  l1dcache.io.b_hart(0) <> pipe.io.b_hart
  l1dcache.io.b_prevmem(0) <> pipe.io.b_dmem
  l1dcache.io.b_nextmem <> io.b_dmem

  // ------------------------------
  //            PIPELINE
  // ------------------------------
  pipe.io.b_dome.free := l1icache.io.b_dome(0).free & l1dcache.io.b_dome(0).free
  pipe.io.b_hart.free := l1icache.io.b_hart(0).free & l1dcache.io.b_hart(0).free

  if (p.debug) io.o_dbg_gpr.get := pipe.io.o_dbg_gpr.get
  if (p.debug) io.o_dbg_csr.get := pipe.io.o_dbg_csr.get

  // ******************************
  //             FLUSH
  // ******************************
  if (p.useMamu) pipe.io.b_flush.get <> l1icache.io.b_flush(0)
}

object Ferrandaise extends App {
  chisel3.Driver.execute(args, () => new Ferrandaise(FerrandaiseDefault))
}
