# Cowlibry - Aubrac core

1. Implement Return Address Stack (RAS = Return Stack Buffer (RSB))
2. Correct BTB tag size:
  - If tag size + low bits < PC size, it can result to possible misprediction without corrections.
  - Solution 1: tag size + low bits = PC size.
  - Solution 2: new comparison after decoding: if instruction is not a branch or a jump, next instruction must be PC + 4
3. HSC and counters:
  - Currently, during a switch, they are resetted.
  - Another implementation may be to increment them to the worst flush time.
  - Another implementation may be to lock them during the flush time.
