#include <stdlib.h>
#include <stdio.h>
#include "VAubracDbg.h"
#include "verilated.h"
#include "verilated_vcd_c.h"
#include "svdpi.h"
#include "VAubracDbg__Dpi.h"
#include <time.h>


int main(int argc, char **argv) {
  // Inputs
  int trigger = atoi(argv[1]);
  char* hexfile = argv[2];
  char* vcdfile = argv[3];

  time_t test_time = time(NULL);

	// Initialize Verilators variables
	Verilated::commandArgs(argc, argv);

  // Create an instance of our module under test
	VAubracDbg *dut = new VAubracDbg;

  // Generate VCD
  Verilated::traceEverOn(true);
  VerilatedVcdC* dut_trace = new VerilatedVcdC;
  dut->trace(dut_trace, 99);
  dut_trace->open((vcdfile));

  // Call task to initialize memory
  svSetScope(svGetScopeFromName("TOP.AubracTop.ram.ram"));
  //Verilated::scopesDump();
  dut->new_readmemh(hexfile);

	// Test variables
  int cycle = 0;      // Cycle
  bool end = false;   // Test end
  int result = -1;    // SW result

  // **********
  //   RESET
  // **********
  for (int i = 0; i < 5; i++) {
    dut->clock = 1;
  	dut->eval();
    cycle = cycle + 1;
    dut_trace->dump(cycle * 10);

		dut->clock = 0;
    dut->reset = 1;
		dut->eval();
    dut_trace->dump(cycle * 10 + 5);
  }
  dut->reset = 0;

  // **********
  // TEST LOOP
  // **********
	while ((!Verilated::gotFinish()) && (end == false)) {
    test_time = time(NULL);

    // Positive edge
		dut->clock = 1;
		dut->eval();
    cycle = cycle + 1;
    dut_trace->dump(cycle * 10);

    // Negative Edge
		dut->clock = 0;
		dut->eval();
    dut_trace->dump(cycle * 10 + 5);

    // SW trigger
    if (dut->io_o_dbg_gpr_31 == 1) {
      end = true;
      result = dut->io_o_dbg_gpr_30;
    }

    if (cycle > trigger) {
      end = true;
    }
	}

  // **********
  //   REPORT
  // **********
  printf("TEST REPORT: \n");
  printf("Test file %s \n", hexfile);
  printf("Cycles: %d \n", cycle);
  if (result != 0) {
    printf("\033[1;31m");
    printf("FAILED: some errors have been detected. \n");
    printf("\033[0m");
    printf("Result: %d \n", result);

    for (int i = 0; i < 32; i++) {
      int error = result & 1;
      if (error == 1) {
        printf("Part %d: FAILED. \n", i + 1);
      }
      result = result>>1;
    }
  } else if (cycle != trigger) {
    printf("\033[1;31m");
    printf("FAILED: the trigger has not been respected. \n");
    printf("\033[0m");
  } else {
    printf("\033[1;32m");
    printf("SUCCESS: test passed.\n");
    printf("\033[0m");
  }

  dut_trace->close();
  exit(EXIT_SUCCESS);
}
