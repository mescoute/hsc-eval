package cowlibry.core.aubrac.rmu

import chisel3._
import chisel3.util._

import cowlibry.common.tools._
import cowlibry.common.tools.{Counter}
import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._


class RsrcStage(p: RmuParams) extends Module {
  val io = IO(new Bundle {
    val o_lock = Output(Bool())
    val i_valid = Input(Bool())
    val i_bus = Input(new RmuReqBus())

    val b_dome = Flipped(new DomeBus(1))
    val b_hart = Flipped(new RsrcBus(1, 1, 1))

    val o_update = Output(Bool())
    val o_next = Output(new DomeConfBus())
    val o_br_boot = Output(new BranchBus(32))

    val b_port = new RmuAckIO()
  })

  // ******************************
  //     CONSTANT FLUSH COUNTER
  // ******************************
  val flush_counter = if (p.useDomeConstFlush) Some(Module(new Counter(log2Ceil(p.nDomeFlushCycle + 1)))) else None

  if (p.useDomeConstFlush) {
    flush_counter.get.io.i_init := true.B
    flush_counter.get.io.i_en := false.B
    flush_counter.get.io.i_limit := p.nDomeFlushCycle.U
  }

  val w_lock = Wire(Bool())

  // ******************************
  //              FSM
  // ******************************
  val s0NEW :: s1FLUSH :: s2UPDATE :: Nil = Enum(3)
  val reg_state = RegInit(s0NEW)

  val reg_hart_valid = RegInit(true.B)
  val reg_hart_flush = RegInit(false.B)

  when (reg_state === s1FLUSH) {
    io.o_lock := true.B
    if (p.useDomeConstFlush) {
      flush_counter.get.io.i_init := false.B
      flush_counter.get.io.i_en := true.B

      when (flush_counter.get.io.o_flag) {
        reg_state := s2UPDATE
      }.otherwise {
        reg_state := s1FLUSH
      }
    } else {
      when (reg_hart_flush & io.b_dome.free & io.b_hart.free) {
        reg_state := s2UPDATE
      }.otherwise {
        reg_state := s1FLUSH
      }
    }
  }.elsewhen (reg_state === s2UPDATE) {
    io.o_lock := false.B
    reg_state := s0NEW

    reg_hart_valid := io.i_bus.al
    reg_hart_flush := false.B
  }.otherwise {
    when (io.i_valid & io.i_bus.rel) {
      io.o_lock := true.B
      reg_state := s1FLUSH

      reg_hart_flush := true.B
    }.elsewhen (io.i_valid & io.i_bus.al) {
      io.o_lock := w_lock
      reg_state := s0NEW
    }.otherwise {
      io.o_lock := false.B
      reg_state := s0NEW
    }
  }

  // ******************************
  //              I/O
  // ******************************
  // ------------------------------
  //            RESOURCE
  // ------------------------------
  io.b_dome.valid := reg_hart_valid
  io.b_dome.flush := reg_hart_flush
  io.b_dome.weight := 0.U

  io.b_hart.valid := reg_hart_valid
  io.b_hart.flush := reg_hart_flush
  io.b_hart.hart := 0.U
  io.b_hart.dome := 0.U
  io.b_hart.port := 0.U

  // ------------------------------
  //             UPDATE
  // ------------------------------
  io.o_update := (reg_state === s2UPDATE)
  io.o_next := io.i_bus.work

  io.o_br_boot.valid := io.i_bus.al & (reg_state === s2UPDATE)
  io.o_br_boot.pc := io.i_bus.work.pc

  // ******************************
  //            OUTPUTS
  // ******************************
  val reg_valid = RegInit(false.B)
  val reg_done = Reg(Bool())

  // ------------------------------
  //             READ
  // ------------------------------
  w_lock := reg_valid & ~io.b_port.ready

  when (reg_valid & io.b_port.ready) {
    reg_valid := false.B
  }

  io.b_port.valid := reg_valid
  io.b_port.done := reg_done

  // ------------------------------
  //             WRITE
  // ------------------------------
  when ((reg_state === s0NEW) & (~reg_valid | io.b_port.ready)) {
    when (io.i_bus.al & ~io.i_bus.rel) {
      reg_valid := true.B
      reg_done := false.B
    }
  }
}

object RsrcStage extends App {
  chisel3.Driver.execute(args, () => new RsrcStage(RmuDefault))
}
