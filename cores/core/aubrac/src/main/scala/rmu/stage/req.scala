package cowlibry.core.aubrac.rmu

import chisel3._
import chisel3.util._

import cowlibry.common.tools._
import cowlibry.core.aubrac.tools._
import cowlibry.core.aubrac.back._


class ReqStage extends Module {
  val io = IO(new Bundle {
    val i_lock = Input(Bool())

    val b_port = new RmuReqIO()
    val i_dome_work = Input(new DomeConfBus())

    val o_valid = Output(Bool())
    val o_bus = Output(new RmuReqBus())
  })

  val reg_valid = RegInit(false.B)
  val reg_bus = Reg(new RmuReqBus())

  // ******************************
  //             INPUT
  // ******************************
  io.b_port.ready := ~reg_valid

  when (~reg_valid) {
    reg_valid := io.b_port.valid
    reg_bus.al := (io.b_port.uop === EXUOP.START.U) | (io.b_port.uop === EXUOP.SWITCH.U)
    reg_bus.rel := (io.b_port.uop === EXUOP.END.U) | (io.b_port.uop === EXUOP.SWITCH.U)
    reg_bus.work := io.i_dome_work
  }

  // ******************************
  //            OUTPUT
  // ******************************
  when (~io.i_lock & reg_valid) {
    reg_valid := false.B
  }

  io.o_valid := reg_valid
  io.o_bus := reg_bus
}

object ReqStage extends App {
  chisel3.Driver.execute(args, () => new ReqStage())
}
