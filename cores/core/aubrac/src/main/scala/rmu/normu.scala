package cowlibry.core.aubrac.rmu
import chisel3._
import chisel3.util._

import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._
import cowlibry.core.aubrac.back._


class NoRmu extends Module {
  val io = IO(new Bundle {
    val b_dome = Flipped(new DomeBus(1))
    val b_hart = Flipped(new RsrcBus(1, 1, 1))
  })

  io.b_dome.valid := true.B
  io.b_dome.flush := false.B
  io.b_dome.weight := 0.U

  io.b_hart.valid := true.B
  io.b_hart.flush := false.B
  io.b_hart.hart := 0.U
  io.b_hart.dome := 0.U
  io.b_hart.port := 0.U
}

object NoRmu extends App {
  chisel3.Driver.execute(args, () => new NoRmu())
}
