package cowlibry.core.aubrac.rmu

import chisel3._
import chisel3.util._


trait RmuParams {
  def useDomeConstFlush: Boolean
  def nDomeFlushCycle: Int
}
