package cowlibry.core.aubrac.rmu

import chisel3._
import chisel3.util._

import cowlibry.core.aubrac.back._


class RmuReqIO extends Bundle {
  val ready = Output(Bool())
  val valid = Input(Bool())
  val uop = Input(UInt(EXUOP.NBIT.W))
}

class RmuAckIO extends Bundle {
  val ready = Input(Bool())
  val valid = Output(Bool())
  val done = Output(Bool())
}

class RmuIO extends Bundle {
  val req = new RmuReqIO()
  val ack = new RmuAckIO()
}
