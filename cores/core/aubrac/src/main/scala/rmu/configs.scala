package cowlibry.core.aubrac.rmu

import chisel3._
import chisel3.util._


object RmuDefault extends RmuParams {
  def useDomeConstFlush: Boolean = true
  def nDomeFlushCycle: Int = 10
}
