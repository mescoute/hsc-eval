package cowlibry.core.aubrac.rmu

import chisel3._


object SIZE {
  def ONE = 0
  def MAX = 1
  def ALL = 2
}
