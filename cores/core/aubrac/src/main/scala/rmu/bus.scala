package cowlibry.core.aubrac.rmu

import chisel3._
import chisel3.util._

import cowlibry.core.aubrac.tools._


class RmuReqBus extends Bundle {
  val al = Bool()
  val rel = Bool()
  val work = new DomeConfBus()
}
