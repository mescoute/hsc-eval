package cowlibry.core.aubrac.rmu
import chisel3._
import chisel3.util._

import cowlibry.common.tools._
import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._
import cowlibry.core.aubrac.back._


class Rmu(p: RmuParams) extends Module {
  val io = IO(new Bundle {
    val b_port = new RmuIO()
    val b_csr = Flipped(new CsrRmuBus())

    val b_dome = Flipped(new DomeBus(1))
    val b_hart = Flipped(new RsrcBus(1, 1, 1))

    val o_br_boot = Output(new BranchBus(32))
  })

  val req = Module(new ReqStage())
  val rsrc = Module(new RsrcStage(p))

  // ******************************
  //         REQUEST STAGE
  // ******************************
  req.io.i_lock := rsrc.io.o_lock
  req.io.b_port <> io.b_port.req
  req.io.i_dome_work := io.b_csr.work

  // ******************************
  //          RSRC STAGE
  // ******************************
  rsrc.io.i_valid := req.io.o_valid
  rsrc.io.i_bus := req.io.o_bus
  rsrc.io.b_dome <> io.b_dome
  rsrc.io.b_hart <> io.b_hart
  rsrc.io.b_port <> io.b_port.ack


  // ******************************
  //              I/O
  // ******************************
  io.b_csr.update := rsrc.io.o_update
  io.b_csr.next := rsrc.io.o_next
  io.o_br_boot := rsrc.io.o_br_boot
}

object Rmu extends App {
  chisel3.Driver.execute(args, () => new Rmu(RmuDefault))
}
