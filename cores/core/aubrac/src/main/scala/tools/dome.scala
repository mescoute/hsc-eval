package cowlibry.core.aubrac.tools

import chisel3._
import chisel3.util._


// ******************************
//              DATA
// ******************************
class DomeConfBus extends Bundle {
  val id = UInt(32.W)
  val pc = UInt(32.W)
  val cap = UInt(32.W)
}
