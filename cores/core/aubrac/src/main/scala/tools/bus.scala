package cowlibry.core.aubrac.tools

import chisel3._
import chisel3.util._


// ******************************
//            BRANCH
// ******************************
class BranchBus(nAddrBit: Int) extends Bundle {
  val valid = Bool()
  val pc = UInt(nAddrBit.W)

  override def cloneType = (new BranchBus(nAddrBit)).asInstanceOf[this.type]
}

class BranchInfoBus(nAddrBit: Int) extends Bundle {
  val valid = Bool()
  val pc = UInt(nAddrBit.W)
  val br = Bool()
  val taken = Bool()
  val jmp = Bool()
  val target = UInt(nAddrBit.W)

  override def cloneType = (new BranchInfoBus(nAddrBit)).asInstanceOf[this.type]
}

// ******************************
//          FETCH PACKET
// ******************************
class FetchBus(nAddrBit: Int, nInstrBit: Int) extends Bundle {
  val pc = UInt(nAddrBit.W)
  val instr = UInt(nInstrBit.W)

  override def cloneType = (new FetchBus(nAddrBit, nInstrBit)).asInstanceOf[this.type]
}

// ******************************
//        BACK-END PACKET
// ******************************
class BackPortBus(nHart : Int, nAddrBit: Int, nInstrBit: Int) extends FetchBus(nAddrBit, nInstrBit) {
  val hart = UInt(log2Ceil(nHart).W)

  override def cloneType = (new BackPortBus(nHart, nAddrBit, nInstrBit)).asInstanceOf[this.type]
}
