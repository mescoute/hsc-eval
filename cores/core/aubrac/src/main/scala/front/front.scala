package cowlibry.core.aubrac.front

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._
import cowlibry.common.rsrc._
import cowlibry.common.tools._
import cowlibry.core.aubrac.tools._


class Front (p: FrontParams, p_mem: PmbParams) extends Module {
  val io = IO(new Bundle {
    // Resource management bus
    val b_hart = new RsrcBus(p.nHart, p.nDome, 1)

    // Branch change and information buses
    val i_br_boot = if (p.useDome) Some(Input(new BranchBus(p.nAddrBit))) else None
    val o_br_next = Output(new BranchBus(p.nAddrBit))
    val i_br_new = Input(new BranchBus(p.nAddrBit))
    val i_br_info = if (p.useNlp) Some(Input(new BranchInfoBus(p.nAddrBit))) else None

    // Instruction memory bus
    val b_imem = new PmbIO(p_mem)

    // Output data buses
    val i_ready = Input(Vec(p.nBackPort, Bool()))
    val o_valid = Output(Vec(p.nBackPort, Bool()))
    val o_fetch = Output(Vec(p.nBackPort, new FetchBus(p.nAddrBit, p.nInstrBit)))
  })

  val pc = Module(new PcStage(p))
  val if0 = Module(new If0Stage(p, p_mem))
  val if1 = Module(new If1Stage(p))
  val if2 = Module(new If2Stage(p, p_mem))

  // ******************************
  //        PROGRAM COUNTER
  // ******************************
  pc.io.b_hart <> io.b_hart
  pc.io.i_lock := if0.io.o_lock
  if (p.useDome) pc.io.i_br_boot.get := io.i_br_boot.get
  pc.io.i_br_new := io.i_br_new
  if(p.useNlp) pc.io.i_br_info.get := io.i_br_info.get

  // ******************************
  //      INSTRUCTION FETCH 0
  // ******************************
  if0.io.b_hart <> io.b_hart
  if0.io.i_lock := if1.io.o_lock
  if0.io.i_flush := io.i_br_new.valid
  if0.io.i_valid := pc.io.o_valid
  if (p.useIMemSeq) if0.io.i_abort.get := pc.io.o_abort.get
  if0.io.i_pc := pc.io.o_pc
  if0.io.b_imem <> io.b_imem.req

  // ******************************
  //      INSTRUCTION FETCH 1
  // ******************************
  if1.io.b_hart <> io.b_hart
  if1.io.i_lock := if2.io.o_lock
  if1.io.i_flush := io.i_br_new.valid
  if1.io.i_valid := if0.io.o_valid
  if1.io.i_abort := if0.io.o_abort
  if1.io.i_pc := if0.io.o_pc

  // ******************************
  //      INSTRUCTION FETCH 2
  // ******************************
  if2.io.b_hart <> io.b_hart
  if2.io.i_flush := io.i_br_new.valid
  if2.io.i_valid := if1.io.o_valid
  if2.io.i_abort := if1.io.o_abort
  if2.io.i_pc := if1.io.o_pc
  if2.io.b_imem <> io.b_imem.ack
  if2.io.i_ready := io.i_ready

  // ******************************
  //           FETCH BUS
  // ******************************
  io.o_br_next.valid := if2.io.o_valid(0)
  io.o_br_next.pc := if2.io.o_fetch(0).pc

  io.o_valid := if2.io.o_valid
  io.o_fetch := if2.io.o_fetch

  // ******************************
  //              FREE
  // ******************************
  io.b_hart.free := pc.io.b_hart.free & if0.io.b_hart.free & if1.io.b_hart.free & if2.io.b_hart.free
}

object Front extends App {
  chisel3.Driver.execute(args, () => new Front(FrontDefault, PmbCfg0))
}
