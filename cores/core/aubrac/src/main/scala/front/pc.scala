package cowlibry.core.aubrac.front

import chisel3._
import chisel3.util._

import cowlibry.common.tools.MATH
import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._
import cowlibry.core.aubrac.front.nlp._


class PcStage(p: FrontParams) extends Module {
  require(MATH.islog2(p.nInstrByte), "Instruction must have a 2^n size.")

  val io = IO(new Bundle {
    // Resource state bus
    val b_hart = new RsrcBus(1, p.nDome, 1)

    // Stage management buses
    val i_lock = Input(Bool())

    // Branch change and information buses
    val i_br_boot = if (p.useDome) Some(Input(new BranchBus(p.nAddrBit))) else None
    val i_br_new = Input(new BranchBus(p.nAddrBit))
    val i_br_info = if (p.useNlp) Some(Input(new BranchInfoBus(p.nAddrBit))) else None

    // Output data buses
    val o_valid = Output(Vec(p.nFetchInstr, Bool()))
    val o_abort = if (p.useIMemSeq) Some(Output(Bool())) else None
    val o_pc = Output(UInt(p.nAddrBit.W))
  })

  val w_lock = Wire(Bool())

  // ******************************
  //           NEXT PC
  // ******************************
  val init_next_pc_boot = Wire(UInt(p.nAddrBit.W))
  val init_next_pc = Wire(UInt(p.nAddrBit.W))

  init_next_pc_boot := (p.pcBoot + p.nFetchInstr * p.nInstrByte).U
  init_next_pc := Cat(init_next_pc_boot(p.nAddrBit - 1, log2Ceil(p.nFetchInstr * p.nInstrByte)), 0.U(log2Ceil(p.nFetchInstr * p.nInstrByte).W))

  val reg_next_pc = RegInit(init_next_pc)
  val w_pc = Wire(UInt(p.nAddrBit.W))
  val w_pc_pack = Wire(UInt(p.nAddrBit.W))
  val w_pc_next = Wire(UInt(p.nAddrBit.W))

  w_pc := reg_next_pc

  when (~w_lock) {
    reg_next_pc := w_pc_next
  }

  when (io.i_br_new.valid) {
    w_pc := io.i_br_new.pc
    if (p.useIMemSeq) {
      when (w_lock) {
        reg_next_pc := io.i_br_new.pc
      }
    }
  }
  if (p.useDome) {
    when (io.i_br_boot.get.valid) {
      w_pc := io.i_br_boot.get.pc
      if (p.useIMemSeq) {
        when (w_lock) {
          reg_next_pc := io.i_br_boot.get.pc
        }
      }
    }
  }

  w_pc_pack := Cat(w_pc(p.nAddrBit - 1,log2Ceil(p.nFetchInstr) + log2Ceil(p.nInstrByte)), 0.U((log2Ceil(p.nFetchInstr) + log2Ceil(p.nInstrByte)).W))

  // ******************************
  //      FETCH PACKET VALIDITY
  // ******************************
  val w_valid_pack = Wire(Vec(p.nFetchInstr, Bool()))

  if (p.nFetchInstr > 1) {
    val w_offset = w_pc(log2Ceil(p.nFetchInstr) + log2Ceil(p.nInstrByte) - 1, log2Ceil(p.nInstrByte))

    for (fi <- 0 until p.nFetchInstr) {
      when (fi.U >= w_offset) {
        if (p.useDome) w_valid_pack(fi) := io.i_br_boot.get.valid | ~io.b_hart.flush else w_valid_pack(fi) := ~io.b_hart.flush
      }.otherwise {
        w_valid_pack(fi) := false.B
      }
    }
  } else {
    if (p.useDome) w_valid_pack(0) := io.i_br_boot.get.valid | ~io.b_hart.flush else w_valid_pack(0) := ~io.b_hart.flush
  }

  // ******************************
  //     NEXT LINE PREDICTION
  // ******************************
  val w_nlp_free = Wire(Bool())
  val w_valid = Wire(Vec(p.nFetchInstr, Bool()))

  w_pc_next := w_pc_pack + (p.nFetchInstr * p.nInstrByte).U

  val nlp = if (p.useNlp) Some(Module(new Nlp(p))) else None

  if (p.useNlp) {
    nlp.get.io.b_rsrc <> io.b_hart
    nlp.get.io.b_read.pc := w_pc_pack
    nlp.get.io.b_info := io.i_br_info.get

    w_valid(0) := w_valid_pack(0)
    when (w_valid(0) & nlp.get.io.b_read.br_new(0).valid) {
      w_pc_next := nlp.get.io.b_read.br_new(0).pc
    }
    for (fi <- 1 until p.nFetchInstr) {
      w_valid(fi) := w_valid_pack(fi) & (~w_valid(fi - 1) | ~nlp.get.io.b_read.br_new(fi - 1).valid)
      when (w_valid(fi) & nlp.get.io.b_read.br_new(fi).valid) {
        w_pc_next := nlp.get.io.b_read.br_new(fi).pc
      }
    }

    w_nlp_free := nlp.get.io.b_rsrc.free
  } else {
    for (fi <- 0 until p.nFetchInstr) {
      w_valid(fi) := w_valid_pack(fi)
    }

    w_nlp_free := true.B
  }

  // ******************************
  //            OUTPUTS
  // ******************************
  // ------------------------------
  //           REGISTERS
  // ------------------------------
  val init_pc_boot = Wire(UInt(p.nAddrBit.W))
  val init_valid = Wire(Vec(p.nFetchInstr, Bool()))
  val init_pc = Wire(UInt(p.nAddrBit.W))

  init_pc_boot := p.pcBoot.U

  if (p.nFetchInstr > 1) {
    val w_offset = init_pc_boot(log2Ceil(p.nFetchInstr * p.nInstrByte) - 1, log2Ceil(p.nInstrByte))

    for (fi <- 0 until p.nFetchInstr) {
      init_valid(fi) := (fi.U >= w_offset)
    }
  } else {
    init_valid(0) := true.B
  }

  init_pc := Cat(init_pc_boot(p.nAddrBit - 1, log2Ceil(p.nFetchInstr * p.nInstrByte)), 0.U(log2Ceil(p.nFetchInstr * p.nInstrByte).W))

  val reg_valid = RegInit(init_valid)
  val reg_abort = RegInit(false.B)
  val reg_pc = RegInit(init_pc)


  val w_flush = Wire(Bool())
  if (p.useDome) w_flush := (~io.i_br_boot.get.valid & io.b_hart.flush) | io.i_br_new.valid else w_flush := io.b_hart.flush | io.i_br_new.valid

  w_lock := (reg_valid.asUInt.orR & io.i_lock)

  when (~w_lock) {
    reg_valid := w_valid
    reg_pc := w_pc_pack
  }

  if (p.useIMemSeq) {
    when (io.i_lock & w_flush & io.b_hart.valid) {
      reg_abort := true.B
    }.elsewhen (~io.i_lock) {
      reg_abort := false.B
    }
  }

  // ------------------------------
  //              I/O
  // ------------------------------
  io.o_valid := reg_valid
  if (p.useIMemSeq) io.o_abort.get := reg_abort
  io.o_pc := reg_pc

  // ------------------------------
  //             FREE
  // ------------------------------
  io.b_hart.free := w_nlp_free & ~reg_valid.asUInt.orR
}

object PcStage extends App {
  chisel3.Driver.execute(args, () => new PcStage(FrontDefault))
}
