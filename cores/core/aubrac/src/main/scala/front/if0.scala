package cowlibry.core.aubrac.front

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._
import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._


class If0Stage(p: FrontParams, p_mem: PmbParams) extends Module {
  val io = IO(new Bundle {
    // Resource management bus
    val b_hart = new RsrcBus(p.nHart, p.nDome, 1)

    // Stage management buses
    val i_lock = Input(Bool())
    val i_flush = Input(Bool())
    val o_lock = Output(Bool())

    // Input data buses
    val i_valid = Input(Vec(p.nFetchInstr, Bool()))
    val i_abort = if (p.useIMemSeq) Some(Input(Bool())) else None
    val i_pc = Input(UInt(p.nAddrBit.W))

    // Instruction memory request bus
    val b_imem = new PmbReqIO(p_mem)

    // Output data buses
    val o_valid = Output(Vec(p.nFetchInstr, Bool()))
    val o_abort = Output(Bool())
    val o_pc = Output(UInt(p.nAddrBit.W))
  })

  val w_rsrc_valid = io.b_hart.valid & ~io.b_hart.flush
  val w_lock = Wire(Bool())

  // ******************************
  //      INSTRUCTION VALIDITY
  // ******************************
  val reg_done = RegInit(false.B)

  if (p.useIMemSeq) {
    when (~reg_done) {
      reg_done := w_rsrc_valid & io.i_valid.asUInt.orR & ~io.i_abort.get & io.b_imem.ready(0) & w_lock
    }.otherwise {
      reg_done := w_lock
    }
  }

  // ******************************
  //   INSTRUCTION MEMORY REQUEST
  // ******************************
  if (p.useIMemSeq) io.b_imem.valid := w_rsrc_valid & io.i_valid.asUInt.orR & ~io.i_abort.get & ~reg_done else io.b_imem.valid := w_rsrc_valid & io.i_valid.asUInt.orR & ~io.i_flush & ~w_lock
  io.b_imem.hart := io.b_hart.hart
  io.b_imem.dome := io.b_hart.dome
  io.b_imem.rw := false.B
  io.b_imem.size := SIZE.max(p.nFetchInstr * p.nInstrBit).U
  io.b_imem.addr := io.i_pc
  io.b_imem.wdata := 0.U

  // ******************************
  //         CONNECT OUTPUT
  // ******************************
  // ------------------------------
  //          REQUEST BUS
  // ------------------------------
  val reg_valid = RegInit(VecInit(Seq.fill(p.nFetchInstr)(false.B)))
  val reg_pc = Reg(UInt(p.nAddrBit.W))
  val reg_abort = RegInit(false.B)

  w_lock := io.i_lock & reg_valid.asUInt.orR

  when (io.b_hart.valid & ~w_lock) {
    for (i <- 0 until p.nFetchInstr) {
      if (p.useIMemSeq) reg_valid(i) := ~io.b_hart.flush & io.i_valid(i) & io.b_imem.ready(0) else reg_valid(i) := ~io.b_hart.flush & io.i_valid(i) & io.b_imem.ready(0) & ~io.i_flush
    }
    reg_pc := io.i_pc
  }

  when (w_lock & io.i_flush) {
    reg_abort := true.B
  }.elsewhen (~io.i_lock) {
    if (p.useIMemSeq) reg_abort := io.i_abort.get | (io.i_flush & io.b_imem.ready(0)) else reg_abort := io.i_flush & io.b_imem.ready(0)
  }

  io.o_valid := reg_valid
  io.o_pc := reg_pc
  io.o_abort := reg_abort

  // ------------------------------
  //             LOCK
  // ------------------------------
  if (p.useIMemSeq) {
    io.o_lock := (~reg_done & ~io.b_imem.ready(0) & io.i_valid.asUInt.orR & ~io.i_flush) | w_lock
  } else {
    io.o_lock := ~io.i_flush & ((io.i_valid.asUInt.orR & ~io.b_imem.ready(0)) | w_lock)
  }

  // ------------------------------
  //             FREE
  // ------------------------------
  io.b_hart.free := ~reg_valid.asUInt.orR
}

object If0Stage extends App {
  chisel3.Driver.execute(args, () => new If0Stage(FrontDefault, PmbCfg0))
}
