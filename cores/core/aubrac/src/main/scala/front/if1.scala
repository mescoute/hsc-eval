package cowlibry.core.aubrac.front

import chisel3._
import chisel3.util._

import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._


class If1Stage(p: FrontParams) extends Module {
  val io = IO(new Bundle {
    // Resource management bus
    val b_hart = new RsrcBus(1, p.nDome, 1)

    // Stage management buses
    val i_lock = Input(Bool())
    val i_flush = Input(Bool())
    val o_lock = Output(Bool())

    // Input data buses
    val i_valid = Input(Vec(p.nFetchInstr, Bool()))
    val i_abort = Input(Bool())
    val i_pc = Input(UInt(p.nAddrBit.W))

    // Output data buses
    val o_valid = Output(Vec(p.nFetchInstr, Bool()))
    val o_abort = Output(Bool())
    val o_pc = Output(UInt(p.nAddrBit.W))
  })

  // ******************************
  //          REQUEST BUS
  // ******************************
  val reg_valid = RegInit(VecInit(Seq.fill(p.nFetchInstr)(false.B)))
  val reg_pc = Reg(UInt(p.nAddrBit.W))
  val reg_abort = RegInit(false.B)

  when (io.b_hart.valid & ~(reg_valid.asUInt.orR & io.i_lock)) {
    reg_valid := io.i_valid
    reg_pc := io.i_pc
  }

  when (io.i_lock & reg_valid.asUInt.orR & io.i_flush) {
    reg_abort := true.B
  }.elsewhen (~io.i_lock) {
    reg_abort := io.i_abort | ((io.b_hart.flush | io.i_flush) & io.i_valid.asUInt.orR)
  }

  if (p.useIf1Stage) {
    io.o_valid := reg_valid
    io.o_pc := reg_pc
    io.o_abort := reg_abort

    io.o_lock := reg_valid.asUInt.orR & io.i_lock

    io.b_hart.free := ~reg_valid.asUInt.orR & ~reg_abort
  } else {
    io.o_valid := io.i_valid
    io.o_pc := io.i_pc
    io.o_abort := io.i_abort

    io.o_lock := io.i_lock

    io.b_hart.free := true.B
  }
}

object If1Stage extends App {
  chisel3.Driver.execute(args, () => new If1Stage(FrontDefault))
}
