package cowlibry.core.aubrac.front

import chisel3._
import chisel3.util._

import cowlibry.common.tools._
import cowlibry.common.mem.pmb._
import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._


class If2Stage(p: FrontParams, p_mem: PmbParams) extends Module {
  val io = IO(new Bundle {
    // Resource management bus
    val b_hart = new RsrcBus(1, p.nDome, 1)

    // Stage management buses
    val i_flush = Input(Bool())
    val o_lock = Output(Bool())

    // Input data buses
    val i_valid = Input(Vec(p.nFetchInstr, Bool()))
    val i_abort = Input(Bool())
    val i_pc = Input(UInt(p.nAddrBit.W))

    // Instruction memory acknowledgement bus
    val b_imem = new PmbAckIO(p_mem)

    // Output data buses
    val i_ready = Input(Vec(p.nBackPort, Bool()))
    val o_valid = Output(Vec(p.nBackPort, Bool()))
    val o_fetch = Output(Vec(p.nBackPort, new FetchBus(p.nAddrBit, p.nInstrBit)))
  })

  val w_lock = Wire(Bool())
  val w_rsrc_valid = io.b_hart.valid & ~io.b_hart.flush

  // ******************************
  //     INSTRUCTION MEMORY ACK
  // ******************************
  io.b_imem.ready(0) := io.i_valid.asUInt.orR & (~w_lock | io.i_flush | io.i_abort)

  // ******************************
  //           FETCH BUS
  // ******************************
  // ------------------------------
  //             FIFO
  // ------------------------------
  if (p.nFetchFifoDepth > 1 || p.nFetchInstr > 1 || p.nBackPort > 1) {
    val reg_done = RegInit(VecInit(Seq.fill(p.nFetchInstr)(false.B)))
    val reg_fetch = Module(new Fifo(2, new FetchBus(p.nAddrBit, p.nInstrBit), p.nFetchFifoDepth, p.nFetchInstr, p.nBackPort))

    for (i <- 0 until p.nFetchInstr) {
      when (reg_fetch.io.o_full(p.nFetchInstr - 1) & ~reg_fetch.io.o_full(i) & io.i_valid(i) & ~io.i_flush & ~io.i_abort) {
        reg_done(i) := true.B
      }.elsewhen (reg_fetch.io.o_full(p.nFetchInstr - 1)) {
        reg_done(i) := reg_done(i)
      }.otherwise {
        reg_done(i) := false.B
      }
    }

    reg_fetch.io.i_flush := io.b_hart.flush | io.i_flush
    for (i <- 0 until p.nFetchInstr) {
      reg_fetch.io.i_wen(i) := w_rsrc_valid & io.i_valid(i) & ~io.i_abort & ~io.i_flush & io.b_imem.valid & ~reg_done(i)
      reg_fetch.io.i_data(i).pc := io.i_pc + (i * p.nInstrByte).U
      reg_fetch.io.i_data(i).instr := io.b_imem.rdata(p.nInstrBit * (i + 1) - 1, p.nInstrBit * i)
    }

    for (bp <- 0 until p.nBackPort) {
      reg_fetch.io.i_ren(bp) := io.i_ready(bp)
      io.o_valid(bp) := ~reg_fetch.io.o_empty(bp)
      io.o_fetch(bp) := reg_fetch.io.o_data(bp)
    }

    w_lock := reg_fetch.io.o_full(p.nFetchInstr - 1)
    io.b_hart.free := reg_fetch.io.o_empty(0)

  // ------------------------------
  //           REGISTERS
  // ------------------------------
  } else {
    val reg_valid = RegInit(false.B)
    val reg_fetch = Reg(new FetchBus(p.nAddrBit, p.nInstrBit))

    when (io.b_hart.valid & ~(reg_valid & ~io.i_ready(0))) {
      reg_valid := io.i_valid(0) & io.b_imem.valid & ~io.b_hart.flush & ~io.i_flush & ~io.i_abort
      reg_fetch.pc := io.i_pc
      reg_fetch.instr := io.b_imem.rdata
    }

    io.o_valid(0) := reg_valid
    io.o_fetch(0) := reg_fetch

    w_lock := reg_valid & ~io.i_ready(0)
    io.b_hart.free := ~reg_valid
  }

  io.o_lock := (~io.b_imem.valid | (w_lock & ~io.i_flush)) & io.i_valid.asUInt.orR
}

object If2Stage extends App {
  chisel3.Driver.execute(args, () => new If2Stage(FrontDefault, PmbCfg0))
}
