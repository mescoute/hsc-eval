package cowlibry.core.aubrac.front

import chisel3._
import chisel3.util._

import cowlibry.core.aubrac.front.nlp._


trait FrontParams extends NlpParams{
  def pcBoot: Int
  def nHart: Int
  def useDome: Boolean
  def nDome: Int

  def nAddrBit: Int
  def nInstrByte: Int
  def nInstrBit: Int = nInstrByte * 8
  def nFetchInstr: Int

  def useIMemSeq: Boolean
  def useIf1Stage: Boolean
  def nFetchFifoDepth: Int
  def useNlp: Boolean
  def nBhtSet: Int
  def nBhtSetEntry: Int
  def nBhtBit: Int
  def nBtbLine: Int
  def nBtbTagBit: Int

  def nBackPort: Int
}
