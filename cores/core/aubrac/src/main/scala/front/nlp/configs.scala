package cowlibry.core.aubrac.front.nlp

import chisel3._
import chisel3.util._


object HTableDefault extends HTableParams {
  def nReadPort: Int = 2
  def nLine: Int = 8
  def nTagBit: Int = 10
  def nDome: Int = 2
}

object NlpDefault extends NlpParams {
  def nDome: Int = 2

  def nAddrBit: Int = 32
  def nInstrByte: Int = 4
  def nFetchInstr: Int = 2

  def nBhtSet: Int = 2
  def nBhtSetEntry: Int = 1
  def nBhtBit: Int = 2
  def nBtbLine: Int = 32
  def nBtbTagBit: Int = 10
}
