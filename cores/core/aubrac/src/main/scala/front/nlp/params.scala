package cowlibry.core.aubrac.front.nlp

import chisel3._
import chisel3.util._

trait HTableParams {
  def nReadPort: Int
  def nLine: Int
  def nTagBit: Int
  def nDome: Int
}

class HTableIntf (
  _nReadPort: Int,
  _nLine: Int,
  _nTagBit: Int,
  _nDome: Int
) extends HTableParams {
  def nDome: Int = _nDome
  def nReadPort: Int = _nReadPort
  def nLine: Int = _nLine
  def nTagBit: Int = _nTagBit
}

trait NlpParams {
  def nDome: Int

  def nAddrBit: Int
  def nInstrByte: Int
  def nFetchInstr: Int

  def nBhtSet: Int
  def nBhtSetEntry: Int
  def nBhtEntry: Int = nBhtSet * nBhtSetEntry
  def nBhtBit: Int
  def nBtbLine: Int
  def nBtbTagBit: Int
}
