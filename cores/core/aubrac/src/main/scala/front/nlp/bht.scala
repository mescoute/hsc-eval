package cowlibry.core.aubrac.front.nlp

import chisel3._
import chisel3.util._

import cowlibry.common.rsrc._


class Bht(p: NlpParams) extends Module {
  val io = IO(new Bundle {
    val b_rsrc = new RsrcBus(1, p.nDome, 1)

    val b_read = new BhtReadBus(p)
    val b_write = new BhtWriteBus(p)
  })

  val reg_valid = RegInit(VecInit(Seq.fill(p.nBhtSet)(false.B)))
  val reg_counter = Mem(p.nBhtSet, Vec(p.nBhtSetEntry, UInt(p.nBhtBit.W)))

  // ******************************
  //              READ
  // ******************************
  for (fi <- 0 until p.nFetchInstr) {
    val w_raddr = Wire(UInt(log2Ceil(p.nBhtEntry).W))
    w_raddr := io.b_read.slct + fi.U

    val w_raddr_set = Wire(UInt(log2Ceil(p.nBhtSet).W))
    val w_raddr_entry = Wire(UInt(log2Ceil(p.nBhtSetEntry).W))

    if (p.nBhtSet > 1) w_raddr_set := w_raddr(log2Ceil(p.nBhtEntry) - 1, log2Ceil(p.nBhtSetEntry)) else w_raddr_set := 0.U
    if (p.nBhtSetEntry > 1) w_raddr_entry := w_raddr(log2Ceil(p.nBhtSetEntry) - 1, 0) else w_raddr_entry := 0.U

    io.b_read.taken(fi) := Mux(reg_valid(w_raddr_set), reg_counter(w_raddr_set)(w_raddr_entry), 0.U)
  }

  // ******************************
  //             WRITE
  // ******************************
  //val w_waddr_set = io.b_write.slct(log2Ceil(p.nBhtEntry) - 1, log2Ceil(p.nBhtSetEntry))
  //val w_waddr_entry = io.b_write.slct(log2Ceil(p.nBhtSetEntry) - 1, 0)

  val w_waddr_set = Wire(UInt(log2Ceil(p.nBhtSet).W))
  val w_waddr_entry = Wire(UInt(log2Ceil(p.nBhtSetEntry).W))

  if (p.nBhtSet > 1) w_waddr_set := io.b_write.slct(log2Ceil(p.nBhtEntry) - 1, log2Ceil(p.nBhtSetEntry)) else w_waddr_set := 0.U
  if (p.nBhtSetEntry > 1) w_waddr_entry := io.b_write.slct(log2Ceil(p.nBhtSetEntry) - 1, 0) else w_waddr_entry := 0.U

  val w_old_counter = Wire(Vec(p.nBhtSetEntry, UInt(p.nBhtBit.W)))

  when (reg_valid(w_waddr_set)) {
    w_old_counter := reg_counter(w_waddr_set)
  }.otherwise {
    for (se <- 0 until p.nBhtSetEntry) {
      w_old_counter(se) := 0.U
    }
  }

  val w_new_counter = Wire(Vec(p.nBhtSetEntry, UInt(p.nBhtBit.W)))
  w_new_counter := w_old_counter

  when (io.b_write.taken & ~w_old_counter(w_waddr_entry).andR) {
    w_new_counter(w_waddr_entry) := w_old_counter(w_waddr_entry) + 1.U
  }.elsewhen (~io.b_write.taken & w_old_counter(w_waddr_entry).orR) {
    w_new_counter(w_waddr_entry) := w_old_counter(w_waddr_entry) - 1.U
  }

  when (io.b_rsrc.flush) {
    for (s <- 0 until p.nBhtSet) {
      reg_valid(s) := 0.U
    }
  }.elsewhen (io.b_write.valid) {
    reg_valid(w_waddr_set) := true.B
    reg_counter(w_waddr_set) := w_new_counter
  }

  // ******************************
  //             FREE
  // ******************************
  io.b_rsrc.free := ~reg_valid.asUInt.orR
}

object Bht extends App {
  chisel3.Driver.execute(args, () => new Bht(NlpDefault))
}
