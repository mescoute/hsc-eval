package cowlibry.core.aubrac.front.nlp

import chisel3._
import chisel3.util._
import scala.math._

import cowlibry.common.rsrc._
import cowlibry.common.mem.replace._


class HTable[T <: Data](gen: T, p: HTableParams) extends Module {
  val io = IO(new Bundle {
    val b_rsrc = new RsrcBus(1, p.nDome, 1)

    val b_read = Vec(p.nReadPort, new HTableReadBus(gen, p))
    val b_write = new HTableWriteBus(gen, p)
  })

  val reg_valid = RegInit(VecInit(Seq.fill(p.nLine)(false.B)))
  val reg_tag = Reg(Vec(p.nLine, UInt(p.nTagBit.W)))
  val reg_data = Mem(p.nLine, gen)

  // ******************************
  //             READ
  // ******************************
  val w_read_here = Wire(Vec(p.nReadPort, Bool()))
  val w_read_line = Wire(Vec(p.nReadPort, UInt(log2Ceil(p.nLine).W)))

  for (r <- 0 until p.nReadPort) {
    w_read_here(r) := false.B
    w_read_line(r) := 0.U
    io.b_read(r).data := reg_data(0)

    for (l <- 0 until p.nLine) {
      when (reg_valid(l) & (io.b_read(r).tag === reg_tag(l))) {
        w_read_here(r) := true.B
        w_read_line(r) := l.U
        io.b_read(r).data := reg_data(l)
      }
    }

    io.b_read(r).ready := w_read_here(r)
  }

  // ******************************
  //             WRITE
  // ******************************
  // ------------------------------
  //       CHECK DATA PRESENCE
  // ------------------------------
  val w_write_here = Wire(Bool())
  val w_write_line = Wire(UInt(log2Ceil(p.nLine).W))

  w_write_here := false.B
  w_write_line := 0.U

  for (l <- 0 until p.nLine) {
    when (reg_valid(l) & (io.b_write.tag === reg_tag(l))) {
      w_write_here := true.B
      w_write_line := l.U
    }
  }

  // ------------------------------
  //         REPLACE POLICY
  // ------------------------------
  val w_rep_av = Wire(Bool())
  val w_rep_line = Wire(UInt(log2Ceil(p.nLine).W))
  val w_pol_free = Wire(Vec(p.nLine, Bool()))

  val plru = Module(new BitPlruPolicy(p.nReadPort, p.nLine))

  for (l <- 0 until p.nLine) {
    plru.io.i_flush(l) := io.b_rsrc.flush
  }
  for (r <- 0 until p.nReadPort) {
    plru.io.i_valid(r) := io.b_read(r).valid & w_read_here(r)
    plru.io.i_line(r) := w_read_line(r)
  }
  plru.io.i_rep := io.b_write.valid & ~w_write_here
  for (l <- 0 until p.nLine) {
    plru.io.i_use_line(l) := true.B
  }

  for (l <- 0 until p.nLine) {
    w_pol_free(l) := plru.io.o_free(l)
  }

  w_rep_av := plru.io.o_rep_av
  w_rep_line := plru.io.o_rep_line

  // ------------------------------
  //            OUTPUTS
  // ------------------------------
  io.b_write.ready := w_write_here | w_rep_av

  // ******************************
  //         REGISTER UPDATE
  // ******************************
  for (l <- 0 until p.nLine) {
    when (io.b_rsrc.flush) {
      reg_valid(l) := false.B
    }.elsewhen (io.b_write.valid & w_write_here) {
      when (l.U === w_write_line) {
        reg_data(l) := io.b_write.data
      }
    }.elsewhen (io.b_write.valid & w_rep_av) {
      when (l.U === w_rep_line) {
        reg_valid(l) := true.B
        reg_tag(l) := io.b_write.tag
        reg_data(l) := io.b_write.data
      }
    }
  }

  // ******************************
  //             FREE
  // ******************************
  io.b_rsrc.free := ~reg_valid.asUInt.orR & w_pol_free.asUInt.orR

}

object HTable extends App {
  chisel3.Driver.execute(args, () => new HTable(new BtbBus(NlpDefault), HTableDefault))
}
