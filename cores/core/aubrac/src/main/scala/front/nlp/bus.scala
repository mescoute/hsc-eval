package cowlibry.core.aubrac.front.nlp

import chisel3._
import chisel3.util._

import cowlibry.core.aubrac.tools._


// ******************************
//         COUNTER TABLE
// ******************************
class BhtReadBus(p: NlpParams) extends Bundle {
  val slct = Input(UInt(log2Ceil(p.nBhtEntry).W))
  val taken = Output(Vec(p.nFetchInstr, Bool()))
}

class BhtWriteBus(p: NlpParams) extends Bundle {
  val valid = Input(Bool())
  val slct = Input(UInt(log2Ceil(p.nBhtEntry).W))
  val taken = Input(Bool())
}

// ******************************
//           HASH TABLE
// ******************************
class HTableReadBus[T <: Data](gen: T, p: HTableParams) extends Bundle {
  val valid = Input(Bool())
  val tag = Input(UInt(p.nTagBit.W))
  val ready = Output(Bool())
  val data = Output(gen)

  override def cloneType = (new HTableReadBus(gen, p)).asInstanceOf[this.type]
}

class HTableWriteBus[T <: Data](gen: T, p: HTableParams) extends Bundle {
  val valid = Input(Bool())
  val tag = Input(UInt(p.nTagBit.W))
  val data = Input(gen)
  val ready = Output(Bool())

  override def cloneType = (new HTableWriteBus(gen, p)).asInstanceOf[this.type]
}

// ******************************
//             NLP
// ******************************
class BtbBus(p: NlpParams) extends Bundle {
  val jmp = Bool()
  val target = UInt((p.nAddrBit - log2Floor(p.nInstrByte)).W)

  override def cloneType = (new BtbBus(p)).asInstanceOf[this.type]
}

class NlpReadBus(p: NlpParams) extends Bundle {
  val pc = Input(UInt(p.nAddrBit.W))
  val br_new = Output(Vec(p.nFetchInstr, new BranchBus(p.nAddrBit)))

  override def cloneType = (new NlpReadBus(p)).asInstanceOf[this.type]
}
