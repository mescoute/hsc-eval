package cowlibry.core.aubrac.front.nlp

import chisel3._
import chisel3.util._
import scala.math._

import cowlibry.common.tools._
import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._


class Nlp (p: NlpParams) extends Module {
  require (p.nBhtEntry > 1, "NLP must have more than one BHT entry.")

  val p_btb = new HTableIntf(p.nFetchInstr, p.nBtbLine, p.nBtbTagBit, p.nDome)

  val io = IO(new Bundle {
    val b_rsrc = new RsrcBus(1, p.nDome, 1)

    val b_read = new NlpReadBus(p)
    val b_info = Input(new BranchInfoBus(p.nAddrBit))
  })

  // ******************************
  //      BRANCH TARGET BUFFER
  // ******************************
  val btb = Module(new HTable(new BtbBus(p), p_btb))
  btb.io.b_rsrc <> io.b_rsrc
  for (fi <- 0 until p.nFetchInstr) {
    btb.io.b_read(fi).valid := true.B
    btb.io.b_read(fi).tag := io.b_read.pc(p.nBtbTagBit + log2Floor(p.nInstrByte) - 1, log2Floor(p.nInstrByte)) + fi.U
  }
  btb.io.b_write.valid := io.b_info.valid
  btb.io.b_write.tag := io.b_info.pc(p.nBtbTagBit + log2Floor(p.nInstrByte) - 1, log2Floor(p.nInstrByte))
  btb.io.b_write.data.jmp := io.b_info.jmp
  btb.io.b_write.data.target := io.b_info.target(p.nAddrBit - 1, log2Floor(p.nInstrByte))

  // ******************************
  //        COUNTER TABLE
  // ******************************
  val bht = Module(new Bht(p))
  bht.io.b_rsrc <> io.b_rsrc
  //bht.io.b_read.slct := io.b_read.pc(log2Ceil(p.nBhtEntry) + 1, log2Floor(p.nInstrByte))
  bht.io.b_read.slct := io.b_read.pc(log2Floor(p.nInstrByte) + log2Ceil(p.nBhtEntry) - 1, log2Floor(p.nInstrByte))
  bht.io.b_write.valid := io.b_info.valid & io.b_info.br
  //bht.io.b_write.slct := w_info.pc(log2Ceil(p.nBhtEntry) + 1, log2Floor(p.nInstrByte))
  bht.io.b_write.slct := io.b_info.pc(log2Floor(p.nInstrByte) + log2Ceil(p.nBhtEntry) - 1, log2Floor(p.nInstrByte))
  bht.io.b_write.taken := io.b_info.taken

  // ******************************
  //            RESULT
  // ******************************
  for (fi <- 0 until p.nFetchInstr) {
    io.b_read.br_new(fi).valid := btb.io.b_read(fi).ready & (btb.io.b_read(fi).data.jmp | bht.io.b_read.taken(fi))
    io.b_read.br_new(fi).pc := Cat(btb.io.b_read(fi).data.target, 0.U(log2Floor(p.nInstrByte).W))
  }

  // ******************************
  //             FREE
  // ******************************
  io.b_rsrc.free := btb.io.b_rsrc.free & bht.io.b_rsrc.free
}


object Nlp extends App {
  chisel3.Driver.execute(args, () => new Nlp(NlpDefault))
}
