package cowlibry.core.aubrac.front

import chisel3._
import chisel3.util._


object FrontDefault extends FrontParams {
  def pcBoot: Int = 0x40
  def nHart: Int = 1
  def useDome: Boolean = false
  def nDome: Int = 1

  def nAddrBit: Int = 32
  def nInstrByte: Int = 4
  def nFetchInstr: Int = 1

  def useIMemSeq: Boolean = true
  def useIf1Stage: Boolean = false
  def nFetchFifoDepth: Int = 2
  def useNlp: Boolean = true
  def nBhtSet: Int = 8
  def nBhtSetEntry: Int = 128
  def nBhtBit: Int = 2
  def nBtbLine: Int = 16
  def nBtbTagBit: Int = 10

  def nBackPort: Int = 1
}
