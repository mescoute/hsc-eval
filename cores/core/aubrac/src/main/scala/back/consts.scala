package cowlibry.core.aubrac.back

import chisel3._


// ******************************
//            OPERAND
// ******************************
object OP {
  val NBIT = 3
  def X = 0

  def REG = 1
  def IMM = 2
  def PC = 3
  def CSR = 4
}

// ******************************
//            IMMEDIATE
// ******************************
object IMM {
  val NBIT = 3
  def X = 0

  def isU = 1
  def isI = 2
  def isJ = 3
  def isB = 4
  def isS = 5
  def isC = 6
}

// ******************************
//         EXECUTION UNIT
// ******************************
object EXUNIT {
  val NBIT = 3

  val X = 0
  val ALU = 1
  val BRU = 2
  val MULDIV = 3
  val RMU = 4
  val MAMU = 5
}

// ******************************
//        EXECUTION MICRO-OP
// ******************************
object EXUOP {
  val NBIT = 4
  val X = 0

  val JAL = 2
  val JALR = 3
  val BEQ = 8
  val BNE = 9
  val BLT = 10
  val BGE = 11
  val BLTU = 12
  val BGEU = 13

  val ADD = 1
  val SUB = 2
  val SLT = 3
  val SLTU = 4
  val OR = 5
  val AND = 6
  val XOR = 7
  val SRA = 8
  val SRL = 9
  val SLL = 10

  val MUL = 1
  val MULH = 2
  val MULHSU = 3
  val MULHU = 4
  val DIV = 8
  val DIVU = 9
  val REM = 10
  val REMU = 11

  val FENCEI = 1

  val START = 1
  val END = 2
  val SWITCH = 3
}

// ******************************
//          MEMORY SIZE
// ******************************
object MEM {
  val NBIT = 3
  val X = 0

  val B = 1
  val H = 2
  val W = 3
  val BU = 5
  val HU = 6
}

// ******************************
//          CSR MICRO-OP
// ******************************
object CSRUOP {
  val NBIT = 2
  val X = 0

  val W = 1
  val S = 2
  val C = 3
}
