package cowlibry.core.aubrac.back

import chisel3._

object GprDefault extends GprParams{
  def debug: Boolean = false
  def nHart: Int = 1

  def nGprRead: Int = 2
  def nGprWrite: Int = 1
}

object BackDefault extends BackParams{
  def debug: Boolean = false
  def pcBoot: Int = 0x40
  def useDome: Boolean = true

  def useMulDiv: Boolean = true
  def useFencei: Boolean = true
  def useMemStage: Boolean = true
  def useBranchReg: Boolean = true
}
