package cowlibry.core.aubrac.back

import chisel3._
import chisel3.util._


// ******************************
//           DATA BUS
// ******************************
class DataSlctBus extends Bundle {
  val rs1 = UInt(5.W)
  val rs2 = UInt(5.W)
  val s1type = UInt(OP.NBIT.W)
  val s2type = UInt(OP.NBIT.W)
  val s3type = UInt(OP.NBIT.W)
  val immtype = UInt(IMM.NBIT.W)
  val csr = UInt(12.W)
}

class DataBus extends Bundle {
  val s1 = UInt(32.W)
  val s2 = UInt(32.W)
  val s3 = UInt(32.W)
}

class ResultBus extends Bundle {
  val s1 = UInt(32.W)
  val s3 = UInt(32.W)
  val res = UInt(32.W)
}

class BypassBus(nHart: Int) extends Bundle {
  val hart = UInt(log2Ceil(nHart).W)
  val en = Bool()
  val ready = Bool()
  val addr = UInt(5.W)
  val data = UInt(32.W)

  override def cloneType = (new BypassBus(nHart)).asInstanceOf[this.type]
}

// ******************************
//       INFORMATIONS BUS
// ******************************
class InfoBus(nHart: Int) extends Bundle {
  val hart = UInt(log2Ceil(nHart).W)
  val pc = UInt(32.W)
  val instr = UInt(32.W)
  val end = Bool()
  val ser = Bool()
  val empty = Bool()

  override def cloneType = (new InfoBus(nHart)).asInstanceOf[this.type]
}

// ******************************
//          UNITS BUS
// ******************************
class ExCtrlBus(nBackPort: Int) extends Bundle {
  val unit = UInt(EXUNIT.NBIT.W)
  val port = UInt(log2Ceil(nBackPort).W)
  val uop = UInt(EXUOP.NBIT.W)

  override def cloneType = (new ExCtrlBus(nBackPort)).asInstanceOf[this.type]
}

class MemCtrlBus extends Bundle {
  val load = Bool()
  val store = Bool()
  val size = UInt(MEM.NBIT.W)
}

class CsrCtrlBus extends Bundle {
  val read = Bool()
  val write = Bool()
  val uop = UInt(CSRUOP.NBIT.W)
}

class WbCtrlBus extends Bundle {
  val en = Bool()
  val addr = UInt(5.W)
  val rmu = Bool()
}

// ******************************
//          STAGE BUS
// ******************************
class ExStageBus(p: BackParams) extends Bundle {
  val info = new InfoBus(p.nHart)

  val ex = new ExCtrlBus(p.nBackPort)
  val mem = new MemCtrlBus()
  val csr = new CsrCtrlBus()
  val wb = new WbCtrlBus()

  val data = new DataBus()

  override def cloneType = (new ExStageBus(p)).asInstanceOf[this.type]
}

class MemStageBus(p: BackParams) extends Bundle {
  val info = new InfoBus(p.nHart)

  val mem = new MemCtrlBus()
  val csr = new CsrCtrlBus()
  val wb = new WbCtrlBus()

  val data = new ResultBus()

  override def cloneType = (new MemStageBus(p)).asInstanceOf[this.type]
}

class WbStageBus(p: BackParams) extends Bundle {
  val info = new InfoBus(p.nHart)

  val mem = new MemCtrlBus()
  val csr = new CsrCtrlBus()
  val wb = new WbCtrlBus()

  val data = new ResultBus()

  override def cloneType = (new WbStageBus(p)).asInstanceOf[this.type]
}
