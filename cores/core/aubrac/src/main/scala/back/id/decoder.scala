package cowlibry.core.aubrac.back

import chisel3._
import chisel3.util._


class Decoder(p: BackParams) extends Module {
  val io = IO(new Bundle {
    val i_instr = Input(UInt(32.W))

    val o_valid = Output(Bool())
    val o_info = Output(new InfoBus(p.nHart))
    val o_ex = Output(new ExCtrlBus(p.nBackPort))
    val o_mem = Output(new MemCtrlBus())
    val o_csr = Output(new CsrCtrlBus())
    val o_wb = Output(new WbCtrlBus())
    val o_data = Output(new DataSlctBus())
  })

  var table_ex = TABLEEXRV32I.table
  table_ex ++= TABLEEXCSR.table
  if (p.useMulDiv)  table_ex ++= TABLEEXRV32M.table
  if (p.useDome)    table_ex ++= TABLEEXDOME.table
  if (p.useFencei)  table_ex ++= TABLEEXZIFENCEI.table

  var table_mem = TABLEMEM.table
  var table_csr = TABLECSR.table

  var table_wb = TABLEWBRV32I.table
  table_wb ++= TABLEWBCSR.table
  if (p.useMulDiv)  table_wb ++= TABLEWBRV32M.table
  if (p.useDome)    table_wb ++= TABLEWBDOME.table
  if (p.useFencei)  table_wb ++= TABLEWBZIFENCEI.table

  val w_dec_ex = ListLookup(io.i_instr, TABLEEXRV32I.default, table_ex)
  val w_dec_mem = ListLookup(io.i_instr, TABLEMEM.default, table_mem)
  val w_dec_csr = ListLookup(io.i_instr, TABLECSR.default, table_csr)
  val w_dec_wb = ListLookup(io.i_instr, TABLEWBRV32I.default, table_wb)

  // ******************************
  //            INFO BUS
  // ******************************
  io.o_valid := w_dec_ex(0)
  io.o_info.hart := 0.U
  io.o_info.pc := 0.U
  io.o_info.instr := 0.U
  io.o_info.end := w_dec_ex(1)
  io.o_info.ser := w_dec_ex(2)
  io.o_info.empty := w_dec_ex(3)

  // ******************************
  //            EX BUS
  // ******************************
  io.o_ex.unit := w_dec_ex(4)
  io.o_ex.port := 0.U
  io.o_ex.uop := w_dec_ex(5)

  // ******************************
  //            MEM BUS
  // ******************************
  io.o_mem.load := w_dec_mem(0)
  io.o_mem.store := w_dec_mem(1)
  io.o_mem.size := w_dec_mem(2)

  // ******************************
  //            CSR BUS
  // ******************************
  io.o_csr.read := w_dec_csr(0)
  io.o_csr.write := w_dec_csr(1)
  io.o_csr.uop := w_dec_csr(2)

  // ******************************
  //            WB BUS
  // ******************************
  io.o_wb.en := w_dec_wb(0)
  io.o_wb.addr := io.i_instr(11,7)
  io.o_wb.rmu := w_dec_wb(1)

  // ******************************
  //            DATA BUS
  // ******************************
  io.o_data.rs1 := io.i_instr(19,15)
  io.o_data.rs2 := io.i_instr(24,20)
  io.o_data.s1type := w_dec_ex(6)
  io.o_data.s2type := w_dec_ex(7)
  io.o_data.s3type := w_dec_ex(8)
  io.o_data.immtype := w_dec_ex(9)
  io.o_data.csr := io.i_instr(31,20)
}

object Decoder extends App {
  chisel3.Driver.execute(args, () => new Decoder(BackDefault))
}
