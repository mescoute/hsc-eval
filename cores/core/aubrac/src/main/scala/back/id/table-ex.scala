package cowlibry.core.aubrac.back

import chisel3._
import chisel3.util._

import cowlibry.common.isa._


// ************************************************************
//
//     DECODE TABLES TO EXTRACT GLOBAL AND EX INFORMATIONS
//
// ************************************************************
trait TABLEEX
{
  val default: List[UInt] =
               List[UInt](0.B,  0.B,  0.B,  0.B,  EXUNIT.X.U,   EXUOP.X.U,  OP.X.U,   OP.X.U,   OP.X.U,   IMM.X.U)
  val table: Array[(BitPat, List[UInt])]
}

object TABLEEXRV32I extends TABLEEX {
  val table : Array[(BitPat, List[UInt])] =
              Array[(BitPat, List[UInt])](

  //                    is Valid ?                    Ex Unit ?                       S1 Type ?                          Imm Type ?
  //                       |   is End ?                 |                                |         S2 Type ?                 |
  //                       |     | is Serial ?          |              Ex Uop ?          |            |      S3 Type ?       |
  //                       |     |     |  wait Empty ?  |                |               |            |          |           |
  //                       |     |     |     |          |                |               |            |          |           |
    RISCV.LUI     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.IMM.U,   OP.X.U,     OP.X.U,    IMM.isU.U),
    RISCV.AUIPC   -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.IMM.U,   OP.PC.U,    OP.X.U,    IMM.isU.U),
    RISCV.JAL     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.BRU.U,     EXUOP.JAL.U,      OP.PC.U,    OP.IMM.U,   OP.X.U,    IMM.isJ.U),
    RISCV.JALR    -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.BRU.U,     EXUOP.JALR.U,     OP.REG.U,   OP.IMM.U,   OP.X.U,    IMM.isI.U),
    RISCV.BEQ     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.BRU.U,     EXUOP.BEQ.U,      OP.REG.U,   OP.REG.U,   OP.IMM.U,  IMM.isB.U),
    RISCV.BNE     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.BRU.U,     EXUOP.BNE.U,      OP.REG.U,   OP.REG.U,   OP.IMM.U,  IMM.isB.U),
    RISCV.BLT     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.BRU.U,     EXUOP.BLT.U,      OP.REG.U,   OP.REG.U,   OP.IMM.U,  IMM.isB.U),
    RISCV.BGE     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.BRU.U,     EXUOP.BGE.U,      OP.REG.U,   OP.REG.U,   OP.IMM.U,  IMM.isB.U),
    RISCV.BLTU    -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.BRU.U,     EXUOP.BLTU.U,     OP.REG.U,   OP.REG.U,   OP.IMM.U,  IMM.isB.U),
    RISCV.BGEU    -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.BRU.U,     EXUOP.BGEU.U,     OP.REG.U,   OP.REG.U,   OP.IMM.U,  IMM.isB.U),
    RISCV.LB      -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.REG.U,   OP.IMM.U,   OP.X.U,    IMM.isI.U),
    RISCV.LH      -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.REG.U,   OP.IMM.U,   OP.X.U,    IMM.isI.U),
    RISCV.LW      -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.REG.U,   OP.IMM.U,   OP.X.U,    IMM.isI.U),
    RISCV.LBU     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.REG.U,   OP.IMM.U,   OP.X.U,    IMM.isI.U),
    RISCV.LHU     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.REG.U,   OP.IMM.U,   OP.X.U,    IMM.isI.U),
    RISCV.SB      -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.REG.U,   OP.IMM.U,   OP.REG.U,  IMM.isS.U),
    RISCV.SH      -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.REG.U,   OP.IMM.U,   OP.REG.U,  IMM.isS.U),
    RISCV.SW      -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.REG.U,   OP.IMM.U,   OP.REG.U,  IMM.isS.U),
    RISCV.ADDI    -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.REG.U,   OP.IMM.U,   OP.X.U,    IMM.isI.U),
    RISCV.SLTI    -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.SLT.U,      OP.REG.U,   OP.IMM.U,   OP.X.U,    IMM.isI.U),
    RISCV.SLTIU   -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.SLTU.U,     OP.REG.U,   OP.IMM.U,   OP.X.U,    IMM.isI.U),
    RISCV.XORI    -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.XOR.U,      OP.REG.U,   OP.IMM.U,   OP.X.U,    IMM.isI.U),
    RISCV.ORI     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.OR.U,       OP.REG.U,   OP.IMM.U,   OP.X.U,    IMM.isI.U),
    RISCV.ANDI    -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.AND.U,      OP.REG.U,   OP.IMM.U,   OP.X.U,    IMM.isI.U),
    RISCV.SLLI    -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.SLL.U,      OP.REG.U,   OP.IMM.U,   OP.X.U,    IMM.isI.U),
    RISCV.SRLI    -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.SRL.U,      OP.REG.U,   OP.IMM.U,   OP.X.U,    IMM.isI.U),
    RISCV.SRAI    -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.SRA.U,      OP.REG.U,   OP.IMM.U,   OP.X.U,    IMM.isI.U),
    RISCV.ADD     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.REG.U,   OP.REG.U,   OP.X.U,    IMM.X.U),
    RISCV.SUB     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.SUB.U,      OP.REG.U,   OP.REG.U,   OP.X.U,    IMM.X.U),
    RISCV.SLL     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.SLL.U,      OP.REG.U,   OP.REG.U,   OP.X.U,    IMM.X.U),
    RISCV.SLT     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.SLT.U,      OP.REG.U,   OP.REG.U,   OP.X.U,    IMM.X.U),
    RISCV.SLTU    -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.SLTU.U,     OP.REG.U,   OP.REG.U,   OP.X.U,    IMM.X.U),
    RISCV.XOR     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.XOR.U,      OP.REG.U,   OP.REG.U,   OP.X.U,    IMM.X.U),
    RISCV.SRL     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.SRL.U,      OP.REG.U,   OP.REG.U,   OP.X.U,    IMM.X.U),
    RISCV.SRA     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.SRA.U,      OP.REG.U,   OP.REG.U,   OP.X.U,    IMM.X.U),
    RISCV.OR      -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.OR.U,       OP.REG.U,   OP.REG.U,   OP.X.U,    IMM.X.U),
    RISCV.AND     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.ALU.U,     EXUOP.AND.U,      OP.REG.U,   OP.REG.U,   OP.X.U,    IMM.X.U))
}

object TABLEEXCSR extends TABLEEX {
  val table : Array[(BitPat, List[UInt])] =
              Array[(BitPat, List[UInt])](

  //                    is Valid ?                    Ex Unit ?                       S1 Type ?                          Imm Type ?
  //                       |   is End ?                 |                                |         S2 Type ?                 |
  //                       |     | is Serial ?          |              Ex Uop ?          |            |      S3 Type ?       |
  //                       |     |     |  wait Empty ?  |                |               |            |          |           |
  //                       |     |     |     |          |                |               |            |          |           |
    RISCV.CSRRW0  -> List(1.B,  0.B,  1.B,  0.B, EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.REG.U,   OP.X.U,   OP.CSR.U,    IMM.X.U),
    RISCV.CSRRW   -> List(1.B,  0.B,  1.B,  0.B, EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.REG.U,   OP.X.U,   OP.CSR.U,    IMM.X.U),
    RISCV.CSRRS0  -> List(1.B,  0.B,  1.B,  0.B, EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.REG.U,   OP.X.U,   OP.CSR.U,    IMM.X.U),
    RISCV.CSRRS   -> List(1.B,  0.B,  1.B,  0.B, EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.REG.U,   OP.X.U,   OP.CSR.U,    IMM.X.U),
    RISCV.CSRRC0  -> List(1.B,  0.B,  1.B,  0.B, EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.REG.U,   OP.X.U,   OP.CSR.U,    IMM.X.U),
    RISCV.CSRRC   -> List(1.B,  0.B,  1.B,  0.B, EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.REG.U,   OP.X.U,   OP.CSR.U,    IMM.X.U),
    RISCV.CSRRWI0 -> List(1.B,  0.B,  1.B,  0.B, EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.IMM.U,   OP.X.U,   OP.CSR.U,    IMM.isC.U),
    RISCV.CSRRWI  -> List(1.B,  0.B,  1.B,  0.B, EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.IMM.U,   OP.X.U,   OP.CSR.U,    IMM.isC.U),
    RISCV.CSRRSI0 -> List(1.B,  0.B,  1.B,  0.B, EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.IMM.U,   OP.X.U,   OP.CSR.U,    IMM.isC.U),
    RISCV.CSRRSI  -> List(1.B,  0.B,  1.B,  0.B, EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.IMM.U,   OP.X.U,   OP.CSR.U,    IMM.isC.U),
    RISCV.CSRRCI0 -> List(1.B,  0.B,  1.B,  0.B, EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.IMM.U,   OP.X.U,   OP.CSR.U,    IMM.isC.U),
    RISCV.CSRRCI  -> List(1.B,  0.B,  1.B,  0.B, EXUNIT.ALU.U,     EXUOP.ADD.U,      OP.IMM.U,   OP.X.U,   OP.CSR.U,    IMM.isC.U))
}

object TABLEEXRV32M extends TABLEEX {
  val table : Array[(BitPat, List[UInt])] =
              Array[(BitPat, List[UInt])](

  //                    is Valid ?                    Ex Unit ?                       S1 Type ?                          Imm Type ?
  //                       |   is End ?                 |                                |         S2 Type ?                 |
  //                       |     | is Serial ?          |              Ex Uop ?          |            |      S3 Type ?       |
  //                       |     |     |  wait Empty ?  |                |               |            |          |           |
  //                       |     |     |     |          |                |               |            |          |           |
    RISCV.MUL     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.MULDIV.U,  EXUOP.MUL.U,      OP.REG.U,   OP.REG.U,   OP.X.U,     IMM.X.U),
    RISCV.MULH    -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.MULDIV.U,  EXUOP.MULH.U,     OP.REG.U,   OP.REG.U,   OP.X.U,     IMM.X.U),
    RISCV.MULHU   -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.MULDIV.U,  EXUOP.MULHU.U,    OP.REG.U,   OP.REG.U,   OP.X.U,     IMM.X.U),
    RISCV.MULHSU  -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.MULDIV.U,  EXUOP.MULHSU.U,   OP.REG.U,   OP.REG.U,   OP.X.U,     IMM.X.U),
    RISCV.DIV     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.MULDIV.U,  EXUOP.DIV.U,      OP.REG.U,   OP.REG.U,   OP.X.U,     IMM.X.U),
    RISCV.DIVU    -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.MULDIV.U,  EXUOP.DIVU.U,     OP.REG.U,   OP.REG.U,   OP.X.U,     IMM.X.U),
    RISCV.REM     -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.MULDIV.U,  EXUOP.REM.U,      OP.REG.U,   OP.REG.U,   OP.X.U,     IMM.X.U),
    RISCV.REMU    -> List(1.B,  0.B,  0.B,  0.B,  EXUNIT.MULDIV.U,  EXUOP.REMU.U,     OP.REG.U,   OP.REG.U,   OP.X.U,     IMM.X.U))
}

object TABLEEXZIFENCEI extends TABLEEX {
  val table : Array[(BitPat, List[UInt])] =
              Array[(BitPat, List[UInt])](

  //                    is Valid ?                    Ex Unit ?                       S1 Type ?                          Imm Type ?
  //                       |   is End ?                 |                                |         S2 Type ?                 |
  //                       |     | is Serial ?          |              Ex Uop ?          |            |      S3 Type ?       |
  //                       |     |     |  wait Empty ?  |                |               |            |          |           |
  //                       |     |     |     |          |                |               |            |          |           |
    RISCV.FENCEI  -> List(1.B,  0.B,  1.B,  1.B,  EXUNIT.MAMU.U,    EXUOP.FENCEI.U,   OP.REG.U,   OP.REG.U,   OP.X.U,     IMM.X.U))
}

object TABLEEXDOME extends TABLEEX {
  val table : Array[(BitPat, List[UInt])] =
              Array[(BitPat, List[UInt])](

  //                    is Valid ?                    Ex Unit ?                       S1 Type ?                          Imm Type ?
  //                       |   is End ?                 |                                |         S2 Type ?                 |
  //                       |     | is Serial ?          |              Ex Uop ?          |            |      S3 Type ?       |
  //                       |     |     |  wait Empty ?  |                |               |            |          |           |
  //                       |     |     |     |          |                |               |            |          |           |
    DOME.SWITCH   -> List(1.B,  1.B,  1.B,  1.B,  EXUNIT.RMU.U,    EXUOP.SWITCH.U,   OP.X.U,      OP.X.U,    OP.X.U,    IMM.X.U),
    DOME.START    -> List(1.B,  0.B,  1.B,  0.B,  EXUNIT.RMU.U,    EXUOP.START.U,    OP.X.U,      OP.X.U,    OP.X.U,    IMM.X.U),
    DOME.END      -> List(1.B,  1.B,  1.B,  1.B,  EXUNIT.RMU.U,    EXUOP.END.U,      OP.X.U,      OP.X.U,    OP.X.U,    IMM.X.U))
}
