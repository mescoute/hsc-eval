package cowlibry.core.aubrac.back

import chisel3._
import chisel3.util._

import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._


class SlctSource(withGpr: Boolean) extends Module {
  val io = IO(new Bundle {
    val i_src_type = Input(UInt(OP.NBIT.W))
    val i_rs = if (withGpr) Some(Input(UInt(32.W))) else None
    val i_imm = Input(UInt(32.W))
    val i_pc = Input(UInt(32.W))
    val i_csr = Input(UInt(12.W))

    val o_val = Output(UInt(32.W))
  })

  when (io.i_src_type === OP.REG.U) {
    if (withGpr) {
      io.o_val := io.i_rs.get
    } else {
      io.o_val := 0.U
    }
  }.elsewhen (io.i_src_type === OP.IMM.U) {
    io.o_val := io.i_imm
  }.elsewhen (io.i_src_type === OP.PC.U) {
    io.o_val := io.i_pc
  }.elsewhen (io.i_src_type === OP.CSR.U) {
    io.o_val := Cat(Fill(20,0.B),io.i_csr)
  }.otherwise {
    io.o_val := 0.U
  }
}

class SlctImm extends Module {
  val io = IO(new Bundle {
    val i_instr = Input(UInt(32.W))
    val i_imm_type = Input(UInt(IMM.NBIT.W))
    val o_val = Output(UInt(32.W))
  })

  when (io.i_imm_type === IMM.isU.U) {
    io.o_val := Cat(io.i_instr(31,12),0.U(12.W))
  }.elsewhen (io.i_imm_type === IMM.isJ.U) {
    io.o_val := Cat(Fill(12,io.i_instr(31)),io.i_instr(19,12),io.i_instr(20),io.i_instr(30,21),0.U(1.W))
  }.elsewhen (io.i_imm_type === IMM.isI.U) {
    io.o_val := Cat(Fill(20,io.i_instr(31)),io.i_instr(31,20))
  }.elsewhen (io.i_imm_type === IMM.isB.U) {
    io.o_val := Cat(Fill(20,io.i_instr(31)),io.i_instr(7),io.i_instr(30,25),io.i_instr(11,8),0.U(1.W))
  }.elsewhen (io.i_imm_type === IMM.isS.U) {
    io.o_val := Cat(Fill(20,io.i_instr(31)),io.i_instr(31,25),io.i_instr(11,7))
  }.elsewhen (io.i_imm_type === IMM.isC.U) {
    io.o_val := Cat(Fill(27,0.B),io.i_instr(19,15))
  }.otherwise {
    io.o_val := 0.U
  }
}

class IdStage(p: BackParams) extends Module {
  val io = IO(new Bundle {
    val b_hart = new RsrcBus(1, 1, 1)

    val i_lock = Input(Bool())
    val o_lock = Output(Bool())
    val i_flush = Input(Bool())

    val i_end = Input(Bool())
    val i_pend = Input(Bool())
    val b_csr = Flipped(new CsrIdBus(p.nHart, p.useDome, p.useMulDiv))

    val i_valid = Input(Bool())
    val i_bus = Input(new BackPortBus(p.nHart, 32, 32))

    val i_rs_wait = Input(Vec(2, Bool()))
    val b_rs = Vec(2, Flipped(new ReadGprIO(p)))
    val i_rmu_wait = if (p.useDome) Some(Input(Bool())) else None

    val o_valid = Output(Bool())
    val o_bus = Output(new ExStageBus(p))
  })

  val w_lock = Wire(Bool())

  // ******************************
  //            DECODER
  // ******************************
  val decoder = Module(new Decoder(p))
  decoder.io.i_instr := io.i_bus.instr

  // ******************************
  //              GPR
  // ******************************
  io.b_rs(0).hart := io.i_bus.hart
  io.b_rs(0).addr := decoder.io.o_data.rs1
  io.b_rs(1).hart := io.i_bus.hart
  io.b_rs(1).addr := decoder.io.o_data.rs2

  // ******************************
  //             IMM
  // ******************************
  val slct_imm = Module(new SlctImm())
  slct_imm.io.i_instr := io.i_bus.instr
  slct_imm.io.i_imm_type := decoder.io.o_data.immtype

  // ******************************
  //             SOURCE
  // ******************************
  // ------------------------------
  //               S1
  // ------------------------------
  val slct_s1 = Module(new SlctSource(true))
  slct_s1.io.i_src_type := decoder.io.o_data.s1type
  slct_s1.io.i_rs.get := io.b_rs(0).data
  slct_s1.io.i_imm := slct_imm.io.o_val
  slct_s1.io.i_pc := io.i_bus.pc
  slct_s1.io.i_csr := decoder.io.o_data.csr

  // ------------------------------
  //               S2
  // ------------------------------
  val slct_s2 = Module(new SlctSource(true))
  slct_s2.io.i_src_type := decoder.io.o_data.s2type
  slct_s2.io.i_rs.get := io.b_rs(1).data
  slct_s2.io.i_imm := slct_imm.io.o_val
  slct_s2.io.i_pc := io.i_bus.pc
  slct_s2.io.i_csr := decoder.io.o_data.csr

  // ------------------------------
  //               S3
  // ------------------------------
  val slct_s3 = Module(new SlctSource(true))
  slct_s3.io.i_src_type := decoder.io.o_data.s3type
  slct_s3.io.i_rs.get := io.b_rs(1).data
  slct_s3.io.i_imm := slct_imm.io.o_val
  slct_s3.io.i_pc := io.i_bus.pc
  slct_s3.io.i_csr := decoder.io.o_data.csr

  // ******************************
  //          DEPENDENCIES
  // ******************************
  // ------------------------------
  //              GPR
  // ------------------------------
  val w_rs_wait = Wire(Bool())
  when (io.i_valid & decoder.io.o_data.s1type === OP.REG.U & io.i_rs_wait(0)) {w_rs_wait := true.B}
  .elsewhen (io.i_valid & decoder.io.o_data.s2type === OP.REG.U & io.i_rs_wait(1)) {w_rs_wait := true.B}
  .elsewhen (io.i_valid & decoder.io.o_data.s3type === OP.REG.U & io.i_rs_wait(1)) {w_rs_wait := true.B}
  .otherwise {w_rs_wait := false.B}

  // ------------------------------
  //              RMU
  // ------------------------------
  val w_rmu_wait = Wire(Bool())
  if (p.useDome) {
    w_rmu_wait := io.i_valid & (decoder.io.o_ex.unit === EXUNIT.RMU.U) & (io.i_rmu_wait.get)
  } else {
    w_rmu_wait := false.B
  }

  // ******************************
  //       WAIT EMPTY PIPELINE
  // ******************************
  val w_empty_wait = Wire(Bool())
  w_empty_wait := io.i_valid & decoder.io.o_info.empty & io.i_pend

  // ******************************
  //          CAPABILITIES
  // ******************************
  val w_cap_valid = Wire(Bool())
  w_cap_valid := true.B

  if (p.useDome) {
    if (p.useMulDiv) {
      when ((decoder.io.o_ex.unit === EXUNIT.MULDIV.U) & ~io.b_csr.cap_muldiv.get(0)) {
        w_cap_valid := false.B
      }
    }
  }

  // ******************************
  //           REGISTERS
  // ******************************
  val reg_valid = RegInit(0.B)
  val reg_bus = Reg(new ExStageBus(p))

  w_lock := io.i_lock & reg_valid

  when (io.b_hart.flush | (io.i_flush & ~w_lock)) {
    reg_valid := false.B
  }.elsewhen (io.b_hart.valid & ~w_lock) {
    reg_valid := io.i_valid & ~io.i_end & ~w_rs_wait & ~w_rmu_wait & ~w_empty_wait & decoder.io.o_valid & w_cap_valid

    reg_bus.info := decoder.io.o_info
    reg_bus.info.pc := io.i_bus.pc

    reg_bus.ex := decoder.io.o_ex
    reg_bus.mem := decoder.io.o_mem
    reg_bus.csr := decoder.io.o_csr
    reg_bus.wb := decoder.io.o_wb

    reg_bus.data.s1 := slct_s1.io.o_val
    reg_bus.data.s2 := slct_s2.io.o_val
    reg_bus.data.s3 := slct_s3.io.o_val
  }

  // ******************************
  //            OUTPUTS
  // ******************************
  // ------------------------------
  //             BUS
  // ------------------------------
  io.o_valid := reg_valid
  io.o_bus := reg_bus

  // ------------------------------
  //             LOCK
  // ------------------------------
  io.o_lock := ~io.b_hart.flush & (io.i_end | w_rs_wait | w_rmu_wait | w_empty_wait | w_lock)

  // ------------------------------
  //             FREE
  // ------------------------------
  io.b_hart.free := ~reg_valid
}

object IdStage extends App {
  chisel3.Driver.execute(args, () => new IdStage(BackDefault))
}
