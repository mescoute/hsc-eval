package cowlibry.core.aubrac.back

import chisel3._
import chisel3.util._

import cowlibry.common.isa._


// ************************************************************
//
//       DECODE TABLES TO EXTRACT WRITE BACK INFORMATIONS
//
// ************************************************************
trait TABLEWB
{
  val default: List[UInt] =
               List[UInt](0.B,  0.B)
  val table: Array[(BitPat, List[UInt])]
}

object TABLEWBRV32I extends TABLEWB {
  val table : Array[(BitPat, List[UInt])] =
              Array[(BitPat, List[UInt])](

  //                      WB ?
  //                       |  use RMU ?
  //                       |     |
    RISCV.LUI     -> List(1.B,  0.B),
    RISCV.AUIPC   -> List(1.B,  0.B),
    RISCV.JAL     -> List(1.B,  0.B),
    RISCV.JALR    -> List(1.B,  0.B),
    RISCV.BEQ     -> List(0.B,  0.B),
    RISCV.BNE     -> List(0.B,  0.B),
    RISCV.BLT     -> List(0.B,  0.B),
    RISCV.BGEU    -> List(0.B,  0.B),
    RISCV.BLTU    -> List(0.B,  0.B),
    RISCV.BGEU    -> List(0.B,  0.B),
    RISCV.LB      -> List(1.B,  0.B),
    RISCV.LH      -> List(1.B,  0.B),
    RISCV.LW      -> List(1.B,  0.B),
    RISCV.LBU     -> List(1.B,  0.B),
    RISCV.LHU     -> List(1.B,  0.B),
    RISCV.SB      -> List(0.B,  0.B),
    RISCV.SH      -> List(0.B,  0.B),
    RISCV.SW      -> List(0.B,  0.B),
    RISCV.ADDI    -> List(1.B,  0.B),
    RISCV.SLTI    -> List(1.B,  0.B),
    RISCV.SLTIU   -> List(1.B,  0.B),
    RISCV.XORI    -> List(1.B,  0.B),
    RISCV.ORI     -> List(1.B,  0.B),
    RISCV.ANDI    -> List(1.B,  0.B),
    RISCV.SLLI    -> List(1.B,  0.B),
    RISCV.SRLI    -> List(1.B,  0.B),
    RISCV.SRAI    -> List(1.B,  0.B),
    RISCV.ADD     -> List(1.B,  0.B),
    RISCV.SUB     -> List(1.B,  0.B),
    RISCV.SLL     -> List(1.B,  0.B),
    RISCV.SLT     -> List(1.B,  0.B),
    RISCV.SLTU    -> List(1.B,  0.B),
    RISCV.XOR     -> List(1.B,  0.B),
    RISCV.SRL     -> List(1.B,  0.B),
    RISCV.SRA     -> List(1.B,  0.B),
    RISCV.OR      -> List(1.B,  0.B),
    RISCV.AND     -> List(1.B,  0.B))
}

object TABLEWBCSR extends TABLEWB {
  val table : Array[(BitPat, List[UInt])] =
              Array[(BitPat, List[UInt])](
  //                      WB ?
  //                       |  use RMU ?
  //                       |     |
    RISCV.CSRRW0  -> List(0.B,  0.B),
    RISCV.CSRRW   -> List(1.B,  0.B),
    RISCV.CSRRS0  -> List(1.B,  0.B),
    RISCV.CSRRS   -> List(1.B,  0.B),
    RISCV.CSRRC0  -> List(1.B,  0.B),
    RISCV.CSRRC   -> List(1.B,  0.B),
    RISCV.CSRRWI0 -> List(0.B,  0.B),
    RISCV.CSRRWI  -> List(1.B,  0.B),
    RISCV.CSRRSI0 -> List(1.B,  0.B),
    RISCV.CSRRSI  -> List(1.B,  0.B),
    RISCV.CSRRCI0 -> List(1.B,  0.B),
    RISCV.CSRRCI  -> List(1.B,  0.B))
}

object TABLEWBRV32M extends TABLEWB {
  val table : Array[(BitPat, List[UInt])] =
              Array[(BitPat, List[UInt])](

  //                      WB ?
  //                       |  use RMU ?
  //                       |     |
    RISCV.MUL     -> List(1.B,  0.B),
    RISCV.MULH    -> List(1.B,  0.B),
    RISCV.MULHU   -> List(1.B,  0.B),
    RISCV.MULHSU  -> List(1.B,  0.B),
    RISCV.DIV     -> List(1.B,  0.B),
    RISCV.DIVU    -> List(1.B,  0.B),
    RISCV.REM     -> List(1.B,  0.B),
    RISCV.REMU    -> List(1.B,  0.B))
}

object TABLEWBZIFENCEI extends TABLEWB {
  val table : Array[(BitPat, List[UInt])] =
              Array[(BitPat, List[UInt])](

  //                      WB ?
  //                       |  use RMU ?
  //                       |     |
    RISCV.FENCEI  -> List(0.B,  0.B))
}

object TABLEWBDOME extends TABLEWB {
  val table : Array[(BitPat, List[UInt])] =
              Array[(BitPat, List[UInt])](

  //                      WB ?
  //                       |  use RMU ?
  //                       |     |
    DOME.SWITCH   -> List(1.B,  1.B),
    DOME.START    -> List(1.B,  1.B),
    DOME.END      -> List(1.B,  1.B))
}
