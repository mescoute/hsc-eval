package cowlibry.core.aubrac.back

import chisel3._
import chisel3.util._

import cowlibry.common.isa._


// ************************************************************
//
//        DECODE TABLES TO EXTRACT MEMORY INFORMATIONS
//
// ************************************************************
object TABLEMEM {
  val default: List[UInt] =
               List[UInt](0.B,   0.B,  MEM.X.U)
  val table : Array[(BitPat, List[UInt])] =
              Array[(BitPat, List[UInt])](

  //                        is Store ?
  //                            |      Mem size
  //                  is Load ? |         |
  //                     |      |         |
  RISCV.LB      -> List(1.B,   0.B,    MEM.B.U),
  RISCV.LH      -> List(1.B,   0.B,    MEM.H.U),
  RISCV.LW      -> List(1.B,   0.B,    MEM.W.U),
  RISCV.LBU     -> List(1.B,   0.B,    MEM.BU.U),
  RISCV.LHU     -> List(1.B,   0.B,    MEM.HU.U),
  RISCV.SB      -> List(0.B,   1.B,    MEM.B.U),
  RISCV.SH      -> List(0.B,   1.B,    MEM.H.U),
  RISCV.SW      -> List(0.B,   1.B,    MEM.W.U))
}

// ************************************************************
//
//          DECODE TABLES TO EXTRACT CSR INFORMATIONS
//
// ************************************************************
object TABLECSR {
  val default: List[UInt] =
               List[UInt](0.B,  0.B,    CSRUOP.X.U)
  val table : Array[(BitPat, List[UInt])] =
              Array[(BitPat, List[UInt])](

  //                           CSR Wen ?
  //                               |         CSR Uop ?
  //                      CSR Ren ?|            |
  //                         |     |            |
  //                         |     |            |
    RISCV.CSRRW0  -> List(  0.B,  1.B,    CSRUOP.W.U),
    RISCV.CSRRW   -> List(  1.B,  1.B,    CSRUOP.W.U),
    RISCV.CSRRS0  -> List(  1.B,  0.B,    CSRUOP.S.U),
    RISCV.CSRRS   -> List(  1.B,  1.B,    CSRUOP.S.U),
    RISCV.CSRRC0  -> List(  1.B,  0.B,    CSRUOP.C.U),
    RISCV.CSRRC   -> List(  1.B,  1.B,    CSRUOP.C.U),
    RISCV.CSRRWI0 -> List(  0.B,  1.B,    CSRUOP.W.U),
    RISCV.CSRRWI  -> List(  1.B,  1.B,    CSRUOP.W.U),
    RISCV.CSRRSI0 -> List(  1.B,  0.B,    CSRUOP.S.U),
    RISCV.CSRRSI  -> List(  1.B,  1.B,    CSRUOP.S.U),
    RISCV.CSRRCI0 -> List(  1.B,  0.B,    CSRUOP.C.U),
    RISCV.CSRRCI  -> List(  1.B,  1.B,    CSRUOP.C.U))
}
