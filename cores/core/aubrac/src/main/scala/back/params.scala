package cowlibry.core.aubrac.back

import chisel3._
import chisel3.util._
import scala.math._


trait GprParams{
  def debug: Boolean
  def nHart: Int

  def nGprRead: Int
  def nGprWrite: Int
}

trait BackParams extends GprParams {
  def debug: Boolean
  def pcBoot: Int
  def nHart: Int = 1
  def useDome: Boolean
  def nDome: Int = 1

  def nBackPort: Int = 1
  def useMulDiv: Boolean
  def useFencei: Boolean
  def useMamu: Boolean = useFencei
  def useMemStage: Boolean
  def useBranchReg: Boolean
  def nGprRead: Int = 2
  def nGprWrite: Int = 1
  def nGprBypass: Int = if (useMemStage) 3 else 2
}
