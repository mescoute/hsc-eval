package cowlibry.core.aubrac.back

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._
import cowlibry.common.mem.flush._
import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._
import cowlibry.core.aubrac.rmu._


class Back(p: BackParams, p_dmem: PmbParams) extends Module {
  val io = IO(new Bundle {
    val b_hart = new RsrcBus(1, 1, 1)
    val o_lock = Output(Bool())
    val o_en = Output(Bool())

    val i_valid = Input(Bool())
    val i_port = Input(new BackPortBus(p.nHart, 32, 32))

    val b_dmem = new PmbIO(p_dmem)
    val i_br_next = Input(new BranchBus(32))
    val o_br_new = Output(new BranchBus(32))
    val o_br_info = Output(new BranchInfoBus(32))
    val b_rmu = if (p.useDome) Some(Flipped(new RmuIO())) else None
    val b_rmu_csr = if (p.useDome) Some(new CsrRmuBus()) else None
    val b_flush = if (p.useMamu) Some(new FlushIO(1, 1, 32)) else None

    val o_dbg_gpr = if (p.debug) Some(Output(Vec(32, UInt(32.W)))) else None
    val o_dbg_csr = if (p.debug) Some(Output(new CsrBus())) else None
  })

  val id = Module(new IdStage(p))
  val ex = Module(new ExStage(p))
  val mem = Module(new MemStage(true, p, p_dmem))
  val csr = Module(new Csr(p))
  val wb = Module(new WbStage(p, p_dmem))

  val gpr = Module(new Gpr(p))
  val bypass = Module(new Bypass(p))

  // ******************************
  //            ID STAGE
  // ******************************
  id.io.b_hart <> io.b_hart
  id.io.i_flush := ex.io.o_br_new.valid
  if (p.useMemStage) id.io.i_lock := ex.io.o_lock else id.io.i_lock := ex.io.o_lock | mem.io.o_lock
  id.io.i_end := ex.io.o_end | mem.io.o_end | wb.io.o_end
  id.io.i_pend := ex.io.o_pend | mem.io.o_pend | wb.io.o_pend
  id.io.b_csr <> csr.io.b_id
  id.io.i_valid := io.i_valid
  id.io.i_bus := io.i_port
  id.io.i_rs_wait := bypass.io.o_wait
  if (p.useDome) id.io.i_rmu_wait.get := ex.io.o_rmu_wait.get | mem.io.o_rmu_wait.get | wb.io.o_rmu_wait.get

  // ******************************
  //         GPR AND BYPASS
  // ******************************
  gpr.io.b_rport(0) <> id.io.b_rs(0)
  gpr.io.b_rport(1) <> id.io.b_rs(1)
  gpr.io.b_wport(0) := wb.io.b_rd

  bypass.io.b_rs(0) <> id.io.b_rs(0)
  bypass.io.i_rs(0) := gpr.io.b_rport(0).data
  bypass.io.b_rs(1) <> id.io.b_rs(1)
  bypass.io.i_rs(1) := gpr.io.b_rport(1).data
  if (p.useMemStage) {
    bypass.io.i_bypass(0) := wb.io.o_bypass
    bypass.io.i_bypass(1) := mem.io.o_bypass
    bypass.io.i_bypass(2) := ex.io.o_bypass.get
  } else {
    bypass.io.i_bypass(0) := wb.io.o_bypass
    bypass.io.i_bypass(1) := mem.io.o_bypass
  }

  // ****************************************
  //       SELECT NEXT CURRENT BRANCH
  // ****************************************
  val w_br_next = Wire(new BranchBus(32))
  w_br_next := io.i_br_next

  // ******************************
  //            EX STAGE
  // ******************************
  ex.io.b_hart <> io.b_hart
  if (p.useMemStage) ex.io.i_lock := mem.io.o_lock else ex.io.i_lock := wb.io.o_lock
  ex.io.i_valid := id.io.o_valid
  ex.io.i_bus := id.io.o_bus
  ex.io.i_br_next := w_br_next
  if (p.useDome) ex.io.b_rmu.get <> io.b_rmu.get.req
  if (p.useMamu) ex.io.b_flush.get <> io.b_flush.get

  // ******************************
  //           MEM STAGE
  // ******************************
  mem.io.b_hart(0) <> io.b_hart
  mem.io.b_backport <> io.b_hart
  mem.io.i_lock := wb.io.o_lock
  mem.io.i_valid := ex.io.o_valid
  mem.io.i_bus := ex.io.o_bus
  mem.io.b_dmem.get <> io.b_dmem.req

  // ******************************
  //              CSR
  // ******************************
  csr.io.b_hart(0) <> io.b_hart
  csr.io.b_read(0) <> mem.io.b_csr
  csr.io.b_write(0) <> wb.io.b_csr
  csr.io.b_ex <> ex.io.b_csr_info
  csr.io.b_wb <> wb.io.b_csr_info
  if (p.useDome) csr.io.b_rmu.get(0) <> io.b_rmu_csr.get
  io.o_en := csr.io.o_en(0)

  // ******************************
  //            WB STAGE
  // ******************************
  wb.io.i_valid := mem.io.o_valid
  wb.io.i_bus := mem.io.o_bus
  wb.io.b_dmem <> io.b_dmem.ack
  if (p.useDome) wb.io.b_rmu.get <> io.b_rmu.get.ack

  // ******************************
  //             I/O
  // ******************************
  // ------------------------------
  //            BRANCH
  // ------------------------------
  io.o_br_new := ex.io.o_br_new
  io.o_br_info := ex.io.o_br_info

  // ------------------------------
  //             LOCK
  // ------------------------------
  io.o_lock := id.io.o_lock

  // ------------------------------
  //             FREE
  // ------------------------------
  io.b_hart.free := id.io.b_hart.free & ex.io.b_hart.free & mem.io.b_hart(0).free

  // ------------------------------
  //             DEBUG
  // ------------------------------
  if (p.debug) io.o_dbg_gpr.get := (gpr.io.o_dbg_gpr.get)(0)
  if (p.debug) io.o_dbg_csr.get := (csr.io.o_dbg_csr.get)(0)
}

object Back extends App {
  chisel3.Driver.execute(args, () => new Back(BackDefault, PmbCfg0))
}
