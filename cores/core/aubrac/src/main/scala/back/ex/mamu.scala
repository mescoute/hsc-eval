package cowlibry.core.aubrac.back

import chisel3._
import chisel3.util._

import cowlibry.core.aubrac.tools._
import cowlibry.common.mem.flush._


class Mamu (nHart: Int, nDome: Int) extends Module {
  val io = IO(new Bundle {
    val i_lock = Input(Bool())
    val i_valid = Input(Bool())
    val i_hart = Input(UInt(log2Ceil(nHart).W))
    val i_dome = Input(UInt(log2Ceil(nHart).W))
    val i_uop = Input(UInt(EXUOP.NBIT.W))
    val i_pc = Input(UInt(32.W))

    val b_flush = new FlushIO(nHart, nDome, 32)

    val o_br_new = Output(new BranchBus(32))
    val o_end = Output(Bool())
  })

  val reg_flush = RegInit(false.B)
  val reg_ready = RegInit(false.B)

  io.b_flush.valid := false.B
  io.b_flush.apply := false.B
  io.b_flush.hart := io.i_hart
  io.b_flush.dome := io.i_dome
  io.b_flush.target := TARGET.X.U
  io.b_flush.mode := MODE.X.U
  io.b_flush.addr := 0.U

  io.o_br_new.valid := false.B
  io.o_br_new.pc := io.i_pc + 4.U

  io.o_end := false.B

  reg_flush := false.B
  reg_ready := false.B

  when (io.i_valid) {
    switch(io.i_uop) {
      is (EXUOP.FENCEI.U) {
        when (~reg_ready) {
          reg_flush := ~io.i_lock
          reg_ready := ~io.i_lock & reg_flush & io.b_flush.ready
        }

        io.b_flush.valid := ~io.i_lock
        io.b_flush.apply := true.B
        io.b_flush.target := TARGET.INSTR.U
        io.b_flush.mode := MODE.FULL.U

        io.o_br_new.valid := reg_ready
        io.o_br_new.pc := io.i_pc + 4.U

        io.o_end := reg_ready
      }
    }
  }
}

object Mamu extends App {
  chisel3.Driver.execute(args, () => new Mamu(1, 1))
}
