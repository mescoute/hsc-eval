package cowlibry.core.aubrac.back

import chisel3._
import chisel3.util._


class Alu extends Module {
  val io = IO(new Bundle {
    val i_valid = Input(Bool())
    val i_uop = Input(UInt(EXUOP.NBIT.W))
    val i_s1 = Input(UInt(32.W))
    val i_s2 = Input(UInt(32.W))

    val o_end = Output(Bool())
    val o_res = Output(UInt(32.W))
  })

  val w_amount = io.i_s2(4,0).asUInt

  when (io.i_valid) {
    io.o_res := 0.U
    switch(io.i_uop) {
      is(EXUOP.ADD.U) {
        io.o_res := io.i_s1 + io.i_s2
      }
      is(EXUOP.SUB.U) {
        io.o_res := io.i_s1 - io.i_s2
      }
      is(EXUOP.SLT.U) {
        io.o_res := (io.i_s1).asSInt < (io.i_s2).asSInt
      }
      is(EXUOP.SLTU.U) {
        io.o_res := io.i_s1 < io.i_s2
      }
      is(EXUOP.OR.U) {
        io.o_res := io.i_s1 | io.i_s2
      }
      is(EXUOP.AND.U) {
        io.o_res := io.i_s1 & io.i_s2
      }
      is(EXUOP.XOR.U) {
        io.o_res := io.i_s1 ^ io.i_s2
      }
      is(EXUOP.SRA.U) {
        io.o_res := ((io.i_s1).asSInt >> w_amount).asUInt
      }
      is(EXUOP.SRL.U) {
        io.o_res := io.i_s1 >> w_amount
      }
      is(EXUOP.SLL.U) {
        io.o_res := io.i_s1 << w_amount
      }
    }
  }.otherwise {
    io.o_res := 0.U
  }
  io.o_end := io.i_valid
}

object Alu extends App {
  chisel3.Driver.execute(args, () => new Alu())
}
