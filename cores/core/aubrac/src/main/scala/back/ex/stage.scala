package cowlibry.core.aubrac.back

import chisel3._
import chisel3.util._

import cowlibry.common.isa._
import cowlibry.common.rsrc._
import cowlibry.common.mem.flush._
import cowlibry.core.aubrac.tools._
import cowlibry.core.aubrac.rmu._


class ExStage (p: BackParams) extends Module {
  val io = IO(new Bundle {
    val b_hart = new RsrcBus(1, 1, 1)
    val i_lock = Input(Bool())
    val o_lock = Output(Bool())

    val o_end = Output(Bool())
    val o_pend = Output(Bool())

    val i_valid = Input(Bool())
    val i_bus = Input(new ExStageBus(p))
    val i_br_next = Input(new BranchBus(32))
    val b_rmu = if (p.useDome) Some(Flipped(new RmuReqIO())) else None
    val b_flush = if (p.useMamu) Some(new FlushIO(1, 1, 32)) else None

    val o_bypass = if (p.useMemStage) Some(Output(new BypassBus(p.nHart))) else None
    val o_br_new = Output(new BranchBus(32))
    val o_br_info = Output(new BranchInfoBus(32))
    val o_rmu_wait = if (p.useDome) Some(Output(Bool())) else None
    val b_csr_info = Flipped(new CsrExBus(1, 1))

    val o_valid = Output(Bool())
    val o_bus = Output(new MemStageBus(p))
  })

  val w_lock = Wire(Bool())
  val w_flush = Wire(Bool())
  val w_i_valid = Wire(Bool())
  val w_br_new = Wire(new BranchBus(32))

  // ******************************
  //    CONNECT EXECUTION UNITS
  // ******************************
  w_i_valid := io.i_valid & ~w_flush

  // ------------------------------
  //            DEFAULT
  // ------------------------------
  val w_end = Wire(Bool())
  val w_res = Wire(UInt(32.W))

  w_res := 0.U
  w_end := true.B

  w_br_new.valid := false.B
  w_br_new.pc := 0.U

  // ------------------------------
  //             ALU
  // ------------------------------
  val alu = Module(new Alu())
  alu.io.i_valid := w_i_valid & (io.i_bus.ex.unit === EXUNIT.ALU.U)
  alu.io.i_uop := io.i_bus.ex.uop
  alu.io.i_s1 := io.i_bus.data.s1
  alu.io.i_s2 := io.i_bus.data.s2

  when (io.i_bus.ex.unit === EXUNIT.ALU.U) {
    w_res := alu.io.o_res
    w_end := alu.io.o_end
  }

  // ------------------------------
  //             BRU
  // ------------------------------
  val bru = Module(new Bru())
  bru.io.i_lock := w_lock
  bru.io.i_valid := w_i_valid & (io.i_bus.ex.unit === EXUNIT.BRU.U) & ~io.b_hart.flush
  bru.io.i_uop := io.i_bus.ex.uop
  bru.io.i_pc := io.i_bus.info.pc
  bru.io.i_br_next := io.i_br_next
  bru.io.i_s1 := io.i_bus.data.s1
  bru.io.i_s2 := io.i_bus.data.s2
  bru.io.i_s3 := io.i_bus.data.s3

  when (io.i_bus.ex.unit === EXUNIT.BRU.U) {
    w_res := bru.io.o_res
    w_end := bru.io.o_end

    w_br_new.valid := bru.io.o_br_new.valid
    w_br_new.pc := bru.io.o_br_new.pc
  }

  // CSR informations
  io.b_csr_info.branch(0)(0) := w_i_valid & (io.i_bus.ex.unit === EXUNIT.BRU.U) & ~w_lock
  io.b_csr_info.mispred(0)(0) := bru.io.o_br_new.valid

  // ------------------------------
  //            MULDIV
  // ------------------------------
  val muldiv = if (p.useMulDiv) Some(Module(new MulDiv())) else None

  if (p.useMulDiv) {
    muldiv.get.io.i_lock := w_lock
    muldiv.get.io.i_flush := io.b_hart.flush
    muldiv.get.io.i_valid := w_i_valid & (io.i_bus.ex.unit === EXUNIT.MULDIV.U)
    muldiv.get.io.i_uop := io.i_bus.ex.uop
    muldiv.get.io.i_s1 := io.i_bus.data.s1
    muldiv.get.io.i_s2 := io.i_bus.data.s2

    when(io.i_bus.ex.unit === EXUNIT.MULDIV.U) {
      w_res := muldiv.get.io.o_res
      w_end := muldiv.get.io.o_end
    }
  }

  // ------------------------------
  //             RMU
  // ------------------------------
  if (p.useDome) {
    io.b_rmu.get.valid := io.b_hart.valid & ~io.b_hart.flush & w_i_valid & (io.i_bus.ex.unit === EXUNIT.RMU.U) & ~w_lock
    io.b_rmu.get.uop := io.i_bus.ex.uop

    when(io.i_bus.ex.unit === EXUNIT.RMU.U) {
      w_res := 0.U
      w_end := io.b_rmu.get.ready
    }
  }

  // ------------------------------
  //             MAMU
  // ------------------------------
  val mamu = if (p.useMamu) Some(Module(new Mamu(p.nHart, p.nDome))) else None

  if (p.useMamu) {
    mamu.get.io.i_lock := w_lock
    mamu.get.io.i_valid := w_i_valid & (io.i_bus.ex.unit === EXUNIT.MAMU.U) & ~io.b_hart.flush
    mamu.get.io.i_dome := io.b_hart.dome
    mamu.get.io.i_hart := io.i_bus.info.hart
    mamu.get.io.i_uop := io.i_bus.ex.uop
    mamu.get.io.i_pc := io.i_bus.info.pc

    mamu.get.io.b_flush <> io.b_flush.get

    when (io.i_bus.ex.unit === EXUNIT.MAMU.U) {
      w_res := 0.U
      w_end := mamu.get.io.o_end

      w_br_new.valid := mamu.get.io.o_br_new.valid
      w_br_new.pc := mamu.get.io.o_br_new.pc
    }
  }

  // ******************************
  //             BRANCH
  // ******************************
  // ------------------------------
  //              NEW
  // ------------------------------
  val init_br_new = Wire(new BranchBus(32))
  init_br_new.valid := false.B
  init_br_new.pc := DontCare

  val reg_br_new = RegInit(init_br_new)

  if (p.useBranchReg) {
    reg_br_new := w_br_new

    w_flush := reg_br_new.valid

    if (p.useMamu) {
      io.o_br_new.valid := reg_br_new.valid | (mamu.get.io.b_flush.valid & mamu.get.io.b_flush.apply)
      io.o_br_new.pc := reg_br_new.pc
    } else {
      io.o_br_new := reg_br_new
    }
  } else {
    w_flush := false.B
    io.o_br_new := w_br_new

    if (p.useMamu) {
      io.o_br_new.valid := w_br_new.valid | (mamu.get.io.b_flush.valid & mamu.get.io.b_flush.apply)
      io.o_br_new.pc := w_br_new.pc
    } else {
      io.o_br_new := w_br_new
    }
  }

  // ------------------------------
  //             INFO
  // ------------------------------
  val init_info = Wire(new BranchInfoBus(32))
  init_info.valid := false.B
  init_info.pc := DontCare
  init_info.br := DontCare
  init_info.taken := DontCare
  init_info.jmp := DontCare
  init_info.target := DontCare

  val reg_info = RegInit(init_info)

  if (p.useBranchReg) {
      reg_info := bru.io.o_br_info
      io.o_br_info := reg_info
    } else {
      io.o_br_info := bru.io.o_br_info
    }

  // ******************************
  //       BYPASS CONNECTIONS
  // ******************************
  if (p.useMemStage) {
    (io.o_bypass.get).hart := io.i_bus.info.hart
    (io.o_bypass.get).en := w_i_valid & ~io.b_hart.flush & io.i_bus.wb.en & w_end
    (io.o_bypass.get).ready := ~(io.i_bus.mem.load | io.i_bus.csr.read | io.i_bus.wb.rmu)
    (io.o_bypass.get).addr := io.i_bus.wb.addr
    (io.o_bypass.get).data := w_res
  }

  // ******************************
  //        RMU DEPENDENCIES
  // ******************************
  if (p.useDome) {
    io.o_rmu_wait.get := w_i_valid & io.i_bus.csr.write & ((io.i_bus.data.s3(11,0) === DOME.CSRNEXTID.U) | (io.i_bus.data.s3(11,0) === DOME.CSRNEXTPC.U) | (io.i_bus.data.s3(11,0) === DOME.CSRNEXTCAP.U))
  }

  // ******************************
  //            OUTPUTS
  // ******************************
  // ------------------------------
  //             BUS
  // ------------------------------
  val reg_valid = RegInit(false.B)
  val reg_bus = Reg(new MemStageBus(p))

  w_lock := io.i_lock & reg_valid

  if (p.useMemStage) {
    when (io.b_hart.flush) {
      reg_valid := false.B
    }.elsewhen (io.b_hart.valid & ~w_lock) {
      reg_valid := w_i_valid & w_end
      reg_bus.info := io.i_bus.info
      reg_bus.mem := io.i_bus.mem
      reg_bus.csr := io.i_bus.csr
      reg_bus.wb := io.i_bus.wb

      reg_bus.data.res := w_res
      reg_bus.data.s3 := io.i_bus.data.s3
    }

    io.o_valid := reg_valid
    io.o_bus := reg_bus
    io.b_hart.free := ~reg_valid
  } else {
    io.o_valid := w_i_valid & ~io.b_hart.flush & w_end
    io.o_bus.info := io.i_bus.info
    io.o_bus.mem := io.i_bus.mem
    io.o_bus.csr := io.i_bus.csr
    io.o_bus.wb := io.i_bus.wb

    io.o_bus.data.s1 := io.i_bus.data.s1
    io.o_bus.data.s3 := io.i_bus.data.s3
    io.o_bus.data.res := w_res
    io.b_hart.free := true.B
  }

  // ------------------------------
  //             LOCK
  // ------------------------------
  io.o_lock := ~io.b_hart.flush & w_i_valid & (~w_end | w_lock)

  // ------------------------------
  //           END & PEND
  // ------------------------------
  io.o_end := w_i_valid & io.i_bus.info.end
  io.o_pend := w_i_valid
}

object ExStage extends App {
  chisel3.Driver.execute(args, () => new ExStage(BackDefault))
}
