package cowlibry.core.aubrac.back

import chisel3._
import chisel3.util._

import cowlibry.core.aubrac.tools._


object TABLEMULT
{
  val default: List[Bool] = List[Bool]( 0.B,  0.B,  0.B,  0.B,  0.B)
  val table : Array[(BitPat, List[Bool])] =
              Array[(BitPat, List[Bool])](

//                                                      Low/Quo or High/Rem ?
//                                                is Mul ?      | S1 signed ?
//                                                   |  is Div ? |     | S2 signed ?
//                                                   |     |     |     |     |
    BitPat(EXUOP.MUL.U(EXUOP.NBIT.W))     ->  List( 1.B,  0.B,  0.B,  1.B,  1.B),
    BitPat(EXUOP.MULH.U(EXUOP.NBIT.W))    ->  List( 1.B,  0.B,  1.B,  1.B,  1.B),
    BitPat(EXUOP.MULHSU.U(EXUOP.NBIT.W))  ->  List( 1.B,  0.B,  1.B,  1.B,  0.B),
    BitPat(EXUOP.MULHU.U(EXUOP.NBIT.W))   ->  List( 1.B,  0.B,  1.B,  0.B,  0.B),
    BitPat(EXUOP.DIV.U(EXUOP.NBIT.W))     ->  List( 0.B,  1.B,  0.B,  1.B,  1.B),
    BitPat(EXUOP.DIVU.U(EXUOP.NBIT.W))    ->  List( 0.B,  1.B,  0.B,  0.B,  0.B),
    BitPat(EXUOP.REM.U(EXUOP.NBIT.W))     ->  List( 0.B,  1.B,  1.B,  1.B,  1.B),
    BitPat(EXUOP.REMU.U(EXUOP.NBIT.W))    ->  List( 0.B,  1.B,  1.B,  0.B,  0.B))
}

class MulShiftAdd extends Module {
  val io = IO(new Bundle {
    val i_s1 = Input(UInt(32.W))
    val i_s2 = Input(UInt(32.W))
    val i_round = Input(UInt(3.W))

    val o_res = Output(UInt(64.W))
  })

  val w_tmp_res0 = Wire(Vec(4, UInt(64.W)))
  for (j <- 0 until 4) {
    w_tmp_res0(j) := 0.U
  }

  for (i <- 0 until 8) {
    when(i.U === io.i_round) {
      for (j <- 0 until 4) {
        when(io.i_s2(i*4+j)) {
          w_tmp_res0(j) := io.i_s1 << (i*4+j).U
        }
      }
    }
  }

  val w_tmp_res1 = Wire(Vec(2, UInt(64.W)))
  for (j <- 0 until 2) {
    w_tmp_res1(j) := w_tmp_res0(j*2) + w_tmp_res0(j*2+1)
  }

  io.o_res := w_tmp_res1(0) + w_tmp_res1(1)
}

class DivShiftSub extends Module {
  val io = IO(new Bundle {
    val i_round = Input(UInt(5.W))
    val i_s1 = Input(UInt(32.W))
    val i_s2 = Input(UInt(32.W))
    val i_quo = Input(UInt(32.W))
    val i_rem = Input(UInt(32.W))

    val o_quo = Output(UInt(32.W))
    val o_rem = Output(UInt(32.W))
  })

  // ******************************
  //         NEW DIVIDEND
  // ******************************
  val w_old_rem = Wire(UInt(32.W))
  w_old_rem := io.i_rem << 1.U

  val w_div = Wire(Vec(32, Bool()))
  w_div := w_old_rem.asBools

  for (i <- 0 until 32) {
    when (i.U === io.i_round) {
      w_div(0) := io.i_s1(31-i)
    }
  }

  // ******************************
  //   NEW QUOTIENT AND REMAINDER
  // ******************************
  val w_old_quo = Wire(UInt(32.W))
  w_old_quo := io.i_quo << 1.U

  val w_quo = Wire(Vec(32, Bool()))
  w_quo := w_old_quo.asBools

  when(w_div.asUInt >= io.i_s2) {
    w_quo(0) := 1.B
    io.o_rem := w_div.asUInt - io.i_s2
  }.otherwise {
    w_quo(0) := 0.B
    io.o_rem := w_div.asUInt
  }
  io.o_quo := w_quo.asUInt
}

class MulDiv extends Module {
  val io = IO(new Bundle {
    val i_lock = Input(Bool())
    val i_flush = Input(Bool())
    val o_free = Output(Bool())

    val i_valid = Input(Bool())
    val i_uop = Input(UInt(EXUOP.NBIT.W))
    val i_s1 = Input(UInt(32.W))
    val i_s2 = Input(UInt(32.W))

    val o_end = Output(Bool())
    val o_res = Output(UInt(32.W))
  })

  // ******************************
  //          DECODING UOP
  // ******************************
  val w_dec = ListLookup(io.i_uop, TABLEMULT.default, TABLEMULT.table)

  // ******************************
  //              FSM
  // ******************************
  val s0IDLE :: s1MULT :: s2DIV :: Nil = Enum(3)
  val reg_state = RegInit(s0IDLE)
  val reg_round = Reg(UInt(5.W))

  when(reg_state === s1MULT) {
    when(io.i_flush | ((reg_round === 7.U) & ~io.i_lock)) {
      reg_state := s0IDLE
    }
  }.elsewhen(reg_state === s2DIV) {
    when(io.i_flush | ((reg_round === 31.U) & ~io.i_lock)) {
      reg_state := s0IDLE
    }
  }.otherwise {
    when (io.i_valid & ~io.i_flush) {
      when(w_dec(0)) {
        reg_state := s1MULT
      }.elsewhen(w_dec(1)) {
        reg_state := s2DIV
      }.otherwise {
        reg_state := s0IDLE
      }
    }.otherwise {
      reg_state := s0IDLE
    }
  }

  // ******************************
  //       SIGNED TO UNSIGNED
  // ******************************
  val w_unsig_s1 = Wire(UInt(32.W))
  val w_unsig_s2 = Wire(UInt(32.W))
  val w_unsig_s1_sign = Wire(Bool())
  val w_unsig_s2_sign = Wire(Bool())

  when (w_dec(3) & w_dec(4)) {
    w_unsig_s1 := Mux(io.i_s1(31), ~(io.i_s1 - 1.U), io.i_s1)
    w_unsig_s2 := Mux(io.i_s2(31), ~(io.i_s2 - 1.U), io.i_s2)
    w_unsig_s1_sign := io.i_s1(31)
    w_unsig_s2_sign := io.i_s2(31)
  }.elsewhen (w_dec(3)) {
    w_unsig_s1 := Mux(io.i_s1(31), ~(io.i_s1 - 1.U), io.i_s1)
    w_unsig_s2 := io.i_s2
    w_unsig_s1_sign := io.i_s1(31)
    w_unsig_s2_sign := 0.B
  }.otherwise {
    w_unsig_s1 := io.i_s1
    w_unsig_s2 := io.i_s2
    w_unsig_s1_sign := 0.B
    w_unsig_s2_sign := 0.B
  }

  // ******************************
  //      EXCEPTION DETECTION
  // ******************************
  val reg_exc_zero = Reg(Bool())
  val reg_exc_over = Reg(Bool())
  when((reg_state === s0IDLE) & io.i_valid & ~io.i_flush) {
    when(w_dec(1) & io.i_s2 === 0.U) {
      reg_exc_zero := true.B
      reg_exc_over := false.B
    }.elsewhen (w_dec(1) & (w_unsig_s1_sign ^ w_unsig_s2_sign) & (io.i_s1.asSInt === -2147483648.S) & (io.i_s2.asSInt === -1.S)) {
      reg_exc_zero := false.B
      reg_exc_over := true.B
    }.otherwise {
      reg_exc_zero := false.B
      reg_exc_over := false.B
    }
  }

  // ******************************
  //  INPUT AND CONTROL REGISTERS
  // ******************************
  val reg_s1 = Reg(UInt(32.W))
  val reg_s2 = Reg(UInt(32.W))
  val reg_s1_sign = Reg(Bool())
  val reg_s2_sign = Reg(Bool())
  val reg_lquo_hrem = Reg(Bool())

  when(reg_state === s1MULT) {
    when(io.i_flush) {
      reg_round := 0.U
    }.elsewhen(reg_round === 7.U) {
      when (~io.i_lock) {
        reg_round := 0.U
      }
    }.otherwise {
      reg_round := reg_round + 1.U
    }
  }.elsewhen(reg_state === s2DIV) {
    when(io.i_flush) {
      reg_round := 0.U
    }.elsewhen(reg_round === 31.U) {
      when (~io.i_lock) {
        reg_round := 0.U
      }
    }.otherwise {
      reg_round := reg_round + 1.U
    }
  }.otherwise {
    when (io.i_valid & ~io.i_flush & (w_dec(0) | w_dec(1))) {
      reg_round := 1.U
      reg_lquo_hrem := w_dec(2)
      reg_s1 := w_unsig_s1
      reg_s2 := w_unsig_s2
      reg_s1_sign := w_unsig_s1_sign
      reg_s2_sign := w_unsig_s2_sign
    }.otherwise {
      reg_round := 0.U
    }
  }

  // ****************************************
  //  INTERMEDIATE POSITIVE VALUE REGISTERS
  // ****************************************
  val reg_tmp_unsig_res = Reg(UInt(64.W))
  val reg_tmp_unsig_quo = Reg(UInt(32.W))
  val reg_tmp_unsig_rem = Reg(UInt(32.W))

  // ******************************
  //    UNITS INPUTS CONNECTIONS
  // ******************************
  val mulsa = Module(new MulShiftAdd())
  mulsa.io.i_round := reg_round
  mulsa.io.i_s1 := Mux((reg_state === s1MULT), reg_s1, w_unsig_s1)
  mulsa.io.i_s2 := Mux((reg_state === s1MULT), reg_s2, w_unsig_s2)

  val divss = Module(new DivShiftSub())
  divss.io.i_round := reg_round
  divss.io.i_s1 := Mux((reg_state === s2DIV), reg_s1, w_unsig_s1)
  divss.io.i_s2 := Mux((reg_state === s2DIV), reg_s2, w_unsig_s2)
  divss.io.i_quo := Mux((reg_state === s2DIV), reg_tmp_unsig_quo, 0.U)
  divss.io.i_rem := Mux((reg_state === s2DIV), reg_tmp_unsig_rem, 0.U)

  // ****************************************
  //  INTERMEDIATE POSITIVE VALUE REGISTERS
  // ****************************************
  when(reg_state === s1MULT) {
    when((reg_round === 7.U) & ~io.i_lock) {
      reg_tmp_unsig_res := reg_tmp_unsig_res
    }.otherwise {
      reg_tmp_unsig_res := reg_tmp_unsig_res + mulsa.io.o_res
    }
  }.elsewhen(reg_state === s2DIV) {
    when((reg_round === 31.U) & ~io.i_lock) {
      reg_tmp_unsig_quo := reg_tmp_unsig_quo
      reg_tmp_unsig_rem := reg_tmp_unsig_rem
    }.otherwise {
      reg_tmp_unsig_quo := divss.io.o_quo
      reg_tmp_unsig_rem := divss.io.o_rem
    }
  }.otherwise {
    when (io.i_valid & ~io.i_flush) {
      reg_tmp_unsig_res := mulsa.io.o_res
      reg_tmp_unsig_quo := divss.io.o_quo
      reg_tmp_unsig_rem := divss.io.o_rem
    }
  }

  // ****************************************
  //    UNSIGNED TO SIGNED AND EXCEPTIONS
  // ****************************************
  val w_res = Wire(UInt(64.W))
  val w_quo = Wire(UInt(32.W))
  val w_rem = Wire(UInt(32.W))

  when(reg_state === s1MULT) {
    w_res := Mux(reg_s1_sign ^ reg_s2_sign, (~reg_tmp_unsig_res + mulsa.io.o_res) + 1.U, reg_tmp_unsig_res + mulsa.io.o_res)
    w_quo := 0.U
    w_rem := 0.U
  }.elsewhen(reg_state === s2DIV) {
    w_res := 0.U
    when(reg_exc_zero) {
      w_quo := Fill(32, 1.B)
      w_rem := Mux(reg_s1_sign, ~reg_s1 + 1.U, reg_s1)
    }.elsewhen (reg_exc_over) {
      w_quo := ~reg_s1 + 1.U
      w_rem := 0.U
    }.otherwise {
      w_quo := Mux(reg_s1_sign ^ reg_s2_sign, (~divss.io.o_quo) + 1.U, divss.io.o_quo)
      w_rem := Mux(reg_s1_sign, (~divss.io.o_rem) + 1.U, divss.io.o_rem)
    }
  }.otherwise {
    w_res := 0.U
    w_quo := 0.U
    w_rem := 0.U
  }

  // ******************************
  //      OUTPUTS CONNECTIONS
  // ******************************
  val w_end = Wire(Bool())

  when(reg_state === s1MULT) {
    when(reg_round === 7.U) {
      w_end := true.B
    }.otherwise {
      w_end := false.B
    }
  }.elsewhen(reg_state === s2DIV) {
    when(reg_round === 31.U) {
      w_end := true.B
    }.otherwise {
      w_end := false.B
    }
  }.otherwise {
    w_end := false.B
  }

  io.o_end := w_end

  when(w_end) {
    when(reg_state === s1MULT) {
      when (reg_lquo_hrem) {
        io.o_res := w_res(63,32)
      }.otherwise {
        io.o_res := w_res(31, 0)
      }
    }.elsewhen(reg_state === s2DIV) {
      when (reg_lquo_hrem) {
        io.o_res := w_rem
      }.otherwise {
        io.o_res := w_quo
      }
    }.otherwise {
      io.o_res := 0.U
    }
  }.otherwise {
    io.o_res := 0.U
  }

  io.o_free := (reg_state === s0IDLE)
}

object MulShiftAdd extends App {
  chisel3.Driver.execute(args, () => new MulShiftAdd())
}

object DivShiftSub extends App {
  chisel3.Driver.execute(args, () => new DivShiftSub())
}

object MulDiv extends App {
  chisel3.Driver.execute(args, () => new MulDiv())
}
