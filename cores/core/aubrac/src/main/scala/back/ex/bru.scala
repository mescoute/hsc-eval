package cowlibry.core.aubrac.back

import chisel3._
import chisel3.util._

import cowlibry.core.aubrac.tools._


class Bru extends Module {
  val io = IO(new Bundle {
    val i_lock = Input(Bool())
    val i_valid = Input(Bool())
    val i_uop = Input(UInt(EXUOP.NBIT.W))
    val i_pc = Input(UInt(32.W))
    val i_br_next = Input(new BranchBus(32))
    val i_s1 = Input(UInt(32.W))
    val i_s2 = Input(UInt(32.W))
    val i_s3 = Input(UInt(32.W))

    val o_br_info = Output(new BranchInfoBus(32))
    val o_br_new = Output(new BranchBus(32))

    val o_end = Output(Bool())
    val o_res = Output(UInt(32.W))
  })

  val w_valid = Wire(Bool())
  val w_br = Wire(Bool())
  val w_taken = Wire(Bool())
  val w_jmp = Wire(Bool())
  val w_target = Wire(UInt(32.W))
  val w_correct = Wire(UInt(32.W))

  w_valid := false.B
  w_br := false.B
  w_taken := false.B
  w_jmp := false.B

  when (io.i_valid & ((io.i_uop === EXUOP.JAL.U) | (io.i_uop === EXUOP.JALR.U))) {
    w_valid := true.B
    w_jmp := true.B
  }.elsewhen (io.i_valid & (io.i_uop === EXUOP.BEQ.U)) {
    w_valid := true.B
    w_br := true.B
    w_taken := (io.i_s1 === io.i_s2)
  }.elsewhen (io.i_valid & (io.i_uop === EXUOP.BNE.U)) {
    w_valid := true.B
    w_br := true.B
    w_taken := (io.i_s1 =/= io.i_s2)
  }.elsewhen (io.i_valid & (io.i_uop === EXUOP.BLT.U)) {
    w_valid := true.B
    w_br := true.B
    w_taken := ((io.i_s1).asSInt < (io.i_s2).asSInt)
  }.elsewhen (io.i_valid & (io.i_uop === EXUOP.BLTU.U)) {
    w_valid := true.B
    w_br := true.B
    w_taken := (io.i_s1 < io.i_s2)
  }.elsewhen (io.i_valid & (io.i_uop === EXUOP.BGE.U)) {
    w_valid := true.B
    w_br := true.B
    w_taken := ((io.i_s1).asSInt >= (io.i_s2).asSInt)
  }.elsewhen (io.i_valid & (io.i_uop === EXUOP.BGEU.U)) {
    w_valid := true.B
    w_br := true.B
    w_taken := (io.i_s1 >= io.i_s2)
  }

  w_target := Mux(w_jmp, io.i_s1 + io.i_s2, io.i_pc + io.i_s3)
  w_correct := Mux(w_jmp | (w_br & w_taken), w_target, io.i_pc + 4.U)

  io.o_end := io.i_valid
  io.o_res := io.i_pc + 4.U

  io.o_br_new.valid := w_valid & (~io.i_br_next.valid | (io.i_br_next.pc =/= w_correct)) & ~io.i_lock
  io.o_br_new.pc := Cat(w_correct(31,1), 0.U(1.W))

  io.o_br_info.valid := w_valid & ~io.i_lock
  io.o_br_info.pc := io.i_pc
  io.o_br_info.br := w_br
  io.o_br_info.taken := w_taken
  io.o_br_info.jmp := (io.i_uop === EXUOP.JAL.U)
  io.o_br_info.target := w_target
}

object Bru extends App {
  chisel3.Driver.execute(args, () => new Bru())
}
