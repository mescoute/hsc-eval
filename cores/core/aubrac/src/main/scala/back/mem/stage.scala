package cowlibry.core.aubrac.back

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._
import cowlibry.common.isa._
import cowlibry.common.rsrc._


class MemStage (useDMem: Boolean, p: BackParams, p_dmem: PmbParams) extends Module {
  val io = IO(new Bundle {
    val b_hart = Vec(p.nHart, new RsrcBus(p.nHart, p.nDome, 1))
    val b_backport = new RsrcBus(p.nHart, p.nDome, 1)

    val i_lock = Input(Bool())
    val i_wait = if (p.nBackPort > 1) Some(Input(Bool())) else None
    val o_lock = Output(Bool())

    val o_end = Output(Bool())
    val o_pend = Output(Bool())

    val i_valid = Input(Bool())
    val i_bus = Input(new MemStageBus(p))

    val b_csr = Flipped(new CsrReadBus(p))
    val b_dmem = if (useDMem) Some(new PmbReqIO(p_dmem)) else None
    val o_bypass = Output(new BypassBus(p.nHart))
    val o_rmu_wait = if (p.useDome) Some(Output(Bool())) else None

    val o_valid = Output(Bool())
    val o_bus = Output(new WbStageBus(p))
  })

  val w_valid = Wire(Bool())
  val w_lock = Wire(Bool())
  val w_flush = Wire(Bool())

  w_flush := io.b_hart(io.i_bus.info.hart).flush

  val w_hart_wait = Wire(Bool())
  if (p.nBackPort > 1) {
    w_hart_wait := io.i_wait.get
  } else {
    w_hart_wait := false.B
  }

  // ******************************
  //            CSR PORT
  // ******************************
  val w_csr_read = io.i_valid & io.i_bus.csr.read & ~w_flush
  val w_csr_wait = Mux(w_csr_read, ~io.b_csr.ready | w_lock, false.B)

  io.b_csr.valid := w_csr_read
  io.b_csr.addr := io.i_bus.data.s3(11,0)

  // ******************************
  //        MEMORY REQUEST
  // ******************************
  val w_mem_wait = Wire(Bool())

  if (useDMem) {
    io.b_dmem.get.valid := io.i_valid & (io.i_bus.mem.load | io.i_bus.mem.store) & ~w_lock & ~w_flush & ~w_hart_wait
    io.b_dmem.get.hart := io.i_bus.info.hart
    io.b_dmem.get.dome := io.b_backport.dome
    io.b_dmem.get.rw := io.i_bus.mem.store
    io.b_dmem.get.size := SIZE.B0.U
    io.b_dmem.get.addr := io.i_bus.data.res
    io.b_dmem.get.wdata := io.i_bus.data.s3

    switch (io.i_bus.mem.size) {
      is (MEM.B.U, MEM.BU.U) {
        io.b_dmem.get.size := SIZE.B1.U
      }
      is (MEM.H.U, MEM.HU.U) {
        io.b_dmem.get.size := SIZE.B2.U
      }
      is (MEM.W.U) {
        io.b_dmem.get.size := SIZE.B4.U
      }
    }

    w_mem_wait := ~io.b_dmem.get.ready(0) & io.i_valid & (io.i_bus.mem.load | io.i_bus.mem.store)
  } else {
    w_mem_wait := false.B
  }

  // ******************************
  //         CONNECT BYPASS
  // ******************************
  io.o_bypass.en := io.i_valid & io.i_bus.wb.en
  io.o_bypass.hart := io.i_bus.info.hart
  io.o_bypass.ready := ~(io.i_bus.mem.load | io.i_bus.wb.rmu | w_csr_wait)
  io.o_bypass.addr := io.i_bus.wb.addr
  io.o_bypass.data := Mux(w_csr_read, io.b_csr.data, io.i_bus.data.res)

  // ******************************
  //        RMU DEPENDENCIES
  // ******************************
  if (p.useDome) {
    io.o_rmu_wait.get := io.i_valid & io.i_bus.csr.write & ((io.i_bus.data.s3(11,0) === DOME.CSRNEXTID.U) | (io.i_bus.data.s3(11,0) === DOME.CSRNEXTPC.U) | (io.i_bus.data.s3(11,0) === DOME.CSRNEXTCAP.U))
  }

  // ******************************
  //            OUTPUTS
  // ******************************
  // ------------------------------
  //             BUS
  // ------------------------------
  val w_reg_flush = Wire(Bool())
  val reg_valid = RegInit(0.B)
  val reg_bus = Reg(new WbStageBus(p))

  w_lock := io.i_lock & reg_valid

  w_valid := io.i_valid & ~w_flush & ~w_hart_wait & ~w_mem_wait & ~w_csr_wait
  w_reg_flush := io.b_hart(reg_bus.info.hart).flush

  when (~w_valid & w_reg_flush) {
    reg_valid := false.B
  }.elsewhen (~w_lock) {
    reg_valid := io.i_valid & ~w_hart_wait & ~w_mem_wait & ~w_csr_wait
    reg_bus.info := io.i_bus.info
    reg_bus.mem := io.i_bus.mem
    reg_bus.csr := io.i_bus.csr
    reg_bus.wb := io.i_bus.wb

    reg_bus.data.s1 := io.i_bus.data.s1
    reg_bus.data.s3 := io.i_bus.data.s3
    reg_bus.data.res := Mux(w_csr_read, io.b_csr.data, io.i_bus.data.res)
  }

  io.o_valid := reg_valid
  io.o_bus := reg_bus

  // ------------------------------
  //           END & PEND
  // ------------------------------
  io.o_end := io.i_valid & io.i_bus.info.end
  io.o_pend := io.i_valid

  // ------------------------------
  //             LOCK
  // ------------------------------
  io.o_lock := w_mem_wait | w_csr_wait | (io.i_lock & reg_valid)

  // ------------------------------
  //             FREE
  // ------------------------------
  io.b_backport.free := ~reg_valid

  for (h <- 0 until p.nHart) {
    when (h.U === reg_bus.info.hart) {
      io.b_hart(h).free := ~reg_valid
    }.otherwise {
      io.b_hart(h).free := true.B
    }
  }
}

object MemStage extends App {
  chisel3.Driver.execute(args, () => new MemStage(true, BackDefault, PmbCfg0))
}
