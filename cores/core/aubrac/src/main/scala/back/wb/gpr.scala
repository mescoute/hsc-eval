package cowlibry.core.aubrac.back

import chisel3._
import chisel3.util._

import cowlibry.common.isa._


class ReadGprIO(p: GprParams) extends Bundle {
  val hart = Input(UInt(log2Ceil(p.nHart).W))
  val addr = Input(UInt(5.W))
  val data = Output(UInt(32.W))

  override def cloneType = (new ReadGprIO(p)).asInstanceOf[this.type]
}

class WriteGprIO(p : GprParams) extends Bundle {
  val hart = Input(UInt(log2Ceil(p.nHart).W))
  val en = Input(Bool())
  val addr = Input(UInt(5.W))
  val data = Input(UInt(32.W))

  override def cloneType = (new WriteGprIO(p)).asInstanceOf[this.type]
}

class Gpr(p : GprParams) extends Module {
  val io = IO(new Bundle {
    val b_rport = Vec(p.nGprRead, new ReadGprIO(p))
    val b_wport = Vec(p.nGprWrite, new WriteGprIO(p))

    val o_dbg_gpr = if (p.debug) Some(Output(Vec(p.nHart, Vec(32, UInt(32.W))))) else None
  })

  val reg_gpr = Reg(Vec(p.nHart, Vec(31, UInt(32.W))))

  // ******************************
  //             WRITE
  // ******************************
  val w_waddr = Wire(Vec(p.nGprWrite, UInt(5.W)))
  for (w <- 0 until p.nGprWrite) {
    w_waddr(w) := io.b_wport(w).addr - 1.U
  }

  for (w <- 0 until p.nGprWrite) {
    when(io.b_wport(w).en & io.b_wport(w).addr =/= RISCV.X0.U) {
      reg_gpr(io.b_wport(w).hart)(w_waddr(w)) := io.b_wport(w).data
    }
  }

  // ******************************
  //              READ
  // ******************************
  val w_raddr = Wire(Vec(p.nGprRead, UInt(5.W)))
  for (r <- 0 until p.nGprRead) {
    w_raddr(r) := io.b_rport(r).addr - 1.U
  }

  for (r <- 0 until p.nGprRead) {
    io.b_rport(r).data := 0.U
    when (io.b_rport(r).addr =/= RISCV.X0.U) {
      io.b_rport(r).data := reg_gpr(io.b_rport(r).hart)(w_raddr(r))
    }
  }

  // ******************************
  //             DEBUG
  // ******************************
  if (p.debug) {
    for (h <- 0 until p.nHart) {
      io.o_dbg_gpr.get(h)(0) := 0.U
      for (i <- 1 until 32) {
        io.o_dbg_gpr.get(h)(i) := reg_gpr(h)(i - 1)
      }
    }
  }
}

class Bypass(p: BackParams) extends Module {
  val io = IO(new Bundle {
    val i_rs = Input(Vec(p.nGprRead, UInt(32.W)))
    val b_rs = Vec(p.nGprRead, new ReadGprIO(p))

    val i_bypass = Input(Vec(p.nGprBypass, new BypassBus(p.nHart)))

    val o_wait = Output(Vec(p.nGprRead, Bool()))
  })

  // ******************************
  //       BYPASS CONNECTION
  // ******************************
  for (r <- 0 until p.nGprRead) {
    io.o_wait(r) := false.B
    io.b_rs(r).data := io.i_rs(r)
    for (b <- 0 until p.nGprBypass) {
      when (io.i_bypass(b).en === 1.B & io.i_bypass(b).addr === io.b_rs(r).addr & io.b_rs(r).addr =/= RISCV.X0.U & io.b_rs(r).hart === io.i_bypass(b).hart) {
        when (io.i_bypass(b).ready) {
          io.b_rs(r).data := io.i_bypass(b).data
          io.o_wait(r) := false.B
        }.otherwise {
          io.o_wait(r) := true.B
        }
      }
    }
  }
}

object Gpr extends App {
  chisel3.Driver.execute(args, () => new Gpr(GprDefault))
}

object Bypass extends App {
  chisel3.Driver.execute(args, () => new Bypass(BackDefault))
}
