package cowlibry.core.aubrac.back

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._
import cowlibry.common.isa._
import cowlibry.core.aubrac.tools._
import cowlibry.core.aubrac.rmu._


class WbStage (p: BackParams, p_dmem: PmbParams) extends Module {
  val io = IO(new Bundle {
    val o_lock = Output(Bool())
    val i_wait = if (p.nBackPort > 1) Some(Input(Bool())) else None

    val o_end = Output(Bool())
    val o_pend = Output(Bool())

    val i_valid = Input(Bool())
    val i_bus = Input(new WbStageBus(p))

    val b_dmem = new PmbAckIO(p_dmem)
    val b_rmu = if (p.useDome) Some(Flipped(new RmuAckIO())) else None
    val o_rmu_wait = if (p.useDome) Some(Output(Bool())) else None

    val o_bypass = Output(new BypassBus(p.nHart))
    val b_csr = Flipped(new CsrWriteBus(p))
    val b_rd = Flipped(new WriteGprIO(p))

    val b_csr_info = Flipped(new CsrWbBus(p.nHart, 1))
  })

  // ******************************
  //         MULTIPLE PORTS
  // ******************************
  val w_hart_wait = Wire(Bool())

  val reg_dmem_done = RegInit(false.B)
  val reg_dmem_rdata = Reg(UInt(32.W))

  val w_dmem_valid = Wire(Bool())
  val w_dmem_rdata = Wire(UInt(32.W))

  if (p.nBackPort > 1) {
    w_hart_wait := io.i_wait.get

    when (~reg_dmem_done) {
      reg_dmem_done := io.i_valid & (io.i_bus.mem.load | io.i_bus.mem.store) & io.b_dmem.valid & io.i_wait.get
      reg_dmem_rdata := io.b_dmem.rdata

      w_dmem_valid := io.b_dmem.valid
      w_dmem_rdata := io.b_dmem.rdata
    }.otherwise {
      reg_dmem_done := io.i_wait.get

      w_dmem_valid := true.B
      w_dmem_rdata := reg_dmem_rdata
    }
  } else {
    w_hart_wait := false.B

    w_dmem_valid := io.b_dmem.valid
    w_dmem_rdata := io.b_dmem.rdata
  }

  // ******************************
  //             MISS
  // ******************************
  val w_dmem_miss = (io.i_bus.mem.load | io.i_bus.mem.store) & io.i_valid & ~w_dmem_valid

  val w_rmu_miss = Wire(Bool())
  if (p.useDome){
    w_rmu_miss := io.i_valid & io.i_bus.wb.rmu & ~(io.b_rmu.get).valid
  } else {
    w_rmu_miss := false.B
  }

  val w_miss = w_dmem_miss | w_rmu_miss

  // ******************************
  //            RESULT
  // ******************************
  val w_res = Wire(UInt(32.W))

  // ------------------------------
  //            DEFAULT
  // ------------------------------
  w_res := io.i_bus.data.res

  // ------------------------------
  //             LOAD
  // ------------------------------
  when (io.i_bus.mem.load) {
    if (p.useDome) (io.b_rmu.get).ready := false.B
    when (io.i_bus.mem.size === MEM.B.U) {
      w_res := Cat(Fill(24, w_dmem_rdata(7)), w_dmem_rdata(7,0))
    }.elsewhen (io.i_bus.mem.size === MEM.BU.U) {
      w_res := Cat(Fill(24, 0.B), w_dmem_rdata(7,0))
    }.elsewhen (io.i_bus.mem.size === MEM.H.U) {
      w_res := Cat(Fill(16, w_dmem_rdata(15)), w_dmem_rdata(15,0))
    }.elsewhen (io.i_bus.mem.size === MEM.HU.U) {
      w_res := Cat(Fill(16, 0.B), w_dmem_rdata(15,0))
    }.elsewhen (io.i_bus.mem.size === MEM.W.U) {
      w_res := w_dmem_rdata
    }
  }

  // ------------------------------
  //              RMU
  // ------------------------------
  if (p.useDome) {
    when (io.i_bus.wb.rmu) {
      w_res := io.b_rmu.get.done
    }
  }

  // ******************************
  //            MEMORY
  // ******************************
  io.b_dmem.ready(0) := io.i_valid & (io.i_bus.mem.load | io.i_bus.mem.store) //& ~w_hart_wait

  // ******************************
  //              RMU
  // ******************************
  if (p.useDome) {
    // ------------------------------
    //          DEPENDENCIES
    // ------------------------------
    io.o_rmu_wait.get := io.i_valid & io.i_bus.csr.write & w_hart_wait & ((io.i_bus.data.s3(11,0) === DOME.CSRNEXTID.U) | (io.i_bus.data.s3(11,0) === DOME.CSRNEXTPC.U) | (io.i_bus.data.s3(11,0) === DOME.CSRNEXTCAP.U))

    // ------------------------------
    //             READY
    // ------------------------------
    (io.b_rmu.get).ready := io.i_valid & io.i_bus.wb.rmu & ~w_hart_wait
  }

  // ******************************
  //              CSR
  // ******************************
  // ------------------------------
  //             WRITE
  // ------------------------------
  io.b_csr.valid := io.i_valid & io.i_bus.csr.write //& ~w_hart_wait
  io.b_csr.addr := io.i_bus.data.s3(11,0)
  io.b_csr.uop := io.i_bus.csr.uop
  io.b_csr.data := io.i_bus.data.res
  io.b_csr.mask := io.i_bus.data.s1

  // ------------------------------
  //           INFORMATION
  // ------------------------------
  for (h <- 0 until p.nHart) {
    when (h.U === io.i_bus.info.hart) {
      io.b_csr_info.instret(h)(0) := io.i_valid & ~w_hart_wait & ~w_miss
    }.otherwise {
      io.b_csr_info.instret(h)(0) := false.B
    }
  }

  // ******************************
  //         CONNECT BYPASS
  // ******************************
  io.o_bypass.en := io.i_valid & io.i_bus.wb.en
  io.o_bypass.hart := io.i_bus.info.hart
  io.o_bypass.ready := ~w_miss
  io.o_bypass.addr := io.i_bus.wb.addr
  io.o_bypass.data := w_res

  // ******************************
  //           GPR WRITE
  // ******************************
  io.b_rd.hart := io.i_bus.info.hart
  io.b_rd.en :=  io.i_bus.wb.en & io.i_valid & ~w_hart_wait & ~w_miss
  io.b_rd.addr := io.i_bus.wb.addr
  io.b_rd.data := w_res

  // ******************************
  //           OUTPUTS
  // ******************************
  io.o_lock := w_miss

  io.o_end := io.i_valid & io.i_bus.info.end
  io.o_pend := w_miss

  if (p.debug) {
    dontTouch(io.i_bus)
  }
}

object WbStage extends App {
  chisel3.Driver.execute(args, () => new WbStage(BackDefault, PmbCfg0))
}
