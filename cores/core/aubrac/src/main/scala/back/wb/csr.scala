package cowlibry.core.aubrac.back

import chisel3._
import chisel3.util._
import scala.math._

import cowlibry.common.isa._
import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._


class CsrReadBus (p: BackParams) extends Bundle {
  val valid = Input(Bool())
  val addr = Input(UInt(12.W))
  val ready = Output(Bool())
  val data = Output(UInt(32.W))

  override def cloneType = (new CsrReadBus(p)).asInstanceOf[this.type]
}

class CsrWriteBus (p: BackParams) extends Bundle {
  val valid = Input(Bool())
  val addr = Input(UInt(12.W))
  val uop = Input(UInt(CSRUOP.NBIT.W))
  val data = Input(UInt(32.W))
  val mask = Input(UInt(32.W))

  override def cloneType = (new CsrWriteBus(p)).asInstanceOf[this.type]
}

class CsrIdBus(nHart: Int, useDome: Boolean, useMulDiv: Boolean) extends Bundle {
  val cap_muldiv = if (useDome && useMulDiv) Some(Output(Vec(nHart, Bool()))) else None

  override def cloneType = (new CsrIdBus(nHart, useDome, useMulDiv)).asInstanceOf[this.type]
}

class CsrExBus(nHart: Int, nBackPort: Int) extends Bundle {
  val branch = Input(Vec(nHart, Vec(nBackPort, Bool())))
  val mispred = Input(Vec(nHart, Vec(nBackPort, Bool())))

  override def cloneType = (new CsrExBus(nHart, nBackPort)).asInstanceOf[this.type]
}

class CsrWbBus(nHart: Int, nBackPort: Int) extends Bundle {
  val instret = Input(Vec(nHart, Vec(nBackPort, Bool())))

  override def cloneType = (new CsrWbBus(nHart, nBackPort)).asInstanceOf[this.type]
}

class CsrRmuBus extends Bundle {
  val cur = Output(new DomeConfBus())
  val work = Output(new DomeConfBus())

  val update = Input(Bool())
  val next = Input(new DomeConfBus())
}

class CsrBus extends Bundle {
  val mhartid = UInt(32.W)
  val mharten = UInt(32.W)
  val cycle = UInt(64.W)
  val time = UInt(64.W)
  val instret = UInt(64.W)
  val mispred = UInt(64.W)
}

class Csr(p: BackParams) extends Module {
  val io = IO(new Bundle {
    val b_hart = Vec(p.nHart, new RsrcBus(p.nHart, p.nDome, p.nHart))

    val b_read = Vec(p.nHart, new CsrReadBus(p))
    val b_write = Vec(p.nHart, new CsrWriteBus(p))

    val o_en = Output(Vec(p.nHart, Bool()))

    val b_id = new CsrIdBus(p.nHart, p.useDome, p.useMulDiv)
    val b_ex = new CsrExBus(p.nHart, p.nBackPort)
    val b_wb = new CsrWbBus(p.nHart, p.nBackPort)
    val b_rmu = if (p.useDome) Some(Vec(p.nHart, new CsrRmuBus())) else None

    val o_dbg_csr = if (p.debug) Some(Output(Vec(p.nHart, new CsrBus()))) else None
  })

  // ******************************
  //          COMMON CSR
  // ******************************
  // ------------------------------
  //              HART
  // ------------------------------
  val reg_mharten = RegInit((pow(2, p.nHart).toInt - 1).U(32.W))
  dontTouch(reg_mharten)

  // ******************************
  //         DUPLICATED CSR
  // ******************************
  // ------------------------------
  //            COUNTER
  // ------------------------------
  val reg_cycle = RegInit(VecInit(Seq.fill(p.nHart)(0.U(64.W))))
  val reg_time = RegInit(VecInit(Seq.fill(p.nHart)(0.U(64.W))))
  val reg_instret = RegInit(VecInit(Seq.fill(p.nHart)(0.U(64.W))))
  val reg_branch = RegInit(VecInit(Seq.fill(p.nHart)(0.U(64.W))))
  val reg_mispred = RegInit(VecInit(Seq.fill(p.nHart)(0.U(64.W))))

  // ------------------------------
  //          INFORMATION
  // ------------------------------
  val init_mhartid = Wire(Vec(p.nHart, UInt(32.W)))
  for (h <- 0 until p.nHart) {
    init_mhartid(h) := h.U
  }

  val reg_mhartid = RegInit(init_mhartid)

  // ------------------------------
  //             DOME
  // ------------------------------
  val init_dome_cur = Wire(Vec(p.nHart, new DomeConfBus()))
  init_dome_cur(0).id := 0.U
  init_dome_cur(0).pc := p.pcBoot.U
  init_dome_cur(0).cap := Cat(  0.U((32 - DOME.NCAPBIT).W),
                                if (p.useMulDiv)  1.U(1.W) else 0.U(1.W),
                                DOME.CAPWEIGHTFULL.U(DOME.NCAPWEIGHTBIT.W))
  for (h <- 1 until p.nHart) {
    init_dome_cur(h).id := 0.U
    init_dome_cur(h).pc := p.pcBoot.U
    init_dome_cur(h).cap := 0.U
  }

  val reg_dome_cur = RegInit(init_dome_cur)
  val reg_dome_work = RegInit(init_dome_cur)

  // ******************************
  //          WRITE: FORMAT
  // ******************************
  // ------------------------------
  //           COUNTER
  // ------------------------------
  val w_h_instret = Wire(Vec(p.nHart, Vec(p.nBackPort, UInt(log2Ceil(p.nBackPort + 1).W))))
  val w_h_branch = Wire(Vec(p.nHart, Vec(p.nBackPort, UInt(log2Ceil(p.nBackPort + 1).W))))
  val w_h_mispred = Wire(Vec(p.nHart, Vec(p.nBackPort, UInt(log2Ceil(p.nBackPort + 1).W))))

  for (h <- 0 until p.nHart) {
    w_h_instret(h)(0) := io.b_wb.instret(h)(0)
    w_h_branch(h)(0) := io.b_ex.branch(h)(0)
    w_h_mispred(h)(0) := io.b_ex.mispred(h)(0)

    for (bp <- 1 until p.nBackPort) {
      w_h_instret(h)(bp) := io.b_wb.instret(h)(bp) + w_h_instret(h)(bp - 1)
      w_h_branch(h)(bp) := io.b_ex.branch(h)(bp) + w_h_branch(h)(bp - 1)
      w_h_mispred(h)(bp) := io.b_ex.mispred(h)(bp) + w_h_mispred(h)(bp - 1)
    }
  }

  // ------------------------------
  //           OPERATION
  // ------------------------------
  val w_wdata = Wire(Vec(p.nHart, UInt(32.W)))

  for (h <- 0 until p.nHart) {
    w_wdata(h) := 0.U

    switch (io.b_write(h).uop) {
      is (CSRUOP.W.U) {w_wdata(h) := io.b_write(h).data}
      is (CSRUOP.S.U) {w_wdata(h) := io.b_write(h).data | io.b_write(h).mask}
      is (CSRUOP.C.U) {w_wdata(h) := (io.b_write(h).data ^ io.b_write(h).mask) & io.b_write(h).data}
    }
  }

  // ******************************
  //          WRITE: UPDATE
  // ******************************
  // ------------------------------
  //             COUNTER
  // ------------------------------
  for (h <- 0 until p.nHart) {
    reg_cycle(h) := reg_cycle(h) + 1.U
    reg_time(h) := reg_time(h) + 1.U
    when (io.b_hart(h).flush) {
      reg_instret(h) := 0.U
      reg_branch(h) := 0.U
      reg_mispred(h) := 0.U
    }.elsewhen(io.b_hart(h).valid) {
      reg_instret(h) := reg_instret(h) + w_h_instret(h)(p.nBackPort - 1)
      reg_branch(h) := reg_branch(h) + w_h_branch(h)(p.nBackPort - 1)
      reg_mispred(h) := reg_mispred(h) + w_h_mispred(h)(p.nBackPort - 1)
    }
  }

  // ------------------------------
  //              HART
  // ------------------------------
  val w_mharten = Wire(Vec(32, Bool()))
  w_mharten := reg_mharten.asBools

  for (h0 <- 0 until p.nHart) {
    when (io.b_write(h0).valid) {
      switch (io.b_write(h0).addr) {
        is (CUSTOM.CSRMHARTEN.U)  {
          for (h1 <- 0 until p.nHart) {
            when (h1.U === h0.U) {
              w_mharten(h1) := reg_mharten(h1) & w_wdata(h0)(h1)
            }.otherwise {
              w_mharten(h1) := reg_mharten(h1) | w_wdata(h0)(h1)
            }
          }
        }
      }
    }
  }

  reg_mharten := w_mharten.asUInt

  // ------------------------------
  //             DOME
  // ------------------------------
  if (p.useDome) {
    for (h <- 0 until p.nHart) {
      when (io.b_rmu.get(h).update) {
        reg_dome_cur(h) := io.b_rmu.get(h).next
      }

      when (io.b_write(h).valid) {
        switch (io.b_write(h).addr) {
          is (DOME.CSRNEXTID.U)  {reg_dome_work(h).id := w_wdata(h)}
          is (DOME.CSRNEXTPC.U)  {reg_dome_work(h).pc := w_wdata(h)}
          is (DOME.CSRNEXTCAP.U) {reg_dome_work(h).cap := w_wdata(h)}
        }
      }
    }
  }

  // ******************************
  //            READ
  // ******************************
  for (h <- 0 until p.nHart) {
    io.b_read(h).ready := ~(io.b_write(h).valid & (io.b_read(h).addr === io.b_write(h).addr))
    io.b_read(h).data := 0.U

    when (io.b_read(h).valid) {
      switch (io.b_read(h).addr) {
        is (RISCV.CSRCYCLE.U)     {io.b_read(h).data := reg_cycle(h)(31,0)}
        is (RISCV.CSRTIME.U)      {io.b_read(h).data := reg_time(h)(31,0)}
        is (RISCV.CSRINSTRET.U)   {io.b_read(h).data := reg_instret(h)(31,0)}
        is (RISCV.CSRCYCLEH.U)    {io.b_read(h).data := reg_cycle(h)(63,32)}
        is (RISCV.CSRTIMEH.U)     {io.b_read(h).data := reg_time(h)(63,32)}
        is (RISCV.CSRINSTRETH.U)  {io.b_read(h).data := reg_instret(h)(63,32)}
        is (CUSTOM.CSRBRANCH.U)   {io.b_read(h).data := reg_branch(h)(31,0)}
        is (CUSTOM.CSRMISPRED.U)  {io.b_read(h).data := reg_mispred(h)(31,0)}
        is (CUSTOM.CSRBRANCHH.U)  {io.b_read(h).data := reg_branch(h)(63,32)}
        is (CUSTOM.CSRMISPREDH.U) {io.b_read(h).data := reg_mispred(h)(63,32)}
        is (CUSTOM.CSRMHARTEN.U)  {io.b_read(h).data := reg_mharten}
        is (RISCV.CSRMHARTID.U)   {io.b_read(h).data := reg_mhartid(h)}
        is (DOME.CSRID.U)         {if (p.useDome) io.b_read(h).data := reg_dome_cur(h).id}
        is (DOME.CSRPC.U)         {if (p.useDome) io.b_read(h).data := reg_dome_cur(h).pc}
        is (DOME.CSRCAP.U)        {if (p.useDome) io.b_read(h).data := reg_dome_cur(h).cap}
        is (DOME.CSRNEXTID.U)     {if (p.useDome) io.b_read(h).data := reg_dome_work(h).id}
        is (DOME.CSRNEXTPC.U)     {if (p.useDome) io.b_read(h).data := reg_dome_work(h).pc}
        is (DOME.CSRNEXTCAP.U)    {if (p.useDome) io.b_read(h).data := reg_dome_work(h).cap}
      }
    }
  }

  // ******************************
  //             OUTPUT
  // ******************************
  if (p.useDome) {
    for (h <- 0 until p.nHart) {
      io.b_rmu.get(h).cur := reg_dome_cur(h)
      io.b_rmu.get(h).work := reg_dome_work(h)
    }
  }

  for (h <- 0 until p.nHart) {
    io.b_hart(h).free := true.B
  }

  for (h <- 0 until p.nHart) {
    if (p.useDome && p.useMulDiv) io.b_id.cap_muldiv.get(h) := reg_dome_cur(h).cap(DOME.POSCAPMULDIV)
  }

  for (h <- 0 until p.nHart) {
    io.o_en(h) := reg_mharten(h)
  }
  dontTouch(io.o_en)

  // ******************************
  //             DEBUG
  // ******************************
  if (p.debug) {
    for (h <- 0 until p.nHart) {
      io.o_dbg_csr.get(h).mhartid := reg_mhartid(h)
      io.o_dbg_csr.get(h).mharten := reg_mharten
      io.o_dbg_csr.get(h).cycle := reg_cycle(h)
      io.o_dbg_csr.get(h).time := reg_time(h)
      io.o_dbg_csr.get(h).instret := reg_instret(h)
      io.o_dbg_csr.get(h).mispred := reg_mispred(h)
    }
  }
}

object Csr extends App {
  chisel3.Driver.execute(args, () => new Csr(BackDefault))
}
