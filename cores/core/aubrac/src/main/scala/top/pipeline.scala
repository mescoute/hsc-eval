package cowlibry.core.aubrac

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._
import cowlibry.common.mem.flush._
import cowlibry.common.rsrc._
import cowlibry.core.aubrac.front._
import cowlibry.core.aubrac.back._
import cowlibry.core.aubrac.rmu._
import cowlibry.core.aubrac.tools._


class Pipeline (p: PipelineParams) extends Module {
  // ******************************
  //           PARAMETERS
  // ******************************
  // ------------------------------
  //             IMEM
  // ------------------------------
  var p_imem = new PmbIntf(1, 1, false, 32, 32 * p.nFetchInstr)

  // ------------------------------
  //             DMEM
  // ------------------------------
  var p_dmem = new PmbIntf(1, 1, false, 32, 32)

  val io = IO(new Bundle {
    val b_dome = Flipped(new DomeBus(1))
    val b_hart = Flipped(new RsrcBus(1, 1, 1))

    val b_imem = new PmbIO(p_imem)
    val b_dmem = new PmbIO(p_dmem)

    val b_flush = if (p.useMamu) Some(new FlushIO(1, 1, 32)) else None

    val o_dbg_gpr = if (p.debug) Some(Output(Vec(32, UInt(32.W)))) else None
    val o_dbg_csr = if (p.debug) Some(Output(new CsrBus())) else None
  })

  val front = Module(new Front(p, p_imem))
  val back = Module(new Back(p, p_dmem))
  val rmu = if (p.useDome) Some(Module(new Rmu(p))) else None
  val normu = if (!p.useDome) Some(Module(new NoRmu())) else None

  // ******************************
  //             FRONT
  // ******************************
  if (p.useDome) front.io.b_hart <> rmu.get.io.b_hart else front.io.b_hart <> normu.get.io.b_hart
  if (p.useDome) front.io.i_br_boot.get := rmu.get.io.o_br_boot
  front.io.i_br_new := back.io.o_br_new
  if (p.useNlp) front.io.i_br_info.get := back.io.o_br_info
  front.io.i_ready(0) := ~back.io.o_lock
  front.io.b_imem <> io.b_imem

  // ******************************
  //             BACK
  // ******************************
  if (p.useDome) back.io.b_hart <> rmu.get.io.b_hart else back.io.b_hart <> normu.get.io.b_hart

  back.io.i_br_next := front.io.o_br_next
  back.io.i_valid := front.io.o_valid(0)
  back.io.i_port.hart := 0.U
  back.io.i_port.pc := front.io.o_fetch(0).pc
  back.io.i_port.instr := front.io.o_fetch(0).instr
  back.io.b_dmem <> io.b_dmem
  if (p.useMamu) back.io.b_flush.get <> io.b_flush.get

  // ******************************
  //              RMU
  // ******************************
  if (p.useDome) {
    rmu.get.io.b_dome <> io.b_dome
    rmu.get.io.b_hart <> io.b_hart
    rmu.get.io.b_port <> back.io.b_rmu.get
    rmu.get.io.b_csr <> back.io.b_rmu_csr.get
    rmu.get.io.b_hart.free := front.io.b_hart.free & back.io.b_hart.free & io.b_hart.free
  } else {
    normu.get.io.b_dome <> io.b_dome
    normu.get.io.b_hart <> io.b_hart
  }

  // ******************************
  //             I/O
  // ******************************
  if (p.debug) io.o_dbg_gpr.get := back.io.o_dbg_gpr.get
  if (p.debug) io.o_dbg_csr.get := back.io.o_dbg_csr.get
}

object Pipeline extends App {
  chisel3.Driver.execute(args, () => new Pipeline(PipelineDefault))
}
