package cowlibry.core.aubrac

import chisel3._
import chisel3.util._


object PipelineDefault extends PipelineParams{
  def debug: Boolean = false
  def pcBoot: Int = 0x40
  def useDome: Boolean = true
  def useDomeConstFlush: Boolean = true
  def nDomeFlushCycle: Int = 3

  def nFetchInstr: Int = 1
  def useIMemSeq: Boolean = true
  def useIf1Stage: Boolean = false
  def nFetchFifoDepth: Int = 1
  def useNlp: Boolean = true
  def nBhtSet: Int = 8
  def nBhtSetEntry: Int = 128
  def nBhtBit: Int = 2
  def nBtbLine: Int = 16
  def nBtbTagBit: Int = 10

  def useMulDiv: Boolean = true
  def useFencei: Boolean = true
  def useMemStage: Boolean = true
  def useBranchReg: Boolean = true
}

object AubracDefault extends AubracParams {
  // ******************************
  //       GLOBAL PARAMETERS
  // ******************************
  def debug = false
  def pcBoot: Int = 0x40
  def useDome: Boolean = true
  def useDomeConstFlush: Boolean = true
  def nDomeFlushCycle: Int = 10

  // ******************************
  //            PIPELINE
  // ******************************
  // ------------------------------
  //             FETCH
  // ------------------------------
  def nFetchInstr: Int = 2
  def useIMemSeq: Boolean = false
  def useIf1Stage: Boolean = false
  def nFetchFifoDepth: Int = 4

  def useNlp: Boolean = false
  def nBhtSet: Int = 8
  def nBhtSetEntry: Int = 128
  def nBhtBit: Int = 2
  def nBtbLine: Int = 16
  def nBtbTagBit: Int = 10

  // ------------------------------
  //           EXECUTION
  // ------------------------------
  def useMulDiv: Boolean = false
  def useFencei: Boolean = true
  def useMemStage: Boolean = true
  def useBranchReg: Boolean = true

  // ******************************
  //           L1I CACHE
  // ******************************
  def nL1ISetReadPort: Int = 1
  def nL1ISetWritePort: Int = 1
  def nL1IWordByte: Int = 16
  def slctL1IPolicy: String = "BitPLRU"
  def nL1ISubCache: Int = 1
  def nL1ISet: Int = 4
  def nL1ILine: Int = 4
  def nL1IWord: Int = 4

  // ******************************
  //           L1D CACHE
  // ******************************
  def nL1DSetReadPort: Int = 1
  def nL1DSetWritePort: Int = 1
  def nL1DWordByte: Int = 16
  def slctL1DPolicy: String = "BitPLRU"
  def nL1DSubCache: Int = 1
  def nL1DSet: Int = 4
  def nL1DLine: Int = 4
  def nL1DWord: Int = 4
}
