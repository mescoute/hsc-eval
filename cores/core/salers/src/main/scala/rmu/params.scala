package cowlibry.core.salers.rmu

import chisel3._
import chisel3.util._
import scala.math._

import cowlibry.core.ferrandaise.rmu.{RmuParams => FerrandaiseRmuParams}

trait RmuParams extends FerrandaiseRmuParams {
  def nHart: Int
  def nDome: Int
  def useDomeConstFlush: Boolean
  def nDomeFlushCycle: Int

  def nBackPort: Int

  def nAlu: Int
  def nMulDiv: Int
}
