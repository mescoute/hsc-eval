package cowlibry.core.salers.rmu

import chisel3._
import chisel3.util._

import cowlibry.common.rsrc._


class Inclusive (nHart: Int, nDome: Int, nRsrc: Int) extends Module {
  // ******************************
  //       INTERNAL PARAMETERS
  // ******************************
  var nBlock: Int = 0
  if (nRsrc < nHart) {
    nBlock = nRsrc
  } else {
    nBlock = nHart
  }

  var nBlockRsrc = new Array[Int](nBlock)
  for (b <- 0 until nBlock) {
    if ((nRsrc % nBlock) > b) {
      nBlockRsrc(b) = (nRsrc / nBlock) + 1
    } else {
      nBlockRsrc(b) = (nRsrc / nBlock)
    }
  }

  var nBlockPort = new Array[Int](nBlock)
  nBlockPort(0) = 0
  for (b <- 1 until nBlock) {
    nBlockPort(b) = nBlockPort(b - 1) + nBlockRsrc(b - 1)
  }

  val io = IO(new Bundle {
    val b_tag = new ExcTagBus(nHart, nDome)
    val b_flush = Vec(nHart, new FlushBus())
    val b_apply = Vec(nHart, new ApplyBus())

    val b_rsrc = Flipped(Vec(nRsrc, new RsrcBus(nHart, nDome, nRsrc)))
  })

  val w_done = Wire(Bool())

  // ******************************
  //       RESOURCE REGISTERS
  // ******************************
  val init_state = Wire(Vec(nBlock, new IncStateReg(nHart, nDome)))
  for (b <- 0 until nBlock) {
    init_state(b).valid(0) := true.B
    for (h <- 1 until nHart) {
      init_state(b).valid(h) := false.B
    }
    init_state(b).flush := false.B
    init_state(b).dome := 0.U
  }

  val init_tag = Wire(Vec(nBlock, new IncTagReg(nHart)))
  for (b <- 0 until nBlock) {
    init_tag(b).al := false.B
    init_tag(b).rel := false.B
    init_tag(b).hart := DontCare
  }

  val reg_state = RegInit(init_state)
  val reg_tag = RegInit(init_tag)

  val w_state = Wire(Vec(nBlock, new IncStateReg(nHart, nDome)))
  val w_tag = Wire(Vec(nBlock, new IncTagReg(nHart)))

  reg_state := w_state
  when (w_done) {
    reg_tag := w_tag
  }

  // ******************************
  //             GLOBAL
  // ******************************
  // ------------------------------
  //          FREE RESOURCE
  // ------------------------------
  val reg_free = RegInit(false.B)

  for (b <- 0 until nBlock) {
    when (~(w_state(b).valid.asUInt.orR | w_tag(b).al)) {
      reg_free := true.B
    }
  }

  // ------------------------------
  //           ALONE USER
  // ------------------------------
  val w_alone = Wire(Vec(nBlock, Bool()))
  for (b <- 0 until nBlock) {
    val w_use = Wire(Vec(nHart, Bool()))
    for (h <- 0 until nHart) {
      when (h.U === reg_tag(b).hart) {
        w_use(h) := true.B
      }.otherwise {
        w_use(h) := ~reg_state(b).valid(h)
      }
    }
    w_alone(b) := w_use.asUInt.andR
  }

  // ******************************
  //            TAG LOGIC
  // ******************************
  w_state := reg_state

  // ------------------------------
  //            SET REL
  // ------------------------------
  val w_rel_tag = Wire(Vec(nBlock, new IncTagReg(nHart)))
  w_rel_tag := reg_tag

  val w_rel_done = Wire(Bool())

  w_rel_done := true.B
  for (b <- 0 until nBlock) {
    when (io.b_tag.rel & reg_state(b).valid(io.b_tag.hart)) {
      when (~reg_tag(b).al & ~reg_tag(b).rel) {
        w_rel_tag(b).rel := true.B
        w_rel_tag(b).hart := io.b_tag.hart
      }.otherwise {
        w_rel_done := false.B
      }
    }
  }

  // ------------------------------
  //            SET AL
  // ------------------------------
  val w_al_tag = Wire(Vec(nBlock, new IncTagReg(nHart)))
  w_al_tag := w_rel_tag

  val w_al_alone = Wire(Vec(nBlock, Bool()))
  for (b <- 0 until nBlock) {
    val w_use = Wire(Vec(nHart, Bool()))
    for (h <- 0 until nHart) {
      when (h.U === io.b_tag.hart) {
        w_use(h) := true.B
      }.otherwise {
        w_use(h) := ~reg_state(b).valid(h)
      }
    }
    w_al_alone(b) := w_use.asUInt.andR
  }

  val w_al_ok = Wire(Vec(nBlock, Bool()))
  for (b <- 0 until nBlock) {
    when ((io.b_tag.weight.asUInt === 1.U) & reg_free) {
      w_al_ok(b) := ~w_rel_tag(b).al & ~reg_state(b).valid.asUInt.orR
    }.otherwise {
      val w_al_free = ~reg_state(b).valid.asUInt.orR
      val w_al_same_dome = (reg_state(b).dome === io.b_tag.dome) & (~w_rel_tag(b).rel | (w_rel_tag(b).hart === io.b_tag.hart))
      val w_al_new_dome = w_al_alone(b) & w_rel_tag(b).rel & (w_rel_tag(b).hart === io.b_tag.hart)

      w_al_ok(b) := ~w_rel_tag(b).al & (w_al_free | w_al_same_dome | w_al_new_dome)
    }
  }

  val w_al_done = Wire(Bool())
  val w_al_weight = Wire(Vec(nBlock + 1, Vec(nHart, Bool())))

  w_al_done := w_al_ok.asUInt.orR
  w_al_weight(0) := io.b_tag.weight

  for (b <- 0 until nBlock) {
    when (io.b_tag.al & w_al_weight(b).asUInt.orR & w_al_ok(b)) {
      w_al_weight(b + 1) := (w_al_weight(b).asUInt >> 1.U).asBools

      w_al_tag(b).al := true.B
      w_al_tag(b).hart := io.b_tag.hart
      w_state(b).dome := io.b_tag.dome
    }.otherwise {
      w_al_weight(b + 1) := w_al_weight(b)
    }
  }

  w_tag := w_al_tag
  w_done := (~io.b_tag.rel | w_rel_done) & (~io.b_tag.al | w_al_done)

  // ------------------------------
  //             REMOVE
  // ------------------------------
  for (b <- 0 until nBlock) {
    when ((reg_tag(b).al | reg_tag(b).rel) & io.b_apply(reg_tag(b).hart).valid) {
      reg_tag(b).al := false.B
      reg_tag(b).rel := false.B
    }
  }

  // ******************************
  //          STATE LOGIC
  // ******************************
  // ------------------------------
  //          FLUSH & FREE
  // ------------------------------
  for (b <- 0 until nBlock) {
    when (reg_tag(b).rel & w_alone(b)) {
      reg_state(b).flush := io.b_flush(reg_tag(b).hart).flush
    }
  }

  val w_free = Wire(Vec(nBlock, Bool()))
  for (b <- 0 until nBlock) {
    val w_block_free = Wire(Vec(nBlockRsrc(b), Bool()))

    for (br <- 0 until nBlockRsrc(b)) {
      w_block_free(br) := io.b_rsrc(nBlockPort(b) + br).free
    }

    w_free(b) := reg_state(b).flush & w_block_free.asUInt.andR
  }

  for (ha <- 0 until nHart) {
    io.b_flush(ha).free := true.B
  }

  for (b <- 0 until nBlock) {
    when (reg_tag(b).rel & ~w_free(b)) {
      io.b_flush(reg_tag(b).hart).free := false.B
    }
  }

  // ------------------------------
  //             APPLY
  // ------------------------------
  for (b <- 0 until nBlock) {
    when (io.b_apply(reg_tag(b).hart).valid & io.b_apply(reg_tag(b).hart).apply) {
      when (reg_tag(b).al) {
        reg_state(b).valid(reg_tag(b).hart) := true.B
      }.elsewhen (reg_tag(b).rel) {
        reg_state(b).valid(reg_tag(b).hart) := false.B
      }
    }
  }

  // ******************************
  //             I/O
  // ******************************
  io.b_tag.done := w_done

  for (b <- 0 until nBlock) {
    for (br <- 0 until nBlockRsrc(b)) {
      io.b_rsrc(nBlockPort(b) + br).valid := reg_state(b).valid.asUInt.orR
      io.b_rsrc(nBlockPort(b) + br).flush := reg_state(b).flush
      io.b_rsrc(nBlockPort(b) + br).hart := reg_tag(b).hart
      io.b_rsrc(nBlockPort(b) + br).dome := reg_state(b).dome
      io.b_rsrc(nBlockPort(b) + br).port := 0.U
    }
  }
}

object Inclusive extends App {
  chisel3.Driver.execute(args, () => new Inclusive(2, 2, 1))
}
