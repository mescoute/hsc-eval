package cowlibry.core.salers.rmu

import chisel3._
import chisel3.util._

import cowlibry.common.rsrc._
import cowlibry.core.aubrac.rmu.{SIZE}


class Dome (nHart: Int, nDome: Int, nDomeIdBit: Int) extends Module {
  val io = IO(new Bundle {
    val b_tag = new DomeTagBus(nHart, nDome, nDomeIdBit)
    val b_flush = Vec(nHart, new FlushBus())
    val b_apply = Vec(nHart, new ApplyBus())

    val b_dome = Flipped(Vec(nDome, new DomeBus(nHart)))
  })

  val w_done = Wire(Bool())

  // ******************************
  //           REGISTERS
  // ******************************
  val init_state = Wire(Vec(nDome, new DomeStateReg(nHart, nDomeIdBit)))
  init_state(0).id := 0.U
  init_state(0).valid(0) := true.B
  for (ha <- 1 until nHart) {
    init_state(0).valid(ha) := false.B
  }
  init_state(0).flush := false.B

  for (d <- 1 until nDome) {
    init_state(d).id := DontCare
    for (ha <- 0 until nHart) {
      init_state(d).valid(ha) := false.B
    }
    init_state(d).flush := false.B
  }

  val init_tag = Wire(Vec(nDome, new DomeTagReg(nHart)))
  for (d <- 0 until nDome) {
    init_tag(d).al := false.B
    init_tag(d).rel := false.B
    init_tag(d).hart := 0.U
  }

  val reg_state = RegInit(init_state)
  val reg_tag = RegInit(init_tag)

  val w_state = Wire(Vec(nDome, new DomeStateReg(nHart, nDomeIdBit)))
  val w_tag = Wire(Vec(nDome, new DomeTagReg(nHart)))

  reg_state := w_state
  when (w_done) {
    reg_tag := w_tag
  }

  // ******************************
  //            TAG LOGIC
  // ******************************
  w_state := reg_state

  // ------------------------------
  //         PLACE REL TAG
  // ------------------------------
  val w_rel_tag = Wire(Vec(nDome, new DomeTagReg(nHart)))
  w_rel_tag := reg_tag

  val w_rel_done = Wire(Bool())

  w_rel_done := true.B
  for (d <- 0 until nDome) {
    when (io.b_tag.rel & reg_state(d).valid(io.b_tag.hart)) {
      when (~reg_tag(d).al & ~reg_tag(d).rel) {
        w_rel_tag(d).rel := true.B
        w_rel_tag(d).hart := io.b_tag.hart
      }.otherwise {
        w_rel_done := false.B
      }
    }
  }

  // ------------------------------
  //         PLACE AL TAG
  // ------------------------------
  val w_al_tag = Wire(Vec(nDome, new DomeTagReg(nHart)))
  w_al_tag := w_rel_tag

  val w_al_exist = Wire(Bool())
  val w_al_exist_free = Wire(Bool())
  val w_al_exist_place = Wire(UInt(log2Ceil(nDome).W))

  w_al_exist := false.B
  w_al_exist_free := false.B
  w_al_exist_place := 0.U

  for (d <- 0 until nDome) {
    when ((reg_state(d).id === io.b_tag.id) & reg_state(d).valid.asUInt.orR) {
      w_al_exist := true.B
      w_al_exist_free := ~(reg_tag(d).al | reg_tag(d).rel)
      w_al_exist_place := d.U
    }
  }

  val w_al_free = Wire(Vec(nDome, Bool()))
  val w_al_free_place = Wire(UInt(log2Ceil(nDome).W))

  for (d <- 0 until nDome) {
    w_al_free(d) := (~reg_state(d).valid.asUInt.orR & ~reg_tag(d).al & ~reg_tag(d).rel)
  }
  w_al_free_place := PriorityEncoder(w_al_free.asUInt)

  val w_al_done = Wire(Bool())
  val w_al_place = Wire(UInt(log2Ceil(nDome).W))

  w_al_done := (w_al_exist | w_al_exist_free) | w_al_free.asUInt.orR
  w_al_place := Mux(w_al_exist, w_al_exist_place, w_al_free_place)

  for (d <- 0 until nDome) {
    when (io.b_tag.al & (d.U === w_al_place)) {
      w_state(d).id := io.b_tag.id
      w_al_tag(d).al := true.B
      w_rel_tag(d).hart := io.b_tag.hart
    }
  }

  w_done := (~io.b_tag.al | w_al_done) & (~io.b_tag.rel | w_rel_done)
  w_tag := w_al_tag

  // ------------------------------
  //             REMOVE
  // ------------------------------
  for (d <- 0 until nDome) {
    when ((reg_tag(d).al | reg_tag(d).rel) & io.b_apply(reg_tag(d).hart).valid) {
      reg_tag(d).al := false.B
      reg_tag(d).rel := false.B
    }
  }

  // ******************************
  //           STATE LOGIC
  // ******************************
  // ------------------------------
  //              ALONE
  // ------------------------------
  val w_alone = Wire(Vec(nDome, Vec(nHart, Bool())))
  for (d <- 0 until nDome) {
    for (ha <- 0 until nHart) {
      when (ha.U === reg_tag(d).hart) {
        w_alone(d)(ha) := true.B
      }.otherwise {
        w_alone(d)(ha) := ~reg_state(d).valid(ha)
      }
    }
  }

  // ------------------------------
  //          FLUSH & FREE
  // ------------------------------
  for (ha <- 0 until nHart) {
    io.b_flush(ha).free := true.B
  }

  for (d <- 0 until nDome) {
    when (reg_tag(d).rel & w_alone(d).asUInt.andR) {
      reg_state(d).flush := io.b_flush(reg_tag(d).hart).flush
      io.b_flush(reg_tag(d).hart).free := io.b_dome(d).free & reg_state(d).flush
    }
  }

  // ------------------------------
  //             APPLY
  // ------------------------------
  for (d <- 0 until nDome) {
    when (io.b_apply(reg_tag(d).hart).valid & io.b_apply(reg_tag(d).hart).apply) {
      when (reg_tag(d).al) {
        reg_state(d).valid(reg_tag(d).hart) := true.B
      }.elsewhen (reg_tag(d).rel) {
        reg_state(d).valid(reg_tag(d).hart) := false.B
      }
    }
  }

  // ******************************
  //             I/O
  // ******************************
  io.b_tag.done := w_done
  io.b_tag.tag := w_al_place

  for (d <- 0 until nDome) {
    io.b_dome(d).valid := reg_state(d).valid.asUInt.orR
    io.b_dome(d).flush := reg_state(d).flush
    io.b_dome(d).weight := 0.U
  }
}

object Dome extends App {
  chisel3.Driver.execute(args, () => new Dome(4, 4, 32))
}
