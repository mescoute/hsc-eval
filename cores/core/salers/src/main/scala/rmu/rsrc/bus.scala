package cowlibry.core.salers.rmu

import chisel3._
import chisel3.util._


class ApplyBus extends Bundle {
  val valid = Input(Bool())
  val apply = Input(Bool())
}

class FlushBus extends Bundle {
  val flush = Input(Bool())
  val free = Output(Bool())
}

// ******************************
//             HART
// ******************************
class HartTagBus(nHart: Int, nDome: Int) extends Bundle {
  val al = Input(Bool())
  val rel = Input(Bool())
  val size = Input(UInt(2.W))
  val chart = Input(UInt(log2Ceil(nHart).W))
  val dome = Input(UInt(log2Ceil(nDome).W))

  val done = Output(Bool())
  val nhart = Output(UInt(log2Ceil(nHart).W))
  val weight = Output(UInt(log2Ceil(nHart + 1).W))

  override def cloneType = (new HartTagBus(nHart, nDome)).asInstanceOf[this.type]
}

class HartStateReg(nHart: Int, nDome: Int) extends Bundle {
  val valid = Bool()
  val lock = Bool()
  val flush = Bool()
  val hart = UInt(log2Ceil(nHart).W)
  val dome = UInt(log2Ceil(nDome).W)

  override def cloneType = (new HartStateReg(nHart, nDome)).asInstanceOf[this.type]
}

class HartTagReg extends Bundle {
  val al = Bool()
  val rel = Bool()
}

// ******************************
//             DOME
// ******************************
class DomeTagBus(nHart: Int, nDome: Int, nDomeIdBit: Int) extends Bundle {
  val al = Input(Bool())
  val rel = Input(Bool())
  val hart = Input(UInt(log2Ceil(nHart).W))
  val id = Input(UInt(nDomeIdBit.W))

  val done = Output(Bool())
  val tag = Output(UInt(log2Ceil(nDome).W))

  override def cloneType = (new DomeTagBus(nHart, nDome, nDomeIdBit)).asInstanceOf[this.type]
}

class DomeStateReg(nHart: Int, nDomeIdBit: Int) extends Bundle {
  val valid = Vec(nHart, Bool())
  val flush = Bool()
  val id = UInt(nDomeIdBit.W)

  override def cloneType = (new DomeStateReg(nHart, nDomeIdBit)).asInstanceOf[this.type]
}

class DomeTagReg(nHart: Int) extends Bundle {
  val al = Bool()
  val rel = Bool()
  val hart = UInt(log2Ceil(nHart).W)

  override def cloneType = (new DomeTagReg(nHart)).asInstanceOf[this.type]
}

// ******************************
//           EXCLUSIVE
// ******************************
class ExcTagBus(nHart: Int, nDome: Int) extends Bundle {
  val al = Input(Bool())
  val rel = Input(Bool())
  val hart = Input(UInt(log2Ceil(nHart).W))
  val dome = Input(UInt(log2Ceil(nDome).W))
  val weight = Input(Vec(nHart, Bool()))


  val done = Output(Bool())

  override def cloneType = (new ExcTagBus(nHart, nDome)).asInstanceOf[this.type]
}

class ExcStateReg(nHart: Int, nDome: Int) extends Bundle {
  val valid = Bool()
  val flush = Bool()
  val hart = UInt(log2Ceil(nHart).W)
  val dome = UInt(log2Ceil(nDome).W)

  override def cloneType = (new ExcStateReg(nHart, nDome)).asInstanceOf[this.type]
}

class ExcTagReg extends Bundle {
  val al = Bool()
  val rel = Bool()
}

// ******************************
//           INCLUSIVE
// ******************************
class IncStateReg(nHart: Int, nDome: Int) extends Bundle {
  val valid = Vec(nHart, Bool())
  val flush = Bool()
  val dome = UInt(log2Ceil(nDome).W)

  override def cloneType = (new IncStateReg(nHart, nDome)).asInstanceOf[this.type]
}

class IncTagReg(nHart: Int) extends Bundle {
  val al = Bool()
  val rel = Bool()
  val hart = UInt(log2Ceil(nHart).W)

  override def cloneType = (new IncTagReg(nHart)).asInstanceOf[this.type]
}
