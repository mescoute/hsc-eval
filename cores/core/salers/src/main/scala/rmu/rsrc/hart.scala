package cowlibry.core.salers.rmu

import chisel3._
import chisel3.util._

import cowlibry.common.rsrc._
import cowlibry.core.aubrac.rmu.{SIZE}


class Hart (nHart: Int, nDome: Int) extends Module {
  val io = IO(new Bundle {
    val b_tag = new HartTagBus(nHart, nDome)
    val b_flush = Vec(nHart, new FlushBus())
    val b_apply = Vec(nHart, new ApplyBus())

    val b_hart = Flipped(Vec(nHart, new RsrcBus(nHart, nDome, 1)))
  })

  val w_done = Wire(Bool())

  // ******************************
  //           REGISTERS
  // ******************************
  val init_state = Wire(Vec(nHart, new HartStateReg(nHart, nDome)))
  init_state(0).valid := true.B
  init_state(0).flush := false.B
  init_state(0).lock := true.B
  init_state(0).hart := 0.U
  init_state(0).dome := 0.U
  for (h <- 1 until nHart) {
    init_state(h).valid := false.B
    init_state(h).flush := false.B
    init_state(h).lock := true.B
    init_state(h).hart := 0.U
    init_state(h).dome := 0.U
  }

  val init_tag = Wire(Vec(nHart, new HartTagReg()))
  for (h <- 0 until nHart) {
    init_tag(h).al := false.B
    init_tag(h).rel := false.B
  }

  val reg_state = RegInit(init_state)
  val reg_tag = RegInit(init_tag)

  val w_state = Wire(Vec(nHart, new HartStateReg(nHart, nDome)))
  val w_tag = Wire(Vec(nHart, new HartTagReg()))

  reg_state := w_state
  when (w_done) {
    reg_tag := w_tag
  }

  // ******************************
  //            DECODER
  // ******************************
  /**
  val w_al_one = io.b_tag.al & (io.b_tag.size === SIZE.ONE.U)
  val w_al_max = io.b_tag.al & (io.b_tag.size === SIZE.MAX.U)
  val w_al_all = io.b_tag.al & (io.b_tag.size === SIZE.ALL.U)
  */

  val w_al_one = Wire(Bool())
  val w_al_max = Wire(Bool())
  val w_al_all = Wire(Bool())

  when (io.b_tag.size === SIZE.MAX.U) {
    w_al_one := false.B
    w_al_max := io.b_tag.al
    w_al_all := false.B
  }.elsewhen (io.b_tag.size === SIZE.ALL.U) {
    w_al_one := false.B
    w_al_max := false.B
    w_al_all := io.b_tag.al
  }.otherwise {
    w_al_one := io.b_tag.al
    w_al_max := false.B
    w_al_all := false.B
  }

  val w_rel_all = io.b_tag.rel

  // ******************************
  //            TAG LOGIC
  // ******************************
  w_state := reg_state

  // ------------------------------
  //         PLACE REL TAG
  // ------------------------------
  val w_rel_tag = Wire(Vec(nHart, new HartTagReg()))
  w_rel_tag := reg_tag

  val w_rel_ok = Wire(Vec(nHart, Bool()))
  for (h <- 0 until nHart) {
    w_rel_ok(h) := (reg_state(h).valid | reg_state(h).lock) & (reg_state(h).hart === io.b_tag.chart)
  }

  for (h <- 0 until nHart) {
    when (w_rel_all & w_rel_ok(h)) {
      w_rel_tag(h).rel := true.B
    }
  }

  // ------------------------------
  //         PLACE AL TAG
  // ------------------------------
  val w_al_tag = Wire(Vec(nHart, new HartTagReg()))
  w_al_tag := w_rel_tag

  val w_al_ok = Wire(Vec(nHart, Bool()))
  for (h <- 0 until nHart) {
    w_al_ok(h) := ~w_rel_tag(h).al & ((~w_rel_tag(h).rel & ~reg_state(h).valid & ~reg_state(h).lock) | (w_rel_tag(h).rel & (reg_state(h).hart === io.b_tag.chart)))
  }

  val w_al_weight = Wire(Vec(nHart + 1, UInt(log2Ceil(nHart + 1).W)))
  w_al_weight(0) := 0.U

  for (h <- 0 until nHart) {
    when (w_al_ok(h)) {
      w_al_weight(h + 1) := w_al_weight(h) + 1.U
    }.otherwise {
      w_al_weight(h + 1) := w_al_weight(h)
    }
  }

  val w_al_done = Wire(Bool())
  val w_al_hart = Wire(UInt(log2Ceil(nHart).W))

  w_al_done := Mux(w_al_all, w_al_ok.asUInt.andR, w_al_ok.asUInt.orR)
  w_al_hart := Mux(io.b_tag.rel, io.b_tag.chart, PriorityEncoder(w_al_ok.asUInt))

  for (h <- 0 until nHart) {
    when (((w_al_one & (h.U === w_al_hart)) | w_al_max | w_al_all) & w_al_ok(h)) {
      w_state(h).hart := w_al_hart
      w_state(h).dome := io.b_tag.dome
      w_al_tag(h).al := true.B
    }
  }

  w_done := ~io.b_tag.al | w_al_done
  w_tag := w_al_tag

  // ------------------------------
  //             REMOVE
  // ------------------------------
  for (h <- 0 until nHart) {
    when ((reg_tag(h).al | reg_tag(h).rel) & io.b_apply(reg_state(h).hart).valid) {
      reg_tag(h).al := false.B
      reg_tag(h).rel := false.B
    }
  }

  // ******************************
  //          STATE LOGIC
  // ******************************
  // ------------------------------
  //          FLUSH & FREE
  // ------------------------------
  for (h <- 0 until nHart) {
    reg_state(h).flush := io.b_flush(h).flush
    io.b_flush(h).free := io.b_hart(h).free
  }

  // ------------------------------
  //             APPLY
  // ------------------------------
  for (h0 <- 0 until nHart) {
    for (h1 <- 0 until nHart) {
      when (io.b_apply(h0).valid & io.b_apply(h0).apply & (h0.U === reg_state(h1).hart)) {
        when (reg_tag(h1).al) {
          reg_state(h1).valid := (h1.U === reg_state(h1).hart)
          reg_state(h1).lock := true.B
        }.elsewhen (reg_tag(h1).rel) {
          reg_state(h1).valid := false.B
          reg_state(h1).lock := false.B
        }
      }
    }
  }

  // ******************************
  //             I/O
  // ******************************
  io.b_tag.done := w_done
  io.b_tag.nhart := w_al_hart
  when (w_al_one) {
    io.b_tag.weight := 1.U
  }.elsewhen (w_al_max) {
    io.b_tag.weight := w_al_weight(nHart)
  }.elsewhen (w_al_all) {
    io.b_tag.weight := nHart.U
  }.otherwise {
    io.b_tag.weight := 0.U
  }

  for (h <- 0 until nHart) {
    io.b_hart(h).valid := reg_state(h).valid
    io.b_hart(h).flush := reg_state(h).flush
    io.b_hart(h).hart := h.U
    io.b_hart(h).dome := reg_state(h).dome
    io.b_hart(h).port := 0.U
  }
}

object Hart extends App {
  chisel3.Driver.execute(args, () => new Hart(2, 2))
}
