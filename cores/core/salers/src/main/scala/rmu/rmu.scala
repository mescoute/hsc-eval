package cowlibry.core.salers.rmu

import chisel3._
import chisel3.util._

import cowlibry.common.tools._
import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._
import cowlibry.core.aubrac.back.{CsrRmuBus}
import cowlibry.core.aubrac.rmu.{RmuIO}


class Rmu (p: RmuParams) extends Module {
  val io = IO(new Bundle {
    val b_port = Vec(p.nHart, new RmuIO())
    val b_csr = Vec(p.nHart, Flipped(new CsrRmuBus()))

    val o_br_boot = Output(Vec(p.nHart, new BranchBus(32)))

    val b_hart = Vec(p.nHart, Flipped(new RsrcBus(p.nHart, p.nDome, 1)))
    val b_dome = Vec(p.nDome, Flipped(new DomeBus(p.nHart)))
    val b_backport = Vec(p.nBackPort, Flipped(new RsrcBus(p.nHart, p.nDome, 1)))
    val b_unit_alu = Vec(p.nAlu, Flipped(new RsrcBus(1, p.nDome, p.nAlu)))
    val b_unit_muldiv = if (p.nMulDiv > 0) Some(Vec(p.nMulDiv, Flipped(new RsrcBus(1, p.nDome, p.nMulDiv)))) else None
  })

  val reqstage = Module(new ReqStage(p))
  val hartstage = Module(new HartStage(p))
  val unitstage = Module(new UnitStage(p))
  val commitstage = Module(new CommitStage(p))

  val hart = Module(new Hart(p.nHart, p.nDome))
  val dome = Module(new Dome(p.nHart, p.nDome, 32))
  val backport = Module(new Exclusive(p.nHart, p.nDome, p.nBackPort))
  val alu = if (p.nAlu != p.nBackPort) Some(Module(new Exclusive(p.nHart, p.nDome, p.nAlu))) else None
  val muldiv = if ((p.nMulDiv > 0) && (p.nMulDiv != p.nBackPort)) Some(Module(new Inclusive(p.nHart, p.nDome, p.nMulDiv))) else None

  // ******************************
  //            PIPELINE
  // ******************************
  // ------------------------------
  //            REQ STAGE
  // ------------------------------
  for (h <- 0 until p.nHart) {
    reqstage.io.i_en(h) := hart.io.b_hart(h).valid
    reqstage.io.i_lock(h) := hartstage.io.o_lock(h)
    reqstage.io.i_wait(h) := hartstage.io.o_wait(h) | unitstage.io.o_wait(h)
    reqstage.io.b_req(h) <> io.b_port(h).req
    reqstage.io.i_dome_work(h) <> io.b_csr(h).work
  }

  // ------------------------------
  //           HART STAGE
  // ------------------------------
  hartstage.io.i_lock := unitstage.io.o_lock
  hartstage.io.i_slct := reqstage.io.o_slct
  hartstage.io.i_valid := reqstage.io.o_valid
  hartstage.io.i_bus := reqstage.io.o_bus

  // ------------------------------
  //           UNIT STAGE
  // ------------------------------
  unitstage.io.i_lock := commitstage.io.o_lock
  unitstage.io.i_slct := hartstage.io.o_slct
  unitstage.io.i_valid := hartstage.io.o_valid
  unitstage.io.i_bus := hartstage.io.o_bus
  for (h <- 0 until p.nHart) {
    unitstage.io.b_ack(h) <> io.b_port(h).ack
  }

  // ------------------------------
  //          COMMIT STAGE
  // ------------------------------
  commitstage.io.i_valid := unitstage.io.o_valid
  commitstage.io.i_bus := unitstage.io.o_bus
  for (h <- 0 until p.nHart) {
    io.b_csr(h).update := commitstage.io.o_update(h)
    io.b_csr(h).next := commitstage.io.o_next(h)
  }
  io.o_br_boot := commitstage.io.o_br_boot

  // ******************************
  //           RESOURCES
  // ******************************
  // ------------------------------
  //             HART
  // ------------------------------
  hart.io.b_tag <> hartstage.io.b_hart_tag
  hart.io.b_flush <> commitstage.io.b_flush
  hart.io.b_apply <> commitstage.io.b_apply
  hart.io.b_hart <> io.b_hart

  // ------------------------------
  //             DOME
  // ------------------------------
  dome.io.b_tag <> hartstage.io.b_dome_tag
  dome.io.b_flush <> commitstage.io.b_flush
  dome.io.b_apply <> commitstage.io.b_apply
  dome.io.b_dome <> io.b_dome

  // ------------------------------
  //              ALU
  // ------------------------------
  if (p.nAlu != p.nBackPort) {
    alu.get.io.b_tag <> unitstage.io.b_alu_tag.get
    alu.get.io.b_flush <> commitstage.io.b_flush
    alu.get.io.b_apply <> commitstage.io.b_apply
    alu.get.io.b_rsrc <> io.b_unit_alu
  }

  // ------------------------------
  //             MULDIV
  // ------------------------------
  if ((p.nMulDiv > 0) && (p.nMulDiv != p.nBackPort)) {
    muldiv.get.io.b_tag <> unitstage.io.b_muldiv_tag.get
    muldiv.get.io.b_flush <> commitstage.io.b_flush
    muldiv.get.io.b_apply <> commitstage.io.b_apply
    muldiv.get.io.b_rsrc <> io.b_unit_muldiv.get
  }

  // ------------------------------
  //           BACK PORT
  // ------------------------------
  backport.io.b_tag <> unitstage.io.b_backport_tag
  backport.io.b_flush <> commitstage.io.b_flush
  backport.io.b_apply <> commitstage.io.b_apply
  if (p.nAlu == p.nBackPort) backport.io.b_rsrc <> io.b_unit_alu
  if (p.nMulDiv == p.nBackPort) backport.io.b_rsrc <> io.b_unit_muldiv.get
  backport.io.b_rsrc <> io.b_backport

  for (bp <- 0 until p.nBackPort) {
    val w_free =  Wire(Vec(3, Bool()))
    w_free(0) := io.b_backport(bp).free
    if (p.nAlu == p.nBackPort) w_free(1) := io.b_unit_alu(bp).free else w_free(1) := true.B
    if (p.nMulDiv == p.nBackPort) w_free(2) := io.b_unit_muldiv.get(bp).free else w_free(2) := true.B
    backport.io.b_rsrc(bp).free := w_free.asUInt.andR
  }


  // ------------------------------
  //          FLUSH & FREE
  // ------------------------------
  for (h <- 0 until p.nHart) {
    val w_free = Wire(Vec(6, Bool()))
    w_free(0) := hart.io.b_flush(h).free
    w_free(1) := dome.io.b_flush(h).free
    w_free(2) := backport.io.b_flush(h).free
    if (p.nAlu != p.nBackPort) w_free(3) := alu.get.io.b_flush(h).free else w_free(3) := true.B
    w_free(4) := true.B
    if ((p.nMulDiv > 0) && (p.nMulDiv != p.nBackPort)) w_free(5) := muldiv.get.io.b_flush(h).free else w_free(5) := true.B

    commitstage.io.b_flush(h).free := w_free.asUInt.andR
  }
}

object Rmu extends App {
  chisel3.Driver.execute(args, () => new Rmu(RmuDefault))
}
