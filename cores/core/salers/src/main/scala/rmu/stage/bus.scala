package cowlibry.core.salers.rmu

import chisel3._
import chisel3.util._

import cowlibry.core.aubrac.tools.{DomeConfBus}


class RmuBus (nHart: Int, nDome: Int) extends Bundle {
  val al = Bool()
  val rel = Bool()
  val done = Bool()
  val work = new DomeConfBus()
  val hart = UInt(log2Ceil(nHart).W)
  val dome = UInt(log2Ceil(nDome).W)
  val weight = UInt(log2Ceil(nHart + 1).W)

  override def cloneType = (new RmuBus(nHart, nDome)).asInstanceOf[this.type]
}
