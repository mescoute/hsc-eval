package cowlibry.core.salers.rmu

import chisel3._
import chisel3.util._

import cowlibry.common.tools._
import cowlibry.core.aubrac.tools.{DomeConfBus}
import cowlibry.core.aubrac.back.{EXUOP}
import cowlibry.core.aubrac.rmu.{RmuReqIO, RmuReqBus}


class ReqStage(p: RmuParams) extends Module {
  val io = IO(new Bundle {
    val i_en = Input(Vec(p.nHart, Bool()))

    val i_lock = Input(Vec(p.nHart, Bool()))
    val i_wait = Input(Vec(p.nHart, Bool()))

    val b_req = Vec(p.nHart, new RmuReqIO())
    val i_dome_work = Input(Vec(p.nHart, new DomeConfBus()))

    val o_slct = Output(UInt(log2Ceil(p.nHart).W))
    val o_valid = Output(Bool())
    val o_bus = Output(new RmuReqBus())
  })

  val w_lock = Wire(Vec(p.nHart, Bool()))
  val w_valid = Wire(Vec(p.nHart, Bool()))

  val reg_valid = RegInit(VecInit(Seq.fill(p.nHart)(false.B)))
  val reg_bus = Reg(Vec(p.nHart, new RmuReqBus()))

  // ******************************
  //            REGISTER
  // ******************************
  for (h <- 0 until p.nHart) {
    w_lock(h) := reg_valid(h) & io.i_lock(h)
  }

  for (h <- 0 until p.nHart) {
    w_valid(h) := io.b_req(h).valid & ~io.i_wait(h)
  }

  for (h <- 0 until p.nHart) {
    io.b_req(h).ready := ~w_lock(h)

    when (~w_lock(h)) {
      reg_valid(h) := w_valid(h)
      reg_bus(h).al := (io.b_req(h).uop === EXUOP.START.U) | (io.b_req(h).uop === EXUOP.SWITCH.U)
      reg_bus(h).rel := (io.b_req(h).uop === EXUOP.END.U) | (io.b_req(h).uop === EXUOP.SWITCH.U)
      reg_bus(h).work := io.i_dome_work(h)
    }
  }

  // ******************************
  //            OUTPUT
  // ******************************
  // ------------------------------
  //            SELECT
  // ------------------------------
  val slct = Module(new StableSlct(false, p.nHart))
  for (h <- 0 until p.nHart) {
    slct.io.i_valid(h) := io.i_en(h)
  }

  // ------------------------------
  //            REGISTER
  // ------------------------------
  for (h <- 0 until p.nHart) {
    when (~io.i_lock(h) & reg_valid(h)) {
      reg_valid(h) := false.B
    }
  }

  // ------------------------------
  //            CONNECT
  // ------------------------------
  io.o_slct := slct.io.o_slct
  io.o_valid := false.B
  io.o_bus := reg_bus(0)

  for (h <- 0 until p.nHart) {
    when (h.U === slct.io.o_slct) {
      io.o_valid := reg_valid(h)
      io.o_bus := reg_bus(h)
    }
  }
}

object ReqStage extends App {
  chisel3.Driver.execute(args, () => new ReqStage(RmuDefault))
}
