package cowlibry.core.salers.rmu

import chisel3._
import chisel3.util._

import cowlibry.common.tools._
import cowlibry.common.isa._
import cowlibry.core.aubrac.tools.{DomeConfBus}
import cowlibry.core.aubrac.back.{EXUOP}
import cowlibry.core.aubrac.rmu.{RmuAckIO, RmuReqBus}


class UnitStage (p: RmuParams) extends Module {
  val io = IO(new Bundle {
    val i_lock = Input(Vec(p.nHart, Bool()))
    val o_lock = Output(Vec(p.nHart, Bool()))
    val o_wait = Output(Vec(p.nHart, Bool()))

    val i_slct = Input(UInt(log2Ceil(p.nHart).W))
    val i_valid = Input(Bool())
    val i_bus = Input(new RmuBus(p.nHart, p.nDome))

    val b_backport_tag = Flipped(new ExcTagBus(p.nHart, p.nDome))
    val b_alu_tag = if (p.nAlu != p.nBackPort) Some(Flipped(new ExcTagBus(p.nHart, p.nDome))) else None
    val b_muldiv_tag = if ((p.nMulDiv > 0) && (p.nMulDiv != p.nBackPort)) Some(Flipped(new ExcTagBus(p.nHart, p.nDome))) else None

    val b_ack = Vec(p.nHart, new RmuAckIO())
    val o_valid = Output(Vec(p.nHart, Bool()))
    val o_bus = Output(Vec(p.nHart, new RmuBus(p.nHart, p.nDome)))
  })

  val w_lock = Wire(Vec(p.nHart, Bool()))

  // ******************************
  //              LOCK
  // ******************************
  for (h <- 0 until p.nHart) {
    when (h.U === io.i_slct) {
      io.o_lock(h) := w_lock(h)
    }.otherwise {
      io.o_lock(h) := true.B
    }
  }

  // ******************************
  //          HART & DOME
  // ******************************
  val w_valid = Wire(Bool())
  val w_done = Wire(Bool())
  val w_weight = Wire(Vec(p.nHart, Bool()))
  val w_cap_muldiv = io.i_bus.work.cap(DOME.POSCAPMULDIV)

  w_valid := io.i_valid & io.i_bus.done & ~w_lock(io.i_slct)

  for (h <- 0 until p.nHart) {
    w_weight(h) := (io.i_bus.weight > h.U)
  }

  io.b_backport_tag.al := w_valid & io.i_bus.al
  io.b_backport_tag.rel := w_valid & io.i_bus.rel
  io.b_backport_tag.hart := io.i_bus.hart
  io.b_backport_tag.dome := io.i_bus.dome
  io.b_backport_tag.weight := w_weight

  val w_alu_done = Wire(Bool())
  if (p.nAlu != p.nBackPort) {
    io.b_alu_tag.get.al := w_valid & io.i_bus.al
    io.b_alu_tag.get.rel := w_valid & io.i_bus.rel
    io.b_alu_tag.get.hart := io.i_bus.hart
    io.b_alu_tag.get.dome := io.i_bus.dome
    io.b_alu_tag.get.weight := w_weight

    w_alu_done := io.b_alu_tag.get.done
  } else {
    w_alu_done := true.B
  }

  val w_muldiv_done = Wire(Bool())
  if ((p.nMulDiv > 0) && (p.nMulDiv != p.nBackPort)) {
    io.b_muldiv_tag.get.al := w_valid & io.i_bus.al & w_cap_muldiv
    io.b_muldiv_tag.get.rel := w_valid & io.i_bus.rel
    io.b_muldiv_tag.get.hart := io.i_bus.hart
    io.b_muldiv_tag.get.dome := io.i_bus.dome
    io.b_muldiv_tag.get.weight := w_weight

    w_muldiv_done := io.b_muldiv_tag.get.done
  } else {
    w_muldiv_done := true.B
  }

  w_done := io.i_bus.done & io.b_backport_tag.done & w_alu_done & w_muldiv_done

  // ******************************
  //            REGISTER
  // ******************************
  val reg_ack_valid = RegInit(VecInit(Seq.fill(p.nHart)(false.B)))
  val reg_ack_done = Reg(Vec(p.nHart, Bool()))

  val reg_valid = RegInit(VecInit(Seq.fill(p.nHart)(false.B)))
  val reg_bus = Reg(Vec(p.nHart, new RmuBus(p.nHart, p.nDome)))

  val w_ack_lock = Wire(Vec(p.nHart, Bool()))
  val w_reg_lock = Wire(Vec(p.nHart, Bool()))

  for (h <- 0 until p.nHart) {
    w_ack_lock(h) := (reg_ack_valid(h) & ~io.b_ack(h).ready)
    w_reg_lock(h) := (reg_valid(h) & io.i_lock(h))
    w_lock(h) := w_ack_lock(h) | w_reg_lock(h)
  }

  // ------------------------------
  //              ACK
  // ------------------------------
  val w_ack_write = Wire(Bool())
  w_ack_write := ~io.i_bus.rel | ~w_done

  for (h <- 0 until p.nHart) {
    when (~w_lock(h)) {
      when (h.U === io.i_slct) {
        reg_ack_valid(h) := io.i_valid & w_ack_write
        reg_ack_done(h) := w_done
      }.otherwise {
        reg_ack_valid(h) := false.B
      }
    }.elsewhen (~w_ack_lock(h)) {
      reg_ack_valid(h) := false.B
    }
  }

  // ------------------------------
  //              BUS
  // ------------------------------
  for (h <- 0 until p.nHart) {
    when (~w_lock(h)) {
      when (h.U === io.i_slct) {
        reg_valid(h) := io.i_valid
        reg_bus(h).al := io.i_bus.al
        reg_bus(h).rel := io.i_bus.rel
        reg_bus(h).done := w_done
        reg_bus(h).work := io.i_bus.work
        reg_bus(h).hart := io.i_bus.hart
        reg_bus(h).dome := io.i_bus.dome
        reg_bus(h).weight := io.i_bus.weight
      }.otherwise {
        reg_valid(h) := false.B
      }
    }.elsewhen (~w_reg_lock(h)) {
      reg_valid(h) := false.B
    }
  }

  // ******************************
  //            OUTPUT
  // ******************************
  for (h <- 0 until p.nHart) {
    io.b_ack(h).valid := reg_ack_valid(h)
    io.b_ack(h).done := reg_ack_done(h)
  }

  for (h <- 0 until p.nHart) {
    io.o_valid(h) := reg_valid(h)
    io.o_bus(h) := reg_bus(h)
  }

  for (h <- 0 until p.nHart) {
    io.o_wait(h) := reg_ack_valid(h) | reg_valid(h)
  }
}

object UnitStage extends App {
  chisel3.Driver.execute(args, () => new UnitStage(RmuDefault))
}
