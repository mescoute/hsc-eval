package cowlibry.core.salers.rmu

import chisel3._
import chisel3.util._

import cowlibry.common.tools._
import cowlibry.common.tools.{Counter}
import cowlibry.common.isa._
import cowlibry.core.aubrac.tools.{DomeConfBus, BranchBus}
import cowlibry.core.aubrac.rmu.{RmuAckIO, RmuReqBus}


class CommitUnit (p: RmuParams) extends Module {
  val io = IO(new Bundle {
    val o_lock = Output(Bool())

    val i_valid = Input(Bool())
    val i_bus = Input(new RmuBus(p.nHart, p.nDome))

    val b_flush = Vec(p.nHart, Flipped(new FlushBus()))
    val b_apply = Vec(p.nHart, Flipped(new ApplyBus()))

    val o_update = Output(Vec(p.nHart, Bool()))
    val o_next = Output(Vec(p.nHart, new DomeConfBus()))
    val o_br_boot = Output(Vec(p.nHart, new BranchBus(32)))
  })

  // ******************************
  //     CONSTANT FLUSH COUNTER
  // ******************************
  val flush_counter = if (p.useDomeConstFlush) Some(Module(new Counter(log2Ceil(p.nDomeFlushCycle + 1)))) else None

  if (p.useDomeConstFlush) {
    flush_counter.get.io.i_init := true.B
    flush_counter.get.io.i_en := false.B
    flush_counter.get.io.i_limit := p.nDomeFlushCycle.U
  }

  // ******************************
  //              FSM
  // ******************************
  val s0NEW :: s1FLUSH :: s2UPDATE :: Nil = Enum(3)
  val reg_state = RegInit(s0NEW)

  when (reg_state === s1FLUSH) {
    io.o_lock := true.B

    if (p.useDomeConstFlush) {
      flush_counter.get.io.i_init := false.B
      flush_counter.get.io.i_en := true.B

      when (flush_counter.get.io.o_flag) {
        reg_state := s2UPDATE
      }.otherwise {
        reg_state := s1FLUSH
      }
    } else {
      when (io.b_flush(io.i_bus.hart).free) {
        reg_state := s2UPDATE
      }
    }
  }.elsewhen (reg_state === s2UPDATE) {
    io.o_lock := false.B

    reg_state := s0NEW
  }.otherwise {
    when (io.i_valid & io.i_bus.rel & io.i_bus.done) {
      io.o_lock := true.B

      reg_state := s1FLUSH
    }.elsewhen (io.i_valid & io.i_bus.al & io.i_bus.done) {
      io.o_lock := true.B

      reg_state := s2UPDATE
    }.otherwise {
      io.o_lock := false.B

      reg_state := s0NEW
    }
  }

  // ******************************
  //             FLUSH
  // ******************************
  for (h <- 0 until p.nHart) {
    when (h.U === io.i_bus.hart) {
      io.b_flush(h).flush := (reg_state === s1FLUSH) | ((reg_state === s0NEW) & io.i_valid & io.i_bus.rel & io.i_bus.done)
    }.otherwise {
      io.b_flush(h).flush := false.B
    }
  }

  // ******************************
  //            UPDATE
  // ******************************
  // ------------------------------
  //             APPLY
  // ------------------------------
  for (h <- 0 until p.nHart) {
    io.b_apply(h).valid := false.B
    io.b_apply(h).apply := false.B
  }

  for (h <- 0 until p.nHart) {
    when (h.U === io.i_bus.hart) {
      when (reg_state === s0NEW) {
        io.b_apply(h).valid := io.i_valid & ~io.i_bus.done
        io.b_apply(h).apply := false.B
      }.elsewhen (reg_state === s2UPDATE) {
        io.b_apply(h).valid := true.B
        io.b_apply(h).apply := true.B
      }
    }
  }

  // ------------------------------
  //              CSR
  // ------------------------------
  for (h <- 0 until p.nHart) {
    io.o_update(h) := (h.U === io.i_bus.hart) & (reg_state === s2UPDATE)
    io.o_next(h) := io.i_bus.work
  }

  // ------------------------------
  //             BOOT
  // ------------------------------
  for (h <- 0 until p.nHart) {
    io.o_br_boot(h).valid := (h.U === io.i_bus.hart) & (reg_state === s2UPDATE)
    io.o_br_boot(h).pc := io.i_bus.work.pc
  }
}

class CommitStage (p: RmuParams) extends Module {
  val io = IO(new Bundle {
    val o_lock = Output(Vec(p.nHart, Bool()))

    val i_valid = Input(Vec(p.nHart, Bool()))
    val i_bus = Input(Vec(p.nHart, new RmuBus(p.nHart, p.nDome)))

    val b_flush = Vec(p.nHart, Flipped(new FlushBus()))
    val b_apply = Vec(p.nHart, Flipped(new ApplyBus()))

    val o_update = Output(Vec(p.nHart, Bool()))
    val o_next = Output(Vec(p.nHart, new DomeConfBus()))
    val o_br_boot = Output(Vec(p.nHart, new BranchBus(32)))
  })

  val unit = Seq.fill(p.nHart) {Module(new CommitUnit(p))}
  for (h <- 0 until p.nHart) {
    io.o_lock(h) := unit(h).io.o_lock

    unit(h).io.i_valid := io.i_valid(h)
    unit(h).io.i_bus := io.i_bus(h)

    unit(h).io.b_flush <> io.b_flush
    unit(h).io.b_apply <> io.b_apply
  }

  for (h0 <- 0 until p.nHart) {
    for (h1 <- 0 until p.nHart) {
      when (unit(h1).io.b_flush(h0).flush) {
        io.b_flush(h0).flush := true.B
      }
    }
  }

  for (h0 <- 0 until p.nHart) {
    for (h1 <- 0 until p.nHart) {
      when (unit(h1).io.b_apply(h0).valid) {
        io.b_apply(h0) <> unit(h1).io.b_apply(h0)
      }
    }
  }


  for (h0 <- 0 until p.nHart) {
    io.o_update(h0) := false.B
    io.o_next(h0) := unit(0).io.o_next(h0)
    io.o_br_boot(h0).valid := false.B
    io.o_br_boot(h0).pc := unit(0).io.o_br_boot(h0).pc

    for (h1 <- 0 until p.nHart) {
      when (unit(h1).io.o_update(h0)) {
        io.o_update(h0) := true.B
        io.o_next(h0) := unit(h1).io.o_next(h0)
        io.o_br_boot(h0) := unit(h1).io.o_br_boot(h0)
      }
    }
  }
}

object CommitUnit extends App {
  chisel3.Driver.execute(args, () => new CommitUnit(RmuDefault))
}

object CommitStage extends App {
  chisel3.Driver.execute(args, () => new CommitStage(RmuDefault))
}
