package cowlibry.core.salers.rmu

import chisel3._
import chisel3.util._

import cowlibry.common.tools._
import cowlibry.core.aubrac.tools.{DomeConfBus}
import cowlibry.core.aubrac.back.{EXUOP}
import cowlibry.core.aubrac.rmu.{RmuReqBus}


class HartStage (p: RmuParams) extends Module {
  val io = IO(new Bundle {
    val i_lock = Input(Vec(p.nHart, Bool()))
    val o_lock = Output(Vec(p.nHart, Bool()))
    val o_wait = Output(Vec(p.nHart, Bool()))

    val i_slct = Input(UInt(log2Ceil(p.nHart).W))
    val i_valid = Input(Bool())
    val i_bus = Input(new RmuReqBus())

    val b_hart_tag = Flipped(new HartTagBus(p.nHart, p.nDome))
    val b_dome_tag = Flipped(new DomeTagBus(p.nHart, p.nDome, 32))

    val o_slct = Output(UInt(log2Ceil(p.nHart).W))
    val o_valid = Output(Bool())
    val o_bus = Output(new RmuBus(p.nHart, p.nDome))
  })

  val w_lock = Wire(Vec(p.nHart, Bool()))

  // ******************************
  //              LOCK
  // ******************************
  for (h <- 0 until p.nHart) {
    when (h.U === io.i_slct) {
      io.o_lock(h) := w_lock(h)
    }.otherwise {
      io.o_lock(h) := true.B
    }
  }

  // ******************************
  //          HART & DOME
  // ******************************
  val w_valid = Wire(Bool())

  w_valid := io.i_valid & ~w_lock(io.i_slct)

  io.b_hart_tag.al := w_valid & io.i_bus.al
  io.b_hart_tag.rel := w_valid & io.i_bus.rel
  io.b_hart_tag.size := io.i_bus.work.cap(1, 0)
  io.b_hart_tag.chart := io.i_slct
  io.b_hart_tag.dome := io.b_dome_tag.tag

  io.b_dome_tag.al := w_valid & io.i_bus.al & io.b_hart_tag.done
  io.b_dome_tag.rel := w_valid & io.i_bus.rel & io.b_hart_tag.done
  io.b_dome_tag.hart := io.b_hart_tag.nhart
  io.b_dome_tag.id := io.i_bus.work.id

  // ******************************
  //            REGISTER
  // ******************************
  val reg_slct = Reg(UInt(log2Ceil(p.nHart).W))
  val reg_valid = RegInit(VecInit(Seq.fill(p.nHart)(false.B)))
  val reg_bus = Reg(Vec(p.nHart, new RmuBus(p.nHart, p.nDome)))

  for (h <- 0 until p.nHart) {
    w_lock(h) := reg_valid(h) & io.i_lock(h)
  }

  reg_slct := io.i_slct
  for (h <- 0 until p.nHart) {
    when (~w_lock(h)) {
      when (h.U === io.i_slct) {
        reg_valid(h) := io.i_valid
        reg_bus(h).al := io.i_bus.al
        reg_bus(h).rel := io.i_bus.rel
        reg_bus(h).done := io.b_hart_tag.done & io.b_dome_tag.done
        reg_bus(h).work := io.i_bus.work
        reg_bus(h).hart := io.b_hart_tag.nhart
        reg_bus(h).dome := io.b_dome_tag.tag
        reg_bus(h).weight := io.b_hart_tag.weight
      }.otherwise {
        reg_valid(h) := false.B
      }
    }
  }

  // ******************************
  //            OUTPUT
  // ******************************
  io.o_slct := reg_slct
  io.o_valid := false.B
  io.o_bus := reg_bus(0)

  for (h <- 0 until p.nHart) {
    when (h.U === reg_slct) {
      io.o_valid := reg_valid(h)
      io.o_bus := reg_bus(h)
    }
  }

  for (h <- 0 until p.nHart) {
    io.o_wait(h) := reg_valid(h)
  }
}

object HartStage extends App {
  chisel3.Driver.execute(args, () => new HartStage(RmuDefault))
}
