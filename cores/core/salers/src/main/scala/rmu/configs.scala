package cowlibry.core.salers.rmu

import chisel3._
import scala.math._


object RmuDefault extends RmuParams {
  def nHart: Int = 2
  def nDome: Int = 2
  def useDomeConstFlush: Boolean = true
  def nDomeFlushCycle: Int = 10

  def nBackPort: Int = 2

  def nAlu: Int = 2
  def nMulDiv: Int = 1
}
