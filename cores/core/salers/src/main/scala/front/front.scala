package cowlibry.core.salers.front

import chisel3._
import chisel3.util._
import cowlibry.core.aubrac.tools._
import cowlibry.core.aubrac.front.{Front => FrontUnit}
import cowlibry.common.mem.pmb._
import cowlibry.common.rsrc._
import cowlibry.common.tools._


class Front(p : FrontParams, p_imem: PmbParams) extends Module {
  val io = IO(new Bundle {
    val b_hart = Vec(p.nHart, new RsrcBus(p.nHart, p.nDome, 1))

    val i_br_boot = if (p.useDome) Some(Input(Vec(p.nHart, new BranchBus(p.nAddrBit)))) else None
    val o_br_next = Output(Vec(p.nHart, new BranchBus(p.nAddrBit)))
    val i_br_new = Input(Vec(p.nHart, new BranchBus(p.nAddrBit)))
    val i_br_info = if (p.useNlp) Some(Vec(p.nHart, Input(new BranchInfoBus(p.nAddrBit)))) else None

    val b_imem = Vec(p.nHart, new PmbIO(p_imem))

    val i_ready = Input(Vec(p.nHart, Vec(p.nBackPort, Bool())))
    val o_valid = Output(Vec(p.nHart, Vec(p.nBackPort, Bool())))
    val o_fetch = Output(Vec(p.nHart, Vec(p.nBackPort, new FetchBus(p.nAddrBit, p.nInstrBit))))
  })

  val unit = Seq.fill(p.nHart) {Module(new FrontUnit(p, p_imem))}

  for (h <- 0 until p.nHart) {
    unit(h).io.b_hart <> io.b_hart(h)
    if (p.useDome) unit(h).io.i_br_boot.get := io.i_br_boot.get(h)
    unit(h).io.i_br_new := io.i_br_new(h)
    if (p.useNlp) {
      for (h <- 0 until p.nHart) {
        unit(h).io.i_br_info.get := io.i_br_info.get(h)
      }
    }
    unit(h).io.b_imem <> io.b_imem(h)
    unit(h).io.i_ready := io.i_ready(h)

    io.o_br_next(h) := unit(h).io.o_br_next
    io.o_valid(h) := unit(h).io.o_valid
    io.o_fetch(h) := unit(h).io.o_fetch
  }
}

object Front extends App {
  chisel3.Driver.execute(args, () => new Front(FrontDefault, PmbCfg1))
}
