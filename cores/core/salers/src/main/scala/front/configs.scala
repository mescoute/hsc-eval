package cowlibry.core.salers.front

import chisel3._


object FrontDefault extends FrontParams{
  def pcBoot: Int = 0x40
  def useDome: Boolean = false
  def nHart: Int = 2
  def nDome: Int = 2

  def nAddrBit: Int = 32
  def nInstrByte: Int = 4
  def nFetchInstr: Int = 2

  def useIMemSeq: Boolean = false
  def useIf1Stage: Boolean = false
  def nFetchFifoDepth: Int = 2
  def useNlp: Boolean = true
  def nBhtSet: Int = 8
  def nBhtSetEntry: Int = 128
  def nBhtBit: Int = 2
  def nBtbLine: Int = 16
  def nBtbTagBit: Int = 10

  def nBackPort: Int = 1
}
