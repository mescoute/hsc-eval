package cowlibry.core.salers.front

import chisel3._
import chisel3.util._
import scala.math._

import cowlibry.core.aubrac.front.{FrontParams => FrontUnitParams}


trait FrontParams extends FrontUnitParams {
  def pcBoot: Int
  def useDome: Boolean
  def nHart: Int
  def nDome: Int

  def nAddrBit: Int
  def nInstrByte: Int
  def nFetchInstr: Int

  def useIMemSeq: Boolean
  def useIf1Stage: Boolean
  def nFetchFifoDepth: Int
  def useNlp: Boolean
  def nBhtSet: Int
  def nBhtSetEntry: Int
  def nBhtBit: Int
  def nBtbLine: Int
  def nBtbTagBit: Int

  def nBackPort: Int
}
