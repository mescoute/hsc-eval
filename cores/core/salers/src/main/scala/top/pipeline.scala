package cowlibry.core.salers

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._
import cowlibry.common.mem.flush._
import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._
import cowlibry.core.aubrac.back.{CsrBus}
import cowlibry.core.salers.rmu._
import cowlibry.core.salers.front._
import cowlibry.core.ferrandaise.back._
import cowlibry.core.ferrandaise.rmu.{NoRmu}


class Pipeline(p: PipelineParams) extends Module {
  // ******************************
  //          IMEM PARAMS
  // ******************************
  var p_imem = new PmbIntf(p.nHart, p.nDome, false, 32, 32 * p.nFetchInstr)

  // ******************************
  //          DMEM PARAMS
  // ******************************
  var p_dmem = new PmbIntf(p.nHart, p.nDome, false, 32, 32)

  val io = IO(new Bundle {
    val b_dome = Vec(p.nDome, Flipped(new DomeBus(p.nHart)))
    val b_hart = Vec(p.nHart, Flipped(new RsrcBus(p.nHart, p.nDome, p.nHart)))

    val b_imem = Vec(p.nHart, new PmbIO(p_imem))
    val b_dmem = Vec(p.nHart, new PmbIO(p_dmem))

    val b_flush = if (p.useMamu) Some(Vec(p.nHart, new FlushIO(p.nHart, p.nDome, 32))) else None

    val o_dbg_gpr = if (p.debug) Some(Output(Vec(2, Vec(32, UInt(32.W))))) else None
    val o_dbg_csr = if (p.debug) Some(Output(Vec(2, new CsrBus()))) else None
  })

  val front = Module(new Front(p, p_imem))
  val scheduler = Module(new Scheduler(p))
  val back = Module(new Back(p, p_dmem))
  val rmu = if (p.useDome) Some(Module(new Rmu(p))) else None
  val normu = if (!p.useDome) Some(Module(new NoRmu(p))) else None

  // ******************************
  //             FRONT
  // ******************************
  if (p.useDome) {
    front.io.b_hart <> rmu.get.io.b_hart
    front.io.i_br_boot.get := rmu.get.io.o_br_boot
  } else {
    front.io.b_hart <> normu.get.io.b_hart
  }
  front.io.i_br_new := back.io.o_br_new
  if (p.useNlp) front.io.i_br_info.get := back.io.o_br_info
  front.io.b_imem <> io.b_imem
  front.io.i_ready := scheduler.io.o_ready

  // ******************************
  //           SCHEDULER
  // ******************************
  if (p.useDome) {
    scheduler.io.b_hart <> rmu.get.io.b_hart
    scheduler.io.b_backport <> rmu.get.io.b_backport
  } else {
    scheduler.io.b_hart <> normu.get.io.b_hart
    scheduler.io.b_backport <> normu.get.io.b_backport
  }

  /**for (h <- 0 until p.nHart) {
    scheduler.io.i_en(h) := true.B
  }*/

  scheduler.io.i_en := back.io.o_en
  scheduler.io.i_valid := front.io.o_valid
  scheduler.io.i_fetch := front.io.o_fetch
  for (bp <- 0 until p.nBackPort) {
    scheduler.io.i_ready(bp) := ~back.io.o_lock(bp)
  }

  // ******************************
  //             BACK
  // ******************************
  if (p.useDome) {
    back.io.b_hart <> rmu.get.io.b_hart
    back.io.b_backport <> rmu.get.io.b_backport
    back.io.b_unit_alu <> rmu.get.io.b_unit_alu
    if (p.nMulDiv > 0) back.io.b_unit_muldiv.get <> rmu.get.io.b_unit_muldiv.get
  } else {
    back.io.b_hart <> normu.get.io.b_hart
    back.io.b_backport <> normu.get.io.b_backport
    back.io.b_unit_alu <> normu.get.io.b_unit_alu
    if (p.nMulDiv > 0) back.io.b_unit_muldiv.get <> normu.get.io.b_unit_muldiv.get
  }

  back.io.i_valid := scheduler.io.o_valid
  back.io.i_port := scheduler.io.o_port
  back.io.i_br_next := front.io.o_br_next
  back.io.b_dmem <> io.b_dmem
  if (p.useMamu) back.io.b_flush.get <> io.b_flush.get

  // ******************************
  //              I/O
  // ******************************
  if (p.useDome) {
    io.b_dome <> rmu.get.io.b_dome
    io.b_hart <> rmu.get.io.b_hart
  } else {
    io.b_dome <> normu.get.io.b_dome
    io.b_hart <> normu.get.io.b_hart
  }

  if (p.debug) io.o_dbg_gpr.get := back.io.o_dbg_gpr.get
  if (p.debug) io.o_dbg_csr.get := back.io.o_dbg_csr.get

  // ******************************
  //              RMU
  // ******************************
  if (p.useDome) {
    rmu.get.io.b_port <> back.io.b_rmu.get
    rmu.get.io.b_csr <> back.io.b_rmu_csr.get
    for (h <- 0 until p.nHart) {
      rmu.get.io.b_hart(h).free := front.io.b_hart(h).free & back.io.b_hart(h).free & io.b_hart(h).free
    }
  }
}

object Pipeline extends App {
  chisel3.Driver.execute(args, () => new Pipeline(PipelineDefault))
}
