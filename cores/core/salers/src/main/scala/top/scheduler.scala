package cowlibry.core.salers

import chisel3._
import chisel3.util._
import scala._

import cowlibry.common.rsrc._
import cowlibry.core.aubrac.tools._


class Scheduler(p: PipelineParams) extends Module {
  require(p.nBackPort >= p.nHart, "Each hart must have at least one back-end port.")

  val io = IO(new Bundle {
    val b_hart = Vec(p.nHart, new RsrcBus(1, p.nDome, 1))
    val b_backport = Vec(p.nBackPort, new RsrcBus(p.nHart, p.nDome, 1))

    val i_en = Input(Vec(p.nHart, Bool()))

    val o_ready = Output(Vec(p.nHart, Vec(p.nBackPort, Bool())))
    val i_valid = Input(Vec(p.nHart, Vec(p.nBackPort, Bool())))
    val i_fetch = Input(Vec(p.nHart, Vec(p.nBackPort, new FetchBus(32, 32))))

    val i_ready = Input(Vec(p.nBackPort, Bool()))
    val o_valid = Output(Vec(p.nBackPort, Bool()))
    val o_port = Output(Vec(p.nBackPort, new BackPortBus(p.nHart, 32, 32)))
  })

  // ******************************
  //          HART ENABLE
  // ******************************
  val w_en = Wire(Vec(p.nHart, Bool()))
  for (h <- 0 until p.nHart) {
    w_en(h) := io.i_en(h) & io.b_hart(h).valid & ~io.b_hart(h).flush
  }

  // ******************************
  //       BACK PORT PRIORITY
  // ******************************
  /** Example 1:
  Port 0: H0 -> H1 -> H2 -> H3 (Starts H0 and increases)
  Port 1: H1 -> H0 -> H3 -> H2 (Starts H1 and decreases)
  Port 2: H2 -> H3 -> H0 -> H1 (Starts H2 and increases)
  Port 3: H3 -> H2 -> H1 -> H0 (Starts H3 and decreases)
  */

  /** Example 2:
  Port 0: H0 -> H1 -> H2 (Starts H0 and increases)
  Port 1: H1 -> H0 -> H2 (Starts H1 and decreases)
  Port 2: H2 -> H0 -> H1 (Starts H2 and increases)
  Port 3: H2 -> H1 -> H0 (Starts H2 and decreases)
  */

  val priority = Array.ofDim[Int](p.nBackPort, p.nHart)

  for (bp <- 0 until p.nBackPort) {
    // ------------------------------
    //           FIRST HART
    // ------------------------------
    var i = 0

    if (bp < p.nHart) {
      i = bp
    } else if (((bp / p.nHart) % 2) == 0) {
      i = bp % p.nHart
    } else {
      i = (p.nHart - 1) - (bp % p.nHart)
    }

    // ------------------------------
    //      INCREASE OR DECREASE
    // ------------------------------
    if ((bp % 2) == 0) {
      for (h <- 0 until p.nHart) {
        priority(bp)(h) = i

        if (i == p.nHart - 1) {
          i = 0
        } else {
          i = i + 1
        }
      }
    } else {
      for (h <- 0 until p.nHart) {
        priority(bp)(h) = i

        if (i == 0) {
          i = p.nHart - 1
        } else {
          i = i - 1
        }
      }
    }
  }

  // ******************************
  //            DEFAULT
  // ******************************
  // ------------------------------
  //         DEFAULT FETCH
  // ------------------------------
  for (h <- 0 until p.nHart) {
    for (bp <- 0 until p.nBackPort) {
      io.o_ready(h)(bp) := 0.B
    }
  }

  // ------------------------------
  //           BACK PORT
  // ------------------------------
  for (bp <- 0 until p.nBackPort) {
    io.o_valid(bp) := 0.B
    io.o_port(bp).pc := 0.U
    io.o_port(bp).hart := 0.U
    io.o_port(bp).instr := 0.U
  }

  // ******************************
  //             CONNECT
  // ******************************
  val w_bp_connect = Wire(Vec(p.nBackPort, Vec(p.nHart + 1, Bool())))

  // ------------------------------
  //     DEFAULT HART (USE DOME)
  // ------------------------------
  for (bp <- 0 until p.nBackPort) {
    if (p.useDome) {
      when (io.b_backport(bp).valid & ~io.b_backport(bp).flush & w_en(io.b_backport(bp).hart)) {
        w_bp_connect(bp)(0)                 := true.B
        io.o_ready(io.b_backport(bp).hart)(bp)  := io.i_ready(bp)

        io.o_valid(bp)      := io.i_valid(io.b_backport(bp).hart)(bp)
        io.o_port(bp).pc    := io.i_fetch(io.b_backport(bp).hart)(bp).pc
        io.o_port(bp).hart  := io.b_backport(bp).hart
        io.o_port(bp).instr := io.i_fetch(io.b_backport(bp).hart)(bp).instr
      }.otherwise {
        w_bp_connect(bp)(0)                 := false.B
      }
    } else {
      w_bp_connect(bp)(0) := false.B
    }

    // ------------------------------
    //            PRIORITY
    // ------------------------------
    for (h0 <- 0 until p.nHart) {
      val h1 = priority(bp)(h0)

      when (io.b_backport(bp).valid & ~io.b_backport(bp).flush & (io.b_hart(h1).dome === io.b_backport(bp).dome) & w_en(h1) & ~w_bp_connect(bp)(h0)) {
        w_bp_connect(bp)(h0 + 1)  := true.B
        io.o_ready(h1)(bp)        := io.i_ready(bp)

        io.o_valid(bp)      := io.i_valid(h1)(bp)
        io.o_port(bp).pc    := io.i_fetch(h1)(bp).pc
        io.o_port(bp).hart  := h1.U
        io.o_port(bp).instr := io.i_fetch(h1)(bp).instr
      }.otherwise {
        w_bp_connect(bp)(h0 + 1)  := w_bp_connect(bp)(h0)
      }
    }
  }

  // ******************************
  //              FREE
  // ******************************
  for (h <- 0 until p.nHart) {
    io.b_hart(h).free := true.B
  }

  for (bp <- 0 until p.nBackPort) {
    io.b_backport(bp).free := true.B
  }
}

object Scheduler extends App {
  chisel3.Driver.execute(args, () => new Scheduler(PipelineDefault))
}
