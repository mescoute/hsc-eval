package cowlibry.core.salers

import chisel3._
import chisel3.util._

import cowlibry.core.ferrandaise.back._
import cowlibry.core.salers.front._
import cowlibry.core.salers.rmu._


trait PipelineParams extends FrontParams
                        with BackParams
                        with RmuParams {

  override def debug: Boolean
  override def pcBoot: Int
  override def nHart: Int = 2
  override def useDome: Boolean
  override def nDome: Int = if (useDome) 2 else 1
  override def useDomeConstFlush: Boolean
  override def nDomeFlushCycle: Int
  override def nAddrBit: Int = 32
  override def nInstrByte: Int = 4

  override def nFetchInstr: Int
  override def useIMemSeq: Boolean
  override def useIf1Stage: Boolean
  override def nFetchFifoDepth: Int
  override def useNlp: Boolean
  override def nBhtSet: Int
  override def nBhtSetEntry: Int
  override def nBhtBit: Int
  override def nBtbLine: Int
  override def nBtbTagBit: Int

  override def nBackPort: Int
  override def nAlu: Int
  override def nMulDiv: Int
  override def useMulDiv: Boolean = (nMulDiv > 0)
  override def useFencei: Boolean
  override def useMemStage: Boolean = true
  override def useBranchReg: Boolean
}

trait SalersParams extends PipelineParams {
  // ******************************
  //       GLOBAL PARAMETERS
  // ******************************
  override def debug: Boolean
  override def pcBoot: Int
  override def useDome: Boolean
  override def useDomeConstFlush: Boolean
  override def nDomeFlushCycle: Int

  // ******************************
  //            PIPELINE
  // ******************************
  // ------------------------------
  //            FRONT-END
  // ------------------------------
  override def nFetchInstr: Int
  override def useIMemSeq: Boolean
  override def useIf1Stage: Boolean
  override def nFetchFifoDepth: Int

  override def useNlp: Boolean
  override def nBhtSet: Int
  override def nBhtSetEntry: Int
  override def nBhtBit: Int
  override def nBtbLine: Int
  override def nBtbTagBit: Int

  // ------------------------------
  //           BACK-END
  // ------------------------------
  override def nBackPort: Int
  override def nAlu: Int
  override def nMulDiv: Int
  override def useFencei: Boolean
  override def useMemStage: Boolean
  override def useBranchReg: Boolean

  // ******************************
  //           L1I CACHE
  // ******************************
  def nL1ISetReadPort: Int
  def nL1ISetWritePort: Int
  def nL1IWordByte: Int
  def slctL1IPolicy: String
  def nL1ISubCache: Int
  def nL1ISet: Int
  def nL1ILine: Int
  def nL1IWord: Int

  // ******************************
  //           L1D CACHE
  // ******************************
  def nL1DSetReadPort: Int
  def nL1DSetWritePort: Int
  def nL1DWordByte: Int
  def slctL1DPolicy: String
  def nL1DSubCache: Int
  def nL1DSet: Int
  def nL1DLine: Int
  def nL1DWord: Int
}
