package cowlibry.core.salers

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._
import cowlibry.mem.manseng._
import cowlibry.core.aubrac.back.{CsrBus}


class Salers(p: SalersParams) extends Module {
  // ******************************
  //           PARAMETERS
  // ******************************
  // ------------------------------
  //             IMEM
  // ------------------------------
  var p_imem = new PmbIntf(p.nHart, p.nDome, true, 32, p.nL1IWordByte * 8)

  // ------------------------------
  //            ICACHE
  // ------------------------------
  val p_l1i = new MansengIntf(
    p.debug,                        // debug
    32,                             // nAddrBit
    p.nHart,                        // nHart
    if (p.useMamu) p.nHart else 0,  // nFlush
    p.nDome,                        // nDome
    false,                          // multiPrevDome
    true,                           // usePartWay
    false,                          // useMpuReg
    4 * p.nFetchInstr,              // nPrevWordByte
    p.nL1IWordByte,                 // nNextWordByte
    2,                              // nPrevMem
    1,                              // nPrevReqFifoDepth
    false,                          // usePrevReqReg
    false,                          // usePrevReadReg
    2,                              // nNextReqFifoDepth
    2,                              // nNextMemFifoDepth
    2,                              // nNextWriteFifoDepth
    p.nL1ISetReadPort,              // nSetReadPort
    p.nL1ISetWritePort,             // nSetWritePort
    p.slctL1IPolicy,                // slctPolicy
    p.nL1ISubCache,                 // nSubCache
    p.nL1ISet,                      // nSet
    p.nL1ILine,                     // nLine
    p.nL1IWord                      // nWord
  )

  // ------------------------------
  //             DMEM
  // ------------------------------
  var p_dmem = new PmbIntf(p.nHart, p.nDome, true, 32, p.nL1DWordByte * 8)

  // ------------------------------
  //            DCACHE
  // ------------------------------
  val p_l1d = new MansengIntf(
    p.debug,            // debug
    32,                 // nAddrBit
    p.nHart,            // nHart
    0,                  // nFlush
    p.nDome,            // nDome
    false,              // multiPrevDome
    true,               // usePartWay
    false,              // useMpuReg
    4,                  // nPrevWordByte
    p.nL1DWordByte,     // nNextWordByte
    2,                  // nPrevMem
    1,                  // nPrevReqFifoDepth
    false,              // usePrevReqReg
    false,              // usePrevReadReg
    2,                  // nNextReqFifoDepth
    2,                  // nNextMemFifoDepth
    2,                  // nNextWriteFifoDepth
    p.nL1DSetReadPort,  // nSetReadPort
    p.nL1DSetWritePort, // nSetWritePort
    p.slctL1DPolicy,    // slctPolicy
    p.nL1DSubCache,     // nSubCache
    p.nL1DSet,          // nSet
    p.nL1DLine,         // nLine
    p.nL1DWord          // nWord
  )

  // ******************************
  //              I/O
  // ******************************
  val io = IO(new Bundle {
    val b_imem = new PmbIO(p_imem)
    val b_dmem = new PmbIO(p_dmem)

    val o_dbg_gpr = if (p.debug) Some(Output(Vec(2, Vec(32, UInt(32.W))))) else None
    val o_dbg_csr = if (p.debug) Some(Output(Vec(2, new CsrBus()))) else None
  })

  val pipe = Module(new Pipeline(p))
  val l1icache = Module(new Manseng(p_l1i))
  val l1dcache = Module(new Manseng(p_l1d))

  // ******************************
  //             MODULES
  // ******************************
  // ------------------------------
  //           L1I CACHE
  // ------------------------------
  for (ha <- 0 until p.nHart) {
    l1icache.io.b_hart(ha) <> pipe.io.b_hart(ha)
    l1icache.io.b_prevmem(ha) <> pipe.io.b_imem(ha)
  }
  for (hs <- 0 until p.nDome) {
    l1icache.io.i_hier_flush(hs) := true.B
    l1icache.io.b_dome(hs) <> pipe.io.b_dome(hs)
  }
  l1icache.io.b_nextmem <> io.b_imem

  // ------------------------------
  //           L1D CACHE
  // ------------------------------
  for (ha <- 0 until p.nHart) {
    l1dcache.io.b_hart(ha) <> pipe.io.b_hart(ha)
    l1dcache.io.b_prevmem(ha) <> pipe.io.b_dmem(ha)
  }
  for (hs <- 0 until p.nDome) {
    l1dcache.io.i_hier_flush(hs) := true.B
    l1dcache.io.b_dome(hs) <> pipe.io.b_dome(hs)
  }
  l1dcache.io.b_nextmem <> io.b_dmem

  // ------------------------------
  //            PIPELINE
  // ------------------------------
  for (ha <- 0 until p.nHart) {
    pipe.io.b_hart(ha).free := l1icache.io.b_hart(ha).free & l1dcache.io.b_hart(ha).free
  }
  for (hs <- 0 until p.nDome) {
    pipe.io.b_dome(hs).free := l1icache.io.b_dome(hs).free & l1dcache.io.b_dome(hs).free
  }

  if (p.debug) io.o_dbg_gpr.get := pipe.io.o_dbg_gpr.get
  if (p.debug) io.o_dbg_csr.get := pipe.io.o_dbg_csr.get

  // ******************************
  //             FLUSH
  // ******************************
  if (p.useMamu) {
    for (h <- 0 until p.nHart) {
      pipe.io.b_flush.get(h) <> l1icache.io.b_flush(h)
    }
  }
}

object Salers extends App {
  chisel3.Driver.execute(args, () => new Salers(SalersDefault))
}
