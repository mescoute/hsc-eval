package cowlibry.core.salers

import scala._
import chisel3._
import chisel3.util._
import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

import cowlibry.common.initram._
import cowlibry.common.rsrc._
import cowlibry.core.aubrac.back.{CsrBus}


class SalersTop(p: SalersParams) extends Module {
  // ******************************
  //          RAM PARAMS
  // ******************************
  val nRamByte: Int = 65536
  var p_ram = new InitRamIntf(nRamByte, p.nHart, p.nDome, true, log2Ceil(nRamByte), 8 * p.nL1DWordByte)

  val io = IO(new Bundle {
    val o_dbg_gpr = if (p.debug) Some(Output(Vec(2, Vec(32, SInt(32.W))))) else None
    val o_dbg_csr = if (p.debug) Some(Output(Vec(2, new CsrBus()))) else None
  })

  val core = Module(new Salers(p))
  val ram = Module(new InitRam(p_ram))

  core.io.b_imem <> ram.io.b_port(0)
  core.io.b_dmem <> ram.io.b_port(1)

  if (p.debug) {
    for (h <- 0 until 2) {
      for (g <- 0 until 32) {
        io.o_dbg_gpr.get(h)(g) := core.io.o_dbg_gpr.get(h)(g).asSInt
      }
      io.o_dbg_csr.get(h) := core.io.o_dbg_csr.get(h)
    }
  }
}
