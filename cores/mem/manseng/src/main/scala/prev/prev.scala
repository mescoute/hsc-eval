package cowlibry.mem.manseng.prev

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._
import cowlibry.common.mem.flush._
import cowlibry.common.tools.{StableSlct}
import cowlibry.common.rsrc._
import cowlibry.mem.manseng.tools._
import cowlibry.mem.manseng.cache._
import cowlibry.mem.manseng.next.{NextPrevReqBus, NextWriteFireBus}


class PrevPipeline(p: PrevParams, p_prev: PmbParams) extends Module {
  require(p.nDome > 0, "At least one DOME is necessary.")

  val io = IO(new Bundle {
    val b_dome = Vec(p.nDome, new DomeBus(1))
    val b_flush = Vec(p.nFlush, Flipped(new FlushIO(p.nHart, p.nDome, p.nAddrBit)))

    val b_mem = Flipped(new PmbIO(p_prev))

    val b_ctrl = Flipped(new CacheAccBus(p))
    val b_read = Flipped(new CacheReadBus(p))
    val b_write = Flipped(new CacheWriteBus(p))
    val o_pend = Vec(p.nPrevPendingAcc, Flipped(new CachePendBus(p)))
    val o_miss = Output(Vec(p.nHart, Bool()))

    val b_next = Flipped(new NextPrevReqBus(p.nHart, p.nDome, p.nNext, p.nAddrBit, p.nTagBit, p.nSet, p.nLineByte, p.nPrevWordBit))
    val b_wfire = Flipped(new NextWriteFireBus(p.nNext))
  })

  val req = Module(new ReqStage(p, p_prev))
  val init = Module(new InitStage(p))
  val read = Module(new ReadStage(p))
  val write = Module(new WriteStage(p, p_prev))

  // ******************************
  //           PIPELINE
  // ******************************
  // ------------------------------
  //           REQ STAGE
  // ------------------------------
  req.io.b_dome <> io.b_dome
  req.io.b_flush <> io.b_flush
  req.io.i_lock := init.io.o_lock
  req.io.b_mem <> io.b_mem.req

  // ------------------------------
  //          INIT STAGE
  // ------------------------------
  init.io.b_dome <> io.b_dome
  init.io.b_flush <> io.b_flush
  init.io.i_lock := read.io.o_lock
  init.io.i_slct := req.io.o_slct
  init.io.i_valid := req.io.o_valid
  init.io.i_bus := req.io.o_bus
  init.io.b_ctrl <> io.b_ctrl
  init.io.b_next <> io.b_next
  io.o_miss := init.io.o_miss

  // ------------------------------
  //          READ STAGE
  // ------------------------------
  read.io.b_dome <> io.b_dome
  read.io.b_flush <> io.b_flush
  read.io.i_lock := write.io.o_lock
  read.io.i_slct := init.io.o_slct
  read.io.i_valid := init.io.o_valid
  read.io.i_bus := init.io.o_bus
  read.io.b_read <> io.b_read.req

  // ------------------------------
  //          WRITE STAGE
  // ------------------------------
  write.io.i_slct := read.io.o_slct
  write.io.i_valid := read.io.o_valid
  write.io.i_bus := read.io.o_bus
  write.io.b_read <> io.b_read.ack
  write.io.b_write <> io.b_write
  write.io.b_wfire <> io.b_wfire
  write.io.b_mem <> io.b_mem.ack

  // ******************************
  //            PENDING
  // ******************************
  for (pv <- 0 until p.nPrev) {
    io.o_pend(pv) := read.io.o_pend(pv)
  }

  if (p.usePrevReadReg) {
    for (pv <- 0 until p.nPrev) {
      io.o_pend(p.nPrev + pv) := init.io.o_pend.get(pv)
    }
  }

  // ******************************
  //             DOME
  // ******************************
  for (d <- 0 until p.nDome) {
    io.b_dome(d).free := req.io.b_dome(d).free & init.io.b_dome(d).free & read.io.b_dome(d).free
  }

  // ******************************
  //             FLUSH
  // ******************************
  for (f <- 0 until p.nFlush) {
    io.b_flush(f).ready := req.io.b_flush(f).ready & init.io.b_flush(f).ready & read.io.b_flush(f).ready
  }
}

class PrevCtrl(p: PrevParams, p_prev: PmbParams) extends Module {
  val io = IO(new Bundle {
    val b_dome = Vec(p.nDome, new DomeBus(1))
    val b_flush = Vec(p.nFlush, Flipped(new FlushIO(p.nHart, p.nDome, p.nAddrBit)))
    val b_mem = Vec(p.nPrevMem, Flipped(new PmbIO(p_prev)))

    val b_ctrl = Vec(p.nPrevMem, Flipped(new CacheAccBus(p)))
    val b_read = Vec(p.nPrevMem, Flipped(new CacheReadBus(p)))
    val b_write = Vec(p.nPrevMem, Flipped(new CacheWriteBus(p)))
    val o_pend = Vec(p.nPendingAcc, Flipped(new CachePendBus(p)))
    val o_miss = Output(Vec(p.nHart, UInt(log2Ceil(p.nPrevMem + 1).W)))

    val b_next = Vec(p.nPrevMem, Flipped(new NextPrevReqBus(p.nHart, p.nDome, p.nNext, p.nAddrBit, p.nTagBit, p.nSet, p.nLineByte, p.nPrevWordBit)))
    val b_wfire = Vec(p.nPrevMem, Flipped(new NextWriteFireBus(p.nNext)))
  })

  val pipe = Seq.fill(p.nPrevMem) {Module(new PrevPipeline(p, p_prev))}

  // ******************************
  //            PIPELINE
  // ******************************
  for (pm <- 0 until p.nPrevMem) {
    pipe(pm).io.b_dome <> io.b_dome
    pipe(pm).io.b_flush <> io.b_flush
    pipe(pm).io.b_mem <> io.b_mem(pm)
    pipe(pm).io.b_ctrl <> io.b_ctrl(pm)
    pipe(pm).io.b_read <> io.b_read(pm)
    pipe(pm).io.b_write <> io.b_write(pm)
    pipe(pm).io.b_next <> io.b_next(pm)
    pipe(pm).io.b_wfire <> io.b_wfire(pm)
  }

  // ******************************
  //            PENDING
  // ******************************
  for (pm <- 0 until p.nPrevMem) {
    for (pva <- 0 until p.nPrevPendingAcc) {
      io.o_pend(pm * p.nPrevPendingAcc + pva) := pipe(pm).io.o_pend(pva)
    }
  }

  // ******************************
  //             MISS
  // ******************************
  for (h <- 0 until p.nHart) {
    val w_miss = Wire(Vec(p.nPrevMem, UInt(log2Ceil(p.nPrevMem + 1).W)))
    w_miss(0) := pipe(0).io.o_miss(h)

    for (pm <- 1 until p.nPrevMem) {
      w_miss(pm) := w_miss(pm - 1) + pipe(pm).io.o_miss(h)
    }

    io.o_miss(h) := w_miss(p.nPrevMem - 1)
  }

  // ******************************
  //             DOME
  // ******************************
  for (d <- 0 until p.nDome) {
    val w_free = Wire(Vec(p.nPrevMem, Bool()))

    for (pm <- 0 until p.nPrevMem) {
      w_free(pm) := pipe(pm).io.b_dome(d).free
    }

    io.b_dome(d).free := w_free.asUInt.andR
  }

  // ******************************
  //             FLUSH
  // ******************************
  for (f <- 0 until p.nFlush) {
    val w_ready = Wire(Vec(p.nPrevMem, Bool()))

    for (pm <- 0 until p.nPrevMem) {
      w_ready(pm) := pipe(pm).io.b_flush(f).ready
    }

    io.b_flush(f).ready := w_ready.asUInt.andR
  }
}

object PrevPipeline extends App {
  chisel3.Driver.execute(args, () => new PrevPipeline(PrevDefault, PmbCfg0))
}

object PrevCtrl extends App {
  chisel3.Driver.execute(args, () => new PrevCtrl(PrevDefault, PmbCfg0))
}
