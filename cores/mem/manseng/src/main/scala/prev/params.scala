package cowlibry.mem.manseng.prev

import chisel3._
import chisel3.util._
import scala.math._

import cowlibry.mem.manseng.cache._


trait PrevParams extends CacheParams {
  def debug: Boolean
  def nAddrBit: Int
  def nDome: Int
  def nHart: Int
  def nFlush: Int
  def usePartWay: Boolean

  def nPrevMem: Int
  def multiPrevDome: Boolean
  def nPrev: Int = {
    if (nDome > 1 && multiPrevDome) {
      return nDome
    } else {
      return 1
    }
  }
  def nPrevWordByte: Int
  def nPrevWordBit: Int = nPrevWordByte * 8
  def nPrevReqFifoDepth: Int
  def usePrevReqReg: Boolean
  def usePrevReadReg: Boolean

  def nNext: Int = nDome

  override def nAccess: Int = nPrevMem
  def nSetReadPort: Int
  def nSetWritePort: Int
  def slctPolicy: String
  def nPrevPendingAcc: Int = {
    if (usePrevReadReg) {
      return nPrev * 2
    } else {
      return nPrev
    }
  }
  override def nPendingAcc: Int = nPrevPendingAcc * nPrevMem

  def nSubCache: Int
  def nSet: Int
  def nLine: Int
  def nWord: Int
  def nWordByte: Int
  def nTagBit: Int = nAddrBit - log2Ceil(nSet) - log2Ceil(nLineByte)
}
