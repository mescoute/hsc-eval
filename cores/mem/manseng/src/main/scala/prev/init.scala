package cowlibry.mem.manseng.prev

import chisel3._
import chisel3.util._

import cowlibry.common.tools._
import cowlibry.common.rsrc._
import cowlibry.common.mem.pmb._
import cowlibry.common.mem.flush._
import cowlibry.mem.manseng.tools._
import cowlibry.mem.manseng.cache._
import cowlibry.mem.manseng.next.{NextPrevReqBus}


class InitStage(p: PrevParams) extends Module {
  val io = IO(new Bundle {
    val b_dome = Vec(p.nDome, new DomeBus(1))
    val b_flush = Vec(p.nFlush, Flipped(new FlushIO(p.nHart, p.nDome, p.nAddrBit)))

    val i_lock = Input(Bool())
    val o_lock = Output(Bool())

    val i_slct = Input(UInt(log2Ceil(p.nPrev).W))
    val i_valid = Input(Bool())
    val i_bus = Input(new PrevReqBus(p))

    val b_ctrl = Flipped(new CacheAccBus(p))
    val b_next = Flipped(new NextPrevReqBus(p.nHart, p.nDome, p.nNext, p.nAddrBit, p.nTagBit, p.nSet, p.nLineByte, p.nPrevWordBit))
    val o_miss = Output(Vec(p.nHart, Bool()))

    val o_pend = if (p.usePrevReadReg) Some(Vec(p.nPrev, Flipped(new CachePendBus(p)))) else None
    val o_slct = Output(UInt(log2Ceil(p.nPrev).W))
    val o_valid = Output(Bool())
    val o_bus = Output(new PrevBus(p))
  })

  val s0NEW :: s1REP :: Nil = Enum(2)
  val reg_state = RegInit(VecInit(Seq.fill(p.nPrev)(s0NEW)))
  val w_cstate = Wire(UInt(2.W))
  val w_nstate = Wire(UInt(2.W))

  val w_next_wait = Wire(Bool())
  val w_lock = Wire(Bool())
  val w_wait = Wire(Bool())

  val w_dome = Wire(UInt(log2Ceil(p.nDome).W))
  val w_valid = Wire(Bool())
  val w_bus = Wire(new PrevBus(p))

  val init_slct = Wire(UInt(log2Ceil(p.nPrev).W))
  init_slct := 0.U

  val reg_slct = RegInit(init_slct)
  val reg_valid = RegInit(VecInit(Seq.fill(p.nPrev)(false.B)))
  val reg_bus = Reg(Vec(p.nPrev, new PrevBus(p)))

  // ******************************
  //  SELECT CURRENT AND NEW VALUE
  // ******************************
  // ------------------------------
  //             DOME
  // ------------------------------
  if (p.multiPrevDome) w_dome := io.i_slct else w_dome := io.i_bus.dome

  // ------------------------------
  //             FSM
  // ------------------------------
  w_cstate := reg_state(0)
  for (pv <- 0 until p.nPrev) {
    when (pv.U === io.i_slct) {
      w_cstate := reg_state(pv)
      reg_state(pv) := w_nstate
    }
  }

  // ------------------------------
  //              LOCK
  // ------------------------------
  if (p.nPrev > 1 && p.usePrevReadReg) {
    w_lock := true.B
    for (pv <- 0 until p.nPrev) {
      when (pv.U === io.i_slct) {
        when (io.i_slct === io.o_slct) {
          w_lock := io.i_lock & reg_valid(pv)
        }.otherwise {
          w_lock := reg_valid(pv)
        }
      }
    }
  } else if (p.usePrevReadReg) {
    w_lock := io.i_lock & reg_valid(0)
  } else {
    w_lock := io.i_lock
  }

  // ------------------------------
  //       WAIT NEXT PIPELINE
  // ------------------------------
  w_next_wait := false.B
  for (d <- 0 until p.nDome) {
    when (d.U === w_dome) {
      when (w_cstate === s1REP) {
        w_next_wait := ~io.b_next.ready(d) & w_bus.rw
      }.otherwise {
        w_next_wait := ~io.b_next.ready(d) & (~io.b_ctrl.ready | w_bus.rw)
      }
    }
  }

  // ******************************
  //             LOGIC
  // ******************************
  // ------------------------------
  //              FSM
  // ------------------------------
  when (w_cstate === s1REP) {
    when (io.b_ctrl.ready & ~w_lock & ~w_next_wait) {
      w_wait := false.B
      w_nstate := s0NEW
    }.otherwise {
      w_wait := true.B
      w_nstate := s1REP
    }
  }.otherwise {
    when (io.i_valid & ~io.b_ctrl.ready & ~w_next_wait) {
      w_wait := true.B
      w_nstate := s1REP
    }.otherwise {
      w_wait := w_lock | w_next_wait
      w_nstate := s0NEW
    }
  }

  // ------------------------------
  //           CTRL SET
  // ------------------------------
  io.b_ctrl.valid := io.i_valid
  io.b_ctrl.dome := w_dome
  io.b_ctrl.tag := io.i_bus.tag
  io.b_ctrl.set := io.i_bus.set
  io.b_ctrl.offset := io.i_bus.offset

  // ------------------------------
  //         NEXT PIPELINE
  // ------------------------------
  when (w_cstate === s1REP) {
    io.b_next.valid := io.b_ctrl.ready & io.i_bus.rw & ~w_lock
  }.otherwise {
    io.b_next.valid := io.i_valid & (~io.b_ctrl.ready | (io.i_bus.rw & ~w_lock))
  }
  io.b_next.hart := io.i_bus.hart
  io.b_next.dome := w_dome
  io.b_next.rep := ~io.b_ctrl.ready
  io.b_next.size := io.i_bus.size
  io.b_next.addr := io.i_bus.addr
  io.b_next.tag := io.i_bus.tag
  io.b_next.set := io.i_bus.set
  io.b_next.offset := io.i_bus.offset
  io.b_next.wdata := w_bus.data

  // ------------------------------
  //              BUS
  // ------------------------------
  w_valid := io.i_valid & io.b_ctrl.ready & ~w_next_wait
  w_bus.dome := io.i_bus.dome
  w_bus.hart := io.i_bus.hart
  w_bus.rw := io.i_bus.rw
  w_bus.size := io.i_bus.size
  w_bus.addr := io.i_bus.addr
  w_bus.set := io.i_bus.set
  w_bus.offset := io.i_bus.offset
  w_bus.tag := io.i_bus.tag
  w_bus.subcache := io.b_ctrl.subcache
  w_bus.line := io.b_ctrl.line
  w_bus.data := io.i_bus.data


  // ******************************
  //            OUTPUTS
  // ******************************
  io.o_lock := w_wait
  io.o_slct := reg_slct

  if (p.usePrevReadReg) {
    if (p.nPrev > 1) {
      reg_slct := io.i_slct

      for (pv <- 0 until p.nPrev) {
        when (pv.U === reg_slct & ~io.i_lock) {
          reg_valid(pv) := false.B
        }

        when (pv.U === io.i_slct & ~w_lock) {
          reg_valid(pv) := w_valid
          reg_bus(pv) := w_bus
        }
      }

      io.o_valid := false.B
      io.o_bus := reg_bus(0)

      for (pv <- 0 until p.nPrev) {
        when (pv.U === reg_slct) {
          io.o_valid := reg_valid(pv)
          io.o_bus := reg_bus(pv)
        }
      }
    } else {
      when (~w_lock) {
        reg_valid(0) := w_valid
        reg_bus(0) := w_bus
      }

      io.o_valid := reg_valid(0)
      io.o_bus := reg_bus(0)
    }
  } else {
    io.o_valid := w_valid
    io.o_bus := w_bus
  }

  // ******************************
  //            PENDING
  // ******************************
  if (p.usePrevReadReg) {
    for (pv <- 0 until p.nPrev) {
      io.o_pend.get(pv).valid := reg_valid(pv)
      io.o_pend.get(pv).subcache := reg_bus(pv).subcache
      io.o_pend.get(pv).set := reg_bus(pv).set
      io.o_pend.get(pv).line := reg_bus(pv).line
    }
  }

  // ******************************
  //             DOME
  // ******************************
  if (p.usePrevReadReg) {
    if (p.nPrev > 1) {
      for (d <- 0 until p.nDome) {
        io.b_dome(d).free := ~reg_valid(d)
      }
    } else {
      for (d <- 0 until p.nDome) {
        when (d.U === reg_bus(0).dome) {
          io.b_dome(d).free := ~reg_valid(0)
        }.otherwise {
          io.b_dome(d).free := true.B
        }
      }
    }
  } else {
    for (d <- 0 until p.nDome) {
      io.b_dome(d).free := true.B
    }
  }

  // ******************************
  //             FLUSH
  // ******************************
  for (f <- 0 until p.nFlush) {
    io.b_flush(f).ready := io.b_flush(f).valid

    if (p.nPrev > 1) {
      for (pv <- 0 until p.nPrev) {
        when (io.b_flush(f).valid & io.b_flush(f).apply & (pv.U === io.b_flush(f).dome)) {
          io.b_flush(f).ready := ~reg_valid(pv)
        }
      }
    } else {
      for (fd <- 0 until p.nPrevReqFifoDepth) {
        when (io.b_flush(f).valid & io.b_flush(f).apply & (reg_bus(0).dome === io.b_flush(f).dome)) {
          io.b_flush(f).ready := ~reg_valid(0)
        }
      }
    }
  }

  // ******************************
  //              MISS
  // ******************************
  val reg_miss = RegInit(VecInit(Seq.fill(p.nPrev)(false.B)))

  when (w_cstate === s0NEW) {
    reg_miss(io.i_slct) := io.i_valid & ~io.b_ctrl.ready
  }.elsewhen(w_cstate === s1REP) {
    reg_miss(io.i_slct) := false.B
  }

  for (ha <- 0 until p.nHart) {
    when (ha.U === io.i_bus.hart) {
      io.o_miss(ha) := ~reg_miss(io.i_slct) & (w_cstate === s0NEW) & io.i_valid & ~io.b_ctrl.ready
    }.otherwise {
      io.o_miss(ha) := false.B
    }
  }
}

object InitStage extends App {
  chisel3.Driver.execute(args, () => new InitStage(PrevDefault))
}
