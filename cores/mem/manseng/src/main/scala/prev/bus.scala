package cowlibry.mem.manseng.prev

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._


class PrevReqBus(p: PrevParams) extends Bundle {
  val dome = UInt(log2Ceil(p.nDome).W)
  val hart = UInt(log2Ceil(p.nHart).W)
  val rw = Bool()
  val size = UInt(SIZE.NBIT.W)
  val addr = UInt(p.nAddrBit.W)
  val tag = UInt(p.nTagBit.W)
  val set = UInt(log2Ceil(p.nSet).W)
  val offset = UInt(log2Ceil(p.nLineByte).W)
  val data = UInt(p.nPrevWordBit.W)

  override def cloneType = (new PrevReqBus(p)).asInstanceOf[this.type]
}

class PrevBus(p: PrevParams) extends Bundle {
  val dome = UInt(log2Ceil(p.nDome).W)
  val hart = UInt(log2Ceil(p.nHart).W)
  val rw = Bool()
  val size = UInt(SIZE.NBIT.W)
  val addr = UInt(p.nAddrBit.W)
  val tag = UInt(p.nTagBit.W)
  val set = UInt(log2Ceil(p.nSet).W)
  val offset = UInt(log2Ceil(p.nLineByte).W)
  val subcache = UInt(log2Ceil(p.nSubCache).W)
  val line = UInt(log2Ceil(p.nLine).W)
  val data = UInt(p.nPrevWordBit.W)

  override def cloneType = (new PrevBus(p)).asInstanceOf[this.type]
}

class RwConflictBus(p: PrevParams) extends Bundle {
  val valid = Bool()
  val bus = new PrevBus(p)

  override def cloneType = (new RwConflictBus(p)).asInstanceOf[this.type]
}
