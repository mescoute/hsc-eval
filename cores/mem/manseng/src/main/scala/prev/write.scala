package cowlibry.mem.manseng.prev

import chisel3._
import chisel3.util._

import cowlibry.common.tools._
import cowlibry.common.mem.pmb._
import cowlibry.mem.manseng.cache._
import cowlibry.mem.manseng.next.{NextWriteFireBus}


class WriteStage(p: PrevParams, p_prev: PmbParams) extends Module {
  val io = IO(new Bundle {
    val o_lock = Output(Bool())

    val i_slct = Input(UInt(log2Ceil(p.nPrev).W))
    val i_valid = Input(Bool())
    val i_bus = Input(new PrevBus(p))

    val b_read = Flipped(new CacheReadAckBus(p))
    val b_write = Flipped(new CacheWriteBus(p))
    val b_wfire = Flipped(new NextWriteFireBus(p.nNext))

    val b_mem = Flipped(new PmbAckIO(p_prev))
  })

  val w_mem_wait = Wire(Bool())
  val w_fire_wait = Wire(Bool())
  val w_write_wait = Wire(Bool())

  val w_data = Wire(Vec(p.nWordByte, UInt(8.W)))

  val reg_data_lock = RegInit(VecInit(Seq.fill(p.nPrev)(false.B)))
  val reg_data = Reg(Vec(p.nPrev, Vec(p.nWordByte, UInt(8.W))))
  val reg_mem_done = RegInit(VecInit(Seq.fill(p.nPrev)(false.B)))

  // ******************************
  //       WAIT NEXT PIPELINE
  // ******************************
  w_fire_wait := false.B
  for (d <- 0 until p.nDome) {
    when(d.U === io.i_bus.dome) {
      when (io.i_valid & io.i_bus.rw) {
        w_fire_wait := ~io.b_wfire.ready(d)
      }
    }
  }

  // ******************************
  //        WAIT PREV MEMORY
  // ******************************
  if (p.nPrev > 1) {
    w_mem_wait := false.B
    for (d <- 0 until p.nDome) {
      when((d.U === io.i_bus.dome) & io.i_valid) {
        w_mem_wait := ~reg_mem_done(d) & ~io.b_mem.ready(d)
      }
    }
  } else {
    when (io.i_valid) {
      w_mem_wait := ~reg_mem_done(0) & ~io.b_mem.ready(0)
    }.otherwise {
      w_mem_wait := false.B
    }
  }

  // ******************************
  //          READ DATA
  // ******************************
  w_data := io.b_read.rdata
  for (pv <- 0 until p.nPrev) {
    when(pv.U === io.i_slct) {
      when (reg_data_lock(pv)) {
        w_data := reg_data(pv)
      }

      when (~reg_data_lock(pv) & io.i_valid & (w_mem_wait | w_fire_wait | w_write_wait)) {
        reg_data_lock(pv) := true.B
        reg_data(pv) := io.b_read.rdata
      }.elsewhen(io.i_valid & ~(w_mem_wait | w_fire_wait | w_write_wait)) {
        reg_data_lock(pv) := false.B
      }
    }
  }

  // ******************************
  //            OUTPUTS
  // ******************************
  io.o_lock := w_mem_wait | w_fire_wait | w_write_wait

  // ------------------------------
  //           WRITE SET
  // ------------------------------
  val wdata = Module(new BitToByte(p.nPrevWordByte, p.nWordByte))
  wdata.io.i_in := io.i_bus.data

  w_write_wait := io.i_valid & io.i_bus.rw & ~io.b_write.ready

  io.b_write.valid := io.i_valid & io.i_bus.rw & ~w_mem_wait & ~w_fire_wait
  io.b_write.dome := io.i_bus.dome
  io.b_write.size := io.i_bus.size
  io.b_write.subcache := io.i_bus.subcache
  io.b_write.set := io.i_bus.set
  io.b_write.line := io.i_bus.line
  io.b_write.offset := io.i_bus.offset
  io.b_write.wdata := wdata.io.o_out

  // ------------------------------
  //        FIRE WRITE NEXT
  // ------------------------------
  for (d <- 0 until p.nDome) {
    io.b_wfire.valid(d) := false.B

    when (d.U === io.i_bus.dome) {
      io.b_wfire.valid(d) := io.i_valid & io.i_bus.rw & ~w_mem_wait & ~w_write_wait
    }
  }

  // ------------------------------
  //            MEMORY
  // ------------------------------
  for (pv <- 0 until p.nPrev) {
    when (pv.U === io.i_slct) {
      when (~reg_mem_done(pv)) {
        reg_mem_done(pv) := io.i_valid & io.i_bus.rw & ~w_mem_wait & (w_write_wait | w_fire_wait)
      }.otherwise {
        reg_mem_done(pv) := w_write_wait | w_fire_wait
      }
    }
  }

  io.b_mem.valid := io.i_valid & ~reg_mem_done(io.i_slct)
  io.b_mem.hart := io.i_bus.hart
  io.b_mem.dome := io.i_bus.dome
  io.b_mem.rdata := w_data.asUInt

  // ******************************
  //             DEBUG
  // ******************************
  if (p.debug) {
    dontTouch(io.i_bus.addr)
  }
}

object WriteStage extends App {
  chisel3.Driver.execute(args, () => new WriteStage(PrevDefault, PmbCfg0))
}
