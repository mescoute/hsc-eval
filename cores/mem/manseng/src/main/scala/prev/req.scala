package cowlibry.mem.manseng.prev

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._
import cowlibry.common.mem.flush._
import cowlibry.common.tools._
import cowlibry.common.rsrc._
import cowlibry.mem.manseng.tools._


class ReqStage(p: PrevParams, p_prev: PmbParams) extends Module {
  require(p.nPrevReqFifoDepth > 0, "Fifo depth of Prev controller requests must be greater than zero.")

  val io = IO(new Bundle {
    val b_dome = Vec(p.nDome, new DomeBus(1))
    val b_flush = Vec(p.nFlush, Flipped(new FlushIO(p.nHart, p.nDome, p.nAddrBit)))
    val i_lock = Input(Bool())

    val b_mem = Flipped(new PmbReqIO(p_prev))

    val o_slct = Output(UInt(log2Ceil(p.nPrev).W))
    val o_valid = Output(Bool())
    val o_bus = Output(new PrevReqBus(p))
  })

  val reg_wait = RegInit(VecInit(Seq.fill(p.nPrev)(false.B)))

  // ******************************
  //      SELECT EXECUTED DOME
  // ******************************
  val reg_slct = Wire(UInt(log2Ceil(p.nNext).W))

  val slct = Module(new StableSlct(false, p.nPrev))
  for (pv <- 0 until p.nPrev) {
    slct.io.i_valid(pv) := io.b_dome(pv).valid
  }
  reg_slct := slct.io.o_slct

  io.o_slct := reg_slct

  // ******************************
  //             FIFO
  // ******************************
  val reg_bus = Seq.fill(p.nPrev) {Module(new Fifo(2, new PrevReqBus(p), p.nPrevReqFifoDepth, 1, 1))}

  for (pv <- 0 until p.nPrev) {
    reg_bus(pv).io.i_flush := false.B

    io.b_mem.ready(pv) := ~reg_bus(pv).io.o_full(0) & ~reg_wait(pv)
    reg_bus(pv).io.i_data(0).dome := io.b_mem.dome
    reg_bus(pv).io.i_data(0).hart := io.b_mem.hart
    reg_bus(pv).io.i_data(0).rw := io.b_mem.rw
    reg_bus(pv).io.i_data(0).size := io.b_mem.size
    reg_bus(pv).io.i_data(0).addr := io.b_mem.addr
    if (p.nTagBit > 0) reg_bus(pv).io.i_data(0).tag := io.b_mem.addr(p.nAddrBit - 1, p.nAddrBit - p.nTagBit) else reg_bus(pv).io.i_data(0).tag := 0.U
    if (p.nSet > 1) reg_bus(pv).io.i_data(0).set := io.b_mem.addr(log2Ceil(p.nLineByte) + log2Ceil(p.nSet) - 1, log2Ceil(p.nLineByte)) else reg_bus(pv).io.i_data(0).set := 0.U
    if (p.nLineByte > 1) reg_bus(pv).io.i_data(0).offset := io.b_mem.addr(log2Ceil(p.nLineByte) - 1, 0) else reg_bus(pv).io.i_data(0).offset := 0.U
    reg_bus(pv).io.i_data(0).data := io.b_mem.wdata
  }

  // ******************************
  //              REG
  // ******************************
  //w_valid := io.b_mem.valid & ~w_wait

  if (p.usePrevReqReg) {
    // ------------------------------
    //            INPUTS
    // ------------------------------
    if (p.nPrev > 1) {
      for (pv <- 0 until p.nPrev) {
        when (pv.U === io.b_mem.dome) {
          reg_bus(pv).io.i_wen(0) := io.b_mem.valid & ~reg_wait(pv)
        }.otherwise {
          reg_bus(pv).io.i_wen(0) := false.B
        }
      }
    } else {
      reg_bus(0).io.i_wen(0) := io.b_mem.valid & ~reg_wait(0)
    }

    // ------------------------------
    //            OUTPUTS
    // ------------------------------
    if (p.nPrev > 1) {
      io.o_valid := false.B
      io.o_bus := reg_bus(0).io.o_data(0)

      for (pv <- 0 until p.nPrev) {
        when (pv.U === reg_slct) {
          reg_bus(pv).io.i_ren(0) := ~io.i_lock

          io.o_valid := ~reg_bus(pv).io.o_empty(0)
          io.o_bus := reg_bus(pv).io.o_data(0)
        }.otherwise {
          reg_bus(pv).io.i_ren(0) := false.B
        }
      }
    } else {
      reg_bus(0).io.i_ren(0) := ~io.i_lock

      io.o_valid := ~reg_bus(0).io.o_empty(0)
      io.o_bus := reg_bus(0).io.o_data(0)
    }

  // ******************************
  //             NO REG
  // ******************************
  } else {
    // ------------------------------
    //            INPUTS
    // ------------------------------
    if (p.nPrev > 1) {
      for (pv <- 0 until p.nPrev) {
        when ((pv.U === io.b_mem.dome) & (io.b_mem.dome === reg_slct)) {
          reg_bus(pv).io.i_wen(0) := io.b_mem.valid & ~reg_wait(pv) & (io.i_lock | ~reg_bus(pv).io.o_empty(0))
        }.elsewhen (pv.U === io.b_mem.dome) {
          reg_bus(pv).io.i_wen(0) := io.b_mem.valid & ~reg_wait(pv)
        }.otherwise {
          reg_bus(pv).io.i_wen(0) := false.B
        }
      }
    } else {
      reg_bus(0).io.i_wen(0) := io.b_mem.valid & ~reg_wait(0) & (io.i_lock | ~reg_bus(0).io.o_empty(0))
    }

    // ------------------------------
    //            OUTPUTS
    // ------------------------------
    if (p.nPrev > 1) {
      io.o_valid := false.B
      io.o_bus := reg_bus(0).io.o_data(0)

      for (pv <- 0 until p.nPrev) {
        reg_bus(pv).io.i_ren(0) := false.B

        when (pv.U === reg_slct) {
          when (~reg_bus(pv).io.o_empty(0)) {
            reg_bus(pv).io.i_ren(0) := ~io.i_lock

            io.o_valid := true.B
            io.o_bus := reg_bus(pv).io.o_data(0)
          }.elsewhen(reg_slct === io.b_mem.dome) {
            reg_bus(pv).io.i_ren(0) := false.B

            io.o_valid := io.b_mem.valid & ~reg_wait(pv)
            io.o_bus.dome := io.b_mem.dome
            io.o_bus.hart := io.b_mem.hart
            io.o_bus.rw := io.b_mem.rw
            io.o_bus.size := io.b_mem.size
            io.o_bus.addr := io.b_mem.addr
            if (p.nTagBit > 0) io.o_bus.tag := io.b_mem.addr(p.nAddrBit - 1, p.nAddrBit - p.nTagBit) else io.o_bus.tag := 0.U
            if (p.nSet > 1) io.o_bus.set := io.b_mem.addr(log2Ceil(p.nLineByte) + log2Ceil(p.nSet) - 1, log2Ceil(p.nLineByte)) else io.o_bus.set := 0.U
            if (p.nLineByte > 1) io.o_bus.offset := io.b_mem.addr(log2Ceil(p.nLineByte) - 1, 0) else io.o_bus.offset := 0.U
            io.o_bus.data := io.b_mem.wdata
          }
        }
      }
    } else {
      when (~reg_bus(0).io.o_empty(0)) {
        reg_bus(0).io.i_ren(0) := ~io.i_lock

        io.o_valid := true.B
        io.o_bus := reg_bus(0).io.o_data(0)
      }.otherwise {
        reg_bus(0).io.i_ren(0) := false.B

        io.o_valid := io.b_mem.valid & ~reg_wait(0)
        io.o_bus.dome := io.b_mem.dome
        io.o_bus.hart := io.b_mem.hart
        io.o_bus.rw := io.b_mem.rw
        io.o_bus.size := io.b_mem.size
        io.o_bus.addr := io.b_mem.addr
        if (p.nTagBit > 0) io.o_bus.tag := io.b_mem.addr(p.nAddrBit - 1, p.nAddrBit - p.nTagBit) else io.o_bus.tag := 0.U
        if (p.nSet > 1) io.o_bus.set := io.b_mem.addr(log2Ceil(p.nLineByte) + log2Ceil(p.nSet) - 1, log2Ceil(p.nLineByte)) else io.o_bus.set := 0.U
        if (p.nLineByte > 1) io.o_bus.offset := io.b_mem.addr(log2Ceil(p.nLineByte) - 1, 0) else io.o_bus.offset := 0.U
        io.o_bus.data := io.b_mem.wdata
      }
    }
  }

  // ******************************
  //             DOME
  // ******************************
  if (p.nPrev > 1) {
    for (pv <- 0 until p.nPrev) {
      io.b_dome(pv).free := reg_bus(pv).io.o_empty(0)
    }
  } else if (p.nDome > 1) {
    for (d <- 0 until p.nDome) {
      when (d.U === reg_bus(0).io.o_data(0).dome) {
        io.b_dome(d).free := reg_bus(0).io.o_empty(0)
      }.otherwise {
        io.b_dome(d).free := true.B
      }
    }
  } else {
    io.b_dome(0).free := reg_bus(0).io.o_empty(0)
  }

  // ******************************
  //             FLUSH
  // ******************************
  // ------------------------------
  //             WAIT
  // ------------------------------
  for (pv <- 0 until p.nPrev) {
    reg_wait(pv) := false.B
  }

  for (f <- 0 until p.nFlush) {
    if (p.nPrev > 1) {
      when (io.b_flush(f).valid & io.b_flush(f).apply) {
        reg_wait(io.b_flush(f).dome) := true.B
      }
    } else {
      when (io.b_flush(f).valid & io.b_flush(f).apply & (io.b_flush(f).dome === io.b_mem.dome)) {
        reg_wait(0) := true.B
      }
    }
  }

  // ------------------------------
  //             READY
  // ------------------------------
  if (p.nFlush > 0) {
    val reg_flush = RegInit(VecInit(Seq.fill(p.nFlush)(false.B)))

    for (f <- 0 until p.nFlush) {
      reg_flush(f) := io.b_flush(f).valid & io.b_flush(f).apply
      io.b_flush(f).ready := io.b_flush(f).valid

      if (p.nPrev > 1) {
        for (pv <- 0 until p.nPrev) {
          when (io.b_flush(f).valid & io.b_flush(f).apply & (pv.U === io.b_flush(f).dome)) {
            io.b_flush(f).ready := reg_bus(pv).io.o_empty(0) & reg_flush(f)
          }
        }
      } else {
        for (fd <- 0 until p.nPrevReqFifoDepth) {
          when (io.b_flush(f).valid & io.b_flush(f).apply & reg_bus(0).io.o_ext_valid(fd) & (reg_bus(0).io.o_ext_fifo(fd).dome === io.b_flush(f).dome)) {
            io.b_flush(f).ready := false.B
          }
        }
      }
    }
  } else {
    for (f <- 0 until p.nFlush) {
      io.b_flush(f).ready := true.B
    }
  }
}

object ReqStage extends App {
  chisel3.Driver.execute(args, () => new ReqStage(PrevDefault, PmbCfg0))
}
