package cowlibry.mem.manseng.prev

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb.{SIZE}
import cowlibry.common.mem.flush._
import cowlibry.common.rsrc._
import cowlibry.mem.manseng.tools._
import cowlibry.mem.manseng.cache.{CacheDataBus, CachePendBus}


class RwConflict(p: PrevParams) extends Module {
  val io = IO(new Bundle {
    val b_read = Input(new RwConflictBus(p))
    val b_write = Input(Vec(p.nPrev, new RwConflictBus(p)))

    val o_wait = Output(Bool())
  })

  // ******************************
  //             READ
  // ******************************
  val w_rmin_offset = Wire(UInt(log2Ceil(p.nLineByte).W))
  val w_rmax_offset = Wire(UInt(log2Ceil(p.nLineByte).W))

  // ------------------------------
  //              MIN
  // ------------------------------
  w_rmin_offset := io.b_read.bus.offset

  // ------------------------------
  //              MAX
  // ------------------------------
  w_rmax_offset := io.b_read.bus.offset

  if(p.nWordByte >= 2) {
    when(io.b_read.bus.size === SIZE.B2.U) {
      w_rmax_offset := io.b_read.bus.offset + 1.U
    }
  }
  if(p.nWordByte >= 4) {
    when(io.b_read.bus.size === SIZE.B4.U) {
      w_rmax_offset := io.b_read.bus.offset + 3.U
    }
  }
  if(p.nWordByte >= 8) {
    when(io.b_read.bus.size === SIZE.B8.U) {
      w_rmax_offset := io.b_read.bus.offset + 7.U
    }
  }
  if(p.nWordByte >= 16) {
    when(io.b_read.bus.size === SIZE.B16.U) {
      w_rmax_offset := io.b_read.bus.offset + 15.U
    }
  }



  // ******************************
  //             WRITE
  // ******************************
  val w_wmin_offset = Wire(Vec(p.nPrev, UInt(log2Ceil(p.nLineByte).W)))
  val w_wmax_offset = Wire(Vec(p.nPrev, UInt(log2Ceil(p.nLineByte).W)))

  for (pv <- 0 until p.nPrev) {
    // ------------------------------
    //              MIN
    // ------------------------------
    w_wmin_offset(pv) := io.b_write(pv).bus.offset

    // ------------------------------
    //              MAX
    // ------------------------------
    w_wmax_offset(pv) := io.b_write(pv).bus.offset

    if(p.nWordByte >= 2) {
      when(io.b_write(pv).bus.size === SIZE.B2.U) {
        w_wmax_offset(pv) := io.b_write(pv).bus.offset + 1.U
      }
    }
    if(p.nWordByte >= 4) {
      when(io.b_write(pv).bus.size === SIZE.B4.U) {
        w_wmax_offset(pv) := io.b_write(pv).bus.offset + 3.U
      }
    }
    if(p.nWordByte >= 8) {
      when(io.b_write(pv).bus.size === SIZE.B8.U) {
        w_wmax_offset(pv) := io.b_write(pv).bus.offset + 7.U
      }
    }
    if(p.nWordByte >= 16) {
      when(io.b_write(pv).bus.size === SIZE.B16.U) {
        w_wmax_offset(pv) := io.b_write(pv).bus.offset + 15.U
      }
    }
  }


  // ******************************
  //            COMPARE
  // ******************************
  io.o_wait := 0.B
  for (wpv <- 0 until p.nPrev) {
    when (io.b_read.valid & ~io.b_read.bus.rw & io.b_write(wpv).valid & io.b_write(wpv).bus.rw) {
      when ((io.b_read.bus.set === io.b_write(wpv).bus.set) & (io.b_read.bus.line === io.b_write(wpv).bus.line)) {
        when ((w_rmin_offset >= w_wmin_offset(wpv)) & (w_rmin_offset <= w_wmax_offset(wpv))) {
          io.o_wait := 1.B
        }.elsewhen ((w_rmax_offset >= w_wmin_offset(wpv)) & (w_rmax_offset <= w_wmax_offset(wpv))) {
          io.o_wait := 1.B
        }
      }
    }
  }
}

class ReadStage(p: PrevParams) extends Module {
  val io = IO(new Bundle {
    val b_dome = Vec(p.nDome, new DomeBus(1))
    val b_flush = Vec(p.nFlush, Flipped(new FlushIO(p.nHart, p.nDome, p.nAddrBit)))
    val i_lock = Input(Bool())
    val o_lock = Output(Bool())

    val i_slct = Input(UInt(log2Ceil(p.nPrev).W))
    val i_valid = Input(Bool())
    val i_bus = Input(new PrevBus(p))

    val b_read = Flipped(new CacheDataBus(p))

    val o_pend = Vec(p.nPrev, Flipped(new CachePendBus(p)))
    val o_slct = Output(UInt(log2Ceil(p.nPrev).W))
    val o_valid = Output(Bool())
    val o_bus = Output(new PrevBus(p))
  })

  val w_wait = Wire(Bool())
  val w_lock = Wire(Bool())
  val w_valid = Wire(Bool())
  val w_bus = Wire(new PrevBus(p))

  val init_slct = Wire(UInt(log2Ceil(p.nPrev).W))
  init_slct := 0.U

  val reg_slct = RegInit(init_slct)
  val reg_valid = RegInit(VecInit(Seq.fill(p.nPrev)(0.B)))
  val reg_bus = Reg(Vec(p.nPrev, new PrevBus(p)))

  // ******************************
  //              LOCK
  // ******************************
  w_lock := true.B
  for (pv <- 0 until p.nPrev) {
    when (pv.U === io.i_slct) {
      when (io.i_slct === io.o_slct) {
        w_lock := io.i_lock & reg_valid(pv)
      }.otherwise {
        w_lock := reg_valid(pv)
      }
    }
  }


  // ******************************
  //             LOGIC
  // ******************************
  // ------------------------------
  //          RW CONFLICT
  // ------------------------------
  val conflict = Module(new RwConflict(p))
  conflict.io.b_read.valid := io.i_valid
  conflict.io.b_read.bus := io.i_bus

  for (pv <- 0 until p.nPrev) {
    conflict.io.b_write(pv).valid := reg_valid(pv)
    conflict.io.b_write(pv).bus := reg_bus(pv)
  }

  // ------------------------------
  //           READ SET
  // ------------------------------
  io.b_read.valid := io.i_valid & ~io.i_bus.rw & ~w_lock & ~conflict.io.o_wait
  io.b_read.dome := io.i_bus.dome
  io.b_read.size := io.i_bus.size
  io.b_read.subcache := io.i_bus.subcache
  io.b_read.set := io.i_bus.set
  io.b_read.line := io.i_bus.line
  io.b_read.offset := io.i_bus.offset

  // ------------------------------
  //             BUS
  // ------------------------------
  w_wait := io.i_valid & ~io.i_bus.rw & (~io.b_read.ready | conflict.io.o_wait)

  w_valid := io.i_valid & ~w_wait
  w_bus := io.i_bus

  // ******************************
  //            OUTPUTS
  // ******************************
  io.o_lock := w_lock | w_wait | conflict.io.o_wait

  for (pv <- 0 until p.nPrev) {
    when (pv.U === reg_slct & ~io.i_lock) {
      reg_valid(pv) := false.B
    }

    when (pv.U === io.i_slct & ~w_lock) {
      reg_valid(pv) := w_valid
      reg_bus(pv) := w_bus
    }
  }

  io.o_slct := reg_slct
  io.o_valid := false.B
  io.o_bus := reg_bus(0)

  for (pv <- 0 until p.nPrev) {
    when (pv.U === reg_slct) {
      io.o_valid := reg_valid(pv)
      io.o_bus := reg_bus(pv)
    }
  }

  // ******************************
  //            PENDING
  // ******************************
  for (pv <- 0 until p.nPrev) {
    io.o_pend(pv).valid := reg_valid(pv) & reg_bus(pv).rw
    io.o_pend(pv).subcache := reg_bus(pv).subcache
    io.o_pend(pv).set := reg_bus(pv).set
    io.o_pend(pv).line := reg_bus(pv).line
  }

  // ******************************
  //             DOME
  // ******************************
  if (p.nPrev > 1) {
    for (pv <- 0 until p.nPrev) {
      io.b_dome(pv).free := ~reg_valid(pv)
    }
  } else {
    for (d <- 0 until p.nDome) {
      when (d.U === reg_bus(0).dome) {
        io.b_dome(d).free := ~reg_valid(0)
      }.otherwise {
        io.b_dome(d).free := true.B
      }
    }
  }

  // ******************************
  //             FLUSH
  // ******************************

  for (f <- 0 until p.nFlush) {
    io.b_flush(f).ready := io.b_flush(f).valid

    if (p.nPrev > 1) {
      for (pv <- 0 until p.nPrev) {
        when (io.b_flush(f).valid & io.b_flush(f).apply & (pv.U === io.b_flush(f).dome)) {
          io.b_flush(f).ready := ~reg_valid(pv)
        }
      }
    } else {
      for (fd <- 0 until p.nPrevReqFifoDepth) {
        when (io.b_flush(f).valid & io.b_flush(f).apply & (reg_bus(0).dome === io.b_flush(f).dome)) {
          io.b_flush(f).ready := ~reg_valid(0)
        }
      }
    }
  }
}

object RwConflict extends App {
  chisel3.Driver.execute(args, () => new RwConflict(PrevDefault))
}

object ReadStage extends App {
  chisel3.Driver.execute(args, () => new ReadStage(PrevDefault))
}
