package cowlibry.mem.manseng.prev

import chisel3._
import scala.math._


object PrevDefault extends PrevParams {
  def debug: Boolean = false
  def nAddrBit: Int = 32
  def nDome: Int = 1
  def nHart: Int = 1
  def nFlush: Int = 1
  def usePartWay: Boolean = true

  def nPrevMem: Int = 2
  def multiPrevDome: Boolean = false
  def nPrevWordByte: Int = 4
  def nPrevReqFifoDepth: Int = 4
  def usePrevReqReg: Boolean = false
  def usePrevReadReg: Boolean = false

  def nSetReadPort: Int = 2
  def nSetWritePort: Int = 1
  def slctPolicy: String = "BitPLRU"
  def nSubCache: Int = 1
  def nSet: Int = 4
  def nLine: Int = 4
  def nWord: Int = 4
  def nWordByte: Int = 16
}
