package cowlibry.mem.manseng.cache

import chisel3._
import chisel3.util._
import scala.math._

import cowlibry.common.mem.pmb.SIZE
import cowlibry.common.tools.MATH


class DataSet(p: SetParams) extends Module {
  require(MATH.islog2(p.nWordByte), "Word must have a power of 2 number of bytes !")

  val io = IO(new Bundle {
    val b_read = Vec(p.nSetReadPort, new SetReadBus(p))
    val b_write = Vec(p.nSetWritePort, new SetWriteBus(p))
  })

  // ******************************
  //          DATA MEMORY
  // ******************************
  val m_data = SyncReadMem(p.nLine * p.nWord, Vec(p.nWordByte, UInt(8.W)))

  // ******************************
  //             READ
  // ******************************
  val reg_rsize = Reg(Vec(p.nSetReadPort, UInt(SIZE.NBIT.W)))
  val w_rword_addr = Wire(Vec(p.nSetReadPort, UInt(log2Ceil(p.nLine * p.nWord).W)))
  val reg_rbyte_addr = Reg(Vec(p.nSetReadPort, UInt(log2Ceil(p.nWordByte).W)))

  val w_rdata = Wire(Vec(p.nSetReadPort, Vec(p.nWordByte, UInt(8.W))))
  val w_rdata_shift = Wire(Vec(p.nSetReadPort, Vec(p.nWordByte, UInt(8.W))))

  for (r <- 0 until p.nSetReadPort) {
    // ------------------------------
    //    SYNCHRONIZE READ REQUEST
    // ------------------------------
    reg_rsize(r) := io.b_read(r).size

    if (log2Ceil(p.nWord * p.nWordByte) > 0) {
      w_rword_addr(r) := Cat(io.b_read(r).line, io.b_read(r).offset(log2Ceil(p.nWord * p.nWordByte) - 1, log2Ceil(p.nWordByte)))
    } else {
      w_rword_addr(r) := 0.U
    }

    if (log2Ceil(p.nWordByte) > 0) {
      reg_rbyte_addr(r) := io.b_read(r).offset(log2Ceil(p.nWordByte) - 1, 0)
    } else {
      reg_rbyte_addr(r) := 0.U
    }

    // ------------------------------
    //     SYNCHRONOUS WORD READ
    // ------------------------------
    w_rdata(r) := m_data.read(w_rword_addr(r))

    // ------------------------------
    //           SHIFT WORD
    // ------------------------------
    for (b <- 0 until p.nWordByte) {
      when((b.U + reg_rbyte_addr(r)) < p.nWordByte.U) {
        w_rdata_shift(r)(b) := w_rdata(r)(b.U + reg_rbyte_addr(r))
      }.otherwise {
        w_rdata_shift(r)(b) := 0.U
      }
    }

    // ------------------------------
    //          FORMAT DATA
    // ------------------------------
    for (i <- 0 until p.nWordByte) {
      io.b_read(r).rdata(i) := 0.U
    }

    when(reg_rsize(r) === SIZE.B1.U) {
      io.b_read(r).rdata(0) := w_rdata_shift(r)(0)
    }
    if(p.nWordByte >= 2) {
      when(reg_rsize(r) === SIZE.B2.U) {
        for (i <- 0 to 1) {
          io.b_read(r).rdata(i) := w_rdata_shift(r)(i)
        }
      }
    }
    if(p.nWordByte >= 4) {
      when(reg_rsize(r) === SIZE.B4.U) {
        for (i <- 0 to 3) {
          io.b_read(r).rdata(i) := w_rdata_shift(r)(i)
        }
      }
    }
    if(p.nWordByte >= 8) {
      when(reg_rsize(r) === SIZE.B8.U) {
        for (i <- 0 to 7) {
          io.b_read(r).rdata(i) := w_rdata_shift(r)(i)
        }
      }
    }
    if(p.nWordByte >= 16) {
      when(reg_rsize(r) === SIZE.B16.U) {
        for (i <- 0 to 15) {
          io.b_read(r).rdata(i) := w_rdata_shift(r)(i)
        }
      }
    }
  }

  // ******************************
  //             WRITE
  // ******************************
  val w_wword_addr = Wire(Vec(p.nSetWritePort, UInt(log2Ceil(p.nLine * p.nWord).W)))
  val w_wbyte_addr = Wire(Vec(p.nSetWritePort, UInt(log2Ceil(p.nWordByte).W)))
  val w_wmask = Wire(Vec(p.nSetWritePort, Vec(p.nWordByte, Bool())))
  val w_wdata = Wire(Vec(p.nSetWritePort, Vec(p.nWordByte, UInt(8.W))))
  val w_wmask_shift = Wire(Vec(p.nSetWritePort, Vec(p.nWordByte, Bool())))
  val w_wdata_shift = Wire(Vec(p.nSetWritePort, Vec(p.nWordByte, UInt(8.W))))

  for (w <- 0 until p.nSetWritePort) {
    // ------------------------------
    //         ADDR AND SIZE
    // ------------------------------
    if ((log2Ceil(p.nWordByte) + log2Ceil(p.nWord)) > 0) {
      w_wword_addr(w) := Cat(io.b_write(w).line, io.b_write(w).offset(log2Ceil(p.nWord * p.nWordByte) - 1, log2Ceil(p.nWordByte)))
    } else {
      w_wword_addr(w) := 0.U
    }

    if (log2Ceil(p.nWordByte) > 0) {
      w_wbyte_addr(w) := io.b_write(w).offset(log2Ceil(p.nWordByte) - 1, 0)
    } else {
      w_wbyte_addr(w) := 0.U
    }

    // ------------------------------
    //         MASK AND DATA
    // ------------------------------
    for (b <- 0 until p.nWordByte) {
      w_wmask(w)(b) := 0.B
      w_wdata(w)(b) := 0.U
    }

    when(io.b_write(w).size === SIZE.B1.U) {
      w_wmask(w)(0) := 1.B
      w_wdata(w)(0) := io.b_write(w).wdata(0)
    }
    if(p.nWordByte >= 2) {
      when(io.b_write(w).size === SIZE.B2.U) {
        for (i <- 0 to 1) {
          w_wmask(w)(i) := 1.B
          w_wdata(w)(i) := io.b_write(w).wdata(i)
        }
      }
    }
    if(p.nWordByte >= 4) {
      when(io.b_write(w).size === SIZE.B4.U) {
        for (i <- 0 to 3) {
          w_wmask(w)(i) := 1.B
          w_wdata(w)(i) := io.b_write(w).wdata(i)
        }
      }
    }
    if(p.nWordByte >= 8) {
      when(io.b_write(w).size === SIZE.B8.U) {
        for (i <- 0 to 7) {
          w_wmask(w)(i) := 1.B
          w_wdata(w)(i) := io.b_write(w).wdata(i)
        }
      }
    }
    if(p.nWordByte >= 16) {
      when(io.b_write(w).size === SIZE.B16.U) {
        for (i <- 0 to 15) {
          w_wmask(w)(i) := 1.B
          w_wdata(w)(i) := io.b_write(w).wdata(i)
        }
      }
    }

    // ------------------------------
    //      SHIFT MASK AND DATA
    // ------------------------------
    for (b <- 0 until p.nWordByte) {
      when(b.U >= w_wbyte_addr(w)) {
        w_wmask_shift(w)(b) := w_wmask(w)(b.U - w_wbyte_addr(w))
        w_wdata_shift(w)(b) := w_wdata(w)(b.U - w_wbyte_addr(w))
      }.otherwise {
        w_wmask_shift(w)(b) := 0.B
        w_wdata_shift(w)(b) := 0.U
      }
    }

    // ------------------------------
    //        SYNCHRONOUS WRITE
    // ------------------------------
    for (l <- 0 until p.nLine) {
      when (io.b_write(w).valid & l.U === io.b_write(w).line) {
        m_data.write(w_wword_addr(w), w_wdata_shift(w), w_wmask_shift(w))
      }
    }
  }
}

object DataSet extends App {
  chisel3.Driver.execute(args, () => new DataSet(SetDefault))
}
