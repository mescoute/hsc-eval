package cowlibry.mem.manseng.cache

import chisel3._
import scala.math._


object SetDefault extends SetParams {
  def nDome: Int = 2
  def nFlush: Int = 2
  def usePartWay: Boolean = true

  def nAccess: Int = 2
  def nSetReadPort: Int = 1
  def nSetWritePort: Int = 1
  def slctPolicy: String = "BitPLRU"
  def nPendingAcc: Int = 4

  def nLine: Int = 4
  def nWord: Int = 4
  def nWordByte: Int = 16
  def nTagBit: Int = 24
}

object SubCacheDefault extends SubCacheParams {
  def nDome: Int = 1
  def nFlush: Int = 2
  def usePartWay: Boolean = true

  def nAccess: Int = 2
  def nSetReadPort: Int = 2
  def nSetWritePort: Int = 1
  def slctPolicy: String = "BitPLRU"
  def nPendingAcc: Int = 4

  def nSet: Int = 4
  def nLine: Int = 4
  def nWord: Int = 4
  def nWordByte: Int = 16
  def nTagBit: Int = 24
}

object CacheDefault extends CacheParams {
  def nDome: Int = 1
  def nFlush: Int = 2
  def usePartWay: Boolean = true

  def nAccess: Int = 2
  def nSetReadPort: Int = 2
  def nSetWritePort: Int = 1
  def slctPolicy: String = "BitPLRU"
  def nPendingAcc: Int = 4

  def nSet: Int = 4
  def nLine: Int = 4
  def nWord: Int = 4
  def nWordByte: Int = 16
  def nSubCache: Int = 1
  def nTagBit: Int = 24
}
