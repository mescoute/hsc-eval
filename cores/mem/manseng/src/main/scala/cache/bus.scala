package cowlibry.mem.manseng.cache

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._


// ******************************
//              CTRL
// ******************************
// ------------------------------
//              SET
// ------------------------------
class SetAccBus(p: SetParams) extends Bundle {
  val valid = Input(Bool())
  val dome = Input(UInt(log2Ceil(p.nDome).W))
  val tag = Input(UInt(p.nTagBit.W))

  val ready = Output(Bool())
  val line = Output(UInt(log2Ceil(p.nLine).W))

  override def cloneType = (new SetAccBus(p)).asInstanceOf[this.type]
}

class SetRepBus(p: SetParams) extends SetAccBus(p) {
  val check = Input(Bool())

  override def cloneType = (new SetRepBus(p)).asInstanceOf[this.type]
}

class SetPendBus(p: SetParams) extends Bundle {
  val valid = Input(Bool())
  val line = Input(UInt(log2Ceil(p.nLine).W))

  override def cloneType = (new SetPendBus(p)).asInstanceOf[this.type]
}

// ------------------------------
//           SUBCACHE
// ------------------------------
class SubCacheAccBus(p: SubCacheParams) extends SetAccBus(p) {
  val set = Input(UInt(log2Ceil(p.nSet).W))

  override def cloneType = (new SubCacheAccBus(p)).asInstanceOf[this.type]
}

class SubCacheRepBus(p: SubCacheParams) extends SubCacheAccBus(p) {
  val check = Input(Bool())

  override def cloneType = (new SubCacheRepBus(p)).asInstanceOf[this.type]
}

class SubCachePendBus(p: SubCacheParams) extends SetPendBus(p) {
  val set = Input(UInt(log2Ceil(p.nSet).W))

  override def cloneType = (new SubCachePendBus(p)).asInstanceOf[this.type]
}

// ------------------------------
//             CACHE
// ------------------------------
class CacheAccBus(p: CacheParams) extends Bundle {
  val valid = Input(Bool())
  val dome = Input(UInt(log2Ceil(p.nDome).W))
  val tag = Input(UInt(p.nTagBit.W))
  val set = Input(UInt(log2Ceil(p.nSet).W))
  val offset = Input(UInt(log2Ceil(p.nLineByte).W))

  val ready = Output(Bool())
  val subcache = Output(UInt(log2Ceil(p.nSubCache).W))
  val line = Output(UInt(log2Ceil(p.nLine).W))

  override def cloneType = (new CacheAccBus(p)).asInstanceOf[this.type]
}

class CacheRepBus(p: CacheParams) extends CacheAccBus(p) {
  val check = Input(Bool())

  override def cloneType = (new CacheRepBus(p)).asInstanceOf[this.type]
}

class CachePendBus(p: CacheParams) extends SubCachePendBus(p) {
  val subcache = Input(UInt(log2Ceil(p.nSubCache).W))

  override def cloneType = (new CachePendBus(p)).asInstanceOf[this.type]
}

// ******************************
//              DATA
// ******************************
// ------------------------------
//              SET
// ------------------------------
class SetDataBus(p: SetParams) extends Bundle {
  val size = Input(UInt(SIZE.NBIT.W))
  val line = Input(UInt(log2Ceil(p.nLine).W))
  val offset = Input(UInt(log2Ceil(p.nLineByte).W))

  override def cloneType = (new SetDataBus(p)).asInstanceOf[this.type]
}

class SetReadBus(p: SetParams) extends SetDataBus(p) {
  val rdata = Output(Vec(p.nWordByte, UInt(8.W)))

  override def cloneType = (new SetReadBus(p)).asInstanceOf[this.type]
}

class SetWriteBus(p: SetParams) extends SetDataBus(p) {
  val valid = Input(Bool())
  val wdata = Input(Vec(p.nWordByte, UInt(8.W)))

  override def cloneType = (new SetWriteBus(p)).asInstanceOf[this.type]
}

// ------------------------------
//            SUBCACHE
// ------------------------------
class SubCacheDataBus(p: SubCacheParams) extends SetDataBus(p) {
  val ready = Output(Bool())
  val valid = Input(Bool())
  val dome = Input(UInt(log2Ceil(p.nDome).W))
  val set = Input(UInt(log2Ceil(p.nSet).W))

  override def cloneType = (new SubCacheDataBus(p)).asInstanceOf[this.type]
}

class SubCacheReadAckBus(p: SubCacheParams) extends Bundle {
  val rdata = Output(Vec(p.nWordByte, UInt(8.W)))

  override def cloneType = (new SubCacheReadAckBus(p)).asInstanceOf[this.type]
}

class SubCacheReadBus(p: SubCacheParams) extends Bundle {
  val req = new SubCacheDataBus(p)
  val ack = new SubCacheReadAckBus(p)

  override def cloneType = (new SubCacheReadBus(p)).asInstanceOf[this.type]
}

class SubCacheWriteBus(p: SubCacheParams) extends SubCacheDataBus(p) {
  val wdata = Input(Vec(p.nWordByte, UInt(8.W)))

  override def cloneType = (new SubCacheWriteBus(p)).asInstanceOf[this.type]
}

// ------------------------------
//             CACHE
// ------------------------------
class CacheDataBus(p: CacheParams) extends SubCacheDataBus(p) {
  val subcache = Input(UInt(log2Ceil(p.nSubCache).W))

  override def cloneType = (new CacheDataBus(p)).asInstanceOf[this.type]
}

class CacheReadAckBus(p: CacheParams) extends Bundle {
  val rdata = Output(Vec(p.nWordByte, UInt(8.W)))

  override def cloneType = (new CacheReadAckBus(p)).asInstanceOf[this.type]
}

class CacheReadBus(p: CacheParams) extends Bundle {
  val req = new CacheDataBus(p)
  val ack = new CacheReadAckBus(p)

  override def cloneType = (new CacheReadBus(p)).asInstanceOf[this.type]
}

class CacheWriteBus(p: CacheParams) extends CacheDataBus(p) {
  val wdata = Input(Vec(p.nWordByte, UInt(8.W)))

  override def cloneType = (new CacheWriteBus(p)).asInstanceOf[this.type]
}
