package cowlibry.mem.manseng.cache

import chisel3._
import chisel3.util._
import scala.math._

import cowlibry.common.tools._
import cowlibry.common.rsrc._
import cowlibry.common.mem.flush._
import cowlibry.mem.manseng.tools._
import cowlibry.mem.manseng.mpu._


class Cache(p: CacheParams) extends Module {
  require((p.nTagBit >= log2Ceil(p.nSubCache)), "There are too many subcaches: they can not all be addressed.")

  val io = IO(new Bundle {
    val b_dome = Vec(p.nDome, new DomeInfoBus(p.nRsrc))
    val b_rsrc = Vec(p.nRsrc, new RsrcBus(1, p.nDome, p.nRsrc))
    val b_flush = Vec(p.nFlush, Flipped(new FlushIO(1, p.nDome, p.nTagBit + log2Ceil(p.nSet) + log2Ceil(p.nLineByte))))

    val b_acc = Vec(p.nAccess, new CacheAccBus(p))
    val b_rep = new CacheRepBus(p)

    val b_read = Vec(p.nCacheReadPort, new CacheReadBus(p))
    val b_write = Vec(p.nCacheWritePort, new CacheWriteBus(p))

    val i_pend = Vec(p.nPendingAcc, new CachePendBus(p))
    val i_end = new CachePendBus(p)
  })

  val subcache = Seq.fill(p.nSubCache){Module(new SubCache(p))}

  // ******************************
  //         SELECT SUBCACHE
  // ******************************
  // ------------------------------
  //             ACCESS
  // ------------------------------
  val w_acc_subcache = Wire(Vec(p.nAccess, UInt(log2Ceil(p.nSubCache).W)))
  for (a <- 0 until p.nAccess) {
    w_acc_subcache(a) := 0.U
    for (s <- 1 until p.nSubCache) {
      when(subcache(s).io.b_acc(a).ready) {
        w_acc_subcache(a) := s.U
      }
    }
  }

  // ------------------------------
  //             CHECK
  // ------------------------------
  val w_chk_subcache = Wire(UInt(log2Ceil(p.nSubCache).W))
  w_chk_subcache := 0.U
  for (s <- 1 until p.nSubCache) {
    when(subcache(s).io.b_rep.ready) {
      w_chk_subcache := s.U
    }
  }

  // ------------------------------
  //            REPLACE
  // ------------------------------
  // Must dynamically select a subcache to store the data

  val w_rep_valid = Wire(Bool())
  val w_rep_subcache = Wire(UInt(log2Ceil(p.nSubCache).W))

  if (((!MATH.islog2(p.nSubCache)) || (p.nDome > 1)) && p.nSubCache > 1) {
    val slct = Module(new SlctIndex(log2Ceil(p.nSubCache)))
    slct.io.i_index := io.b_rep.tag(log2Ceil(p.nSubCache) - 1, 0)
    slct.io.i_max := (p.nSubCache - 1).U

    if (!p.usePartWay) {
      w_rep_valid := false.B
      for (d <- 0 until p.nDome) {
        when (d.U === io.b_rep.dome) {
          slct.io.i_max := io.b_dome(d).weight
        }
      }

      w_rep_subcache := 0.U
      for (s <- 0 until p.nSubCache) {
        when (io.b_rsrc(s).valid & ~io.b_rsrc(s).flush & (io.b_rsrc(s).dome === io.b_rep.dome) & (io.b_rsrc(s).port === slct.io.o_slct)) {
          w_rep_valid := true.B
          w_rep_subcache := s.U
        }
      }
    } else {
      w_rep_valid := true.B
      w_rep_subcache := slct.io.o_slct
    }
  } else if (MATH.islog2(p.nSubCache) && p.nSubCache > 1) {
    w_rep_valid := true.B
    w_rep_subcache := io.b_rep.tag(log2Ceil(p.nSubCache) - 1, 0)
  } else {
    w_rep_valid := true.B
    w_rep_subcache := 0.U
  }

  // ******************************
  //            CONNECT
  // ******************************
  // ------------------------------
  //            ACCESS
  // ------------------------------
  for (a <- 0 until p.nAccess) {
    for (s <- 0 until p.nSubCache) {
      if (!p.usePartWay) {
        subcache(s).io.b_acc(a).valid := Mux(io.b_rsrc(s).valid & ~io.b_rsrc(s).flush & (io.b_acc(a).dome === io.b_rsrc(s).dome), io.b_acc(a).valid, false.B)
      } else {
        subcache(s).io.b_acc(a).valid := io.b_acc(a).valid
      }
      subcache(s).io.b_acc(a).dome := io.b_acc(a).dome
      subcache(s).io.b_acc(a).tag := io.b_acc(a).tag
      subcache(s).io.b_acc(a).set := io.b_acc(a).set
    }
  }

  for (a <- 0 until p.nAccess) {
    io.b_acc(a).ready := false.B
    io.b_acc(a).subcache := w_acc_subcache(a)
    io.b_acc(a).line := 0.U

    for (s <- 0 until p.nSubCache) {
      when (s.U === w_acc_subcache(a)) {
        io.b_acc(a).ready := subcache(s).io.b_acc(a).ready
        io.b_acc(a).line := subcache(s).io.b_acc(a).line
      }
    }
  }

  // ------------------------------
  //        CHECK & REPLACE
  // ------------------------------
  for (s <- 0 until p.nSubCache) {
    when (io.b_rep.check) {
      if (!p.usePartWay) {
        subcache(s).io.b_rep.valid := Mux(io.b_rsrc(s).valid & ~io.b_rsrc(s).flush & (io.b_rep.dome === io.b_rsrc(s).dome), io.b_rep.valid, false.B)
      } else {
        subcache(s).io.b_rep.valid := io.b_rep.valid
      }
    }.otherwise {
      subcache(s).io.b_rep.valid := Mux(s.U === w_rep_subcache, io.b_rep.valid & w_rep_valid, false.B)
    }
    subcache(s).io.b_rep.dome := io.b_rep.dome
    subcache(s).io.b_rep.check := io.b_rep.check
    subcache(s).io.b_rep.tag := io.b_rep.tag
    subcache(s).io.b_rep.set := io.b_rep.set
  }

  io.b_rep.ready := false.B
  io.b_rep.subcache := w_chk_subcache
  io.b_rep.line := subcache(0).io.b_rep.line
  for (s <- 0 until p.nSubCache) {
    when (io.b_rep.check & s.U === w_chk_subcache) {
      io.b_rep.ready := subcache(s).io.b_rep.ready
      io.b_rep.line := subcache(s).io.b_rep.line
    }.elsewhen (~io.b_rep.check & s.U === w_rep_subcache) {
      io.b_rep.ready := subcache(s).io.b_rep.ready
      io.b_rep.line := subcache(s).io.b_rep.line
    }
  }

  // ------------------------------
  //             READ
  // ------------------------------
  for (r <- 0 until p.nCacheReadPort) {
    io.b_read(r).req.ready := false.B

    for (s <- 0 until p.nSubCache) {
      subcache(s).io.b_read(r).req.valid := Mux(s.U === io.b_read(r).req.subcache, io.b_read(r).req.valid, false.B)
      subcache(s).io.b_read(r).req.dome :=  io.b_read(r).req.dome
      subcache(s).io.b_read(r).req.size :=  io.b_read(r).req.size
      subcache(s).io.b_read(r).req.set :=  io.b_read(r).req.set
      subcache(s).io.b_read(r).req.line :=  io.b_read(r).req.line
      subcache(s).io.b_read(r).req.offset :=  io.b_read(r).req.offset

      when (s.U === io.b_read(r).req.subcache) {
        subcache(s).io.b_read(r).req.valid := io.b_read(r).req.valid
        io.b_read(r).req.ready := subcache(s).io.b_read(r).req.ready
      }.otherwise {
        subcache(s).io.b_read(r).req.valid := false.B
      }
    }
  }

  // Set register to return read value
  val reg_rsubcache = Reg(Vec(p.nCacheReadPort, UInt(log2Ceil(p.nSubCache).W)))
  for (r <- 0 until p.nCacheReadPort) {
    reg_rsubcache(r) := io.b_read(r).req.subcache
  }

  for (r <- 0 until p.nCacheReadPort) {
    for (b <- 0 until p.nWordByte) {
      io.b_read(r).ack.rdata(b) := 0.U
    }

    for (s <- 0 until p.nSubCache) {
      when (s.U === reg_rsubcache(r)) {
        io.b_read(r).ack.rdata := subcache(s).io.b_read(r).ack.rdata
      }
    }
  }

  // ------------------------------
  //             WRITE
  // ------------------------------
  for (w <- 0 until p.nCacheWritePort) {
    io.b_write(w).ready := false.B

    for (s <- 0 until p.nSubCache) {
      subcache(s).io.b_write(w).valid := Mux(s.U === io.b_write(w).subcache, io.b_write(w).valid, false.B)
      subcache(s).io.b_write(w).dome := io.b_write(w).dome
      subcache(s).io.b_write(w).size := io.b_write(w).size
      subcache(s).io.b_write(w).set := io.b_write(w).set
      subcache(s).io.b_write(w).line := io.b_write(w).line
      subcache(s).io.b_write(w).offset := io.b_write(w).offset
      subcache(s).io.b_write(w).wdata := io.b_write(w).wdata

      when (s.U === io.b_write(w).subcache) {
        subcache(s).io.b_write(w).valid := io.b_write(w).valid
        io.b_write(w).ready := subcache(s).io.b_write(w).ready
      }.otherwise {
        subcache(s).io.b_write(w).valid := false.B
      }
    }
  }

  // ------------------------------
  //            PENDING
  // ------------------------------
  for (pa <- 0 until p.nPendingAcc) {
    for (s <- 0 until p.nSubCache) {
      subcache(s).io.i_pend(pa).valid := Mux(s.U === io.i_pend(pa).subcache, io.i_pend(pa).valid, false.B)
      subcache(s).io.i_pend(pa).set := io.i_pend(pa).set
      subcache(s).io.i_pend(pa).line := io.i_pend(pa).line
    }
  }

  // ------------------------------
  //          END REPLACE
  // ------------------------------
  for (s <- 0 until p.nSubCache) {
    subcache(s).io.i_end.valid := Mux(s.U === io.i_end.subcache, io.i_end.valid, false.B)
    subcache(s).io.i_end.set := io.i_end.set
    subcache(s).io.i_end.line := io.i_end.line
  }

  // ******************************
  //            RESOURCE
  // ******************************
  for (s <- 0 until p.nSubCache) {
    subcache(s).io.b_dome <> io.b_dome
  }

  if (p.usePartWay) {
    for (l <- 0 until p.nLine) {
      val w_free = Wire(Vec(p.nSubCache, Bool()))

      for (s <- 0 until p.nSubCache) {
        subcache(s).io.b_rsrc(l) <> io.b_rsrc(l)
        w_free(s) := subcache(s).io.b_rsrc(l).free
      }

      io.b_rsrc(l).free := w_free.asUInt.andR
    }
  } else {
    for (s <- 0 until p.nSubCache) {
      val w_free = Wire(Vec(p.nLine, Bool()))

      for (l <- 0 until p.nLine) {
        subcache(s).io.b_rsrc(l) <> io.b_rsrc(s)
        w_free(l) := subcache(s).io.b_rsrc(l).free
      }

      io.b_rsrc(s).free := w_free.asUInt.andR
    }
  }

  // ******************************
  //             FLUSH
  // ******************************
  for (f <- 0 until p.nFlush) {
    val w_ready = Wire(Vec(p.nSubCache, Bool()))

    for (s <- 0 until p.nSubCache) {
      subcache(s).io.b_flush(f) <> io.b_flush(f)
      if (!p.usePartWay) {
        subcache(s).io.b_flush(f).valid := Mux((io.b_flush(f).dome === io.b_rsrc(s).dome), io.b_flush(f).valid, false.B)
      } else {
        subcache(s).io.b_flush(f).valid := io.b_flush(f).valid
      }

      w_ready(s) := subcache(s).io.b_flush(f).ready
    }

    io.b_flush(f).ready := w_ready.asUInt.andR
  }
}

object SlctIndex extends App {
  chisel3.Driver.execute(args, () => new SlctIndex(4))
}

object Cache extends App {
  chisel3.Driver.execute(args, () => new Cache(CacheDefault))
}
