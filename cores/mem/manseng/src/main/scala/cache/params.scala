package cowlibry.mem.manseng.cache

import chisel3._
import chisel3.util._
import scala.math._


trait SetParams {
  def nDome: Int
  def nFlush: Int
  def usePartWay: Boolean

  def nAccess: Int
  def nSetReadPort: Int
  def nSetWritePort: Int
  def slctPolicy: String
  def nPendingAcc: Int

  def nWord: Int
  def nWordByte: Int
  def nLine: Int
  def nLineByte: Int = (nWord * nWordByte)
  def nTagBit: Int
}

trait SubCacheParams extends SetParams {
  def nDome: Int
  def nFlush: Int
  def usePartWay: Boolean

  def nAccess: Int
  def nCacheReadPort: Int = nAccess
  def nCacheWritePort: Int = nAccess + 1
  def nSetReadPort: Int
  def nSetWritePort: Int
  def slctPolicy: String
  def nPendingAcc: Int

  def nWord: Int
  def nWordByte: Int
  def nLine: Int
  def nSet: Int
  def nTagBit: Int
}

trait CacheParams extends SubCacheParams {
  def nDome: Int
  def nFlush: Int
  def usePartWay: Boolean

  def nAccess: Int
  def nSetReadPort: Int
  def nSetWritePort: Int
  def nRsrc: Int = {
    if (usePartWay) {
      return nLine
    } else {
      return nSubCache
    }
  }
  def slctPolicy: String
  def nPendingAcc: Int

  def nWord: Int
  def nWordByte: Int
  def nLine: Int
  def nSet: Int
  def nSubCache: Int
  def nTagBit: Int
}
