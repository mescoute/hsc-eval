package cowlibry.mem.manseng.cache

import chisel3._
import chisel3.util._

import cowlibry.common.tools.MATH
import cowlibry.common.mem.flush._
import cowlibry.common.mem.replace._
import cowlibry.common.rsrc._
import cowlibry.mem.manseng.tools._
import cowlibry.mem.manseng.mpu._


class CtrlSet(p: SetParams) extends Module {
  require((p.slctPolicy == "LRU") || (p.slctPolicy == "BitPLRU") || (p.slctPolicy == "Direct"), "Available replace policies are only Direct | LRU | BitPLRU.")
  require((p.slctPolicy != "Direct") || (MATH.islog2(p.nLine)), "Set must have a power of 2 number of lines for Direct replace policy !")

  val io = IO(new Bundle {
    val b_dome = Vec(p.nDome, new DomeInfoBus(p.nLine))
    val b_rsrc = Vec(p.nLine, new RsrcBus(1, p.nDome, p.nLine))
    val b_flush = Vec(p.nFlush, Flipped(new FlushIO(1, p.nDome, p.nTagBit)))

    val b_acc = Vec(p.nAccess, new SetAccBus(p))
    val b_rep = new SetRepBus(p)

    val i_pend = Vec(p.nPendingAcc, new SetPendBus(p))
    val i_end = new SetPendBus(p)
  })

  val w_flush = Wire(Vec(p.nLine, Bool()))

  // ******************************
  //        CONTROL REGISTERS
  // ******************************
  val reg_vtag = RegInit(VecInit(Seq.fill(p.nLine)(0.B)))
  val reg_vline = RegInit(VecInit(Seq.fill(p.nLine)(0.B)))
  val reg_tag = Reg(Vec(p.nLine, UInt(p.nTagBit.W)))

  // ******************************
  //            ACCESS
  // ******************************
  // ------------------------------
  //       CHECK DATA PRESENCE
  // ------------------------------
  val w_acc_use_line = Wire(Vec(p.nAccess, Vec(p.nLine, Bool())))
  val w_acc_here = Wire(Vec(p.nAccess, Bool()))
  val w_acc_line = Wire(Vec(p.nAccess, UInt(p.nLine.W)))

  for (a <- 0 until p.nAccess) {
    for (l <- 0 until p.nLine) {
      if (p.usePartWay) {
        w_acc_use_line(a)(l) := io.b_rsrc(l).valid & ~w_flush(l) & (io.b_acc(a).dome === io.b_rsrc(l).dome)
      } else {
        w_acc_use_line(a)(l) := ~w_flush(l)
      }
    }
  }

  for (a <- 0 until p.nAccess) {
    w_acc_here(a) := false.B
    w_acc_line(a) := 0.U
    for (l <- 0 until p.nLine) {
      when(reg_vtag(l) & reg_vline(l) & io.b_acc(a).valid & (reg_tag(l) === io.b_acc(a).tag) & w_acc_use_line(a)(l)) {
        w_acc_here(a) := true.B
        w_acc_line(a) := l.U
      }
    }
    io.b_acc(a).ready := w_acc_here(a)
    io.b_acc(a).line := w_acc_line(a)
  }

  // ******************************
  //            REPLACE
  // ******************************
  // ------------------------------
  //       DETECT IN-USE LINE
  // ------------------------------
  val w_use = Wire(Vec(p.nLine, Bool()))
  for (l <- 0 until p.nLine) {
    w_use(l) := false.B
    for (a <- 0 until p.nAccess) {
      when(io.b_acc(a).valid & w_acc_here(a) & (l.U === w_acc_line(a))) {
        w_use(l) := true.B
      }
    }
    for (a <- 0 until p.nPendingAcc) {
      when(io.i_pend(a).valid & (l.U === io.i_pend(a).line)) {
        w_use(l) := true.B
      }
    }
  }

  // ------------------------------
  //      CHECK DATA PRESENCE
  // ------------------------------
  val w_rep_use_line = Wire(Vec(p.nLine, Bool()))
  val w_rep_here = Wire(Bool())
  val w_rep_line = Wire(UInt(log2Ceil(p.nLine).W))

  for (l <- 0 until p.nLine) {
    if (p.usePartWay) {
      w_rep_use_line(l) := io.b_rsrc(l).valid & ~w_flush(l) & (io.b_rep.dome === io.b_rsrc(l).dome)
    } else {
      w_rep_use_line(l) := ~w_flush(l)
    }
  }

  w_rep_here := false.B
  w_rep_line := 0.U

  for (l <- 0 until p.nLine) {
    when(reg_vtag(l) & io.b_rep.valid & (reg_tag(l) === io.b_rep.tag) & w_rep_use_line(l)) {
      w_rep_here := true.B
      w_rep_line := l.U
    }
  }

  // ------------------------------
  //    UPDATE POLICY REGISTERS
  // ------------------------------
  val w_pol_free = Wire(Vec(p.nLine, Bool()))
  val w_rep_av = Wire(Bool())
  val w_rep_new_line = Wire(UInt(log2Ceil(p.nLine).W))

  // ### LRU ###
  if (p.slctPolicy == "LRU") {
    val lru = Module(new LruPolicy(p.nAccess, p.nLine))
    for (l <- 0 until p.nLine) {
      lru.io.i_flush(l) := w_flush(l)
    }
    for (a <- 0 until p.nAccess) {
      lru.io.i_valid(a) := w_acc_here(a)
      lru.io.i_line(a) := w_acc_line(a)
    }
    lru.io.i_rep := io.b_rep.valid & ~w_rep_here & ~io.b_rep.check
    for (l <- 0 until p.nLine) {
      lru.io.i_use_line(l) := ~w_use(l) & ~(reg_vtag(l) & ~reg_vline(l)) & w_rep_use_line(l)
    }

    w_pol_free := lru.io.o_free
    w_rep_av := lru.io.o_rep_av
    w_rep_new_line := lru.io.o_rep_line

  // ### BIT PSEUDO-LRU ###
} else if (p.slctPolicy == "BitPLRU") {
    val bitplru = Module(new BitPlruPolicy(p.nAccess, p.nLine))
    for (l <- 0 until p.nLine) {
      bitplru.io.i_flush(l) := w_flush(l)
    }
    for (a <- 0 until p.nAccess) {
      bitplru.io.i_valid(a) := w_acc_here(a)
      bitplru.io.i_line(a) := w_acc_line(a)
    }
    bitplru.io.i_rep := io.b_rep.valid & ~w_rep_here & ~io.b_rep.check
    for (l <- 0 until p.nLine) {
      bitplru.io.i_use_line(l) := ~w_use(l) & ~(reg_vtag(l) & ~reg_vline(l)) & w_rep_use_line(l)
    }

    w_pol_free := bitplru.io.o_free
    w_rep_av := bitplru.io.o_rep_av
    w_rep_new_line := bitplru.io.o_rep_line

  // ### DIRECT ###
  } else {
    for (l <- 0 until p.nLine) {
      w_pol_free(l) := true.B
    }
    if (p.usePartWay && p.nLine > 1) {
      val slct = Module(new SlctIndex(log2Ceil(p.nLine)))
      slct.io.i_max := io.b_dome(io.b_rep.dome).weight
      slct.io.i_index := io.b_rep.tag(log2Ceil(p.nLine) - 1,0)

      w_rep_av := 0.B
      w_rep_new_line := 0.U
      for (l <- 0 until p.nLine) {
        when (io.b_rsrc(l).valid & ~w_flush(l) & (io.b_rsrc(l).dome === io.b_rep.dome) & (io.b_rsrc(l).port === slct.io.o_slct)) {
          w_rep_av := ~(w_use(l) | (reg_vtag(l) & ~reg_vline(l))) & w_rep_use_line(l)
          w_rep_new_line := l.U
        }
      }
    } else if (p.nLine > 1) {
      w_rep_av := 0.B
      w_rep_new_line := io.b_rep.tag(log2Ceil(p.nLine) - 1,0)
      for (l <- 0 until p.nLine) {
        when ((l.U === io.b_rep.tag(log2Ceil(p.nLine) - 1,0)) & ~w_flush(l)) {
          w_rep_av := ~(w_use(l) | (reg_vtag(l) & ~reg_vline(l))) & w_rep_use_line(l)
        }
      }
    } else {
      w_rep_av := ~w_use(0) & ~(reg_vtag(0) & ~reg_vline(0)) & w_rep_use_line(0) & ~w_flush(0)
      w_rep_new_line := 0.U
    }
  }

  io.b_rep.ready := Mux(io.b_rep.check, w_rep_here, w_rep_here | w_rep_av)
  io.b_rep.line := Mux(io.b_rep.check, w_rep_line, w_rep_new_line)

  // ------------------------------
  //    UPDATE CONTROL REGISTERS
  // ------------------------------
  for (l <- 0 until p.nLine) {
    when(w_flush(l) & reg_vtag(l) & reg_vline(l) & ~w_use(l)) {
      reg_vtag(l) := false.B
      reg_vline(l) := false.B
    }.elsewhen (io.i_end.valid & (l.U === io.i_end.line)) {
      reg_vline(l) := true.B
    }.elsewhen (io.b_rep.valid & ~io.b_rep.check & w_rep_av & ~w_rep_here & (l.U === w_rep_new_line)) {
      reg_vtag(l) := true.B
      reg_vline(l) := false.B
      reg_tag(l) := io.b_rep.tag
    }
  }

  // ******************************
  //             FLUSH
  // ******************************
  val w_flush_here = Wire(Vec(p.nFlush, Bool()))
  val w_flush_line = Wire(Vec(p.nFlush, UInt(p.nLine.W)))

  for (f <- 0 until p.nFlush) {
    w_flush_here(f) := false.B
    w_flush_line(f) := 0.U
    for (l <- 0 until p.nLine) {
      if (p.usePartWay) {
        when (reg_vtag(l) & reg_vline(l) & io.b_flush(f).valid & io.b_flush(f).apply & (reg_tag(l) === io.b_flush(f).addr) & io.b_rsrc(l).valid & (io.b_flush(f).dome === io.b_rsrc(l).dome)) {
          w_flush_here(f) := true.B
          w_flush_line(f) := l.U
        }
      } else {
        when (reg_vtag(l) & reg_vline(l) & io.b_flush(f).valid & io.b_flush(f).apply & (reg_tag(l) === io.b_flush(f).addr)) {
          w_flush_here(f) := true.B
          w_flush_line(f) := l.U
        }
      }
    }
  }

  for (l <- 0 until p.nLine) {
    w_flush(l) := io.b_rsrc(l).flush

    for (f <- 0 until p.nFlush) {
      when ((io.b_flush(f).dome === io.b_rsrc(l).dome) & ((io.b_flush(f).mode === MODE.FULL.U) | (w_flush_here(f) & ((io.b_flush(f).mode === MODE.SET.U) | ((io.b_flush(f).mode === MODE.LINE.U) & (l.U === w_flush_line(f))))))) {
        w_flush(l) := io.b_flush(f).valid & io.b_flush(f).apply
      }
    }
  }

  // ******************************
  //             FREE
  // ******************************
  for (l <- 0 until p.nLine) {
    io.b_rsrc(l).free := ~reg_vtag(l) & ~reg_vline(l) & w_pol_free(l)
  }

  for (f <- 0 until p.nFlush) {
    io.b_flush(f).ready := io.b_flush(f).valid

    when ((w_flush_here(f) & (io.b_flush(f).mode === MODE.SET.U)) | (io.b_flush(f).mode === MODE.FULL.U)) {
      for (l <- 0 until p.nLine) {
        when ((io.b_flush(f).dome === io.b_rsrc(l).dome) & (reg_vtag(l) | reg_vline(l) | ~w_pol_free(l))) {
          io.b_flush(f).ready := false.B
        }
      }
    }.elsewhen (w_flush_here(f)) {
      io.b_flush(f).ready := ~reg_vtag(w_flush_line(f)) & ~reg_vline(w_flush_line(f)) & w_pol_free(w_flush_line(f))
    }
  }
}

object CtrlSet extends App {
  chisel3.Driver.execute(args, () => new CtrlSet(SetDefault))
}
