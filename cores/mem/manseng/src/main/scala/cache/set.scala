package cowlibry.mem.manseng.cache

import chisel3._
import chisel3.util._

import cowlibry.common.mem.flush._
import cowlibry.common.rsrc._
import cowlibry.mem.manseng.tools._
import cowlibry.mem.manseng.mpu._


class Set(p: SetParams) extends Module {
  val io = IO(new Bundle {
    val b_dome = Vec(p.nDome, new DomeInfoBus(p.nLine))
    val b_rsrc = Vec(p.nLine, new RsrcBus(1, p.nDome, p.nLine))
    val b_flush = Vec(p.nFlush, Flipped(new FlushIO(1, p.nDome, p.nTagBit)))

    val b_acc = Vec(p.nAccess, new SetAccBus(p))
    val b_rep = new SetRepBus(p)

    val b_read = Vec(p.nSetReadPort, new SetReadBus(p))
    val b_write = Vec(p.nSetWritePort, new SetWriteBus(p))

    val i_pend = Vec(p.nPendingAcc, new SetPendBus(p))
    val i_end = new SetPendBus(p)
  })

  // ******************************
  //             CTRL
  // ******************************
  val ctrl = Module(new CtrlSet(p))
  ctrl.io.b_dome <> io.b_dome
  ctrl.io.b_rsrc <> io.b_rsrc
  ctrl.io.b_flush <> io.b_flush
  ctrl.io.b_acc <> io.b_acc
  ctrl.io.b_rep <> io.b_rep
  ctrl.io.i_pend <> io.i_pend
  ctrl.io.i_end <> io.i_end

  // ******************************
  //             DATA
  // ******************************
  val data = Module(new DataSet(p))
  data.io.b_read <> io.b_read
  data.io.b_write <> io.b_write
}

object Set extends App {
  chisel3.Driver.execute(args, () => new Set(SetDefault))
}
