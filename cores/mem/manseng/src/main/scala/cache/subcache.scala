package cowlibry.mem.manseng.cache

import chisel3._
import chisel3.util._
import scala.math._

import cowlibry.common.tools.{MATH, StableSlct}
import cowlibry.common.rsrc._
import cowlibry.common.mem.flush._
import cowlibry.mem.manseng.tools._
import cowlibry.mem.manseng.mpu._


class SubCache(p: SubCacheParams) extends Module {
  require(MATH.islog2(p.nSet), "SubCache must have a power of 2 number of sets !")
  require(p.nCacheReadPort >= p.nSetReadPort, "Cache can not have less read ports than a set.")
  require(p.nCacheWritePort >= p.nSetWritePort, "Cache can not have less write ports than a set.")

  val io = IO(new Bundle {
    val b_dome = Vec(p.nDome, new DomeInfoBus(p.nLine))
    val b_rsrc = Vec(p.nLine, new RsrcBus(1, p.nDome, p.nLine))
    val b_flush = Vec(p.nFlush, Flipped(new FlushIO(1, p.nDome, p.nTagBit + log2Ceil(p.nSet) + log2Ceil(p.nLineByte))))

    val b_acc = Vec(p.nAccess, new SubCacheAccBus(p))
    val b_rep = new SubCacheRepBus(p)

    val b_read = Vec(p.nCacheReadPort, new SubCacheReadBus(p))
    val b_write = Vec(p.nCacheWritePort, new SubCacheWriteBus(p))

    val i_pend = Vec(p.nPendingAcc, new SubCachePendBus(p))
    val i_end = new SubCachePendBus(p)
  })

  val set = Seq.fill(p.nSet) {Module(new Set(p))}

  // ******************************
  //           RESOURCE
  // ******************************
  for (s <- 0 until p.nSet) {
    set(s).io.b_dome <> io.b_dome
    set(s).io.b_rsrc <> io.b_rsrc
  }

  for (l <- 0 until p.nLine) {
    val w_free = Wire(Vec(p.nSet, Bool()))
    for (s <- 0 until p.nSet) {
      w_free(s) := set(s).io.b_rsrc(l).free
    }
    io.b_rsrc(l).free := w_free.asUInt.andR
  }

  // ******************************
  //             CTRL
  // ******************************
  // ------------------------------
  //            ACCESS
  // ------------------------------
  // Default
  for (s <- 0 until p.nSet) {
    for (a <- 0 until p.nAccess) {
      set(s).io.b_acc(a).valid := false.B
      set(s).io.b_acc(a).dome := io.b_acc(a).dome
      set(s).io.b_acc(a).tag := io.b_acc(a).tag
    }
  }

  for (a <- 0 until p.nAccess) {
    io.b_acc(a).ready := false.B
    io.b_acc(a).line := 0.U
  }

  // Connections
  for (a <- 0 until p.nAccess) {
    for (s <- 0 until p.nSet) {
      when(s.U === io.b_acc(a).set) {
        set(s).io.b_acc(a).valid := io.b_acc(a).valid
        io.b_acc(a).ready := set(s).io.b_acc(a).ready
        io.b_acc(a).line := set(s).io.b_acc(a).line
      }
    }
  }

  // ------------------------------
  //         CHECK & REPLACE
  // ------------------------------
  // Default
  for (s <- 0 until p.nSet) {
    set(s).io.b_rep.valid := false.B
    set(s).io.b_rep.dome := io.b_rep.dome
    set(s).io.b_rep.check := io.b_rep.check
    set(s).io.b_rep.tag := io.b_rep.tag
  }

  io.b_rep.ready := set(0).io.b_rep.ready
  io.b_rep.line := set(0).io.b_rep.line

  // Connections
  for (s <- 0 until p.nSet) {
    when(s.U === io.b_rep.set) {
      set(s).io.b_rep.valid := io.b_rep.valid
      io.b_rep.ready := set(s).io.b_rep.ready
      io.b_rep.line := set(s).io.b_rep.line
    }
  }

  // ------------------------------
  //           PENDING
  // ------------------------------
  // Default
  for (s <- 0 until p.nSet) {
    for (a <- 0 until p.nPendingAcc) {
      set(s).io.i_pend(a).valid := false.B
      set(s).io.i_pend(a).line := io.i_pend(a).line
    }
  }

  // Connections
  for (a <- 0 until p.nPendingAcc) {
    for (s <- 0 until p.nSet) {
      when (s.U === io.i_pend(a).set) {
        set(s).io.i_pend(a).valid := io.i_pend(a).valid
      }
    }
  }

  // ------------------------------
  //          END REPLACE
  // ------------------------------
  // Default
  for (s <- 0 until p.nSet) {
    set(s).io.i_end.valid := false.B
    set(s).io.i_end.line := io.i_end.line
  }

  // Connections
  for (s <- 0 until p.nSet) {
    when (s.U === io.i_end.set) {
      set(s).io.i_end.valid := io.i_end.valid
    }
  }

  // ******************************
  //             DATA
  // ******************************
  // ------------------------------
  //           DOME SLCT
  // ------------------------------
  var nSetPort: Int = max(p.nSetReadPort, p.nSetWritePort)

  val w_rslct = Wire(Vec(p.nSet, Vec(nSetPort, UInt(log2Ceil(p.nDome).W))))
  val reg_wslct = Reg(Vec(p.nSet, Vec(nSetPort, UInt(log2Ceil(p.nDome).W))))

  reg_wslct := w_rslct

  if (!p.usePartWay || (p.nDome == 1)) {
    for (s <- 0 until p.nSet) {
      for (sp <- 0 until nSetPort) {
        w_rslct(s)(sp) := 0.U
      }
    }
  } else {
    val w_chain = Wire(Vec(p.nSet, UInt(log2Ceil(p.nDome).W)))

    for (s <- 0 until p.nSet) {
      val slct = Seq.fill(nSetPort) {Module(new StableSlct(true, p.nDome))}

      for (sp <- 0 until nSetPort) {
        for (d <- 0 until p.nDome) {
          slct(sp).io.i_valid(d) := io.b_dome(d).valid
        }
      }

      w_chain(s) := slct(0).io.o_chain.get
      if (s == 0) {
        slct(0).io.i_chain.get := slct(0).io.o_slct
      } else {
        slct(0).io.i_chain.get := w_chain(s - 1)
      }

      for (sp <- 1 until nSetPort) {
        slct(sp).io.i_chain.get := slct(sp - 1).io.o_chain.get
      }

      for (sp <- 0 until nSetPort) {
        w_rslct(s)(sp) := slct(sp).io.o_slct
      }
    }
  }

  // ------------------------------
  //        SYNCHRONOUS READ
  // ------------------------------
  // Read port of set is used ?
  val w_read_use = Wire(Vec(p.nCacheReadPort + 1, Vec(p.nSet, Vec(p.nSetReadPort, Bool()))))
  // Read request has a port ?
  val w_read_done = Wire(Vec(p.nCacheReadPort, Vec(p.nSetReadPort + 1, Bool())))

  val reg_rset = Reg(Vec(p.nCacheReadPort, UInt(log2Ceil(p.nSet).W)))
  val reg_rport = Reg(Vec(p.nCacheReadPort, UInt(log2Ceil(p.nSetReadPort).W)))

  for (cr <- 0 until p.nCacheReadPort) {
    reg_rset(cr) := io.b_read(cr).req.set
  }

  for (s <- 0 until p.nSet) {
    for (sr <- 0 until p.nSetReadPort) {
      w_read_use(0)(s)(sr) := false.B
    }
  }

  for (cr <- 1 to p.nCacheReadPort) {
    for (s <- 0 until p.nSet) {
      for (sr <- 0 until p.nSetReadPort) {
        w_read_use(cr)(s)(sr) := w_read_use(cr - 1)(s)(sr)
      }
    }
  }

  for (cr <- 0 until p.nCacheReadPort) {
    w_read_done(cr)(0) := false.B

    for (sr <- 1 to p.nSetReadPort) {
      w_read_done(cr)(sr) := w_read_done(cr)(sr - 1)
    }
  }

  // Default
  for (s <- 0 until p.nSet) {
    for (sr <- 0 until p.nSetReadPort) {
      set(s).io.b_read(sr).size := io.b_read(sr).req.size
      set(s).io.b_read(sr).line := io.b_read(sr).req.line
      set(s).io.b_read(sr).offset := io.b_read(sr).req.offset
    }
  }

  for (cr <- 0 until p.nCacheReadPort) {
    io.b_read(cr).req.ready := false.B
    io.b_read(cr).ack.rdata := set(0).io.b_read(0).rdata
  }

  // Connections
  if (p.nSetReadPort == p.nCacheReadPort) {
    for (cr <- 0 until p.nCacheReadPort) {
      io.b_read(cr).req.ready := true.B
      reg_rport(cr) := cr.U
    }
  } else {
    for (cr <- 0 until p.nCacheReadPort) {
      for (s <- 0 until p.nSet) {
        when (s.U === io.b_read(cr).req.set) {
          for (sr <- 0 until p.nSetReadPort) {
            when (io.b_read(cr).req.valid & ~w_read_done(cr)(sr) & ~w_read_use(cr)(s)(sr) & (io.b_read(cr).req.dome === w_rslct(s)(sr))) {
              w_read_use(cr + 1)(s)(sr) := true.B
              w_read_done(cr)(sr + 1) := true.B

              set(s).io.b_read(sr).size := io.b_read(cr).req.size
              set(s).io.b_read(sr).line := io.b_read(cr).req.line
              set(s).io.b_read(sr).offset := io.b_read(cr).req.offset

              io.b_read(cr).req.ready := true.B
              reg_rport(cr) := sr.U
            }
          }
        }
      }
    }
  }

  //Response connections (post-request cycle)
  for (cr <- 0 until p.nCacheReadPort) {
    for (s <- 0 until p.nSet) {
      when (s.U === reg_rset(cr)) {
        io.b_read(cr).ack.rdata := set(s).io.b_read(reg_rport(cr)).rdata
      }
    }
  }

  // ------------------------------
  //             WRITE
  // ------------------------------
  // Write port of set is used ?
  val w_write_use = Wire(Vec(p.nCacheWritePort + 1, Vec(p.nSet, Vec(p.nSetWritePort, Bool()))))
  // Write request has a port ?
  val w_write_done = Wire(Vec(p.nCacheWritePort, Vec(p.nSetWritePort + 1, Bool())))

  for (s <- 0 until p.nSet) {
    for (sw <- 0 until p.nSetWritePort) {
      w_write_use(0)(s)(sw) := false.B
    }
  }

  for (cw <- 1 to p.nCacheWritePort) {
    for (s <- 0 until p.nSet) {
      for (sw <- 0 until p.nSetWritePort) {
        w_write_use(cw)(s)(sw) := w_write_use(cw - 1)(s)(sw)
      }
    }
  }

  for (cw <- 0 until p.nCacheWritePort) {
    w_write_done(cw)(0) := false.B

    for (sw <- 1 to p.nSetWritePort) {
      w_write_done(cw)(sw) := w_write_done(cw)(sw - 1)
    }
  }


  // Default
  for (s <- 0 until p.nSet) {
    for (sw <- 0 until p.nSetWritePort) {
      set(s).io.b_write(sw).valid := false.B
      set(s).io.b_write(sw).size := io.b_write(sw).size
      set(s).io.b_write(sw).line := io.b_write(sw).line
      set(s).io.b_write(sw).offset := io.b_write(sw).offset
      set(s).io.b_write(sw).wdata := io.b_write(sw).wdata
    }
  }

  for (cw <- 0 until p.nCacheWritePort) {
    io.b_write(cw).ready := false.B
  }

  // Connections
  if (p.nSetWritePort == p.nCacheWritePort) {
    for (s <- 0 until p.nSet) {
      for (sw <- 0 until p.nSetWritePort) {
        set(s).io.b_write(sw).valid := io.b_write(sw).valid & (s.U === io.b_write(sw).set)
        io.b_write(sw).ready := true.B
      }
    }
  } else {
    for (cw <- 0 until p.nCacheWritePort) {
      for (s <- 0 until p.nSet) {
        when (s.U === io.b_write(cw).set) {
          for (sw <- 0 until p.nSetWritePort) {
            when (io.b_write(cw).valid & ~w_write_done(cw)(sw) & ~w_write_use(cw)(s)(sw) & (io.b_write(cw).dome === reg_wslct(s)(sw))) {
              w_write_use(cw + 1)(s)(sw) := true.B
              w_write_done(cw)(sw + 1) := true.B

              set(s).io.b_write(sw).valid := true.B
              set(s).io.b_write(sw).size := io.b_write(cw).size
              set(s).io.b_write(sw).line := io.b_write(cw).line
              set(s).io.b_write(sw).offset := io.b_write(cw).offset
              set(s).io.b_write(sw).wdata := io.b_write(cw).wdata

              io.b_write(cw).ready := true.B
            }
          }
        }
      }
    }
  }

  // ******************************
  //             FLUSH
  // ******************************
  val w_flush_tag = Wire(Vec(p.nFlush, UInt(p.nTagBit.W)))
  val w_flush_set = Wire(Vec(p.nFlush, UInt(log2Ceil(p.nSet).W)))

  for (f <- 0 until p.nFlush) {
    if (p.nTagBit > 0) w_flush_tag(f) := io.b_flush(f).addr(p.nTagBit + log2Ceil(p.nSet) + log2Ceil(p.nLineByte) - 1, log2Ceil(p.nSet) + log2Ceil(p.nLineByte)) else w_flush_tag(f) := 0.U
    if (p.nSet > 1) w_flush_set(f) := io.b_flush(f).addr(log2Ceil(p.nLineByte) + log2Ceil(p.nSet) - 1, log2Ceil(p.nLineByte)) else w_flush_set(f) := 0.U
  }

  for (f <- 0 until p.nFlush) {
    for (s <- 0 until p.nSet) {
      set(s).io.b_flush(f) <> io.b_flush(f)
      set(s).io.b_flush(f).addr <> w_flush_tag(f)
    }
    io.b_flush(f).ready := false.B

    for (s <- 0 until p.nSet) {
      when (s.U === w_flush_set(f)) {
        set(s).io.b_flush(f).valid := io.b_flush(f).valid
        io.b_flush(f).ready := set(s).io.b_flush(f).ready
      }.otherwise {
        set(s).io.b_flush(f).valid := io.b_flush(f).valid & (io.b_flush(f).mode === MODE.FULL.U)
      }
    }
  }
}

object SubCache extends App {
  chisel3.Driver.execute(args, () => new SubCache(SubCacheDefault))
}
