package cowlibry.mem.manseng.next

import chisel3._
import chisel3.util._
import scala.math._

import cowlibry.mem.manseng.cache._


trait NextParams extends CacheParams {
  def debug: Boolean
  def nAddrBit: Int
  def nDome: Int
  def nHart: Int
  def nFlush: Int
  def usePartWay: Boolean

  def nPrevMem: Int
  def nPrev: Int
  def nPrevWordByte: Int
  def nPrevWordBit: Int = nPrevWordByte * 8

  def nNext: Int = {
    if (nDome > 1) {
      return nDome
    } else {
      return 1
    }
  }
  def nNextWordByte: Int
  def nNextWordBit: Int = nNextWordByte * 8
  def nNextReqFifoDepth: Int
  def nNextMemFifoDepth: Int
  def nNextWriteFifoDepth: Int
  def nNextRepCycle: Int = (nWordByte * nWord) / nNextWordByte

  def nAccess: Int
  def nSetReadPort: Int
  def nSetWritePort: Int
  def slctPolicy: String
  def nPendingAcc: Int

  def nWord: Int
  override def nWordByte: Int = max(nPrevWordByte, nNextWordByte)
  def nLine: Int
  def nSet: Int
  def nSubCache: Int
  def nTagBit: Int = nAddrBit - log2Ceil(nSet) - log2Ceil(nLineByte)
}
