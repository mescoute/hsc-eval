package cowlibry.mem.manseng.next

import chisel3._
import chisel3.util._
import scala.math._


object NextDefault extends NextParams {
  def debug: Boolean = true
  def nAddrBit: Int = 32
  def nDome: Int = 1
  def nHart: Int = 1
  def nFlush: Int = 1
  def usePartWay: Boolean = true

  def nPrevMem: Int = 1
  def nPrev: Int = 1
  def nPrevWordByte: Int = 4

  def nNextWordByte: Int = 8
  def nNextReqFifoDepth: Int = 2
  def nNextMemFifoDepth: Int = 2
  def nNextWriteFifoDepth: Int = 2

  def nAccess: Int = 1
  def nSetReadPort: Int = 1
  def nSetWritePort: Int = 1
  def slctPolicy: String = "BitPLRU"
  def nPendingAcc: Int = 4

  def nWord: Int = 4
  def nLine: Int = 4
  def nSet: Int = 4
  def nSubCache: Int = 1
}
