package cowlibry.mem.manseng.next

import chisel3._
import chisel3.util._

import cowlibry.common.tools._
import cowlibry.common.rsrc._
import cowlibry.common.mem.pmb._
import cowlibry.common.mem.flush._
import cowlibry.mem.manseng.tools._
import cowlibry.mem.manseng.cache._


class InitStage(p: NextParams) extends Module {
  val io = IO(new Bundle {
    val b_dome = Vec(p.nDome, new DomeBus(1))
    val b_flush = Vec(p.nFlush, Flipped(new FlushIO(p.nHart, p.nDome, p.nAddrBit)))
    val i_lock = Input(Bool())
    val o_lock = Output(Bool())

    val i_slct = Input(UInt(log2Ceil(p.nNext).W))
    val i_valid = Input(Bool())
    val i_bus = Input(new NextReqBus(p))

    val b_ctrl = Flipped(new CacheRepBus(p))

    val o_slct = Output(UInt(log2Ceil(p.nNext).W))
    val o_valid = Output(Bool())
    val o_bus = Output(new NextBus(p))
  })

  dontTouch(io.o_bus.end)

  val s0NEW :: s1REP :: s2CHECK :: Nil = Enum(3)
  val reg_state = RegInit(VecInit(Seq.fill(p.nNext)(s0NEW)))
  val w_cstate = Wire(UInt(2.W))
  val w_nstate = Wire(UInt(2.W))

  val w_rep_offset = Wire(UInt(log2Ceil(p.nLineByte).W))
  val w_nrep_offset = Wire(UInt(log2Ceil(p.nLineByte).W))
  val reg_rep_offset = Reg(Vec(p.nNext, UInt(log2Ceil(p.nLineByte).W)))

  val w_lock = Wire(Bool())
  val w_wait = Wire(Bool())

  val w_valid = Wire(Bool())
  val w_bus = Wire(new NextBus(p))

  val init_slct = Wire(UInt(log2Ceil(p.nNext).W))
  init_slct := 0.U

  val reg_slct = RegInit(init_slct)
  val reg_valid = RegInit(VecInit(Seq.fill(p.nNext)(0.B)))
  val reg_bus = Reg(Vec(p.nNext, new NextBus(p)))

  // ******************************
  //  SELECT CURRENT AND NEW VALUE
  // ******************************
  // ------------------------------
  //             FSM
  // ------------------------------
  w_cstate := reg_state(0)
  for (n <- 0 until p.nNext) {
    when (n.U === io.i_slct) {
      w_cstate := reg_state(n)
      reg_state(n) := w_nstate
    }
  }

  // ------------------------------
  //          REPLACE OFFSET
  // ------------------------------
  w_rep_offset := reg_rep_offset(0)
  for (n <- 0 until p.nNext) {
    when (n.U === io.i_slct) {
      w_rep_offset := reg_rep_offset(n)
      reg_rep_offset(n) := w_nrep_offset
    }
  }

  // ------------------------------
  //              LOCK
  // ------------------------------
  w_lock := true.B
  for (n <- 0 until p.nNext) {
    when (n.U === io.i_slct) {
      when (io.i_slct === reg_slct) {
        w_lock := io.i_lock & reg_valid(n)
      }.otherwise {
        w_lock := reg_valid(n)
      }
    }
  }

  // ******************************
  //             LOGIC
  // ******************************
  // ------------------------------
  //              FSM
  // ------------------------------
  // ### S1: REP ###
  when(w_cstate === s1REP) {
    when (~w_lock & io.b_ctrl.ready) {
      if (p.nNextRepCycle > 1) w_nstate := s2CHECK else w_nstate := s0NEW
      if (p.nNextRepCycle > 1) w_nrep_offset := p.nNextWordByte.U else w_nrep_offset := 0.U
      if (p.nNextRepCycle > 1) w_wait := true.B else w_wait := false.B
    }.otherwise {
      w_nstate := s1REP
      w_nrep_offset := 0.U
      w_wait := true.B
    }

// ### S2: CHECK ###
}.elsewhen(w_cstate === s2CHECK) {
    when (~w_lock & io.b_ctrl.ready & w_rep_offset === (p.nLineByte - p.nNextWordByte).U) {
      w_nstate := s0NEW
      w_nrep_offset := 0.U
      w_wait := false.B
    }.elsewhen (~w_lock & io.b_ctrl.ready) {
      w_nstate := s2CHECK
      w_nrep_offset := w_rep_offset + p.nNextWordByte.U
      w_wait := true.B
    }.elsewhen (~io.b_ctrl.ready) {
      w_nstate := s1REP
      w_nrep_offset := 0.U
      w_wait := true.B
    }.otherwise {
      w_nstate := s2CHECK
      w_nrep_offset := w_rep_offset
      w_wait := true.B
    }

  // ### S0: NEW ###
  }.otherwise {
    when (io.i_valid & io.i_bus.rep & ~w_lock & io.b_ctrl.ready) {
      w_nstate := s0NEW
      w_nrep_offset := 0.U
      w_wait := false.B
    }.elsewhen (io.i_valid & io.i_bus.rep & ~w_lock) {
      w_nstate := s1REP
      w_nrep_offset := 0.U
      w_wait := true.B
    }.otherwise {
      w_nstate := s0NEW
      w_nrep_offset := 0.U
      w_wait := w_lock
    }
  }

  // ------------------------------
  //           CTRL SET
  // ------------------------------
  io.b_ctrl.valid := io.i_valid
  io.b_ctrl.tag := io.i_bus.tag
  io.b_ctrl.set := io.i_bus.set
  io.b_ctrl.offset := io.i_bus.offset
  io.b_ctrl.check := Mux(w_cstate === s1REP, false.B, true.B)
  io.b_ctrl.dome := io.i_slct

  // ------------------------------
  //             BUS
  // ------------------------------
  w_valid := io.i_valid & io.b_ctrl.ready
  w_bus.prev := io.i_bus.prev
  w_bus.hart := io.i_bus.hart
  w_bus.addr := io.i_bus.addr
  w_bus.tag := io.i_bus.tag
  w_bus.set := io.i_bus.set
  w_bus.offset := io.i_bus.offset
  w_bus.subcache := io.b_ctrl.subcache
  w_bus.line := io.b_ctrl.line
  w_bus.wdata := io.i_bus.wdata

  when(w_cstate === s1REP) {
    w_bus.rep := true.B
    if (p.nNextRepCycle > 1) w_bus.end := false.B else w_bus.end := true.B
    w_bus.size := SIZE.max(p.nNextWordBit).U
    w_bus.offset := 0.U
  }.elsewhen(w_cstate === s2CHECK) {
    w_bus.rep := true.B
    w_bus.end := (w_rep_offset === (p.nLineByte - p.nNextWordByte).U)
    w_bus.size := SIZE.max(p.nNextWordBit).U
    w_bus.offset := w_rep_offset
  }.otherwise {
    w_bus.rep := io.i_bus.rep
    w_bus.end := false.B
    w_bus.size := Mux(io.i_bus.rep, SIZE.max(p.nNextWordBit).U, io.i_bus.size)
    w_bus.offset := io.b_ctrl.offset
  }

  // ******************************
  //            OUTPUTS
  // ******************************
  reg_slct := io.i_slct
  for (n <- 0 until p.nNext) {
    when (n.U === reg_slct & ~io.i_lock) {
      reg_valid(n) := false.B
    }

    when (n.U === io.i_slct & ~w_lock) {
      reg_valid(n) := w_valid
      reg_bus(n) := w_bus
    }
  }

  io.o_lock := w_wait
  io.o_slct := reg_slct
  io.o_valid := false.B
  io.o_bus := reg_bus(0)

  for (n <- 0 until p.nNext) {
    when (n.U === reg_slct) {
      io.o_valid := reg_valid(n)
      io.o_bus := reg_bus(n)
    }
  }

  // ******************************
  //             DOME
  // ******************************
  for (n <- 0 until p.nNext) {
    io.b_dome(n).free := ~reg_valid(n)
  }

  // ******************************
  //             FLUSH
  // ******************************
  for (f <- 0 until p.nFlush) {
    io.b_flush(f).ready := io.b_flush(f).valid

    for (n <- 0 until p.nNext) {
      when (io.b_flush(f).valid & io.b_flush(f).apply & (n.U === io.b_flush(f).dome)) {
        io.b_flush(f).ready := ~reg_valid(n)
      }
    }
  }
}

object InitStage extends App {
  chisel3.Driver.execute(args, () => new InitStage(NextDefault))
}
