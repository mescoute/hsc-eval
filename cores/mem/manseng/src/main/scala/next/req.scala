package cowlibry.mem.manseng.next

import chisel3._
import chisel3.util._

import cowlibry.common.tools._
import cowlibry.common.rsrc._
import cowlibry.common.mem.pmb._
import cowlibry.common.mem.flush._
import cowlibry.mem.manseng.tools._
import cowlibry.mem.manseng.cache._


class ReqStage(p: NextParams) extends Module {
  require(p.nNextWordByte >= p.nPrevWordByte, "Next bus size must be greater than or equal to previous bus size for write through.")

  val io = IO(new Bundle {
    val b_dome = Vec(p.nDome, new DomeBus(1))
    val b_flush = Vec(p.nFlush, Flipped(new FlushIO(p.nHart, p.nDome, p.nAddrBit)))
    val i_lock = Input(Bool())

    val b_prev = Vec(p.nPrevMem, new NextPrevReqBus(p.nHart, p.nDome, p.nNext, p.nAddrBit, p.nTagBit, p.nSet, p.nLineByte, p.nPrevWordBit))

    val o_slct = Output(UInt(log2Ceil(p.nNext).W))
    val o_valid = Output(Bool())
    val o_bus = Output(new NextReqBus(p))
  })

  // ******************************
  //      SELECT EXECUTED DOME
  // ******************************
  val reg_slct = Wire(UInt(log2Ceil(p.nNext).W))

  val slct = Module(new StableSlct(false, p.nNext))
  for (n <- 0 until p.nNext) {
    slct.io.i_valid(n) := io.b_dome(n).valid
  }

  reg_slct := slct.io.o_slct

  // ******************************
  //             FIFO
  // ******************************
  val reg_bus = Seq.fill(p.nNext) {Module(new Fifo(3, new NextReqBus(p), p.nNextReqFifoDepth, p.nPrevMem, 1))}

  for (n <- 0 until p.nNext) {
    reg_bus(n).io.i_flush := false.B

    for (pm <- 0 until p.nPrevMem) {
      io.b_prev(pm).ready(n) := ~reg_bus(n).io.o_full(pm)

      reg_bus(n).io.i_wen(pm) := false.B
      reg_bus(n).io.i_data(pm).prev := pm.U
      reg_bus(n).io.i_data(pm).hart := io.b_prev(pm).hart
      reg_bus(n).io.i_data(pm).rep := io.b_prev(pm).rep
      reg_bus(n).io.i_data(pm).size := io.b_prev(pm).size
      reg_bus(n).io.i_data(pm).addr := io.b_prev(pm).addr
      reg_bus(n).io.i_data(pm).tag := io.b_prev(pm).tag
      reg_bus(n).io.i_data(pm).set := io.b_prev(pm).set
      reg_bus(n).io.i_data(pm).offset := io.b_prev(pm).offset
      reg_bus(n).io.i_data(pm).wdata := io.b_prev(pm).wdata
    }
  }

  if (p.nNext > 1) {
    for (n <- 0 until p.nNext) {
      for (pm <- 0 until p.nPrevMem) {
        when(n.U === io.b_prev(pm).dome) {
          reg_bus(n).io.i_wen(pm) := io.b_prev(pm).valid
        }
      }
    }
  } else {
    for (pm <- 0 until p.nPrevMem) {
      reg_bus(0).io.i_wen(pm) := io.b_prev(pm).valid
    }
  }

  // ******************************
  //         CONNECT OUTPUT
  // ******************************
  io.o_slct := reg_slct
  io.o_valid := false.B
  io.o_bus := reg_bus(0).io.o_data(0)

  for (n <- 0 until p.nNext) {
    when (n.U === reg_slct) {
      reg_bus(n).io.i_ren(0) := ~io.i_lock

      io.o_valid := ~reg_bus(n).io.o_empty(0)
      io.o_bus := reg_bus(n).io.o_data(0)
    }.otherwise {
      reg_bus(n).io.i_ren(0) := false.B
    }
  }

  // ******************************
  //             DOME
  // ******************************
  for (n <- 0 until p.nNext) {
    io.b_dome(n).free := reg_bus(n).io.o_empty(0)
  }

  // ******************************
  //             FLUSH
  // ******************************
  for (f <- 0 until p.nFlush) {
    io.b_flush(f).ready := io.b_flush(f).valid

    for (n <- 0 until p.nNext) {
      when (io.b_flush(f).valid & io.b_flush(f).apply & (n.U === io.b_flush(f).dome)) {
        io.b_flush(f).ready := reg_bus(n).io.o_empty(0)
      }
    }
  }
}

object ReqStage extends App {
  chisel3.Driver.execute(args, () => new ReqStage(NextDefault))
}
