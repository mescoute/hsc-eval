package cowlibry.mem.manseng.next

import chisel3._
import chisel3.util._

import cowlibry.common.tools._
import cowlibry.common.rsrc._
import cowlibry.common.mem.pmb._
import cowlibry.common.mem.flush._
import cowlibry.mem.manseng.tools._
import cowlibry.mem.manseng.cache._


class NextCtrl(p: NextParams, p_next: PmbParams) extends Module {
  val io = IO(new Bundle {
    val b_dome = Vec(p.nDome, new DomeBus(1))
    val b_flush = Vec(p.nFlush, Flipped(new FlushIO(p.nHart, p.nDome, p.nAddrBit)))

    val b_prev = Vec(p.nPrevMem, new NextPrevReqBus(p.nHart, p.nDome, p.nNext, p.nAddrBit, p.nTagBit, p.nSet, p.nLineByte, p.nPrevWordBit))

    val b_ctrl = Flipped(new CacheRepBus(p))
    val b_write = Flipped(new CacheWriteBus(p))
    val o_end = Flipped(new CachePendBus(p))

    val b_wfire = Vec(p.nPrevMem, new NextWriteFireBus(p.nNext))
    val b_mem = new PmbIO(p_next)
  })

  val req = Module(new ReqStage(p))
  val init = Module(new InitStage(p))
  val mem = Module(new MemStage(p, p_next))
  val write = Module(new WriteStage(p, p_next))

  // ******************************
  //           REQ STAGE
  // ******************************
  req.io.b_dome <> io.b_dome
  req.io.b_flush <> io.b_flush
  req.io.i_lock := init.io.o_lock
  req.io.b_prev <> io.b_prev

  // ******************************
  //          INIT STAGE
  // ******************************
  init.io.b_dome <> io.b_dome
  init.io.b_flush <> io.b_flush
  init.io.i_lock := mem.io.o_lock
  init.io.i_slct := req.io.o_slct
  init.io.i_valid := req.io.o_valid
  init.io.i_bus := req.io.o_bus
  init.io.b_ctrl <> io.b_ctrl

  // ******************************
  //          MEM STAGE
  // ******************************
  mem.io.b_dome <> io.b_dome
  mem.io.b_flush <> io.b_flush
  mem.io.i_lock := write.io.o_lock
  mem.io.i_slct := init.io.o_slct
  mem.io.i_valid := init.io.o_valid
  mem.io.i_bus := init.io.o_bus
  mem.io.b_wfire <> io.b_wfire
  mem.io.b_mem <> io.b_mem.req

  // ******************************
  //          WRITE STAGE
  // ******************************
  write.io.i_slct := mem.io.o_slct
  write.io.i_valid := mem.io.o_valid
  write.io.i_bus := mem.io.o_bus
  write.io.b_ack <> mem.io.b_ack
  write.io.b_write <> io.b_write
  write.io.b_mem <> io.b_mem.ack

  // ******************************
  //              I/O
  // ******************************
  io.o_end := write.io.o_end

  // ******************************
  //             DOME
  // ******************************
  for (n <- 0 until p.nNext) {
    io.b_dome(n).free := req.io.b_dome(n).free & init.io.b_dome(n).free & mem.io.b_dome(n).free
  }

  // ******************************
  //             FLUSH
  // ******************************
  for (f <- 0 until p.nFlush) {
    io.b_flush(f).ready := req.io.b_flush(f).ready & init.io.b_flush(f).ready & mem.io.b_flush(f).ready
  }
}

object NextCtrl extends App {
  chisel3.Driver.execute(args, () => new NextCtrl(NextDefault, PmbCfg0))
}
