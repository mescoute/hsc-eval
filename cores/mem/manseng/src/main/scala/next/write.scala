package cowlibry.mem.manseng.next

import chisel3._
import chisel3.util._

import cowlibry.common.tools._
import cowlibry.common.mem.pmb._
import cowlibry.mem.manseng.cache._

class CacheWriteBuffer(p: NextParams) extends Bundle {
  val end = Bool()
  val dome = UInt(log2Ceil(p.nDome).W)
  val size = UInt(SIZE.NBIT.W)
  val addr = UInt(p.nAddrBit.W)
  val set = UInt(log2Ceil(p.nSet).W)
  val offset = UInt(log2Ceil(p.nLineByte).W)
  val subcache = UInt(log2Ceil(p.nSubCache).W)
  val line = UInt(log2Ceil(p.nLine).W)
  val wdata = Vec(p.nWordByte, UInt(8.W))

  override def cloneType = (new CacheWriteBuffer(p)).asInstanceOf[this.type]
}

class WriteStage(p: NextParams, p_next: PmbParams) extends Module {
  val io = IO(new Bundle {
    val o_lock = Output(Bool())

    val i_slct = Input(UInt(log2Ceil(p.nNext).W))
    val i_valid = Input(Bool())
    val i_bus = Input(new NextBus(p))
    val b_ack = Vec(p.nNext, new NextAckBus())

    val b_write = Flipped(new CacheWriteBus(p))
    val o_end = Flipped(new CachePendBus(p))

    val b_mem = new PmbAckIO(p_next)
  })

  dontTouch(io.i_bus.end)

  val w_wait = Wire(Bool())
  val w_write_wait = Wire(Bool())

  val w_ack_valid = Wire(Bool())
  val w_ack_data = Wire(Vec(p.nWordByte, UInt(8.W)))

  // ******************************
  //     NEXT MEMORY LEVEL ACK
  // ******************************
  // ------------------------------
  //             INPUTS
  // ------------------------------
  val reg_data = Seq.fill(p.nNext) {Module(new Fifo(2, UInt(p.nNextWordBit.W), p.nNextMemFifoDepth, 1, 1))}

  for (n <- 0 until p.nNext) {
    reg_data(n).io.i_flush := false.B

    io.b_mem.ready(n) := ~reg_data(n).io.o_full(0) & io.b_ack(n).ready
    reg_data(n).io.i_data(0) := io.b_mem.rdata
  }

  for (n <- 0 until p.nNext) {
    when ((n.U === io.b_mem.dome) & (io.b_mem.dome === io.i_slct)) {
      reg_data(n).io.i_wen(0) := io.b_mem.valid & (~reg_data(n).io.o_empty(0) | w_write_wait)
      io.b_ack(n).valid := io.b_mem.valid & ~reg_data(n).io.o_full(0)
    }.elsewhen (n.U === io.b_mem.dome) {
      reg_data(n).io.i_wen(0) := io.b_mem.valid
      io.b_ack(n).valid := io.b_mem.valid & ~reg_data(n).io.o_full(0)
    }.otherwise {
      reg_data(n).io.i_wen(0) := false.B

    }
  }

  for (n <- 0 until p.nNext) {
    when (n.U === io.b_mem.dome) {
      io.b_ack(n).valid := io.b_mem.valid & ~reg_data(n).io.o_full(0)
    }.otherwise {
      io.b_ack(n).valid := false.B
    }
  }

  // ------------------------------
  //            OUTPUTS
  // ------------------------------
  val rdata = Module(new BitToByte(p.nNextWordByte, p.nWordByte))
  rdata.io.i_in := reg_data(0).io.o_data(0)

  w_ack_valid := false.B
  w_ack_data := rdata.io.o_out

  for (n <- 0 until p.nNext) {
    reg_data(n).io.i_ren(0) := false.B

    when (n.U === io.i_slct) {
      when (~reg_data(n).io.o_empty(0)) {
        reg_data(n).io.i_ren(0) := ~w_write_wait

        w_ack_valid := true.B
        rdata.io.i_in := reg_data(n).io.o_data(0)
      }.elsewhen (io.i_slct === io.b_mem.dome) {
        w_ack_valid := io.b_mem.valid
        rdata.io.i_in := io.b_mem.rdata
      }
    }
  }

  // ******************************
  //             LOCK
  // ******************************
  w_wait := io.i_valid & (~w_ack_valid | w_write_wait)

  // ******************************
  //            OUTPUTS
  // ******************************
  io.o_lock := w_wait

  // ******************************
  //       WRITE & END REPLACE
  // ******************************
  val reg_write = if (p.nDome > 1) Some(Module(new Fifo(0, new CacheWriteBuffer(p), p.nDome, 1, 1))) else None

  if (p.nDome > 1) {
    reg_write.get.io.i_flush := false.B
    w_write_wait := io.i_valid & io.i_bus.rep & reg_write.get.io.o_full(0)
    reg_write.get.io.i_wen(0) := io.i_valid & io.i_bus.rep & w_ack_valid
    reg_write.get.io.i_data(0).end := io.i_bus.end
    reg_write.get.io.i_data(0).dome := io.i_slct
    reg_write.get.io.i_data(0).size := io.i_bus.size
    reg_write.get.io.i_data(0).addr := io.i_bus.addr
    reg_write.get.io.i_data(0).set := io.i_bus.set
    reg_write.get.io.i_data(0).offset := io.i_bus.offset
    reg_write.get.io.i_data(0).subcache := io.i_bus.subcache
    reg_write.get.io.i_data(0).line := io.i_bus.line
    reg_write.get.io.i_data(0).wdata := w_ack_data

    reg_write.get.io.i_ren(0) := io.b_write.ready
    io.b_write.valid := ~reg_write.get.io.o_empty(0)
    io.b_write.dome := reg_write.get.io.o_data(0).dome
    io.b_write.size := reg_write.get.io.o_data(0).size
    io.b_write.set := reg_write.get.io.o_data(0).set
    io.b_write.offset := reg_write.get.io.o_data(0).offset
    io.b_write.subcache := reg_write.get.io.o_data(0).subcache
    io.b_write.line := reg_write.get.io.o_data(0).line
    io.b_write.wdata := reg_write.get.io.o_data(0).wdata

    io.o_end.valid := ~reg_write.get.io.o_empty(0) & io.b_write.ready & reg_write.get.io.o_data(0).end
    io.o_end.set := reg_write.get.io.o_data(0).set
    io.o_end.subcache := reg_write.get.io.o_data(0).subcache
    io.o_end.line := reg_write.get.io.o_data(0).line

  } else {
    w_write_wait := io.i_valid & io.i_bus.rep & ~io.b_write.ready
    io.b_write.valid := io.i_valid & io.i_bus.rep & w_ack_valid
    io.b_write.dome := io.i_slct
    io.b_write.size := io.i_bus.size
    io.b_write.set := io.i_bus.set
    io.b_write.offset := io.i_bus.offset
    io.b_write.subcache := io.i_bus.subcache
    io.b_write.line := io.i_bus.line
    io.b_write.wdata := w_ack_data

    io.o_end.valid := io.i_valid & w_ack_valid & ~w_write_wait & io.i_bus.end
    io.o_end.set := io.i_bus.set
    io.o_end.subcache := io.i_bus.subcache
    io.o_end.line := io.i_bus.line
  }

  // ******************************
  //             DEBUG
  // ******************************
  if (p.debug) {
    if (p.nDome > 1) {
      dontTouch(reg_write.get.io.o_data(0).addr)
    } else {
      dontTouch(io.i_bus.addr)
    }
  }
}

object WriteStage extends App {
  chisel3.Driver.execute(args, () => new WriteStage(NextDefault, PmbCfg0))
}
