package cowlibry.mem.manseng.next

import chisel3._
import chisel3.util._

import cowlibry.common.mem.pmb._


class NextPrevReqBus(nHart: Int, nDome: Int, nNext: Int, nAddrBit: Int, nTagBit: Int, nSet: Int, nLineByte: Int, nPrevWordBit: Int) extends Bundle {
  val ready = Output(Vec(nNext, Bool()))
  val valid = Input(Bool())
  val hart = Input(UInt(log2Ceil(nHart).W))
  val dome = Input(UInt(log2Ceil(nDome).W))
  val rep = Input(Bool())
  val size = Input(UInt(SIZE.NBIT.W))
  val addr = Input(UInt(nAddrBit.W))
  val tag = Input(UInt(nTagBit.W))
  val set = Input(UInt(log2Ceil(nSet).W))
  val offset = Input(UInt(log2Ceil(nLineByte).W))
  val wdata = Input(UInt(nPrevWordBit.W))

  override def cloneType = (new NextPrevReqBus(nHart, nDome, nNext, nAddrBit, nTagBit, nSet, nLineByte, nPrevWordBit)).asInstanceOf[this.type]
}

class NextWriteFireBus(nNext: Int) extends Bundle {
  val ready = Output(Vec(nNext, Bool()))
  val valid = Input(Vec(nNext, Bool()))

  override def cloneType = (new NextWriteFireBus(nNext)).asInstanceOf[this.type]
}

class NextReqBus(p: NextParams) extends Bundle {
  val prev = UInt(log2Ceil(p.nPrevMem).W)
  val hart = UInt(log2Ceil(p.nHart).W)
  val rep = Bool()
  val size = UInt(SIZE.NBIT.W)
  val addr = UInt(p.nAddrBit.W)
  val tag = UInt(p.nTagBit.W)
  val set = UInt(log2Ceil(p.nSet).W)
  val offset = UInt(log2Ceil(p.nLineByte).W)
  val wdata = UInt(p.nPrevWordBit.W)

  override def cloneType = (new NextReqBus(p)).asInstanceOf[this.type]
}

class NextBus(p: NextParams) extends Bundle {
  val prev = UInt(log2Ceil(p.nPrevMem).W)
  val hart = UInt(log2Ceil(p.nHart).W)
  val rep = Bool()
  val end = Bool()
  val size = UInt(SIZE.NBIT.W)
  val addr = UInt(p.nAddrBit.W)
  val tag = UInt(p.nTagBit.W)
  val set = UInt(log2Ceil(p.nSet).W)
  val offset = UInt(log2Ceil(p.nLineByte).W)
  val subcache = UInt(log2Ceil(p.nSubCache).W)
  val line = UInt(log2Ceil(p.nLine).W)
  val wdata = UInt(p.nPrevWordBit.W)

  override def cloneType = (new NextBus(p)).asInstanceOf[this.type]
}

class NextAckBus extends Bundle {
  val ready = Input(Bool())
  val valid = Output(Bool())
}
