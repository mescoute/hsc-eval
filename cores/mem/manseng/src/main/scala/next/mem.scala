package cowlibry.mem.manseng.next

import chisel3._
import chisel3.util._

import cowlibry.common.tools._
import cowlibry.common.rsrc._
import cowlibry.common.mem.pmb._
import cowlibry.common.mem.flush._
import cowlibry.mem.manseng.tools._
import cowlibry.mem.manseng.cache._


class MemStage(p: NextParams, p_next: PmbParams) extends Module {
  val io = IO(new Bundle {
    val b_dome = Vec(p.nDome, new DomeBus(1))
    val b_flush = Vec(p.nFlush, Flipped(new FlushIO(p.nHart, p.nDome, p.nAddrBit)))
    val i_lock = Input(Bool())
    val o_lock = Output(Bool())

    val i_slct = Input(UInt(log2Ceil(p.nNext).W))
    val i_valid = Input(Bool())
    val i_bus = Input(new NextBus(p))

    val b_wfire = Vec(p.nPrevMem, new NextWriteFireBus(p.nNext))
    val b_mem = new PmbReqIO(p_next)

    val o_slct = Output(UInt(log2Ceil(p.nNext).W))
    val o_valid = Output(Bool())
    val o_bus = Output(new NextBus(p))
    val b_ack = Flipped(Vec(p.nNext, new NextAckBus()))
  })

  dontTouch(io.o_bus.end)

  val w_lock = Wire(Bool())
  val w_wait = Wire(Bool())
  val w_mem_wait = Wire(Bool())

  val w_wfire_wait = Wire(Bool())
  val w_nwfire_ren = Wire(Bool())

  val w_valid = Wire(Bool())
  val w_bus = Wire(new NextBus(p))

  val reg_slct = Reg(UInt(log2Ceil(p.nNext).W))
  val reg_bus = Seq.fill(p.nNext) {Module(new Fifo(2, new NextBus(p), p.nNextMemFifoDepth, 1, 1))}
  val reg_ack = Seq.fill(p.nNext) {Module(new Fifo(2, Bool(), p.nNextMemFifoDepth, 1, 1))}

  // ******************************
  //          FIRE WRITE
  // ******************************
  val w_wfire = Wire(Vec(p.nNext, Vec(p.nPrevMem, Bool())))
  val w_wfire_ren = Wire(Vec(p.nNext, Vec(p.nPrevMem, Bool())))

  for (n <- 0 until p.nNext) {
    val reg_wfire = Seq.fill(p.nPrevMem) {Module(new Fifo(2, UInt(0.W), p.nNextWriteFifoDepth, 1, 1))}

    for (pm <- 0 until p.nPrevMem) {
      reg_wfire(pm).io.i_flush := false.B
      reg_wfire(pm).io.i_wen(0) := io.b_wfire(pm).valid(n)
      reg_wfire(pm).io.i_data(0) := 0.U

      io.b_wfire(pm).ready(n) := ~reg_wfire(pm).io.o_full(0)

      reg_wfire(pm).io.i_ren(0) := w_wfire_ren(n)(pm)
      w_wfire(n)(pm) := ~reg_wfire(pm).io.o_empty(0)
    }
  }

  // ******************************
  //  SELECT CURRENT AND NEW VALUE
  // ******************************
  // ------------------------------
  //              LOCK
  // ------------------------------
  w_lock := true.B
  for (n <- 0 until p.nNext) {
    when (n.U === io.i_slct) {
      w_lock := reg_bus(n).io.o_full(0) | reg_ack(n).io.o_full(0)
    }
  }

  // ------------------------------
  //   NEXT MEMORY LEVEL REQUEST
  // ------------------------------
  if (p.nNext > 1) {
    w_mem_wait := false.B
    for (n <- 0 until p.nNext) {
      when (n.U === io.i_slct) {
        w_mem_wait := io.b_mem.ready(n)
      }
    }
  } else {
    w_mem_wait := io.b_mem.ready(0)
  }

  // ------------------------------
  //             FIRE
  // ------------------------------
  w_wfire_wait := false.B
  for (n <- 0 until p.nNext) {
    for (pv <- 0 until p.nPrevMem) {
      when (n.U === io.i_slct & pv.U === io.i_bus.prev) {
        w_wfire_wait := ~w_wfire(n)(pv) & ~io.i_bus.rep

        w_wfire_ren(n)(pv) := w_nwfire_ren
      }.otherwise {
        w_wfire_ren(n)(pv) := false.B
      }
    }
  }

  // ******************************
  //             LOGIC
  // ******************************
  // ------------------------------
  //   NEXT MEMORY LEVEL REQUEST
  // ------------------------------
  io.b_mem.valid := io.i_valid & ~w_wfire_wait & ~w_lock
  io.b_mem.hart := io.i_bus.hart
  io.b_mem.dome := io.i_slct
  io.b_mem.rw := ~io.i_bus.rep
  io.b_mem.size := io.i_bus.size
  io.b_mem.addr := Cat(io.i_bus.tag, io.i_bus.set, io.i_bus.offset)
  io.b_mem.wdata := io.i_bus.wdata

  // ------------------------------
  //              BUS
  // ------------------------------
  w_valid := io.i_valid & ~w_wfire_wait & w_mem_wait
  w_bus := io.i_bus

  w_wait := io.i_valid & (w_lock | w_wfire_wait | ~w_mem_wait)
  w_nwfire_ren := io.i_valid & ~io.i_bus.rep & ~w_lock & w_mem_wait

  // ******************************
  //            OUTPUTS
  // ******************************
  // ------------------------------
  //              BUS
  // ------------------------------
  for (n <- 0 until p.nNext) {
    reg_bus(n).io.i_flush := false.B
    reg_ack(n).io.i_flush := false.B
  }

  reg_slct := io.i_slct
  for (n <- 0 until p.nNext) {
    reg_bus(n).io.i_data(0) := w_bus
    reg_ack(n).io.i_data(0) := true.B
    when (n.U === io.i_slct) {
      reg_bus(n).io.i_wen(0) := w_valid & ~reg_ack(n).io.o_full(0)
      reg_ack(n).io.i_wen(0) := w_valid & ~reg_bus(n).io.o_full(0)
    }.otherwise {
      reg_bus(n).io.i_wen(0) := false.B
      reg_ack(n).io.i_wen(0) := false.B
    }
  }

  io.o_lock := w_wait

  io.o_slct := reg_slct
  io.o_valid := false.B
  io.o_bus := reg_bus(0).io.o_data(0)

  for (n <- 0 until p.nNext) {
    io.b_ack(n).ready := ~reg_ack(n).io.o_empty(0)
    reg_ack(n).io.i_ren(0) := io.b_ack(n).valid

    when (n.U === reg_slct) {
      reg_bus(n).io.i_ren(0) := ~io.i_lock

      io.o_valid := ~reg_bus(n).io.o_empty(0)
      io.o_bus := reg_bus(n).io.o_data(0)
    }.otherwise {
      reg_bus(n).io.i_ren(0) := false.B
    }
  }

  // ******************************
  //             DOME
  // ******************************
  for (n <- 0 until p.nNext) {
    io.b_dome(n).free := reg_bus(n).io.o_empty(0)
  }

  // ******************************
  //             FLUSH
  // ******************************
  for (f <- 0 until p.nFlush) {
    io.b_flush(f).ready := io.b_flush(f).valid

    for (n <- 0 until p.nNext) {
      when (io.b_flush(f).valid & io.b_flush(f).apply & (n.U === io.b_flush(f).dome)) {
        io.b_flush(f).ready := reg_bus(n).io.o_empty(0)
      }
    }
  }
}

object MemStage extends App {
  chisel3.Driver.execute(args, () => new MemStage(NextDefault, PmbCfg0))
}
