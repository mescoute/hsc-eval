package cowlibry.mem.manseng.mpu

import chisel3._
import chisel3.util._
import scala.math._


trait MpuParams {
  def nHart: Int
  def nDome: Int
  def nRsrc: Int

  def useMpuReg: Boolean
}

class MpuIntf(
  _nHart: Int,
  _nDome: Int,
  _nRsrc: Int,
  _useMpuReg: Boolean
) extends MpuParams {
  def nHart: Int = _nHart
  def nDome: Int = _nDome
  def nRsrc: Int = _nRsrc

  def useMpuReg: Boolean = _useMpuReg
}
