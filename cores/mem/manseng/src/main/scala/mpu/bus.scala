package cowlibry.mem.manseng.mpu

import chisel3._
import chisel3.util._


// ******************************
//             DOME
// ******************************
class DomeInfoBus (nRsrc: Int) extends Bundle {
  val valid = Input(Bool())
  val weight = Input(UInt(log2Ceil(nRsrc + 1).W))

  override def cloneType = (new DomeInfoBus(nRsrc)).asInstanceOf[this.type]
}

// ******************************
//           REGISTERS
// ******************************
class DomeReg (nRsrc: Int) extends Bundle {
  val valid = Bool()
  val flush = Bool()
  val free = Bool()
  val weight = UInt(log2Ceil(nRsrc + 1).W)

  override def cloneType = (new DomeReg(nRsrc)).asInstanceOf[this.type]
}

class RsrcReg (nDome: Int, nRsrc: Int, nHartRsrc: Int) extends Bundle {
  val valid = Bool()
  val flush = Bool()
  val free = Bool()
  val dome = UInt(log2Ceil(nDome).W)
  val port = Vec(nHartRsrc, UInt(log2Ceil(nRsrc).W))

  override def cloneType = (new RsrcReg(nDome, nRsrc, nHartRsrc)).asInstanceOf[this.type]
}
