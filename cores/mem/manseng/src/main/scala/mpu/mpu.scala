package cowlibry.mem.manseng.mpu

import chisel3._
import chisel3.util._

import cowlibry.common.tools._
import cowlibry.common.rsrc._


class Mpu (p: MpuParams) extends Module {
  require(((p.nRsrc / p.nHart) >= 1), "At least one cache space (line or subcache) must be allocable by each hart.")

  // ******************************
  //       INTERNAL PARAMETERS
  // ******************************
  var nHartRsrc = new Array[Int](p.nHart)
  for (h <- 0 until p.nHart) {
    if ((p.nRsrc % p.nHart) > h) {
      nHartRsrc(h) = (p.nRsrc / p.nHart) + 1
    } else {
      nHartRsrc(h) = (p.nRsrc / p.nHart)
    }
  }

  var nHartPort = new Array[Int](p.nHart)
  nHartPort(0) = 0
  for (h <- 1 until p.nHart) {
    nHartPort(h) = nHartPort(h - 1) + nHartRsrc(h - 1)
  }


  val io = IO(new Bundle {
    val b_dome = Vec(p.nDome, new DomeBus(p.nHart))
    val b_hart = Vec(p.nHart, new RsrcBus(p.nHart, p.nDome, p.nHart))

    val b_dome_info = Flipped(Vec(p.nDome, new DomeInfoBus(p.nRsrc)))
    val b_dome_rsrc = Flipped(Vec(p.nDome, new DomeBus(p.nRsrc)))
    val b_rsrc = Flipped(Vec(p.nRsrc, new RsrcBus(1, p.nDome, p.nRsrc)))
  })

  // ******************************
  //             DOME
  // ******************************
  // ------------------------------
  //            REGISTER
  // ------------------------------
  val init_dome = Wire(Vec(p.nDome, new DomeReg(p.nRsrc)))

  init_dome(0).valid := true.B
  init_dome(0).flush := false.B
  init_dome(0).free := false.B
  init_dome(0).weight := p.nRsrc.U
  for (d <- 1 until p.nDome) {
    init_dome(d).valid := false.B
    init_dome(d).flush := true.B
    init_dome(d).free := true.B
    init_dome(d).weight := 0.U
  }

  val w_dome = Wire(Vec(p.nDome, new DomeReg(p.nRsrc)))
  val reg_dome = if (p.useMpuReg) Some(RegInit(init_dome)) else Some(Wire(Vec(p.nDome, new DomeReg(p.nRsrc))))

  reg_dome.get := w_dome

  // ------------------------------
  //         VALID & FLUSH
  // ------------------------------
  for (d <- 0 until p.nDome) {
    w_dome(d).valid := io.b_dome(d).valid
    w_dome(d).flush := io.b_dome(d).flush
  }

  // ------------------------------
  //             WEIGHT
  // ------------------------------
  val w_dome_weight = Wire(Vec(p.nDome, Vec(p.nHart + 1, UInt(log2Ceil(p.nRsrc + 1).W))))

  for (d <- 0 until p.nDome) {
    w_dome_weight(d)(0) := 0.U
  }

  for (d <- 0 until p.nDome) {
    for (ha <- 0 until p.nHart) {
      when (io.b_hart(ha).valid & (d.U === io.b_hart(ha).dome)) {
        w_dome_weight(d)(ha + 1) := w_dome_weight(d)(ha) + nHartRsrc(ha).U
      }.otherwise {
        w_dome_weight(d)(ha + 1) := w_dome_weight(d)(ha)
      }
    }
  }

  for (d <- 0 until p.nDome) {
    w_dome(d).weight := w_dome_weight(d)(p.nHart)
  }

  // ------------------------------
  //              FREE
  // ------------------------------
  for (d <- 0 until p.nDome) {
    if (p.useMpuReg) w_dome(d).free := reg_dome.get(d).flush & io.b_dome_rsrc(d).free else w_dome(d).free := io.b_dome_rsrc(d).free
  }

  // ******************************
  //            RESOURCE
  // ******************************
  // ------------------------------
  //            REGISTER
  // ------------------------------
  val init_rsrc = Wire(Vec(p.nHart, new RsrcReg(p.nDome, p.nRsrc, nHartRsrc(0))))

  for (ha <- 0 until p.nHart) {
    init_rsrc(ha).valid := true.B
    init_rsrc(ha).flush := false.B
    init_rsrc(ha).free := false.B
    init_rsrc(ha).dome := 0.U

    for (har <- 0 until nHartRsrc(0)) {
      init_rsrc(ha).port(har) := nHartPort(ha).U + har.U
    }
  }

  val reg_rsrc = if (p.useMpuReg) Some(RegInit(init_rsrc)) else Some(Wire(Vec(p.nHart, new RsrcReg(p.nDome, p.nRsrc, nHartRsrc(0)))))
  val w_rsrc = Wire(Vec(p.nHart, new RsrcReg(p.nDome, p.nRsrc, nHartRsrc(0))))

  reg_rsrc.get := w_rsrc

  // ------------------------------
  //       VALID, FLUSH & DOME
  // ------------------------------
  for (ha <- 0 until p.nHart) {
    w_rsrc(ha).valid := io.b_hart(ha).valid
    w_rsrc(ha).flush := io.b_hart(ha).flush
    w_rsrc(ha).dome := io.b_hart(ha).dome
  }

  // ------------------------------
  //              PORT
  // ------------------------------
  for (ha <- 0 until p.nHart) {
    for (har <- 0 until nHartRsrc(0)) {
      w_rsrc(ha).port(har) := nHartPort(ha).U + har.U
    }
  }

  val w_rsrc_dome = Wire(Vec(p.nDome, Vec(p.nRsrc, Bool())))

  for (d <- 0 until p.nDome) {
    for (ha <- 0 until p.nHart) {
      for (har <- 0 until nHartRsrc(ha)) {
        w_rsrc_dome(d)(nHartPort(ha) + har) := io.b_hart(ha).valid & (d.U === io.b_hart(ha).dome)
      }
    }
  }

  val w_rsrc_port = Wire(Vec(p.nDome, Vec(p.nRsrc, UInt(log2Ceil(p.nRsrc).W))))

  for (d <- 0 until p.nDome) {
    w_rsrc_port(d)(0) := w_rsrc_dome(d)(0)

    for (r <- 1 until p.nRsrc) {
      when (w_rsrc_dome(d)(r)) {
        w_rsrc_port(d)(r) := w_rsrc_port(d)(r - 1) + 1.U
      }.otherwise{
        w_rsrc_port(d)(r) := w_rsrc_port(d)(r - 1)
      }
    }
  }

  for (ha <- 0 until p.nHart) {
    for (har <- 0 until nHartRsrc(ha)) {
      w_rsrc(ha).port(har) := w_rsrc_port(io.b_hart(ha).dome)(nHartPort(ha) + har)
    }
  }

  // ------------------------------
  //              FREE
  // ------------------------------
  for (ha <- 0 until p.nHart) {
    val w_free = Wire(Vec(nHartRsrc(ha), Bool()))

    for (har <- 0 until nHartRsrc(ha)) {
      w_free(har) := io.b_rsrc(nHartPort(ha) + har).free
    }

    if (p.useMpuReg) w_rsrc(ha).free := reg_rsrc.get(ha).flush & w_free.asUInt.andR else w_rsrc(ha).free := w_free.asUInt.andR
  }

  // ******************************
  //              I/O
  // ******************************
  // ------------------------------
  //             DOME
  // ------------------------------
  for (d <- 0 until p.nDome) {
    io.b_dome(d).free := reg_dome.get(d).free

    io.b_dome_info(d).valid := reg_dome.get(d).valid
    io.b_dome_info(d).weight := reg_dome.get(d).weight

    io.b_dome_rsrc(d).valid := reg_dome.get(d).valid
    io.b_dome_rsrc(d).flush := reg_dome.get(d).flush
    io.b_dome_rsrc(d).weight := 0.U
  }
  // ------------------------------
  //              HART
  // ------------------------------
  for (ha <- 0 until p.nHart) {
    io.b_hart(ha).free := reg_rsrc.get(ha).free
  }

  // ------------------------------
  //             RSRC
  // ------------------------------
  for (ha <- 0 until p.nHart) {
    for (har <- 0 until nHartRsrc(ha)) {
      io.b_rsrc(nHartPort(ha) + har).valid := reg_rsrc.get(ha).valid
      io.b_rsrc(nHartPort(ha) + har).flush := reg_rsrc.get(ha).flush
      io.b_rsrc(nHartPort(ha) + har).hart := 0.U
      io.b_rsrc(nHartPort(ha) + har).dome := reg_rsrc.get(ha).dome
      io.b_rsrc(nHartPort(ha) + har).port := reg_rsrc.get(ha).port(har)
    }
  }
}


object Mpu extends App {
  chisel3.Driver.execute(args, () => new Mpu(MpuDefault))
}
