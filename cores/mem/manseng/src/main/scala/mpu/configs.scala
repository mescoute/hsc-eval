package cowlibry.mem.manseng.mpu

import chisel3._
import scala.math._


object MpuDefault extends MpuParams{
  def nHart: Int = 3
  def nDome: Int = 3
  def nRsrc: Int = 7

  def useMpuReg: Boolean = true
}
