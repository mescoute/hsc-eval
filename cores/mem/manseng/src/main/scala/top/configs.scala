package cowlibry.mem.manseng

import chisel3._
import scala.math._


object MansengDefault extends MansengParams {
  // ******************************
  //       GLOBAL PARAMETERS
  // ******************************
  override def debug: Boolean = true
  override def nAddrBit: Int = 32
  override def nHart: Int = 1
  override def nFlush: Int = 1

  // ******************************
  //        DOME PARAMETERS
  // ******************************
  override def nDome: Int = 1
  override def multiPrevDome: Boolean = false
  override def usePartWay: Boolean = true
  override def useMpuReg: Boolean = true

  // ******************************
  //       WORD PARAMETERS
  // ******************************
  override def nPrevWordByte: Int = 4
  override def nNextWordByte: Int = 8

  // ******************************
  //   PREV CONTROLLER PARAMETERS
  // ******************************
  override def nPrevMem: Int = 1
  override def nPrevReqFifoDepth: Int = 2
  override def usePrevReqReg: Boolean = false
  override def usePrevReadReg: Boolean = false

  // ******************************
  //   NEXT CONTROLLER PARAMETERS
  // ******************************
  override def nNextReqFifoDepth: Int = 2
  override def nNextMemFifoDepth: Int = 2
  override def nNextWriteFifoDepth: Int = 2

  // ******************************
  //        CACHE PARAMETERS
  // ******************************
  override def nSetReadPort: Int = 1
  override def nSetWritePort: Int = 1
  override def slctPolicy: String = "BitPLRU"

  override def nSubCache: Int = 1
  override def nSet: Int = 4
  override def nLine: Int = 4
  override def nWord: Int = 4
}
