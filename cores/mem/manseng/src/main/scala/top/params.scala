package cowlibry.mem.manseng

import chisel3._
import chisel3.util._
import scala.math._

import cowlibry.mem.manseng.cache._
import cowlibry.mem.manseng.prev._
import cowlibry.mem.manseng.next._
import cowlibry.mem.manseng.mpu._


trait MansengParams extends CacheParams
                      with PrevParams
                      with NextParams
                      with MpuParams {

  // ******************************
  //       GLOBAL PARAMETERS
  // ******************************
  override def debug: Boolean
  override def nAddrBit: Int
  override def nHart: Int
  override def nFlush: Int

  // ******************************
  //         DOME PARAMETERS
  // ******************************
  override def nDome: Int
  override def multiPrevDome: Boolean
  override def usePartWay: Boolean
  override def nRsrc: Int = {
    if (usePartWay) {
      return nLine
    } else {
      return nSubCache
    }
  }
  override def useMpuReg: Boolean

  // ******************************
  //       WORD PARAMETERS
  // ******************************
  override def nPrevWordByte: Int
  override def nPrevWordBit: Int = nPrevWordByte * 8
  override def nNextWordByte: Int
  override def nNextWordBit: Int = nNextWordByte * 8
  override def nWordByte: Int = max(nPrevWordByte, nNextWordByte)

  // ******************************
  //   PREV CONTROLLER PARAMETERS
  // ******************************
  override def nPrevMem: Int
  override def nPrev: Int = {
    if (nDome > 1 && multiPrevDome) {
      return nDome
    } else {
      return 1
    }
  }
  override def nPrevReqFifoDepth: Int
  override def usePrevReqReg: Boolean
  override def usePrevReadReg: Boolean

  // ******************************
  //   NEXT CONTROLLER PARAMETERS
  // ******************************
  override def nNext: Int = nDome
  override def nNextReqFifoDepth: Int
  override def nNextMemFifoDepth: Int
  override def nNextWriteFifoDepth: Int

  // ******************************
  //        CACHE PARAMETERS
  // ******************************
  override def nAccess: Int = nPrevMem
  override def nSetReadPort: Int
  override def nSetWritePort: Int
  override def slctPolicy: String
  override def nPendingAcc: Int = nPrevPendingAcc * nPrevMem

  override def nSubCache: Int
  override def nSet: Int
  override def nLine: Int
  override def nWord: Int
  override def nTagBit: Int = nAddrBit - log2Ceil(nSet) - log2Ceil(nLineByte)
}

class MansengIntf (
  // ******************************
  //       GLOBAL PARAMETERS
  // ******************************
  _debug: Boolean,
  _nAddrBit: Int,
  _nHart: Int,
  _nFlush: Int,

  // ******************************
  //         DOME PARAMETERS
  // ******************************
  _nDome: Int,
  _multiPrevDome: Boolean,
  _usePartWay: Boolean,
  _useMpuReg: Boolean,

  // ******************************
  //       WORD PARAMETERS
  // ******************************
  _nPrevWordByte: Int,
  _nNextWordByte: Int,

  // ******************************
  //   PREV CONTROLLER PARAMETERS
  // ******************************
  _nPrevMem: Int,
  _nPrevReqFifoDepth: Int,
  _usePrevReqReg: Boolean,
  _usePrevReadReg: Boolean,

  // ******************************
  //   NEXT CONTROLLER PARAMETERS
  // ******************************
  _nNextReqFifoDepth: Int,
  _nNextMemFifoDepth: Int,
  _nNextWriteFifoDepth: Int,

  // ******************************
  //        CACHE PARAMETERS
  // ******************************
  _nSetReadPort: Int,
  _nSetWritePort: Int,
  _slctPolicy: String,

  _nSubCache: Int,
  _nSet: Int,
  _nLine: Int,
  _nWord: Int

) extends MansengParams  {
  // ******************************
  //       GLOBAL PARAMETERS
  // ******************************
  override def debug: Boolean = _debug
  override def nAddrBit: Int = _nAddrBit
  override def nHart: Int = _nHart
  override def nFlush: Int = _nFlush

  // ******************************
  //         DOME PARAMETERS
  // ******************************
  override def nDome: Int = _nDome
  override def multiPrevDome: Boolean = _multiPrevDome
  override def usePartWay: Boolean = _usePartWay
  override def useMpuReg: Boolean = _useMpuReg

  // ******************************
  //       WORD PARAMETERS
  // ******************************
  override def nPrevWordByte: Int = _nPrevWordByte
  override def nNextWordByte: Int = _nNextWordByte

  // ******************************
  //   PREV CONTROLLER PARAMETERS
  // ******************************
  override def nPrevMem: Int = _nPrevMem
  override def nPrevReqFifoDepth: Int = _nPrevReqFifoDepth
  override def usePrevReqReg: Boolean = _usePrevReqReg
  override def usePrevReadReg: Boolean = _usePrevReadReg

  // ******************************
  //   NEXT CONTROLLER PARAMETERS
  // ******************************
  override def nNextReqFifoDepth: Int = _nNextReqFifoDepth
  override def nNextMemFifoDepth: Int = _nNextMemFifoDepth
  override def nNextWriteFifoDepth: Int = _nNextWriteFifoDepth

  // ******************************
  //        CACHE PARAMETERS
  // ******************************
  override def nSetReadPort: Int = _nSetReadPort
  override def nSetWritePort: Int = _nSetWritePort
  override def slctPolicy: String = _slctPolicy

  override def nSubCache: Int = _nSubCache
  override def nSet: Int = _nSet
  override def nLine: Int = _nLine
  override def nWord: Int = _nWord
}
