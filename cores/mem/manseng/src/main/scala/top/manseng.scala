package cowlibry.mem.manseng

import chisel3._
import chisel3.util._
import scala.math._

import cowlibry.common.mem.pmb._
import cowlibry.common.mem.flush._
import cowlibry.common.rsrc._
import cowlibry.mem.manseng.tools._
import cowlibry.mem.manseng.cache._
import cowlibry.mem.manseng.prev._
import cowlibry.mem.manseng.next._
import cowlibry.mem.manseng.mpu._


class Manseng (p: MansengParams) extends Module {
  require(p.nDome > 0, "At least one DOME is necessary.")

  // ******************************
  //             PARAMS
  // ******************************
  // ------------------------------
  //            PREV BUS
  // ------------------------------
  var p_prev = new PmbIntf(p.nHart, p.nDome, p.multiPrevDome, p.nAddrBit, p.nPrevWordBit)

  // ------------------------------
  //            NEXT BUS
  // ------------------------------
  var p_next = new PmbIntf(p.nHart, p.nDome, true, p.nAddrBit, p.nNextWordBit)

  // ******************************
  //              I/O
  // ******************************
  val io = IO(new Bundle {
    val b_dome = Vec(p.nDome, new DomeBus(p.nHart))
    val b_hart = Vec(p.nHart, new RsrcBus(p.nHart, p.nDome, p.nHart))
    val b_flush = Vec(p.nFlush, Flipped(new FlushIO(p.nHart, p.nDome, p.nAddrBit)))

    val i_hier_flush = Input(Vec(p.nDome, Bool()))
    val o_hier_free = Output(Vec(p.nDome, Bool()))

    val b_prevmem = Vec(p.nPrevMem, Flipped(new PmbIO(p_prev)))
    val b_nextmem = new PmbIO(p_next)

    val o_miss = Output(Vec(p.nHart, UInt(log2Ceil(p.nPrevMem + 1).W)))
  })

  val prev = Module(new PrevCtrl(p, p_prev))
  val next = Module(new NextCtrl(p, p_next))
  val cache = Module(new Cache(p))
  val mpu = Module(new Mpu(p))

  // ******************************
  //            MODULES
  // ******************************
  // ------------------------------
  //         PREV PIPELINE
  // ------------------------------
  prev.io.b_dome <> mpu.io.b_dome_rsrc
  prev.io.b_flush <> io.b_flush
  prev.io.b_mem <> io.b_prevmem
  io.o_miss := prev.io.o_miss

  // ------------------------------
  //         NEXT PIPELINE
  // ------------------------------
  next.io.b_dome <> mpu.io.b_dome_rsrc
  next.io.b_flush <> io.b_flush
  next.io.b_prev <> prev.io.b_next
  next.io.b_wfire <> prev.io.b_wfire
  next.io.b_mem <> io.b_nextmem

  // ------------------------------
  //             CACHE
  // ------------------------------
  cache.io.b_dome <> mpu.io.b_dome_info
  cache.io.b_rsrc <> mpu.io.b_rsrc
  cache.io.b_flush <> io.b_flush
  cache.io.b_acc <> prev.io.b_ctrl
  cache.io.b_rep <> next.io.b_ctrl
  cache.io.b_read <> prev.io.b_read
  for (pv <- 0 until p.nPrevMem) {
    cache.io.b_write(pv) <> prev.io.b_write(pv)
  }
  cache.io.b_write(p.nCacheWritePort - 1) <> next.io.b_write
  cache.io.i_pend := prev.io.o_pend
  cache.io.i_end := next.io.o_end

  // ------------------------------
  //              MPU
  // ------------------------------
  mpu.io.b_dome <> io.b_dome
  mpu.io.b_hart <> io.b_hart

  // ******************************
  //        DOME & RESOURCES
  // ******************************
  // ------------------------------
  //            FLUSH
  // ------------------------------
  for (d <- 0 until p.nDome) {
    for (r <- 0 until p.nRsrc) {
      when (mpu.io.b_dome_rsrc(mpu.io.b_rsrc(r).dome).flush) {
        cache.io.b_rsrc(r).flush := io.i_hier_flush(mpu.io.b_rsrc(r).dome) & prev.io.b_dome(mpu.io.b_rsrc(r).dome).free & next.io.b_dome(mpu.io.b_rsrc(r).dome).free & mpu.io.b_rsrc(r).flush
      }.otherwise {
        cache.io.b_rsrc(r).flush := mpu.io.b_rsrc(r).flush
      }
    }
  }

  for (d <- 0 until p.nDome) {
    prev.io.b_dome(d).flush := mpu.io.b_dome_rsrc(d).flush & io.i_hier_flush(d)
    next.io.b_dome(d).flush := mpu.io.b_dome_rsrc(d).flush & io.i_hier_flush(d)
  }

  // ------------------------------
  //             FREE
  // ------------------------------
  for (d <- 0 until p.nDome) {
    mpu.io.b_dome_rsrc(d).free := prev.io.b_dome(d).free & next.io.b_dome(d).free & io.o_hier_free(d)
    io.o_hier_free(d) := prev.io.b_dome(d).free & next.io.b_dome(d).free
  }

  for (r <- 0 until p.nRsrc) {
    mpu.io.b_rsrc(r).free := cache.io.b_rsrc(r).free
  }

  // ******************************
  //             FLUSH
  // ******************************
  for (f <- 0 until p.nFlush) {
    cache.io.b_flush(f).valid := io.b_flush(f).valid & prev.io.b_flush(f).ready & next.io.b_flush(f).ready
    io.b_flush(f).ready := cache.io.b_flush(f).ready & prev.io.b_flush(f).ready & next.io.b_flush(f).ready
  }

  // ******************************
  //             DEBUG
  // ******************************
  if (p.debug) {
    dontTouch(io.o_miss)
  }
}

object Manseng extends App {
  chisel3.Driver.execute(args, () => new Manseng(MansengDefault))
}
