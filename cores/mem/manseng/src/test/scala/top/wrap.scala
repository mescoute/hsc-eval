package cowlibry.mem.manseng


import scala._
import chisel3._
import chisel3.util._
import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

import cowlibry.common.rsrc._
import cowlibry.common.mem.pmb._
import cowlibry.common.mem.flush._
import cowlibry.common.tools.HexSizer
import cowlibry.common.initram._
import cowlibry.mem.manseng.tools._


class MansengTop (p: MansengParams) extends Module {
  // ******************************
  //            PARAMS
  // ******************************
  // ------------------------------
  //             PREV
  // ------------------------------
  var p_prev = new PmbIntf(p.nHart, p.nDome, p.multiPrevDome, p.nAddrBit, p.nPrevWordBit)

  // ------------------------------
  //              RAM
  // ------------------------------
  val nRamByte: Int = 65536
  var p_ram = new InitRamIntf(nRamByte, p.nHart, p.nDome, true, log2Ceil(nRamByte), p.nNextWordBit)

  // ******************************
  //              I/O
  // ******************************
  val io = IO(new Bundle {
    val b_dome = Vec(p.nDome, new DomeBus(p.nHart))
    val b_hart = Vec(p.nHart, new RsrcBus(p.nHart, p.nDome, p.nHart))
    val b_flush = Vec(p.nFlush, Flipped(new FlushIO(p.nHart, p.nDome, p.nAddrBit)))

    val i_hier_flush = Input(Vec(p.nDome, Bool()))
    val o_hier_free = Output(Vec(p.nDome, Bool()))

    val b_prevmem = Vec(p.nPrevMem, Flipped(new PmbIO(p_prev)))

    val o_miss = Output(Vec(p.nHart, UInt(log2Ceil(p.nPrevMem + 1).W)))
  })

  dontTouch(io.o_miss)

  // ******************************
  //            MODULES
  // ******************************
  // ------------------------------
  //             CACHE
  // ------------------------------
  val cache = Module(new Manseng(p))
  cache.io.b_dome <> io.b_dome
  cache.io.b_hart <> io.b_hart
  cache.io.b_flush <> io.b_flush
  cache.io.i_hier_flush := io.i_hier_flush
  cache.io.b_prevmem <> io.b_prevmem
  io.o_miss := cache.io.o_miss

  // ------------------------------
  //             RAM
  // ------------------------------
  val ram = Module(new InitRam(p_ram))
  cache.io.b_nextmem <> ram.io.b_port(1)
  ram.io.b_port(0).req.valid := false.B
  ram.io.b_port(0).req.hart := 0.U
  ram.io.b_port(0).req.dome := 0.U
  ram.io.b_port(0).req.rw := 0.B
  ram.io.b_port(0).req.size := SIZE.B0.U
  ram.io.b_port(0).req.addr := 0.U
  ram.io.b_port(0).req.wdata := 0.U
  if (p.nDome > 1) {
    for (d <- 0 until p.nDome) {
      ram.io.b_port(0).ack.ready(d) := 0.B
    }
  } else {
    ram.io.b_port(0).ack.ready(0) := 0.B
  }

  // ******************************
  //            OUTPUTS
  // ******************************
  io.o_hier_free := cache.io.o_hier_free
}
