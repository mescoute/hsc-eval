#include <stdlib.h>
#include <stdio.h>
#include "VMansengDbg0.h"
#include "verilated.h"
#include "verilated_vcd_c.h"
#include "svdpi.h"
#include "VMansengDbg0__Dpi.h"
#include <time.h>


bool poke_req(VMansengDbg0 *dut, int port, int valid, int hart, int rw, int size, int addr, int wdata) {
  dut->io_b_prevmem_0_req_valid = valid;
}

int main(int argc, char **argv) {
  // Inputs
  int trigger = atoi(argv[1]);
  char* hexfile = argv[2];
  char* vcdfile = argv[3];

  time_t test_time = time(NULL);

	// Initialize Verilators variables
	Verilated::commandArgs(argc, argv);

  // Create an instance of our module under test
	VMansengDbg0 *dut = new VMansengDbg0;

  // Generate VCD
  Verilated::traceEverOn(true);
  VerilatedVcdC* dut_trace = new VerilatedVcdC;
  dut->trace(dut_trace, 99);
  dut_trace->open((vcdfile));

  // Call task to initialize memory
  svSetScope(svGetScopeFromName("TOP.MansengTop.ram.ram"));
  //Verilated::scopesDump();
  dut->new_readmemh(hexfile);

	// Test variables
  int cycle = 0;                // Cycle
  bool end = false;             // Test end
  bool done [2] {false, false}; // Hart done
  int result [2] = {-1, -1};    // Hart SW result

  // **********
  //   RESET
  // **********
  for (int i = 0; i < 5; i++) {
    dut->clock = 1;
  	dut->eval();
    cycle = cycle + 1;
    dut_trace->dump(cycle * 10);

		dut->clock = 0;
    dut->reset = 1;
		dut->eval();
    dut_trace->dump(cycle * 10 + 5);
  }
  dut->reset = 0;

  // **********
  // TEST LOOP
  // **********
	while ((!Verilated::gotFinish()) && (end == false)) {
    test_time = time(NULL);

    // Positive edge
		dut->clock = 1;
		dut->eval();
    cycle = cycle + 1;
    dut_trace->dump(cycle * 10);

    // Negative Edge
		dut->clock = 0;
		dut->eval();
    dut_trace->dump(cycle * 10 + 5);

    if (cycle > 10) {
      change_valid(dut);
    }

    // Hart 0 SW trigger
    /*if (dut->io_o_dbg_gpr_0_31 == 1) {
      printf("Hart 0 is done");
      done[0] = true;
      result[0] = dut->io_o_dbg_gpr_0_30;
    }

    // Hart 1 SW trigger
    if (dut->io_o_dbg_gpr_1_31 == 1) {
      printf("Hart 1 is done");
      done[1] = true;
      result[1] = dut->io_o_dbg_gpr_1_30;
    }*/

    // End test loop
    if ((cycle > trigger) || (done[0] && done[1])) {
      end = true;
    }
	}

  // **********
  //   REPORT
  // **********
  /*printf("TEST REPORT: \n");
  printf("Test file %s \n", hexfile);
  printf("Cycles: %d \n", cycle);

  if ((result[0] != 0) || (result[1] != 0)) {
    printf("\033[1;31m");
    printf("FAILED: some errors have been detected. \n");
    printf("\033[0m");
    for (int h = 0; h < 2; h++) {
      printf("Result hart %d: %d \n", h, result[h]);

      for (int i = 0; i < 32; i++) {
        int error = result[h] & 1;
        if (error == 1) {
          printf("Part %d: FAILED. \n", i + 1);
        }
        result[h] = result[h]>>1;
      }
    }
  } else if (cycle != trigger) {
    printf("\033[1;31m");
    printf("FAILED: the trigger has not been respected. \n");
    printf("\033[0m");
  } else {
    printf("\033[1;32m");
    printf("SUCCESS: test passed.\n");
    printf("\033[0m");
  }*/

  dut_trace->close();
  exit(EXIT_SUCCESS);
}
