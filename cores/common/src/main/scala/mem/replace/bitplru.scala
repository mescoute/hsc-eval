package cowlibry.common.mem.replace

import scala._
import chisel3._
import chisel3.util._


class BitPlruPolicy (nAccess: Int, nLine: Int) extends Module {
  val io = IO(new Bundle {
    val i_flush = Input(Vec(nLine, Bool()))
    val o_free = Output(Vec(nLine, Bool()))

    val i_valid = Input(Vec(nAccess, Bool()))
    val i_line = Input(Vec(nAccess, UInt(log2Ceil(nLine).W)))

    val i_rep = Input(Bool())
    val i_use_line = Input(Vec(nLine, Bool()))
    val o_rep_av = Output(Bool())
    val o_rep_line = Output(UInt(log2Ceil(nLine).W))
  })

  val reg_mru = RegInit(VecInit(Seq.fill(nLine)(false.B)))

  // ******************************
  //            ACCESS
  // ******************************
  val w_mru = Wire(Vec(nLine, Bool()))
  w_mru := reg_mru

  for (a <- 0 until nAccess) {
    for (l <- 0 until nLine) {
      when (io.i_valid(a) & (l.U === io.i_line(a))) {
        w_mru(l) := true.B
      }
    }
  }

  // ******************************
  //            REPLACE
  // ******************************
  val w_zero = Wire(Vec(nLine + 1, Bool()))
  val w_av = Wire(Vec(nLine + 1, Bool()))
  val w_rep_line = Wire(UInt(log2Ceil(nLine).W))
  val w_rep_mru = Wire(Vec(nLine, Bool()))

  w_zero(0) := false.B
  w_av(0) := false.B
  w_rep_line := 0.U

  for (l <- 0 until nLine) {
    when(io.i_rep & ~w_av(l) & io.i_use_line(l) & ~io.i_flush(l)) {
      w_zero(l + 1) := ~reg_mru(l)
      w_av(l + 1) := true.B
      w_rep_line := l.U
      w_rep_mru(l) := true.B
    }.elsewhen (io.i_rep & w_av(l) & ~w_zero(l) & ~reg_mru(l) & io.i_use_line(l) & ~io.i_flush(l)) {
      w_zero(l + 1) := true.B
      w_av(l + 1) := true.B
      w_rep_line := l.U
      w_rep_mru(l) := true.B
    }.otherwise {
      w_zero(l + 1) := w_zero(l)
      w_av(l + 1) := w_av(l)
      w_rep_mru(l) := w_mru(l)
    }
  }

  // ******************************
  //  MOST RECENTLY-USED REGISTERS
  // ******************************
  for (l <- 0 until nLine) {
    reg_mru(l) := Mux(w_rep_mru.asUInt.andR | io.i_flush(l), false.B, w_rep_mru(l))
  }

  // ******************************
  //            OUTPUTS
  // ******************************
  io.o_rep_av := w_av(nLine)
  io.o_rep_line := w_rep_line

  for (l <- 0 until nLine) {
    io.o_free(l) := ~reg_mru(l)
  }
}

object BitPlruPolicy extends App {
  chisel3.Driver.execute(args, () => new BitPlruPolicy(2, 8))
}
