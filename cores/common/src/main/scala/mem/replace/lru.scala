package cowlibry.common.mem.replace

import scala._
import chisel3._
import chisel3.util._


class LruPolicy (nAccess: Int, nLine: Int) extends Module {
  val io = IO(new Bundle {
    val i_flush = Input(Vec(nLine, Bool()))
    val o_free = Output(Vec(nLine, Bool()))

    val i_valid = Input(Vec(nAccess, Bool()))
    val i_line = Input(Vec(nAccess, UInt(log2Ceil(nLine).W)))

    val i_rep = Input(Bool())
    val i_use_line = Input(Vec(nLine, Bool()))
    val o_rep_av = Output(Bool())
    val o_rep_line = Output(UInt(log2Ceil(nLine).W))
  })

  // ******************************
  //        HISTORY REGISTERS
  // ******************************
  val reg_counter = Reg(Vec(nLine, UInt(log2Ceil(nLine).W)))

  // ******************************
  //            ZEROING
  // ******************************
  // ------------------------------
  //            ACCESS
  // ------------------------------
  val w_zav = Wire(Vec(nLine, Bool()))

  for (l <- 0 until nLine) {
    w_zav(l) := false.B
    for(a <- 0 until nAccess) {
      when(io.i_valid(a) & (l.U === io.i_line(a))) {
        w_zav(l) := true.B
      }
    }
  }

  // ------------------------------
  //            REPLACE
  // ------------------------------
  val w_zrep_av = Wire(Vec(nLine, Bool()))
  val w_zrep_line = Wire(Vec(nLine, UInt(log2Ceil(nLine).W)))
  val w_zrep_counter = Wire(Vec(nLine, UInt(log2Ceil(nLine).W)))

  w_zrep_av(0) := io.i_rep & io.i_use_line(0) & ~w_zav(0) & ~io.i_flush(0)
  w_zrep_line(0) := 0.U
  w_zrep_counter(0) := reg_counter(0)
  for (l <- 1 until nLine) {
    when(io.i_rep & io.i_use_line(l) & ~w_zav(l) & ~w_zrep_av(l - 1) & ~io.i_flush(l)) {
      w_zrep_av(l) := true.B
      w_zrep_line(l) := l.U
      w_zrep_counter(l) := reg_counter(l)
    }.elsewhen (io.i_rep & io.i_use_line(l) & ~w_zav(l) & (reg_counter(l) > w_zrep_counter(l - 1) & ~io.i_flush(l))) {
      w_zrep_av(l) := true.B
      w_zrep_line(l) := l.U
      w_zrep_counter(l) := reg_counter(l)
    }.otherwise {
      w_zrep_av(l) := w_zrep_av(l - 1)
      w_zrep_line(l) := w_zrep_line(l - 1)
      w_zrep_counter(l) := w_zrep_counter(l - 1)
    }
  }

  io.o_rep_av := w_zrep_av(nLine - 1)
  io.o_rep_line := w_zrep_line(nLine - 1)

  // ------------------------------
  //            FINALE
  // ------------------------------
  val w_znew = Wire(Vec(nLine, Bool()))
  for (l <- 0 until nLine) {
    w_znew(l) := w_zav(l)
    when ((l.U === w_zrep_line(nLine - 1)) & w_zrep_av(nLine - 1)) {
      w_znew(l) := true.B
    }
  }

  // ******************************
  //        VALUE INVENTORY
  // ******************************
  val w_inventory = Wire(Vec(nLine, Bool()))
  for (l0 <- 0 until nLine) {
    w_inventory(l0) := false.B
    for (l1 <- 0 until nLine) {
      when(~w_znew(l1) & l0.U === reg_counter(l1)) {
        w_inventory(l0) := true.B
      }
    }
  }

  // ******************************
  //        UPDATE COUNTER
  // ******************************
  for (l0 <- 0 until nLine) {
    for (l1 <- 0 until nLine - 1) {
      when(io.i_flush(l0) | w_znew(l0)) {
        reg_counter(l0) := 0.U
      }.elsewhen (~w_znew(l0) & (l1.U === reg_counter(l0)) & ~w_inventory(l1 + 1)) {
        reg_counter(l0) := reg_counter(l0) + 1.U
      }
    }
  }

  // ******************************
  //              FREE
  // ******************************
  for (l <- 0 until nLine) {
    io.o_free(l) := (reg_counter(l) === 0.U)
  }
}

object LruPolicy extends App {
  chisel3.Driver.execute(args, () => new LruPolicy(2, 8))
}
