package cowlibry.common.mem.flush

import chisel3._
import chisel3.util._


// ******************************
//             FLUSH
// ******************************
class FlushIO (nHart: Int, nDome: Int, nAddrBit: Int) extends Bundle {
  val ready = Input(Bool())
  val valid = Output(Bool())
  val apply = Output(Bool())
  val hart = Output(UInt(log2Ceil(nHart).W))
  val dome = Output(UInt(log2Ceil(nDome).W))
  val target = Output(UInt(TARGET.NBIT.W))
  val mode = Output(UInt(MODE.NBIT.W))
  val addr = Output(UInt(nAddrBit.W))

  override def cloneType = (new FlushIO(nHart, nDome, nAddrBit)).asInstanceOf[this.type]
}
