package cowlibry.common.mem.flush

import chisel3._


// ******************************
//             FLUSH
// ******************************
object MODE {
  val NBIT = 2
  def X = 0

  def LINE = 1
  def SET = 2
  def FULL = 3
}

object TARGET {
  val NBIT = 2
  def X = 0

  def INSTR = 1
  def DATA = 2
  def ALL = 3
}
