package cowlibry.common.mem.pmb

import chisel3._
import chisel3.util._


class PmbReqIO(p: PmbParams) extends Bundle {
  val ready = Input(Vec(p.nReadyBit, Bool()))
  val valid = Output(Bool())
  val hart = Output(UInt(log2Ceil(p.nHart).W))
  val dome = Output(UInt(log2Ceil(p.nDome).W))
  val rw = Output(Bool())
  val size = Output(UInt(SIZE.NBIT.W))
  val addr = Output(UInt(p.nAddrBit.W))
  val wdata = Output(UInt(p.nWordBit.W))

  override def cloneType = (new PmbReqIO(p)).asInstanceOf[this.type]
}

class PmbAckIO(p: PmbParams) extends Bundle {
  val ready = Output(Vec(p.nReadyBit, Bool()))
  val valid = Input(Bool())
  val hart = Input(UInt(log2Ceil(p.nHart).W))
  val dome = Input(UInt(log2Ceil(p.nDome).W))
  val rdata = Input(UInt(p.nWordBit.W))

  override def cloneType = (new PmbAckIO(p)).asInstanceOf[this.type]
}

class PmbIO(p: PmbParams) extends Bundle {
  val req = new PmbReqIO(p)
  val ack = new PmbAckIO(p)

  override def cloneType = (new PmbIO(p)).asInstanceOf[this.type]
}
