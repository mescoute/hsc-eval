package cowlibry.common.mem.pmb

import chisel3._
import chisel3.util._
import scala.math._


object PmbCfg0 extends PmbParams {
  def nHart: Int = 1
  def nDome: Int = 1
  def multiDome: Boolean = false

  def nAddrBit: Int = 32
  def nWordBit: Int = 32
}

object PmbCfg1 extends PmbParams {
  def nHart: Int = 1
  def nDome: Int = 1
  def multiDome: Boolean = false

  def nAddrBit: Int = 32
  def nWordBit: Int = 64
}

object PmbCfg5 extends PmbParams {
  def nHart: Int = 2
  def nDome: Int = 2
  def multiDome: Boolean = false

  def nAddrBit: Int = 32
  def nWordBit: Int = 32
}

object PmbCfg6 extends PmbParams {
  def nHart: Int = 2
  def nDome: Int = 2
  def multiDome: Boolean = true

  def nAddrBit: Int = 32
  def nWordBit: Int = 32
}
