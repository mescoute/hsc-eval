package cowlibry.common.mem.pmb

import chisel3._
import chisel3.util._
import scala.math._


trait PmbParams {
  def nHart: Int
  def nDome: Int
  def multiDome: Boolean

  def nAddrBit: Int
  def nWordBit: Int
  def nWordByte: Int = (nWordBit / 8).toInt
  def nReadyBit: Int = {
    if (multiDome) {
      return nDome
    } else {
      return 1
    }
  }
}

class PmbIntf(
  _nHart: Int,
  _nDome: Int,
  _multiDome: Boolean,

  _nAddrBit: Int,
  _nWordBit: Int
) extends PmbParams {
  def nHart: Int = _nHart
  def nDome: Int = _nDome
  def multiDome: Boolean = _multiDome

  def nAddrBit: Int = _nAddrBit
  def nWordBit: Int = _nWordBit
}
