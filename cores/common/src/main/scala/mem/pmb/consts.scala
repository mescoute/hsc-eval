package cowlibry.common.mem.pmb

import chisel3._

object SIZE {
  def NBIT = 3

  def B0 = 0
  def B1 = 1
  def B2 = 2
  def B4 = 3
  def B8 = 4
  def B16 = 5

  def max (nbit: Int) = {
    if (nbit >= 128) B16
    else if (nbit >= 64) B8
    else if (nbit >= 32) B4
    else if (nbit >= 16) B2
    else if (nbit >= 8) B1
    else B0
  }
}
