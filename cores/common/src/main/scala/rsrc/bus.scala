package cowlibry.common.rsrc

import chisel3._
import chisel3.util._


// ******************************
//            RESOURCE
// ******************************
class DomeBus (nRsrc: Int) extends Bundle {
  val valid = Input(Bool())
  val flush = Input(Bool())
  val free = Output(Bool())
  val weight = Input(UInt(log2Ceil(nRsrc + 1).W))

  override def cloneType = (new DomeBus(nRsrc)).asInstanceOf[this.type]
}

class RsrcBus (nHart: Int, nDome: Int, nRsrc: Int) extends Bundle {
  val valid = Input(Bool())
  val flush = Input(Bool())
  val free = Output(Bool())
  val hart = Input(UInt(log2Ceil(nHart).W))
  val dome = Input(UInt(log2Ceil(nDome).W))
  val port = Input(UInt(log2Ceil(nRsrc).W))

  override def cloneType = (new RsrcBus(nHart, nDome, nRsrc)).asInstanceOf[this.type]
}
