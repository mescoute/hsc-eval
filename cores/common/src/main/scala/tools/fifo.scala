package cowlibry.common.tools

import chisel3._
import chisel3.util._


// ** nSeqLvl = 0: cross read possible, free places calculate combinatorially, individual dynamic full signal
// ** nSeqLvl = 1: only synchronous read, free places calculate combinatorially, individual dynamic full signal
// ** nSeqLvl = 2: only synchronous read, free places calculate synchronously, individual dynamic full signal
// ** nSeqLvl = 3: only synchronous read, free places calculate synchronously, individual static full signal
// ** nSeqLvl = 4: only synchronous read, free places calculate synchronously, global static full signal

class Fifo[T <: Data](nSeqLvl: Int, gen: T, depth: Int, nWritePort: Int, nReadPort: Int) extends Module {
  val io = IO(new Bundle {
    val i_flush = Input(Bool())

    val o_full = Output(Vec(nWritePort, Bool()))
    val i_wen = Input(Vec(nWritePort, Bool()))
    val i_data = Input(Vec(nWritePort, gen))

    val o_empty = Output(Vec(nReadPort, Bool()))
    val i_ren = Input(Vec(nReadPort, Bool()))
    val o_data = Output(Vec(nReadPort, gen))

    val o_ext_valid = Output(Vec(depth, Bool()))
    val o_ext_fifo = Output(Vec(depth, gen))
  })

  val reg_pt = RegInit(0.U((log2Ceil(depth + 1)).W))
  val reg_fifo = Reg(Vec(depth, gen))

  // ******************************
  //              READ
  // ******************************
  val w_read_pt = Wire(Vec(nReadPort + 1, UInt((log2Ceil(depth + 1)).W)))
  val w_read_in_pt = Wire(Vec(nReadPort + 1, UInt((log2Ceil(nWritePort + 1)).W)))

  w_read_pt(0) := 0.U
  w_read_in_pt(0) := nWritePort.U
  if (nSeqLvl <= 0) {
    for (w <- 0 until nWritePort) {
      when (io.i_wen(nWritePort - 1 - w)) {
        w_read_in_pt(0) := (nWritePort - 1 - w).U
      }
    }
  }

  // ------------------------------
  //       SUPPORT CROSS READ
  // ------------------------------
  if (nSeqLvl <= 0) {
    for (r <- 0 until nReadPort) {
      io.o_empty(r) := (w_read_pt(r) >= reg_pt) & (w_read_in_pt(r) >= nWritePort.U)
      io.o_data(r) := reg_fifo(0)
      w_read_pt(r + 1) := w_read_pt(r)
      w_read_in_pt(r + 1) := w_read_in_pt(r)

      for (d <- 0 until depth) {
        when(w_read_pt(r) < reg_pt & d.U === w_read_pt(r)) {
          io.o_data(r) := reg_fifo(d)
        }
      }

      for (w <- 0 until nWritePort) {
        when((w_read_pt(r) >= reg_pt) & (w_read_in_pt(r) < nWritePort.U) & w.U === w_read_in_pt(r)) {
          io.o_data(r) := io.i_data(w)
        }
      }

      when(io.i_ren(r) & (w_read_pt(r) < reg_pt)) {
        w_read_pt(r + 1) := w_read_pt(r) + 1.U
      }

      when(io.i_ren(r) & (w_read_pt(r) >= reg_pt) & (w_read_in_pt(r) < nWritePort.U)) {
        w_read_in_pt(r + 1) := nWritePort.U
        for (w <- 0 until nWritePort) {
          when (io.i_wen(nWritePort - 1 - w) & ((nWritePort - 1 - w).U > w_read_in_pt(r))) {
            w_read_in_pt(r + 1) := (nWritePort - 1 - w).U
          }
        }
      }
    }

  // ------------------------------
  //         NO CROSS READ
  // ------------------------------
  } else {
    for (r <- 0 until nReadPort) {
      io.o_empty(r) := (w_read_pt(r) >= reg_pt)
      io.o_data(r) := reg_fifo(0)
      w_read_pt(r + 1) := w_read_pt(r)
      w_read_in_pt(r + 1) := w_read_in_pt(r)

      for (d <- 0 until depth) {
        when(d.U === w_read_pt(r)) {
          io.o_data(r) := reg_fifo(d)
        }
      }

      when(io.i_ren(r) & (w_read_pt(r) < reg_pt)) {
        w_read_pt(r + 1) := w_read_pt(r) + 1.U
      }
    }
  }

  // ******************************
  //             SHIFT
  // ******************************
  for (r <- 0 to nReadPort) {
    when (r.U === w_read_pt(nReadPort)) {
      for (s <- 0 until depth - r) {
        reg_fifo(s) := reg_fifo(s + r)
      }
    }
  }

  // ******************************
  //             WRITE
  // ******************************
  val w_write_pt = Wire(Vec(nWritePort + 1, UInt((log2Ceil(depth + 1)).W)))
  val w_full_pt = Wire(Vec(nWritePort + 1, UInt((log2Ceil(depth + 1)).W)))

  w_write_pt(0) := reg_pt - w_read_pt(nReadPort)
  if (nSeqLvl <= 1) {
    w_full_pt(0) := reg_pt - w_read_pt(nReadPort)
  } else if ((nSeqLvl == 2) || (nSeqLvl == 3)) {
    w_full_pt(0) := reg_pt
  } else {
    when((depth.U - reg_pt) >= nWritePort.U) {
      w_full_pt(0) := reg_pt
    }.otherwise {
      w_full_pt(0) := depth.U
    }
  }

  for (w <- 0 until nWritePort) {
    w_write_pt(w + 1) := w_write_pt(w)

    if (nSeqLvl <= 0) {
      io.o_full(w) := (w_full_pt(w) >= depth.U) & (w_read_in_pt(nReadPort) <= w.U)

      w_full_pt(w + 1) := w_full_pt(w)

      when(io.i_wen(w) & (w_full_pt(w) < depth.U) & (w_read_in_pt(nReadPort) <= w.U)) {
        for (d <- 0 until depth) {
          when(d.U === w_write_pt(w)) {
            reg_fifo(d) := io.i_data(w)
          }
        }
        w_write_pt(w + 1) := w_write_pt(w) + 1.U
        w_full_pt(w + 1) := w_full_pt(w) + 1.U
      }
    } else {
      io.o_full(w) := (w_full_pt(w) >= depth.U)

      if (nSeqLvl == 3) {
        w_full_pt(w + 1) := w_full_pt(w) + 1.U
      } else if (nSeqLvl >= 4) {
        w_full_pt(w + 1) := w_full_pt(0)
      } else {
        w_full_pt(w + 1) := w_full_pt(w)
      }

      when(io.i_wen(w) & (w_full_pt(w) < depth.U)) {
        for (d <- 0 until depth) {
          when(d.U === w_write_pt(w)) {
            reg_fifo(d) := io.i_data(w)
          }
        }
        w_write_pt(w + 1) := w_write_pt(w) + 1.U
        if (nSeqLvl <= 2) w_full_pt(w + 1) := w_full_pt(w) + 1.U
      }
    }
  }

  // ******************************
  //           REGISTER
  // ******************************
  when(io.i_flush) {
    reg_pt := 0.U
  }.otherwise {
    reg_pt := w_write_pt(nWritePort)
  }

  // ******************************
  //        EXTERNAL ACCESS
  // ******************************
  for (d <- 0 until depth) {
    io.o_ext_valid(d) := (d.U < reg_pt)
    io.o_ext_fifo(d) := reg_fifo(d)
  }
}

object Fifo extends App {
  chisel3.Driver.execute(args, () => new Fifo(2, UInt(8.W), 4, 2, 2))
}
