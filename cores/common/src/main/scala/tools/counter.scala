package cowlibry.common.tools

import chisel3._
import chisel3.util._
import scala.math._


class Counter (nBit: Int) extends Module {
  val io = IO(new Bundle {
    val i_init = Input(Bool())
    val i_en = Input(Bool())
    val i_limit = Input(UInt(nBit.W))

    val o_flag = Output(Bool())
    val o_val = Output(UInt(nBit.W))
  })

  val reg_counter = RegInit(0.U(nBit.W))
  val w_counter = Wire(UInt(nBit.W))
  val w_flag = Wire(Bool())

  w_flag := (reg_counter === (io.i_limit - 1.U))

  when (io.i_init) {
    w_counter := 0.U
  }.elsewhen (io.i_en & w_flag) {
    w_counter := 0.U
  }.elsewhen (io.i_en) {
    w_counter := reg_counter + 1.U
  }.otherwise {
    w_counter := reg_counter
  }

  reg_counter := w_counter

  io.o_flag := w_flag
  io.o_val := reg_counter
}

object Counter extends App {
  chisel3.Driver.execute(args, () => new Counter(32))
}
