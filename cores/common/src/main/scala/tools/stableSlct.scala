package cowlibry.common.tools

import chisel3._
import chisel3.util._


class StableSlct(useChain: Boolean, nTarget: Int) extends Module {
  val io = IO(new Bundle {
    val i_valid = Input(Vec(nTarget, Bool()))
    val i_chain = if (useChain) Some(Input(UInt(log2Ceil(nTarget).W))) else None

    val o_slct = Output(UInt(log2Ceil(nTarget).W))
    val o_chain = if (useChain) Some(Output(UInt(log2Ceil(nTarget).W))) else None
  })

  if (nTarget > 1) {
    // ******************************
    //             INIT
    // ******************************
    val init_slct = Wire(UInt(log2Ceil(nTarget).W))
    init_slct := 0.U
    val reg_slct = RegInit(init_slct)

    io.o_slct := reg_slct

    // ******************************
    //             SLCT
    // ******************************
    val w_slct = Wire(UInt(log2Ceil(nTarget).W))
    if (useChain) w_slct := io.i_chain.get else w_slct := reg_slct

    val w_next_slct_low = Wire(Vec(nTarget + 1, Bool()))
    val w_next_slct_high = Wire(Vec(nTarget + 1, Bool()))
    val w_next_slct = Wire(UInt(log2Ceil(nTarget).W))

    w_next_slct_low(0) := 0.B
    w_next_slct_high(0) := 0.B
    w_next_slct := 0.U

    for (t <- 0 until nTarget) {
      when (t.U <= w_slct) {
        w_next_slct_high(t + 1) := 0.B
        when (~w_next_slct_low(t)) {
          w_next_slct_low(t + 1) := io.i_valid(t)
          w_next_slct := t.U
        }.otherwise {
          w_next_slct_low(t + 1) := w_next_slct_low(t)
        }
      }.otherwise {
        w_next_slct_low(t + 1) := w_next_slct_low(t)
        when (~w_next_slct_high(t) & ~w_next_slct_low(t)) {
          w_next_slct_high(t + 1) := io.i_valid(t)
          w_next_slct := t.U
        }.elsewhen (~w_next_slct_high(t) & w_next_slct_low(t) & io.i_valid(t)) {
          w_next_slct_high(t + 1) := 1.B
          w_next_slct := t.U
        }.otherwise {
          w_next_slct_high(t + 1) := w_next_slct_high(t)
        }
      }
    }

    reg_slct := w_next_slct
    if (useChain) io.o_chain.get := w_next_slct
  } else {
    io.o_slct := 0.U
    if (useChain) io.o_chain.get := 0.U
  }
}

object StableSlct extends App {
  chisel3.Driver.execute(args, () => new StableSlct(true, 4))
}
