package cowlibry.common.tools

import chisel3._
import chisel3.util._


class ByteToBit(inByte : Int, outByte : Int) extends Module {
  val io = IO(new Bundle {
    val i_in = Input(Vec(inByte,UInt(8.W)))
    val o_out = Output(UInt((outByte * 8).W))
  })

  val w_concat = Wire(Vec(outByte, UInt((outByte * 8).W)))

  w_concat(0) := io.i_in(0)
  if(outByte > 1 && inByte >= outByte) {
    for (i <- 1 until outByte) {
      w_concat(i) := Cat(io.i_in(i), w_concat(i - 1)(i*8 - 1,0))
    }
  } else if (outByte > 1) {
    for (i <- 1 until outByte) {
      if(i < inByte) {
        w_concat(i) := Cat(io.i_in(i), w_concat(i - 1)(i*8 - 1,0))
      }else {
        w_concat(i) := Cat(0.U(8.W), w_concat(i - 1)(i*8 - 1,0))
      }
    }
  } else {
    w_concat(outByte - 1) := io.i_in(0)
  }

  io.o_out := w_concat(outByte - 1)
}


class BitToByte(inByte : Int, outByte : Int) extends Module {
  val io = IO(new Bundle {
    val i_in = Input(UInt((inByte * 8).W))
    val o_out = Output(Vec(outByte, UInt(8.W)))
  })

  if (inByte > outByte) {
    for (i <- 0 until outByte) {
      io.o_out(i) := io.i_in(i*8 + 7, i*8)
    }
  } else {
    for (i <- 0 until outByte) {
      if (i < inByte) {
        io.o_out(i) := io.i_in(i*8 + 7, i*8)
      } else {
        io.o_out(i) := 0.U
      }
    }
  }
}

class ResizeVec(nIn : Int, nOut : Int) extends Module {
  val io = IO(new Bundle {
    val i_in = Input(Vec(nIn,UInt(8.W)))
    val o_out = Output(Vec(nOut,UInt(8.W)))
  })

  if (nIn > nOut) {
    for (i <- 0 until nOut) {
      io.o_out(i) := io.i_in(i)
    }
  } else if (nIn == nOut) {
    io.o_out := io.i_in
  } else {
    for (i <- 0 until nOut) {
      if (i < nIn) {
        io.o_out(i) := io.i_in(i)
      } else {
        io.o_out(i) := 0.U
      }
    }
  }
}
