package cowlibry.common.tools

import chisel3._
import scala.math._

object MATH {
  def log2 (x: Int): Double = log10(x)/log10(2.0)
  def islog2 (x: Int): Boolean = (log2(x).ceil == log2(x).floor)
}
