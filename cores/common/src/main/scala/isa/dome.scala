package cowlibry.common.isa

import chisel3._
import chisel3.util._


object DOME {
  // ******************************
  //              DOME
  // ******************************
  // ------------------------------
  //      INSTRUCTION ENCODING
  // ------------------------------
  val SWITCH = BitPat("b00000000000000000010?????1110111")
  val START  = BitPat("b00000000000000000011?????1110111")
  val END    = BitPat("b00000000000000000100?????1110111")

  // ------------------------------
  //         CSR ADDRESSES
  // ------------------------------
  val CSRID = "hfc0"
  val CSRPC = "hfc1"
  val CSRCAP = "hfc2"
  val CSRNEXTID = "hbc0"
  val CSRNEXTPC = "hbc1"
  val CSRNEXTCAP = "hbc2"

  // ------------------------------
  //         CAPABILITIES
  // ------------------------------
  val NCAPBIT = 3
  val POSCAPWEIGHT = 0
  val NCAPWEIGHTBIT = 2
  val CAPWEIGHTMIN = 0
  val CAPWEIGHTMAX = 1
  val CAPWEIGHTFULL = 2

  val POSCAPMULDIV = 2
  val NCAPMULDIVBIT = 1
}
