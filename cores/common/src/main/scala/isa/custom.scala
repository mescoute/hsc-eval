package cowlibry.common.isa

import chisel3._
import chisel3.util._


object CUSTOM {
  // ******************************
  //     INSTRUCTION ENCODING
  // ******************************

  // ******************************
  //            REGISTERS
  // ******************************
  // ------------------------------
  //          CSR: COUNTERS
  // ------------------------------
  val CSRBRANCH = "hc03"
  val CSRMISPRED = "hc04"
  val CSRBRANCHH = "hc83"
  val CSRMISPREDH = "hc84"

  // ------------------------------
  //          CSR: HART
  // ------------------------------
  val CSRMHARTEN = "h7c0"

}
