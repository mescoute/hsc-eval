package cowlibry.common.initram

import chisel3._
import chisel3.util._
import chisel3.experimental._

import cowlibry.common.mem.pmb._


class InitRamReqBus(p: InitRamParams) extends Bundle {
  val valid = Bool()
  val rw = Bool()
  val size = UInt(3.W)
  val addr = UInt(p.nAddrBit.W)
  val wdata = UInt(p.nWordBit.W)

  override def cloneType = (new InitRamReqBus(p)).asInstanceOf[this.type]
}
