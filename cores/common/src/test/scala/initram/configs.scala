package cowlibry.common.initram

import chisel3._
import chisel3.util._
import chisel3.experimental._


object InitRamDefault extends InitRamParams {
  def nHart: Int = 1
  def nDome: Int = 1
  def multiDome: Boolean = true

  def nAddrBit: Int = 32
  def nWordBit: Int = 32

  def nRamByte: Int = 256
}


object InitRamDbg0 extends InitRamParams {
  def nHart: Int = 2
  def nDome: Int = 2
  def multiDome: Boolean = true

  def nAddrBit: Int = 32
  def nWordBit: Int = 32

  def nRamByte: Int = 256
}
