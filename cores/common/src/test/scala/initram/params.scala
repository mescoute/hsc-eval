package cowlibry.common.initram

import chisel3._
import chisel3.util._
import scala.math._

import cowlibry.common.mem.pmb._


trait InitRamParams extends PmbParams {
  def nRamByte: Int
}

class InitRamIntf(
  _nRamByte: Int,

  _nHart: Int,
  _nDome: Int,
  _multiDome: Boolean,

  _nAddrBit: Int,
  _nWordBit: Int
) extends InitRamParams {
  def nRamByte: Int = _nRamByte

  def nHart: Int = _nHart
  def nDome: Int = _nDome
  def multiDome: Boolean = _multiDome

  def nAddrBit: Int = _nAddrBit
  def nWordBit: Int = _nWordBit
}
