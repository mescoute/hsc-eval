package cowlibry.common.initram

import chisel3._
import chisel3.util._
import chisel3.experimental._

import cowlibry.common.mem.pmb._


class InitRamSv(NBYTE: Int, NADDRBIT: Int, NWORDBYTE: Int)
  extends BlackBox(Map( "NBYTE" -> NBYTE,
                        "NADDRBIT" -> NADDRBIT,
                        "NWORDBYTE" -> NWORDBYTE)) with HasBlackBoxResource {
  val io = IO(new Bundle() {
    val i_clk = Input(Clock())
    val i_reset = Input(Reset())

    val i_p0_req_valid = Input(Bool())
    val i_p0_req_size = Input(UInt(3.W))
    val i_p0_req_addr = Input(UInt(NADDRBIT.W))
    val o_p0_ack_rdata = Output(UInt((NWORDBYTE * 8).W))

    val i_p1_req_valid = Input(Bool())
    val i_p1_req_rw = Input(Bool())
    val i_p1_req_size = Input(UInt(3.W))
    val i_p1_req_addr = Input(UInt(32.W))
    val i_p1_ack_wdata = Input(UInt((NWORDBYTE * 8).W))
    val o_p1_ack_rdata = Output(UInt((NWORDBYTE * 8).W))

    val o_debug_memory = Output(UInt((NBYTE*8).W))
  })

  setResource("/sv/initram.sv")
}

class InitRam(p: InitRamParams) extends Module {
  val io = IO(new Bundle() {
    val b_port = Vec(2, Flipped(new PmbIO(p)))

    val o_dbg_ram = Output(UInt((p.nRamByte * 8).W))
  })

  val s0NONE :: s1READ :: s2DELAYREAD :: s3WRITE :: Nil = Enum(4)

  // ******************************
  //              RAM
  // ******************************
  val ram = Module(new InitRamSv(p.nRamByte, p.nAddrBit, p.nWordByte))
  ram.io.i_clk := clock
  ram.io.i_reset := reset

  io.o_dbg_ram := ram.io.o_debug_memory

  // ******************************
  //             PORT
  // ******************************
  val w_ready = Wire(Vec(2, Bool()))
  val w_nreq = Wire(Vec(2, new InitRamReqBus(p)))
  val w_ack = Wire(Vec(2, UInt(p.nWordBit.W)))

  val reg_state = RegInit(VecInit(Seq.fill(2)(s0NONE)))
  val reg_hart = Reg(Vec(2, UInt(log2Ceil(p.nHart).W)))
  val reg_dome = Reg(Vec(2, UInt(log2Ceil(p.nDome).W)))
  val reg_size = Reg(Vec(2, UInt(3.W)))
  val reg_addr = Reg(Vec(2, UInt(p.nAddrBit.W)))
  val reg_wdata = Reg(Vec(2, UInt(p.nWordBit.W)))
  val reg_rdata = Reg(Vec(2, UInt(p.nWordBit.W)))

  // ------------------------------
  //             READY
  // ------------------------------
  for (pio <- 0 until 2) {
    if (p.nDome > 1 & p.multiDome) {
      w_ready(pio) := false.B
      for (d <- 0 until p.nDome) {
        when (d.U === reg_dome(pio)) {
          w_ready(pio) := io.b_port(pio).ack.ready(d)
        }
      }
    } else {
      w_ready(pio) := io.b_port(pio).ack.ready(0)
    }
  }

  // ------------------------------
  //             FSM
  // ------------------------------
  for (pio <- 0 until 2) {
    when (reg_state(pio) === s0NONE) {
      when(io.b_port(pio).req.valid & io.b_port(pio).req.rw) {
        reg_state(pio) := s3WRITE
      }.elsewhen(io.b_port(pio).req.valid) {
        reg_state(pio) := s1READ
      }.otherwise {
        reg_state(pio) := s0NONE
      }
    }.elsewhen (reg_state(pio) === s1READ | reg_state(pio) === s2DELAYREAD) {
      when(~w_ready(pio)) {
        reg_state(pio) := s2DELAYREAD
      }.elsewhen (io.b_port(pio).req.valid & io.b_port(pio).req.rw) {
        reg_state(pio) := s3WRITE
      }.elsewhen(io.b_port(pio).req.valid) {
        reg_state(pio) := s1READ
      }.otherwise {
        reg_state(pio) := s0NONE
      }
    }.elsewhen (reg_state(pio) === s3WRITE) {
      when(~w_ready(pio) | (io.b_port(pio).req.valid & io.b_port(pio).req.rw)) {
        reg_state(pio) := s3WRITE
      }.otherwise {
        reg_state(pio) := s0NONE
      }
    }.otherwise {
      reg_state(pio) := s0NONE
    }
  }

  // ------------------------------
  //           REGISTERS
  // ------------------------------
  for (pio <- 0 until 2) {
    when (reg_state(pio) === s0NONE) {
      reg_hart(pio) := io.b_port(pio).req.hart
      reg_dome(pio) := io.b_port(pio).req.dome
      reg_size(pio) := io.b_port(pio).req.size
      reg_addr(pio) := io.b_port(pio).req.addr
      reg_wdata(pio) := io.b_port(pio).req.wdata
      reg_rdata(pio) := 0.U
    }.elsewhen (reg_state(pio) === s1READ) {
      when(w_ready(pio)) {
        reg_hart(pio) := io.b_port(pio).req.hart
        reg_dome(pio) := io.b_port(pio).req.dome
        reg_size(pio) := io.b_port(pio).req.size
        reg_addr(pio) := io.b_port(pio).req.addr
        reg_wdata(pio) := io.b_port(pio).req.wdata
      }
      reg_rdata(pio) := w_ack(pio)
    }.elsewhen (reg_state(pio) === s2DELAYREAD) {
      when(w_ready(pio)) {
        reg_hart(pio) := io.b_port(pio).req.hart
        reg_dome(pio) := io.b_port(pio).req.dome
        reg_size(pio) := io.b_port(pio).req.size
        reg_addr(pio) := io.b_port(pio).req.addr
        reg_wdata(pio) := io.b_port(pio).req.wdata
      }
    }.elsewhen (reg_state(pio) === s3WRITE) {
      when(w_ready(pio)) {
        reg_hart(pio) := io.b_port(pio).req.hart
        reg_dome(pio) := io.b_port(pio).req.dome
        reg_size(pio) := io.b_port(pio).req.size
        reg_addr(pio) := io.b_port(pio).req.addr
        reg_wdata(pio) := io.b_port(pio).req.wdata
        reg_rdata(pio) := 0.U
      }
    }.otherwise {
      reg_hart(pio) := 0.U
      reg_dome(pio) := 0.U
      reg_size(pio) := 0.U
      reg_addr(pio) := 0.U
      reg_wdata(pio) := 0.U
      reg_rdata(pio) := 0.U
    }
  }

  // ------------------------------
  //             PORT
  // ------------------------------
  for (pio <- 0 until 2) {
    when (reg_state(pio) === s0NONE) {
      if (p.nDome > 1 & p.multiDome) {
        for (d <- 0 until p.nDome) {
          io.b_port(pio).req.ready(d) := 1.B
        }
      } else {
        io.b_port(pio).req.ready(0) := 1.B
      }

      io.b_port(pio).ack.hart := 0.U
      io.b_port(pio).ack.dome := 0.U
      io.b_port(pio).ack.valid := 0.B
      io.b_port(pio).ack.rdata := 0.U
    }.elsewhen (reg_state(pio) === s1READ) {
      if (p.nDome > 1 & p.multiDome) {
        for (d <- 0 until p.nDome) {
          io.b_port(pio).req.ready(d) := w_ready(pio)
        }
      } else {
        io.b_port(pio).req.ready(0) := w_ready(pio)
      }

      io.b_port(pio).ack.hart := reg_hart(pio)
      io.b_port(pio).ack.dome := reg_dome(pio)
      io.b_port(pio).ack.valid := true.B
      io.b_port(pio).ack.rdata := w_ack(pio)
    }.elsewhen (reg_state(pio) === s2DELAYREAD) {
      if (p.nDome > 1 & p.multiDome) {
        for (d <- 0 until p.nDome) {
          io.b_port(pio).req.ready(d) := w_ready(pio)
        }
      } else {
        io.b_port(pio).req.ready(0) := w_ready(pio)
      }

      io.b_port(pio).ack.hart := reg_hart(pio)
      io.b_port(pio).ack.dome := reg_dome(pio)
      io.b_port(pio).ack.valid := true.B
      io.b_port(pio).ack.rdata := reg_rdata(pio)
    }.elsewhen (reg_state(pio) === s3WRITE) {
      if (p.nDome > 1 & p.multiDome) {
        for (d <- 0 until p.nDome) {
          when (io.b_port(pio).req.rw) {
            io.b_port(pio).req.ready(d) := w_ready(pio)
          }.otherwise {
            io.b_port(pio).req.ready(d) := false.B
          }
        }
      } else {
        when (io.b_port(pio).req.rw) {
          io.b_port(pio).req.ready(0) := w_ready(pio)
        }.otherwise {
          io.b_port(pio).req.ready(0) := false.B
        }
      }

      io.b_port(pio).ack.hart := reg_hart(pio)
      io.b_port(pio).ack.dome := reg_dome(pio)
      io.b_port(pio).ack.valid := true.B
      io.b_port(pio).ack.rdata := 0.U
    }.otherwise {
      if (p.nDome > 1 & p.multiDome) {
        for (d <- 0 until p.nDome) {
          io.b_port(pio).req.ready(d) := false.B
        }
      } else {
        io.b_port(pio).req.ready(0) := false.B
      }

      io.b_port(pio).ack.hart := 0.U
      io.b_port(pio).ack.dome := 0.U
      io.b_port(pio).ack.valid := false.B
      io.b_port(pio).ack.rdata := 0.U
    }
  }

  // ------------------------------
  //          RAM REQUEST
  // ------------------------------
  for (pio <- 0 until 2) {
    when (reg_state(pio) === s0NONE | reg_state(pio) === s1READ | reg_state(pio) === s2DELAYREAD) {
      w_nreq(pio).valid := io.b_port(pio).req.valid & ~io.b_port(pio).req.rw
      w_nreq(pio).rw := 0.B
      w_nreq(pio).size := io.b_port(pio).req.size
      w_nreq(pio).addr := io.b_port(pio).req.addr
      w_nreq(pio).wdata := 0.U
    }.elsewhen (reg_state(pio) === s3WRITE) {
      w_nreq(pio).valid := w_ready(pio)
      w_nreq(pio).rw := 1.B
      w_nreq(pio).size := reg_size(pio)
      w_nreq(pio).addr := reg_addr(pio)
      w_nreq(pio).wdata := reg_wdata(pio)
    }.otherwise {
      w_nreq(pio).valid := false.B
      w_nreq(pio).rw := 0.B
      w_nreq(pio).size := 0.U
      w_nreq(pio).addr := 0.U
      w_nreq(pio).wdata := 0.U
    }
  }

  ram.io.i_p0_req_valid := w_nreq(0).valid
  ram.io.i_p0_req_size := w_nreq(0).size
  ram.io.i_p0_req_addr := w_nreq(0).addr
  w_ack(0) := ram.io.o_p0_ack_rdata

  ram.io.i_p1_req_valid := w_nreq(1).valid
  ram.io.i_p1_req_rw := w_nreq(1).rw
  ram.io.i_p1_req_size := w_nreq(1).size
  ram.io.i_p1_req_addr := w_nreq(1).addr
  ram.io.i_p1_ack_wdata := w_nreq(1).wdata
  w_ack(1) := ram.io.o_p1_ack_rdata
}

class InitRamWrap(p: InitRamParams) extends Module {
  val io = IO(new Bundle() {
    val b_port = Vec(2, Flipped(new PmbIO(p)))
    val i_wdata = Input(Vec(2, Vec(p.nWordByte, UInt(8.W))))

    val o_dbg_ram = Output(UInt((p.nRamByte*8).W))
  })

  val ram = Module(new InitRam(p))
  for (pio <- 0 until 2) {
    ram.io.b_port(pio) <> io.b_port(pio)
    ram.io.b_port(pio).req.wdata := io.i_wdata(pio).asUInt
  }

  io.o_dbg_ram := ram.io.o_dbg_ram
}

object InitRam extends App {
  chisel3.Driver.execute(args, () => new InitRam(InitRamDefault))
}

object InitRamWrap extends App {
  chisel3.Driver.execute(args, () => new InitRamWrap(InitRamDefault))
}
