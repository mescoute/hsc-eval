module InitRamSv
  #(  parameter NBYTE = 64,
      parameter NADDRBIT = 32,
      parameter NWORDBYTE = 8)
  (   input logic                     i_clk,
      input logic                     i_reset,

      // P1 READ-ONLY PORT
      input logic                     i_p0_req_valid,
      input logic [2:0]               i_p0_req_size,
      input logic [NADDRBIT-1:0]      i_p0_req_addr,
      output logic [NWORDBYTE*8-1:0]  o_p0_ack_rdata,

      // P2 READ-WRITE PORT
      input logic                     i_p1_req_valid,
      input logic                     i_p1_req_rw,
      input logic [2:0]               i_p1_req_size,
      input logic [NADDRBIT-1:0]      i_p1_req_addr,
      input logic [NWORDBYTE*8-1:0]   i_p1_ack_wdata,
      output logic [NWORDBYTE*8-1:0]  o_p1_ack_rdata,

      output logic [NBYTE*8-1:0]      o_debug_memory);

  logic [7:0] memory [NBYTE-1:0];

  always_comb begin
    for(int i = 0; i < NBYTE; i++) begin
      o_debug_memory[i*8 +: 8] = memory[i];
    end
  end

  `ifdef verilator
    export "DPI-C" task new_readmemh;

    task new_readmemh;
      input string INITFILE;
      $readmemh(INITFILE, memory);
    endtask
  `endif

  logic [7:0] p0_rdata [NWORDBYTE-1:0];
  logic [7:0] p0_nbyte;

  logic [7:0] p1_wdata [NWORDBYTE-1:0];
  logic [7:0] p1_rdata [NWORDBYTE-1:0];
  logic [7:0] p1_nbyte;

  // OPERATION SIZES
  generate
    if (NWORDBYTE == 2) begin
      assign p0_nbyte = (i_p0_req_size == 3'h1) ? 8'h01:
                        (i_p0_req_size == 3'h2) ? 8'h02: 8'h00;
      assign p1_nbyte = (i_p1_req_size == 3'h1) ? 8'h01:
                        (i_p1_req_size == 3'h2) ? 8'h02: 8'h00;
    end
    else if (NWORDBYTE == 4) begin
      assign p0_nbyte = (i_p0_req_size == 3'h1) ? 8'h01:
                        (i_p0_req_size == 3'h2) ? 8'h02:
                        (i_p0_req_size == 3'h3) ? 8'h04: 8'h00;
      assign p1_nbyte = (i_p1_req_size == 3'h1) ? 8'h01:
                        (i_p1_req_size == 3'h2) ? 8'h02:
                        (i_p1_req_size == 3'h3) ? 8'h04: 8'h00;
    end
    else if (NWORDBYTE == 8) begin
      assign p0_nbyte = (i_p0_req_size == 3'h1) ? 8'h01:
                        (i_p0_req_size == 3'h2) ? 8'h02:
                        (i_p0_req_size == 3'h3) ? 8'h04:
                        (i_p0_req_size == 3'h4) ? 8'h08: 8'h00;
      assign p1_nbyte = (i_p1_req_size == 3'h1) ? 8'h01:
                        (i_p1_req_size == 3'h2) ? 8'h02:
                        (i_p1_req_size == 3'h3) ? 8'h04:
                        (i_p1_req_size == 3'h4) ? 8'h08: 8'h00;
    end
    else if (NWORDBYTE == 16) begin
      assign p0_nbyte = (i_p0_req_size == 3'h1) ? 8'h01:
                        (i_p0_req_size == 3'h2) ? 8'h02:
                        (i_p0_req_size == 3'h3) ? 8'h04:
                        (i_p0_req_size == 3'h4) ? 8'h08:
                        (i_p0_req_size == 3'h5) ? 8'h10: 8'h00;
      assign p1_nbyte = (i_p1_req_size == 3'h1) ? 8'h01:
                        (i_p1_req_size == 3'h2) ? 8'h02:
                        (i_p1_req_size == 3'h3) ? 8'h04:
                        (i_p1_req_size == 3'h4) ? 8'h08:
                        (i_p1_req_size == 3'h5) ? 8'h10: 8'h00;
    end
    else begin
      assign p0_nbyte = (i_p0_req_size == 3'h1) ? 8'h01: 8'h00;
      assign p1_nbyte = (i_p1_req_size == 3'h1) ? 8'h01: 8'h00;
    end
  endgenerate

  // P1 READ
  always_ff @(posedge i_clk) begin
    if (i_p0_req_valid) begin
      for(int i = 0; i < NWORDBYTE; i++) begin
        if (i < p0_nbyte) begin
          p0_rdata[i] = memory[i_p0_req_addr + i];
        end
        else begin
          p0_rdata[i] = 8'h0;
        end
      end
    end
    else begin
      for(int i = 0; i < NWORDBYTE; i++) begin
        p0_rdata[i] = 8'h0;
      end
    end
  end

  // P2 READ
  always_ff @(posedge i_clk) begin
    if (i_p1_req_valid) begin
      for(int i = 0; i < NWORDBYTE; i++) begin
        if (i < p1_nbyte) begin
          p1_rdata[i] = memory[i_p1_req_addr + i];
        end
        else begin
          p1_rdata[i] = 8'h0;
        end
      end
    end
    else begin
      for(int i = 0; i < NWORDBYTE; i++) begin
        p1_rdata[i] = 8'h0;
      end
    end
  end

  // P2 WRITE
  always_ff @(posedge i_clk) begin
    if (i_p1_req_valid && i_p1_req_rw) begin
      for(int i = 0; i < p1_nbyte; i++) begin
        memory[i_p1_req_addr + i] = p1_wdata[i];
      end
    end
  end

  // I/O INTERFACE
  always_comb begin
    for(int i = 0; i < NWORDBYTE; i++) begin
      o_p0_ack_rdata[i*8 +: 8] = p0_rdata[i];
    end
  end

  always_comb begin
    for(int i = 0; i < NWORDBYTE; i++) begin
      o_p1_ack_rdata[i*8 +: 8] = p1_rdata[i];
      p1_wdata[i] = i_p1_ack_wdata[i*8 +: 8];
    end
  end
endmodule
