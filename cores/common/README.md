# Cowlibry - Common designs and constants

---
:warning: **Warning:** This repository is not a self-running repository.
It must be used with a top repository like [cowlibry](https://gitlab.com/cowlibry/cowlibry).

---
