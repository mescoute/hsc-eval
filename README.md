# HSC evaluation

## Automated, reproducible builds

### Prerequisites

Install the nix package manager:
```
curl -L https://nixos.org/nix/install | sh
```

### Building the modified toolchain (riscv32-inria-elf)

You do not necessarily need to **install** the toolchain: some experiments get the toolchain with a *nix-shell* command that do not require to install permanently any package on your machine.
The easiest way to install the modified toolchain is from binaries by going to the *toolchain* directory and installing the correct nix package.
The toolchain requires to download around 500MB of data from the internet.

```
cd toolchain
nix-env -i -f riscv32-inria-elf.nix
```

Now, the toolchain binaries can be used with the **riscv32-inria-elf** triplet (e.g. riscv32-inria-elf-gcc for the modifiec compiler).

Alternatively, to build from sources, read the *Readme.md* in the toolchain directory.

### Security evaluation

Security evaluation can be done with two test suites:
- hsc_evaluator: the historic benchmarks including single-threaded and multi-threaded.
- timesecbench: the modern test suite for single-threaded tests.
#### Hex
Build the evaluation binaries.
**hsc_evaluator**:
```
eval/hsc_evaluator/build_apps.sh
```

**timesecbench**:
For single-threaded benchmarks: use the timesecbench bench suite.
```
eval/timesecbench/build_apps.sh
```

#### Cores

We will generate different cores. One for each benchmark : first we selected core configurations that **increase** the leakage to show the efficiency of our countermeasure.
The countermeasure is enabled only for the *pXXX* benchmarks, and it is the only difference with respect to *uXXX* benchmarks. Core configuration can be seen, and modified, in the *src/test/scala* folders.

**hsc_evaluator**:
First build all the cores.
```
verilator/security_eval/build_cores.sh
```

Optional: it is possible to selectively build a pair of cores (protected and unprotected).
```
verilator/security_eval/build_cores.sh aubrac l1d
```

**timesecbench**:
Build all the cores.
```
verilator/timesecbench/build_cores.sh
```
#### Eval

**hsc_evaluator**:
Then launch the evaluation executables and the cores you just built.
```
verilator/security_eval/build_evals.sh
```

Optional: it is possible to selectively evaluate an hex:
```
verilator/security_eval/build_evals.sh aubrac l1d 8
```
(8 is the matrix size, and depends on the selected benchmark).

**timesecbench**:
Launch the benchmark with
```
verilator/timesecbench/build_evals.sh
```

You can generate the channel matrix figures, if the benchmark results are present in the corresponding folder, with
```
verilator/timesecbench/display_evals.sh
```

## Manual version

### Prerequisites

Install the last version of [Verilator](https://www.veripool.org/projects/verilator/wiki/Installing)
:warning: This version is not compatible with the one necesary for Chisel3 back-end.
Then, to be able to use Verilator in each case, both versions must be separately installed.

```
  $ git submodule init
  $ git submodule update
```

### HSC evaluation

To generate Verilog for Aubrac unprotected L1D cache:
```
  sbt 'test:runMain eval.aubrac.AubracUL1D --target-dir verilator/src --top-name AubracUL1D'
```

To generate the executable and traces:
```
  $ cd verilator/
  $ verilator -Wno-WIDTH -cc src/AubracUL1D.v src/initram.sv --exe --build src/aubrac-ul1d.cpp --trace
```
To launch the executable with a .hex file argument and generate a .vcd file:
```
  $ mkdir vcd
  $ ./obj_dir/VAubracUL1D ../src/test/resources/hex/eval/pl1d.hex vcd/pl1d.vcd
```

### Embench

To generate the Verilog files of Aubrac core:
```
  $ sbt 'test:runMain eval.aubrac.AubracEmbench0 --target-dir verilator/src'
```

Then, the different Verilog files are in *verilator/src*.
To generate the executable file of Aubrac with Verilator:
```
  $ cd verilator/
  $ verilator -Wno-WIDTH -cc src/AubracTop.v src/initram.sv --exe --build src/aubrac-embench.cpp --trace
```

To launch the executable with a .hex file argument and generate a .vcd file:
```
  $ ./obj_dir/VAubracTop ../src/test/resources/hex/embench/aha-mont64.hex vcd/aha-mont64.vcd
```

|    _Benchmark_    | _Speed AubracU_ | _Speed AubracP_ | _Speed AubracPP_ |
|-------------------|:----------------|:----------------|:-----------------|
| `aha-mont64`      |    6,249,186    |    5,267,826    |     5,216,597    |
| `crc32`           |    7,439,069    |    6,046,445    |     5,925,409    |
| `cubic`           |   15,089,609    |   14,845,427    |    13,221,114    |
| `edn`             |    9,075,371    |    7,753,903    |     7,687,901    |
| `huffbench`       |    4,075,732    |    3,105,147    |     3,087,519    |
| `matmult-int`     |    9,900,391    |    8,547,639    |     5,958,431    |
| `minver`          |   12,314,473    |   12,080,792    |     9,027,269    |
| `nbody`           |    6,659,980    |    6,394,446    |     5,529,196    |
| `nettle-aes`      |    8,600,245    |    8,456,881    |     5,450,906    |
| `nettle-sha256`   |    5,807,900    |    5,740,915    |     4,941,964    |
| `nsichneu`        |    7,480,871    |    7,480,871    |     7,480,871    |
| `picojpeg`        |    6,358,037    |    5,359,520    |     4,931,391    |
| `qrduino`         |    4,716,908    |    4,188,669    |     4,179,483    |
| `sglib-combined`  |    4,879,830    |    4,193,283    |     3,972,935    |
| `slre`            |    5,308,250    |    5,062,867    |     4,066,804    |
| `st`              |    9,731,206    |    9,637,284    |     6,931,864    |
| `statemate`       |    2,469,797    |    2,439,683    |     1,761,132    |
| `ud`              |    7,740,408    |    7,533,358    |     6,596,353    |
| `wikisort`        |    2,908,728    |    2,791,519    |     2,422,078    |

**NOTE:** Speed is measured in cycles.
The results correspond to the number of cycles with parametrized tests at 1 MHz (to accelerate simulation time).
The results must be converted to fit the real frequency of the cores.
