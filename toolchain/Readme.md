# Installing the riscv32-inria-elf toolchain

## From binaries (preferred)

Requires to download around 500MB.

```
nix-env -i -f riscv32-inria-elf.nix
```

### From sources

Requires to download around 5GB.

```
./build_patch_toolchain.sh
```

