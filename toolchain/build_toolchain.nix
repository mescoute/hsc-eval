with import <nixpkgs> {};

let
     pkgs = import (builtins.fetchGit {
         # Descriptive name to make the store path easier to identify                
         name = "my-old-revision";                                                 
         url = "https://github.com/nixos/nixpkgs-channels/";                       
         ref = "refs/heads/nixpkgs-unstable";                     
         rev = "c83e13315caadef275a5d074243c67503c558b3b";                                           
     }) {};  

in

stdenv.mkDerivation {
    hardeningDisable = [ "format" ];
    name = "riscv32-inria-elf";
    RISCV = toString ./riscv32-inria-elf;

    nativeBuildInputs = [
        pkgs.git
        pkgs.gcc
        pkgs.curl
        pkgs.perl
        pkgs.wget
        pkgs.bison
        pkgs.flex
        pkgs.texinfo
        pkgs.expat
        pkgs.automake
        pkgs.autoconf
        pkgs.python3
        pkgs.libmpc
        pkgs.gawk
        pkgs.libtool
        pkgs.patchutils
        pkgs.gperf
        pkgs.bc
        pkgs.gmp
        pkgs.mpfr
        pkgs.isl
        pkgs.openssl
        pkgs.cacert
    ];

    shellHook = ''
        export PATH=$PATH:$RISCV/bin
    '';
}