#!/usr/bin/env nix-shell
#!nix-shell build_toolchain.nix --pure -i bash

# 7f1f4ab5b0e04f267a197c2e99449de775891a4d

git clone https://gitlab.inria.fr/mescoute/riscv-toolchain-custom.git
git clone --recursive https://github.com/riscv/riscv-gnu-toolchain.git

cd riscv-gnu-toolchain
git checkout 7f1f4ab5b0e04f267a197c2e99449de775891a4d
cd ..

cp riscv-toolchain-custom/src/binutils/riscv-opc.h riscv-gnu-toolchain/riscv-binutils/include/opcode/riscv-opc.h
cp riscv-toolchain-custom/src/binutils/riscv-opc.c riscv-gnu-toolchain/riscv-binutils/opcodes/riscv-opc.c
cp riscv-toolchain-custom/src/Makefile.in riscv-gnu-toolchain/Makefile.in

cd riscv-gnu-toolchain

make clean
./configure --prefix=$RISCV --with-arch=rv32im --with-abi=ilp32 
make -j4

make clean
