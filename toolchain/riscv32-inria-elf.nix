with (import <nixpkgs> {});

stdenv.mkDerivation {
    name = "riscv32-inria-elf";
    version = "0.1";

    src = fetchurl {
        url = "https://gitlab.inria.fr/rlasherm/hsc_patched_toolchain/-/raw/master/riscv32-inria-elf.tar.bz2?inline=false";
        sha256 =  "63ad8ade0a41375c9e0fbb24aef84ba7c2720db3fdfe610b8f8f5df00841f9c3";
    };

    nativeBuildInputs = [autoPatchelfHook ];

    phases = [ "unpackPhase" "installPhase" ];

    installPhase = ''
        mkdir -p $out
        cp -r * $out
    '';
  
}