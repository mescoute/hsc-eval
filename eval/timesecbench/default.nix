/*
 * File: default.nix
 * Project: timesecbench
 * Created Date: Thursday November 5th 2020
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 5th November 2020 1:58:19 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2020 INRIA
 */
with import <nixpkgs> {};

let
  riscv32-inria-elf = (import ../../toolchain/riscv32-inria-elf.nix);


  pkgs_python = import (builtins.fetchGit {
         # Descriptive name to make the store path easier to identify                
         name = "my-old-revision";                                                 
         url = "https://github.com/nixos/nixpkgs-channels/";                       
         ref = "refs/heads/nixos-20.03";                     
         rev = "1975b8687474764c157e6a220fdcad2c5dc348a1";                                           
     }) {}; 

  pkgs = import (builtins.fetchGit {
         # Descriptive name to make the store path easier to identify                
         name = "my-old-revision";                                                 
         url = "https://github.com/nixos/nixpkgs-channels/";                       
         ref = "refs/heads/nixpkgs-unstable";                     
         rev = "c83e13315caadef275a5d074243c67503c558b3b";                                           
     }) {};  
in 

 # Make a new "derivation" that represents our shell
stdenv.mkDerivation {
  name = "apps_builder";

  # The packages in the `buildInputs` list will be added to the PATH in our shell
  buildInputs = with pkgs; [
    pkgs_python.python3
    pkgs_python.pythonPackages.click
    pkgs_python.pythonPackages.pyelftools
    bash
    riscv32-inria-elf
    
    git
    gcc
    curl
    perl
    wget
    bison
    flex
    texinfo
    expat
    automake
    autoconf
    libmpc
    gawk
    libtool
    patchutils
    gperf
    bc
    gmp
    mpfr
    isl
    openssl
    cacert
  ];

}