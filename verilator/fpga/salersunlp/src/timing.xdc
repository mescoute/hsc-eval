#create_clock -add -name clock -period 7.0 -waveform {0 3.5} [get_ports { clock }];
#create_clock -add -name clock -period 6.9 -waveform {0 3.45} [get_ports { clock }];
#create_clock -add -name clock -period 7.0 -waveform {0 3.5} [get_ports { clock }];
#create_clock -add -name clock -period 7.2 -waveform {0 3.6} [get_ports { clock }];
#create_clock -add -name clock -period 7.5 -waveform {0 3.75} [get_ports { clock }];

#create_clock -add -name clock -period 7.56 -waveform {0 3.78} [get_ports { clock }];

#create_clock -add -name clock -period 7.6 -waveform {0 3.8} [get_ports { clock }];
#create_clock -add -name clock -period 7.7 -waveform {0 3.85} [get_ports { clock }];

create_clock -add -name clock -period 7.5 -waveform {0 3.75} [get_ports { clock }];
