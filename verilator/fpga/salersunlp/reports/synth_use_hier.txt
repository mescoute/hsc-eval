Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
| Date         : Tue Oct  6 20:08:40 2020
| Host         : tyranocif running 64-bit Ubuntu 20.04.1 LTS
| Command      : report_utilization -hierarchical -file salersunlp/reports/synth_use_hier.txt
| Design       : Salers
| Device       : xczu7evffvc1156-2
| Design State : Synthesized
----------------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Utilization by Hierarchy

1. Utilization by Hierarchy
---------------------------

+---------------------------+--------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
|          Instance         |       Module       | Total LUTs | Logic LUTs | LUTRAMs | SRLs |  FFs | RAMB36 | RAMB18 | URAM | DSP48 Blocks |
+---------------------------+--------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
| Salers                    |              (top) |      26607 |      22511 |    4096 |    0 | 9996 |      0 |      0 |    0 |            0 |
|   (Salers)                |              (top) |          0 |          0 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   l1dcache                |          Manseng_1 |       5538 |       4514 |    1024 |    0 | 1794 |      0 |      0 |    0 |            0 |
|     cache                 |            Cache_1 |       4661 |       3637 |    1024 |    0 |  980 |      0 |      0 |    0 |            0 |
|       subcache_0          |         SubCache_1 |       4661 |       3637 |    1024 |    0 |  980 |      0 |      0 |    0 |            0 |
|         (subcache_0)      |         SubCache_1 |          0 |          0 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|         set_0             |              Set_4 |       1149 |        893 |     256 |    0 |  236 |      0 |      0 |    0 |            0 |
|           ctrl            |       CtrlSet_4_56 |        162 |        162 |       0 |    0 |  108 |      0 |      0 |    0 |            0 |
|             (ctrl)        |       CtrlSet_4_56 |         42 |         42 |       0 |    0 |  104 |      0 |      0 |    0 |            0 |
|             BitPlruPolicy | BitPlruPolicy_2_58 |        120 |        120 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|           data            |         DataSet_57 |        987 |        731 |     256 |    0 |  128 |      0 |      0 |    0 |            0 |
|         set_1             |           Set_4_45 |       1145 |        889 |     256 |    0 |  236 |      0 |      0 |    0 |            0 |
|           ctrl            |       CtrlSet_4_53 |        162 |        162 |       0 |    0 |  108 |      0 |      0 |    0 |            0 |
|             (ctrl)        |       CtrlSet_4_53 |         42 |         42 |       0 |    0 |  104 |      0 |      0 |    0 |            0 |
|             BitPlruPolicy | BitPlruPolicy_2_55 |        120 |        120 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|           data            |         DataSet_54 |        983 |        727 |     256 |    0 |  128 |      0 |      0 |    0 |            0 |
|         set_2             |           Set_4_46 |       1220 |        964 |     256 |    0 |  268 |      0 |      0 |    0 |            0 |
|           ctrl            |       CtrlSet_4_50 |        172 |        172 |       0 |    0 |  108 |      0 |      0 |    0 |            0 |
|             (ctrl)        |       CtrlSet_4_50 |         49 |         49 |       0 |    0 |  104 |      0 |      0 |    0 |            0 |
|             BitPlruPolicy | BitPlruPolicy_2_52 |        123 |        123 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|           data            |         DataSet_51 |       1048 |        792 |     256 |    0 |  160 |      0 |      0 |    0 |            0 |
|         set_3             |           Set_4_47 |       1147 |        891 |     256 |    0 |  236 |      0 |      0 |    0 |            0 |
|           ctrl            |          CtrlSet_4 |        162 |        162 |       0 |    0 |  108 |      0 |      0 |    0 |            0 |
|             (ctrl)        |          CtrlSet_4 |         42 |         42 |       0 |    0 |  104 |      0 |      0 |    0 |            0 |
|             BitPlruPolicy | BitPlruPolicy_2_49 |        120 |        120 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|           data            |         DataSet_48 |        985 |        729 |     256 |    0 |  128 |      0 |      0 |    0 |            0 |
|     next                  |         NextCtrl_1 |        415 |        415 |       0 |    0 |  514 |      0 |      0 |    0 |            0 |
|       init                |        InitStage_5 |         17 |         17 |       0 |    0 |   78 |      0 |      0 |    0 |            0 |
|       mem                 |         MemStage_4 |         41 |         41 |       0 |    0 |   38 |      0 |      0 |    0 |            0 |
|         Fifo              |            Fifo_13 |          0 |          0 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|         Fifo_1            |         Fifo_13_43 |          2 |          2 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|         reg_ack_0         |          Fifo_6_44 |          2 |          2 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|         reg_bus_0         |            Fifo_19 |         37 |         37 |       0 |    0 |   32 |      0 |      0 |    0 |            0 |
|       req                 |         ReqStage_5 |         81 |         81 |       0 |    0 |  140 |      0 |      0 |    0 |            0 |
|         reg_bus_0         |            Fifo_18 |         81 |         81 |       0 |    0 |  140 |      0 |      0 |    0 |            0 |
|       write               |       WriteStage_5 |        276 |        276 |       0 |    0 |  258 |      0 |      0 |    0 |            0 |
|         reg_data_0        |         Fifo_15_42 |        276 |        276 |       0 |    0 |  258 |      0 |      0 |    0 |            0 |
|     prev                  |         PrevCtrl_1 |        462 |        462 |       0 |    0 |  300 |      0 |      0 |    0 |            0 |
|       pipe_0              |     PrevPipeline_2 |        170 |        170 |       0 |    0 |  150 |      0 |      0 |    0 |            0 |
|         init              |     InitStage_3_37 |          1 |          1 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|         read              |     ReadStage_2_38 |         45 |         45 |       0 |    0 |   46 |      0 |      0 |    0 |            0 |
|         req               |      ReqStage_3_39 |         92 |         92 |       0 |    0 |   69 |      0 |      0 |    0 |            0 |
|           reg_bus_0       |         Fifo_16_41 |         92 |         92 |       0 |    0 |   69 |      0 |      0 |    0 |            0 |
|         write             |    WriteStage_3_40 |         32 |         32 |       0 |    0 |   34 |      0 |      0 |    0 |            0 |
|       pipe_1              |  PrevPipeline_2_36 |        292 |        292 |       0 |    0 |  150 |      0 |      0 |    0 |            0 |
|         init              |        InitStage_3 |          4 |          4 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|         read              |        ReadStage_2 |        201 |        201 |       0 |    0 |   46 |      0 |      0 |    0 |            0 |
|         req               |         ReqStage_3 |         55 |         55 |       0 |    0 |   69 |      0 |      0 |    0 |            0 |
|           reg_bus_0       |            Fifo_16 |         55 |         55 |       0 |    0 |   69 |      0 |      0 |    0 |            0 |
|         write             |       WriteStage_3 |         32 |         32 |       0 |    0 |   34 |      0 |      0 |    0 |            0 |
|   l1icache                |            Manseng |       3261 |       2237 |    1024 |    0 | 1547 |      0 |      0 |    0 |            0 |
|     cache                 |              Cache |       1968 |        944 |    1024 |    0 |  952 |      0 |      0 |    0 |            0 |
|       subcache_0          |           SubCache |       1968 |        944 |    1024 |    0 |  952 |      0 |      0 |    0 |            0 |
|         (subcache_0)      |           SubCache |          0 |          0 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|         set_0             |                Set |        492 |        236 |     256 |    0 |  236 |      0 |      0 |    0 |            0 |
|           ctrl            |         CtrlSet_33 |        108 |        108 |       0 |    0 |  108 |      0 |      0 |    0 |            0 |
|             (ctrl)        |         CtrlSet_33 |         44 |         44 |       0 |    0 |  104 |      0 |      0 |    0 |            0 |
|             BitPlruPolicy | BitPlruPolicy_2_35 |         64 |         64 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|           data            |         DataSet_34 |        384 |        128 |     256 |    0 |  128 |      0 |      0 |    0 |            0 |
|         set_1             |             Set_24 |        580 |        324 |     256 |    0 |  240 |      0 |      0 |    0 |            0 |
|           ctrl            |         CtrlSet_30 |         68 |         68 |       0 |    0 |  108 |      0 |      0 |    0 |            0 |
|             (ctrl)        |         CtrlSet_30 |         38 |         38 |       0 |    0 |  104 |      0 |      0 |    0 |            0 |
|             BitPlruPolicy | BitPlruPolicy_2_32 |         30 |         30 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|           data            |         DataSet_31 |        512 |        256 |     256 |    0 |  132 |      0 |      0 |    0 |            0 |
|         set_2             |             Set_25 |        448 |        192 |     256 |    0 |  236 |      0 |      0 |    0 |            0 |
|           ctrl            |         CtrlSet_27 |         64 |         64 |       0 |    0 |  108 |      0 |      0 |    0 |            0 |
|             (ctrl)        |         CtrlSet_27 |         34 |         34 |       0 |    0 |  104 |      0 |      0 |    0 |            0 |
|             BitPlruPolicy | BitPlruPolicy_2_29 |         30 |         30 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|           data            |         DataSet_28 |        384 |        128 |     256 |    0 |  128 |      0 |      0 |    0 |            0 |
|         set_3             |             Set_26 |        448 |        192 |     256 |    0 |  236 |      0 |      0 |    0 |            0 |
|           ctrl            |            CtrlSet |         64 |         64 |       0 |    0 |  108 |      0 |      0 |    0 |            0 |
|             (ctrl)        |            CtrlSet |         34 |         34 |       0 |    0 |  104 |      0 |      0 |    0 |            0 |
|             BitPlruPolicy |    BitPlruPolicy_2 |         30 |         30 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|           data            |            DataSet |        384 |        128 |     256 |    0 |  128 |      0 |      0 |    0 |            0 |
|     next                  |           NextCtrl |        862 |        862 |       0 |    0 |  394 |      0 |      0 |    0 |            0 |
|       init                |        InitStage_2 |         10 |         10 |       0 |    0 |   40 |      0 |      0 |    0 |            0 |
|       mem                 |         MemStage_3 |        591 |        591 |       0 |    0 |   32 |      0 |      0 |    0 |            0 |
|         reg_ack_0         |          Fifo_6_23 |          3 |          3 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|         reg_bus_0         |            Fifo_11 |        588 |        588 |       0 |    0 |   30 |      0 |      0 |    0 |            0 |
|       req                 |         ReqStage_2 |        189 |        189 |       0 |    0 |   64 |      0 |      0 |    0 |            0 |
|         reg_bus_0         |            Fifo_10 |        189 |        189 |       0 |    0 |   64 |      0 |      0 |    0 |            0 |
|       write               |       WriteStage_2 |         72 |         72 |       0 |    0 |  258 |      0 |      0 |    0 |            0 |
|         reg_data_0        |            Fifo_15 |         72 |         72 |       0 |    0 |  258 |      0 |      0 |    0 |            0 |
|     prev                  |           PrevCtrl |        431 |        431 |       0 |    0 |  201 |      0 |      0 |    0 |            0 |
|       pipe_0              |       PrevPipeline |        223 |        223 |       0 |    0 |  103 |      0 |      0 |    0 |            0 |
|         init              |       InitStage_18 |          0 |          0 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|         read              |       ReadStage_19 |          2 |          2 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|         req               |        ReqStage_20 |        156 |        156 |       0 |    0 |   31 |      0 |      0 |    0 |            0 |
|           (req)           |        ReqStage_20 |          0 |          0 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|           reg_bus_0       |          Fifo_8_22 |        156 |        156 |       0 |    0 |   30 |      0 |      0 |    0 |            0 |
|         write             |      WriteStage_21 |         65 |         65 |       0 |    0 |   65 |      0 |      0 |    0 |            0 |
|       pipe_1              |    PrevPipeline_16 |        208 |        208 |       0 |    0 |   98 |      0 |      0 |    0 |            0 |
|         init              |          InitStage |          1 |          1 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|         read              |          ReadStage |          3 |          3 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|         req               |           ReqStage |        139 |        139 |       0 |    0 |   31 |      0 |      0 |    0 |            0 |
|           reg_bus_0       |          Fifo_8_17 |        139 |        139 |       0 |    0 |   31 |      0 |      0 |    0 |            0 |
|         write             |         WriteStage |         65 |         65 |       0 |    0 |   65 |      0 |      0 |    0 |            0 |
|   pipe                    |           Pipeline |      17808 |      15760 |    2048 |    0 | 6655 |      0 |      0 |    0 |            0 |
|     back                  |               Back |      12625 |      12625 |       0 |    0 | 4381 |      0 |      0 |    0 |            0 |
|       (back)              |               Back |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|       bypass              |             Bypass |        107 |        107 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|       csr                 |                Csr |         38 |         38 |       0 |    0 |  457 |      0 |      0 |    0 |            0 |
|       ex                  |            ExStage |       3072 |       3072 |       0 |    0 |  516 |      0 |      0 |    0 |            0 |
|         (ex)              |            ExStage |       2028 |       2028 |       0 |    0 |  311 |      0 |      0 |    0 |            0 |
|         mamu_0            |               Mamu |          1 |          1 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|         mamu_1            |            Mamu_15 |          1 |          1 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|         muldiv_0          |             MulDiv |       1042 |       1042 |       0 |    0 |  201 |      0 |      0 |    0 |            0 |
|           (muldiv_0)      |             MulDiv |        840 |        840 |       0 |    0 |  201 |      0 |      0 |    0 |            0 |
|           divss           |        DivShiftSub |        202 |        202 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|       gpr                 |                Gpr |       5362 |       5362 |       0 |    0 | 1984 |      0 |      0 |    0 |            0 |
|       id                  |            IdStage |        213 |        213 |       0 |    0 |  288 |      0 |      0 |    0 |            0 |
|         unit_0            |             IdUnit |        146 |        146 |       0 |    0 |  144 |      0 |      0 |    0 |            0 |
|         unit_1            |          IdUnit_14 |         67 |         67 |       0 |    0 |  144 |      0 |      0 |    0 |            0 |
|       iss                 |           IssStage |       2921 |       2921 |       0 |    0 |  302 |      0 |      0 |    0 |            0 |
|       lsu                 |                Lsu |        562 |        562 |       0 |    0 |  648 |      0 |      0 |    0 |            0 |
|         (lsu)             |                Lsu |          0 |          0 |       0 |    0 |   66 |      0 |      0 |    0 |            0 |
|         fifo_bp_0         |             Fifo_4 |         14 |         14 |       0 |    0 |    8 |      0 |      0 |    0 |            0 |
|         fifo_bp_1         |          Fifo_4_11 |         15 |         15 |       0 |    0 |    8 |      0 |      0 |    0 |            0 |
|         fifo_dmem_0       |             Fifo_6 |         74 |         74 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|         fifo_dmem_1       |          Fifo_6_12 |          9 |          9 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|         fifo_req_0        |             Fifo_2 |        225 |        225 |       0 |    0 |  279 |      0 |      0 |    0 |            0 |
|         fifo_req_1        |          Fifo_2_13 |        225 |        225 |       0 |    0 |  279 |      0 |      0 |    0 |            0 |
|       mem                 |         MemStage_2 |        310 |        310 |       0 |    0 |  120 |      0 |      0 |    0 |            0 |
|         unit_0            |           MemStage |        116 |        116 |       0 |    0 |   60 |      0 |      0 |    0 |            0 |
|         unit_1            |        MemStage_10 |        194 |        194 |       0 |    0 |   60 |      0 |      0 |    0 |            0 |
|       wb                  |          WbStage_2 |         32 |         32 |       0 |    0 |   66 |      0 |      0 |    0 |            0 |
|         unit_0            |            WbStage |         16 |         16 |       0 |    0 |   33 |      0 |      0 |    0 |            0 |
|         unit_1            |          WbStage_9 |         16 |         16 |       0 |    0 |   33 |      0 |      0 |    0 |            0 |
|     front                 |            Front_2 |       5063 |       3015 |    2048 |    0 | 2274 |      0 |      0 |    0 |            0 |
|       unit_0              |              Front |       2462 |       1438 |    1024 |    0 | 1137 |      0 |      0 |    0 |            0 |
|         (unit_0)          |              Front |        171 |        171 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         if0               |         If0Stage_1 |          9 |          9 |       0 |    0 |   32 |      0 |      0 |    0 |            0 |
|         if2               |         If2Stage_2 |        527 |        527 |       0 |    0 |  252 |      0 |      0 |    0 |            0 |
|           (if2)           |         If2Stage_2 |          0 |          0 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|           Fifo            |             Fifo_8 |        527 |        527 |       0 |    0 |  251 |      0 |      0 |    0 |            0 |
|         pc                |          PcStage_3 |       1755 |        731 |    1024 |    0 |  853 |      0 |      0 |    0 |            0 |
|           (pc)            |          PcStage_3 |          7 |          7 |       0 |    0 |   61 |      0 |      0 |    0 |            0 |
|           nlp             |              Nlp_4 |       1748 |        724 |    1024 |    0 |  792 |      0 |      0 |    0 |            0 |
|             bht           |              Bht_5 |       1133 |        109 |    1024 |    0 |    8 |      0 |      0 |    0 |            0 |
|             btb           |           HTable_6 |        615 |        615 |       0 |    0 |  784 |      0 |      0 |    0 |            0 |
|               (btb)       |           HTable_6 |        411 |        411 |       0 |    0 |  768 |      0 |      0 |    0 |            0 |
|               plru        |    BitPlruPolicy_7 |        204 |        204 |       0 |    0 |   16 |      0 |      0 |    0 |            0 |
|       unit_1              |            Front_0 |       2601 |       1577 |    1024 |    0 | 1137 |      0 |      0 |    0 |            0 |
|         (unit_1)          |            Front_0 |        171 |        171 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         if0               |           If0Stage |          5 |          5 |       0 |    0 |   32 |      0 |      0 |    0 |            0 |
|         if2               |           If2Stage |        673 |        673 |       0 |    0 |  252 |      0 |      0 |    0 |            0 |
|           (if2)           |           If2Stage |          0 |          0 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|           Fifo            |               Fifo |        673 |        673 |       0 |    0 |  251 |      0 |      0 |    0 |            0 |
|         pc                |            PcStage |       1752 |        728 |    1024 |    0 |  853 |      0 |      0 |    0 |            0 |
|           (pc)            |            PcStage |          4 |          4 |       0 |    0 |   61 |      0 |      0 |    0 |            0 |
|           nlp             |                Nlp |       1748 |        724 |    1024 |    0 |  792 |      0 |      0 |    0 |            0 |
|             bht           |                Bht |       1133 |        109 |    1024 |    0 |    8 |      0 |      0 |    0 |            0 |
|             btb           |             HTable |        615 |        615 |       0 |    0 |  784 |      0 |      0 |    0 |            0 |
|               (btb)       |             HTable |        411 |        411 |       0 |    0 |  768 |      0 |      0 |    0 |            0 |
|               plru        |      BitPlruPolicy |        204 |        204 |       0 |    0 |   16 |      0 |      0 |    0 |            0 |
|     scheduler             |          Scheduler |        120 |        120 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
+---------------------------+--------------------+------------+------------+---------+------+------+--------+--------+------+--------------+


