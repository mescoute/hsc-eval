Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
| Date         : Tue Oct  6 21:45:48 2020
| Host         : tyranocif running 64-bit Ubuntu 20.04.1 LTS
| Command      : report_utilization -hierarchical -file salersp/reports/synth_use_hier.txt
| Design       : Salers
| Device       : xczu7evffvc1156-2
| Design State : Synthesized
-------------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Utilization by Hierarchy

1. Utilization by Hierarchy
---------------------------

+---------------------------+-------------------+------------+------------+---------+------+-------+--------+--------+------+--------------+
|          Instance         |       Module      | Total LUTs | Logic LUTs | LUTRAMs | SRLs |  FFs  | RAMB36 | RAMB18 | URAM | DSP48 Blocks |
+---------------------------+-------------------+------------+------------+---------+------+-------+--------+--------+------+--------------+
| Salers                    |             (top) |      22975 |      20927 |    2048 |    0 | 10889 |      0 |      0 |    0 |            0 |
|   (Salers)                |             (top) |          0 |          0 |       0 |    0 |     0 |      0 |      0 |    0 |            0 |
|   l1dcache                |         Manseng_1 |       5890 |       4866 |    1024 |    0 |  2603 |      0 |      0 |    0 |            0 |
|     (l1dcache)            |         Manseng_1 |          2 |          2 |       0 |    0 |     0 |      0 |      0 |    0 |            0 |
|     cache                 |           Cache_1 |       3911 |       2887 |    1024 |    0 |   978 |      0 |      0 |    0 |            0 |
|       subcache_0          |        SubCache_1 |       3911 |       2887 |    1024 |    0 |   978 |      0 |      0 |    0 |            0 |
|         (subcache_0)      |        SubCache_1 |          0 |          0 |       0 |    0 |     6 |      0 |      0 |    0 |            0 |
|         StableSlct_3      |   StableSlct_4_57 |          0 |          0 |       0 |    0 |     1 |      0 |      0 |    0 |            0 |
|         StableSlct_5      |   StableSlct_4_58 |          0 |          0 |       0 |    0 |     1 |      0 |      0 |    0 |            0 |
|         set_0             |             Set_4 |        961 |        705 |     256 |    0 |   236 |      0 |      0 |    0 |            0 |
|           ctrl            |      CtrlSet_4_70 |         59 |         59 |       0 |    0 |   108 |      0 |      0 |    0 |            0 |
|             (ctrl)        |      CtrlSet_4_70 |         18 |         18 |       0 |    0 |   104 |      0 |      0 |    0 |            0 |
|             BitPlruPolicy |  BitPlruPolicy_72 |         41 |         41 |       0 |    0 |     4 |      0 |      0 |    0 |            0 |
|           data            |        DataSet_71 |        902 |        646 |     256 |    0 |   128 |      0 |      0 |    0 |            0 |
|         set_1             |          Set_4_59 |        961 |        705 |     256 |    0 |   236 |      0 |      0 |    0 |            0 |
|           ctrl            |      CtrlSet_4_67 |         59 |         59 |       0 |    0 |   108 |      0 |      0 |    0 |            0 |
|             (ctrl)        |      CtrlSet_4_67 |         18 |         18 |       0 |    0 |   104 |      0 |      0 |    0 |            0 |
|             BitPlruPolicy |  BitPlruPolicy_69 |         41 |         41 |       0 |    0 |     4 |      0 |      0 |    0 |            0 |
|           data            |        DataSet_68 |        902 |        646 |     256 |    0 |   128 |      0 |      0 |    0 |            0 |
|         set_2             |          Set_4_60 |       1028 |        772 |     256 |    0 |   262 |      0 |      0 |    0 |            0 |
|           ctrl            |      CtrlSet_4_64 |         62 |         62 |       0 |    0 |   108 |      0 |      0 |    0 |            0 |
|             (ctrl)        |      CtrlSet_4_64 |         20 |         20 |       0 |    0 |   104 |      0 |      0 |    0 |            0 |
|             BitPlruPolicy |  BitPlruPolicy_66 |         42 |         42 |       0 |    0 |     4 |      0 |      0 |    0 |            0 |
|           data            |        DataSet_65 |        966 |        710 |     256 |    0 |   154 |      0 |      0 |    0 |            0 |
|         set_3             |          Set_4_61 |        961 |        705 |     256 |    0 |   236 |      0 |      0 |    0 |            0 |
|           ctrl            |         CtrlSet_4 |         59 |         59 |       0 |    0 |   108 |      0 |      0 |    0 |            0 |
|             (ctrl)        |         CtrlSet_4 |         18 |         18 |       0 |    0 |   104 |      0 |      0 |    0 |            0 |
|             BitPlruPolicy |  BitPlruPolicy_63 |         41 |         41 |       0 |    0 |     4 |      0 |      0 |    0 |            0 |
|           data            |        DataSet_62 |        902 |        646 |     256 |    0 |   128 |      0 |      0 |    0 |            0 |
|     next                  |        NextCtrl_1 |       1058 |       1058 |       0 |    0 |  1321 |      0 |      0 |    0 |            0 |
|       init                |       InitStage_5 |         71 |         71 |       0 |    0 |   163 |      0 |      0 |    0 |            0 |
|       mem                 |        MemStage_4 |         93 |         93 |       0 |    0 |    73 |      0 |      0 |    0 |            0 |
|         (mem)             |        MemStage_4 |          0 |          0 |       0 |    0 |     1 |      0 |      0 |    0 |            0 |
|         Fifo              |           Fifo_16 |          2 |          2 |       0 |    0 |     2 |      0 |      0 |    0 |            0 |
|         Fifo_1            |        Fifo_16_51 |          1 |          1 |       0 |    0 |     2 |      0 |      0 |    0 |            0 |
|         Fifo_2            |        Fifo_16_52 |          2 |          2 |       0 |    0 |     2 |      0 |      0 |    0 |            0 |
|         Fifo_3            |        Fifo_16_53 |          1 |          1 |       0 |    0 |     2 |      0 |      0 |    0 |            0 |
|         reg_ack_0         |         Fifo_6_54 |          4 |          4 |       0 |    0 |     2 |      0 |      0 |    0 |            0 |
|         reg_ack_1         |         Fifo_6_55 |          4 |          4 |       0 |    0 |     2 |      0 |      0 |    0 |            0 |
|         reg_bus_0         |           Fifo_27 |         14 |         14 |       0 |    0 |    30 |      0 |      0 |    0 |            0 |
|         reg_bus_1         |        Fifo_27_56 |         65 |         65 |       0 |    0 |    30 |      0 |      0 |    0 |            0 |
|       req                 |        ReqStage_6 |        335 |        335 |       0 |    0 |   281 |      0 |      0 |    0 |            0 |
|         reg_bus_0         |           Fifo_25 |         81 |         81 |       0 |    0 |   140 |      0 |      0 |    0 |            0 |
|         reg_bus_1         |        Fifo_25_49 |        252 |        252 |       0 |    0 |   140 |      0 |      0 |    0 |            0 |
|         slct              |     StableSlct_50 |          2 |          2 |       0 |    0 |     1 |      0 |      0 |    0 |            0 |
|       write               |      WriteStage_5 |        559 |        559 |       0 |    0 |   804 |      0 |      0 |    0 |            0 |
|         reg_data_0        |        Fifo_20_46 |        203 |        203 |       0 |    0 |   258 |      0 |      0 |    0 |            0 |
|         reg_data_1        |        Fifo_20_47 |         71 |         71 |       0 |    0 |   258 |      0 |      0 |    0 |            0 |
|         reg_write         |        Fifo_22_48 |        285 |        285 |       0 |    0 |   288 |      0 |      0 |    0 |            0 |
|     prev                  |        PrevCtrl_1 |        919 |        919 |       0 |    0 |   304 |      0 |      0 |    0 |            0 |
|       pipe_0              |    PrevPipeline_2 |        342 |        342 |       0 |    0 |   152 |      0 |      0 |    0 |            0 |
|         init              |    InitStage_3_41 |          0 |          0 |       0 |    0 |     1 |      0 |      0 |    0 |            0 |
|         read              |    ReadStage_2_42 |         42 |         42 |       0 |    0 |    47 |      0 |      0 |    0 |            0 |
|         req               |     ReqStage_4_43 |        201 |        201 |       0 |    0 |    70 |      0 |      0 |    0 |            0 |
|           reg_bus_0       |        Fifo_23_45 |        201 |        201 |       0 |    0 |    70 |      0 |      0 |    0 |            0 |
|         write             |   WriteStage_3_44 |         99 |         99 |       0 |    0 |    34 |      0 |      0 |    0 |            0 |
|       pipe_1              | PrevPipeline_2_40 |        577 |        577 |       0 |    0 |   152 |      0 |      0 |    0 |            0 |
|         init              |       InitStage_3 |          0 |          0 |       0 |    0 |     1 |      0 |      0 |    0 |            0 |
|         read              |       ReadStage_2 |        223 |        223 |       0 |    0 |    47 |      0 |      0 |    0 |            0 |
|         req               |        ReqStage_4 |        336 |        336 |       0 |    0 |    70 |      0 |      0 |    0 |            0 |
|           reg_bus_0       |           Fifo_23 |        336 |        336 |       0 |    0 |    70 |      0 |      0 |    0 |            0 |
|         write             |      WriteStage_3 |         18 |         18 |       0 |    0 |    34 |      0 |      0 |    0 |            0 |
|   l1icache                |           Manseng |       3794 |       2770 |    1024 |    0 |  2209 |      0 |      0 |    0 |            0 |
|     cache                 |             Cache |       1867 |        843 |    1024 |    0 |   956 |      0 |      0 |    0 |            0 |
|       subcache_0          |          SubCache |       1867 |        843 |    1024 |    0 |   956 |      0 |      0 |    0 |            0 |
|         (subcache_0)      |          SubCache |          0 |          0 |       0 |    0 |     6 |      0 |      0 |    0 |            0 |
|         StableSlct        |      StableSlct_4 |          0 |          0 |       0 |    0 |     1 |      0 |      0 |    0 |            0 |
|         StableSlct_1      |   StableSlct_4_27 |          0 |          0 |       0 |    0 |     1 |      0 |      0 |    0 |            0 |
|         set_0             |               Set |        441 |        185 |     256 |    0 |   236 |      0 |      0 |    0 |            0 |
|           ctrl            |        CtrlSet_37 |        121 |        121 |       0 |    0 |   108 |      0 |      0 |    0 |            0 |
|             (ctrl)        |        CtrlSet_37 |         42 |         42 |       0 |    0 |   104 |      0 |      0 |    0 |            0 |
|             BitPlruPolicy |  BitPlruPolicy_39 |         79 |         79 |       0 |    0 |     4 |      0 |      0 |    0 |            0 |
|           data            |        DataSet_38 |        320 |         64 |     256 |    0 |   128 |      0 |      0 |    0 |            0 |
|         set_1             |            Set_28 |        436 |        180 |     256 |    0 |   236 |      0 |      0 |    0 |            0 |
|           ctrl            |        CtrlSet_34 |        105 |        105 |       0 |    0 |   108 |      0 |      0 |    0 |            0 |
|             (ctrl)        |        CtrlSet_34 |         46 |         46 |       0 |    0 |   104 |      0 |      0 |    0 |            0 |
|             BitPlruPolicy |  BitPlruPolicy_36 |         59 |         59 |       0 |    0 |     4 |      0 |      0 |    0 |            0 |
|           data            |        DataSet_35 |        331 |         75 |     256 |    0 |   128 |      0 |      0 |    0 |            0 |
|         set_2             |            Set_29 |        560 |        304 |     256 |    0 |   240 |      0 |      0 |    0 |            0 |
|           ctrl            |        CtrlSet_31 |        101 |        101 |       0 |    0 |   108 |      0 |      0 |    0 |            0 |
|             (ctrl)        |        CtrlSet_31 |         43 |         43 |       0 |    0 |   104 |      0 |      0 |    0 |            0 |
|             BitPlruPolicy |  BitPlruPolicy_33 |         58 |         58 |       0 |    0 |     4 |      0 |      0 |    0 |            0 |
|           data            |        DataSet_32 |        459 |        203 |     256 |    0 |   132 |      0 |      0 |    0 |            0 |
|         set_3             |            Set_30 |        430 |        174 |     256 |    0 |   236 |      0 |      0 |    0 |            0 |
|           ctrl            |           CtrlSet |         99 |         99 |       0 |    0 |   108 |      0 |      0 |    0 |            0 |
|             (ctrl)        |           CtrlSet |         43 |         43 |       0 |    0 |   104 |      0 |      0 |    0 |            0 |
|             BitPlruPolicy |     BitPlruPolicy |         56 |         56 |       0 |    0 |     4 |      0 |      0 |    0 |            0 |
|           data            |           DataSet |        331 |         75 |     256 |    0 |   128 |      0 |      0 |    0 |            0 |
|     next                  |          NextCtrl |       1518 |       1518 |       0 |    0 |  1048 |      0 |      0 |    0 |            0 |
|       init                |       InitStage_2 |         46 |         46 |       0 |    0 |    79 |      0 |      0 |    0 |            0 |
|       mem                 |        MemStage_3 |        808 |        808 |       0 |    0 |    45 |      0 |      0 |    0 |            0 |
|         (mem)             |        MemStage_3 |          1 |          1 |       0 |    0 |     1 |      0 |      0 |    0 |            0 |
|         reg_ack_0         |         Fifo_6_24 |          3 |          3 |       0 |    0 |     2 |      0 |      0 |    0 |            0 |
|         reg_ack_1         |         Fifo_6_25 |          3 |          3 |       0 |    0 |     2 |      0 |      0 |    0 |            0 |
|         reg_bus_0         |           Fifo_12 |        777 |        777 |       0 |    0 |    20 |      0 |      0 |    0 |            0 |
|         reg_bus_1         |        Fifo_12_26 |         24 |         24 |       0 |    0 |    20 |      0 |      0 |    0 |            0 |
|       req                 |        ReqStage_3 |        134 |        134 |       0 |    0 |   129 |      0 |      0 |    0 |            0 |
|         reg_bus_0         |           Fifo_10 |         42 |         42 |       0 |    0 |    64 |      0 |      0 |    0 |            0 |
|         reg_bus_1         |        Fifo_10_22 |         77 |         77 |       0 |    0 |    64 |      0 |      0 |    0 |            0 |
|         slct              |     StableSlct_23 |         15 |         15 |       0 |    0 |     1 |      0 |      0 |    0 |            0 |
|       write               |      WriteStage_2 |        530 |        530 |       0 |    0 |   795 |      0 |      0 |    0 |            0 |
|         reg_data_0        |           Fifo_20 |        262 |        262 |       0 |    0 |   258 |      0 |      0 |    0 |            0 |
|         reg_data_1        |        Fifo_20_21 |         70 |         70 |       0 |    0 |   258 |      0 |      0 |    0 |            0 |
|         reg_write         |           Fifo_22 |        198 |        198 |       0 |    0 |   279 |      0 |      0 |    0 |            0 |
|     prev                  |          PrevCtrl |        409 |        409 |       0 |    0 |   205 |      0 |      0 |    0 |            0 |
|       pipe_0              |      PrevPipeline |        222 |        222 |       0 |    0 |   105 |      0 |      0 |    0 |            0 |
|         init              |      InitStage_16 |          0 |          0 |       0 |    0 |     1 |      0 |      0 |    0 |            0 |
|         read              |      ReadStage_17 |          1 |          1 |       0 |    0 |     7 |      0 |      0 |    0 |            0 |
|         req               |     ReqStage_1_18 |        188 |        188 |       0 |    0 |    32 |      0 |      0 |    0 |            0 |
|           (req)           |     ReqStage_1_18 |          0 |          0 |       0 |    0 |     1 |      0 |      0 |    0 |            0 |
|           reg_bus_0       |         Fifo_8_20 |        188 |        188 |       0 |    0 |    31 |      0 |      0 |    0 |            0 |
|         write             |     WriteStage_19 |         33 |         33 |       0 |    0 |    65 |      0 |      0 |    0 |            0 |
|       pipe_1              |   PrevPipeline_15 |        187 |        187 |       0 |    0 |   100 |      0 |      0 |    0 |            0 |
|         init              |         InitStage |          0 |          0 |       0 |    0 |     1 |      0 |      0 |    0 |            0 |
|         read              |         ReadStage |          1 |          1 |       0 |    0 |     2 |      0 |      0 |    0 |            0 |
|         req               |        ReqStage_1 |        152 |        152 |       0 |    0 |    32 |      0 |      0 |    0 |            0 |
|           (req)           |        ReqStage_1 |          0 |          0 |       0 |    0 |     1 |      0 |      0 |    0 |            0 |
|           reg_bus_0       |            Fifo_8 |        152 |        152 |       0 |    0 |    31 |      0 |      0 |    0 |            0 |
|         write             |        WriteStage |         34 |         34 |       0 |    0 |    65 |      0 |      0 |    0 |            0 |
|   pipe                    |          Pipeline |      13291 |      13291 |       0 |    0 |  6077 |      0 |      0 |    0 |            0 |
|     back                  |              Back |      11555 |      11555 |       0 |    0 |  4652 |      0 |      0 |    0 |            0 |
|       (back)              |              Back |         22 |         22 |       0 |    0 |     0 |      0 |      0 |    0 |            0 |
|       bypass              |            Bypass |        153 |        153 |       0 |    0 |     0 |      0 |      0 |    0 |            0 |
|       csr                 |               Csr |        103 |        103 |       0 |    0 |   834 |      0 |      0 |    0 |            0 |
|       ex                  |           ExStage |       2180 |       2180 |       0 |    0 |   394 |      0 |      0 |    0 |            0 |
|         (ex)              |           ExStage |        846 |        846 |       0 |    0 |   189 |      0 |      0 |    0 |            0 |
|         mamu_0            |              Mamu |         21 |         21 |       0 |    0 |     2 |      0 |      0 |    0 |            0 |
|         mamu_1            |           Mamu_14 |          0 |          0 |       0 |    0 |     2 |      0 |      0 |    0 |            0 |
|         muldiv_0          |            MulDiv |       1313 |       1313 |       0 |    0 |   201 |      0 |      0 |    0 |            0 |
|           (muldiv_0)      |            MulDiv |       1041 |       1041 |       0 |    0 |   201 |      0 |      0 |    0 |            0 |
|           divss           |       DivShiftSub |        272 |        272 |       0 |    0 |     0 |      0 |      0 |    0 |            0 |
|       gpr                 |               Gpr |       5362 |       5362 |       0 |    0 |  1984 |      0 |      0 |    0 |            0 |
|       id                  |           IdStage |        234 |        234 |       0 |    0 |   294 |      0 |      0 |    0 |            0 |
|         unit_0            |            IdUnit |        146 |        146 |       0 |    0 |   147 |      0 |      0 |    0 |            0 |
|         unit_1            |         IdUnit_13 |         88 |         88 |       0 |    0 |   147 |      0 |      0 |    0 |            0 |
|       iss                 |          IssStage |       2492 |       2492 |       0 |    0 |   308 |      0 |      0 |    0 |            0 |
|       lsu                 |               Lsu |        592 |        592 |       0 |    0 |   648 |      0 |      0 |    0 |            0 |
|         (lsu)             |               Lsu |          0 |          0 |       0 |    0 |    66 |      0 |      0 |    0 |            0 |
|         fifo_bp_0         |            Fifo_4 |         48 |         48 |       0 |    0 |     8 |      0 |      0 |    0 |            0 |
|         fifo_bp_1         |         Fifo_4_10 |         16 |         16 |       0 |    0 |     8 |      0 |      0 |    0 |            0 |
|         fifo_dmem_0       |            Fifo_6 |          3 |          3 |       0 |    0 |     4 |      0 |      0 |    0 |            0 |
|         fifo_dmem_1       |         Fifo_6_11 |         70 |         70 |       0 |    0 |     4 |      0 |      0 |    0 |            0 |
|         fifo_req_0        |            Fifo_2 |        227 |        227 |       0 |    0 |   279 |      0 |      0 |    0 |            0 |
|         fifo_req_1        |         Fifo_2_12 |        228 |        228 |       0 |    0 |   279 |      0 |      0 |    0 |            0 |
|       mem                 |        MemStage_2 |        417 |        417 |       0 |    0 |   124 |      0 |      0 |    0 |            0 |
|         unit_0            |          MemStage |        217 |        217 |       0 |    0 |    62 |      0 |      0 |    0 |            0 |
|         unit_1            |        MemStage_9 |        200 |        200 |       0 |    0 |    62 |      0 |      0 |    0 |            0 |
|       wb                  |         WbStage_2 |          0 |          0 |       0 |    0 |    66 |      0 |      0 |    0 |            0 |
|         unit_0            |           WbStage |          0 |          0 |       0 |    0 |    33 |      0 |      0 |    0 |            0 |
|         unit_1            |         WbStage_8 |          0 |          0 |       0 |    0 |    33 |      0 |      0 |    0 |            0 |
|     front                 |           Front_2 |       1018 |       1018 |       0 |    0 |   688 |      0 |      0 |    0 |            0 |
|       unit_0              |             Front |        396 |        396 |       0 |    0 |   344 |      0 |      0 |    0 |            0 |
|         if0               |        If0Stage_4 |         52 |         52 |       0 |    0 |    32 |      0 |      0 |    0 |            0 |
|         if2               |        If2Stage_5 |        342 |        342 |       0 |    0 |   252 |      0 |      0 |    0 |            0 |
|           (if2)           |        If2Stage_5 |          0 |          0 |       0 |    0 |     1 |      0 |      0 |    0 |            0 |
|           Fifo            |            Fifo_7 |        342 |        342 |       0 |    0 |   251 |      0 |      0 |    0 |            0 |
|         pc                |         PcStage_6 |          2 |          2 |       0 |    0 |    60 |      0 |      0 |    0 |            0 |
|       unit_1              |           Front_3 |        622 |        622 |       0 |    0 |   344 |      0 |      0 |    0 |            0 |
|         if0               |          If0Stage |          6 |          6 |       0 |    0 |    32 |      0 |      0 |    0 |            0 |
|         if2               |          If2Stage |        614 |        614 |       0 |    0 |   252 |      0 |      0 |    0 |            0 |
|           (if2)           |          If2Stage |          0 |          0 |       0 |    0 |     1 |      0 |      0 |    0 |            0 |
|           Fifo            |              Fifo |        614 |        614 |       0 |    0 |   251 |      0 |      0 |    0 |            0 |
|         pc                |           PcStage |          2 |          2 |       0 |    0 |    60 |      0 |      0 |    0 |            0 |
|     rmu                   |               Rmu |        718 |        718 |       0 |    0 |   737 |      0 |      0 |    0 |            0 |
|       backport            |         Exclusive |         25 |         25 |       0 |    0 |    12 |      0 |      0 |    0 |            0 |
|       commitstage         |       CommitStage |         29 |         29 |       0 |    0 |    14 |      0 |      0 |    0 |            0 |
|         unit_0            |        CommitUnit |         10 |         10 |       0 |    0 |     6 |      0 |      0 |    0 |            0 |
|           (unit_0)        |        CommitUnit |          2 |          2 |       0 |    0 |     2 |      0 |      0 |    0 |            0 |
|           flush_counter   |         Counter_1 |          8 |          8 |       0 |    0 |     4 |      0 |      0 |    0 |            0 |
|         unit_1            |      CommitUnit_0 |         19 |         19 |       0 |    0 |     8 |      0 |      0 |    0 |            0 |
|           (unit_1)        |      CommitUnit_0 |         11 |         11 |       0 |    0 |     4 |      0 |      0 |    0 |            0 |
|           flush_counter   |           Counter |          8 |          8 |       0 |    0 |     4 |      0 |      0 |    0 |            0 |
|       hart                |              Hart |        246 |        246 |       0 |    0 |    14 |      0 |      0 |    0 |            0 |
|       hartstage           |         HartStage |         61 |         61 |       0 |    0 |   209 |      0 |      0 |    0 |            0 |
|       hsc                 |               Hsc |         55 |         55 |       0 |    0 |    76 |      0 |      0 |    0 |            0 |
|       muldiv              |         Inclusive |         14 |         14 |       0 |    0 |     8 |      0 |      0 |    0 |            0 |
|       reqstage            |          ReqStage |         75 |         75 |       0 |    0 |   199 |      0 |      0 |    0 |            0 |
|         (reqstage)        |          ReqStage |          6 |          6 |       0 |    0 |   198 |      0 |      0 |    0 |            0 |
|         slct              |        StableSlct |         69 |         69 |       0 |    0 |     1 |      0 |      0 |    0 |            0 |
|       unitstage           |         UnitStage |        213 |        213 |       0 |    0 |   205 |      0 |      0 |    0 |            0 |
+---------------------------+-------------------+------------+------------+---------+------+-------+--------+--------+------+--------------+


