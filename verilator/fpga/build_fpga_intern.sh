#!/usr/bin/env nix-shell
#!nix-shell ../../chisel.nix -i bash

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
ROOT_PATH="$(realpath "$SCRIPTPATH/../..")"

VIVADO=$1

TEST_VIVADO=$($VIVADO -help)
if [[ "$TEST_VIVADO" == *"command not found"* ]]; then
    echo "Xilinx Vivado 2019.2 must be installed !"
    exit
else
    echo "Xilinx Vivado 2019.2 found."
fi

function build_aubrac_verilog {
    cd $ROOT_PATH
    sbt "test:runMain eval.aubrac.AubracSynth${1^^} --target-dir verilator/fpga/aubrac$1/src --top-name AubracTop"
}

function build_salers_verilog {
    cd $ROOT_PATH
    sbt "test:runMain eval.salers.SalersSynth${1^^} --target-dir verilator/fpga/salers$1/src --top-name SalersTop"
}

function impl_aubrac {
    cd $SCRIPTPATH
    $VIVADO -source aubrac$1/build.tcl -mode tcl
}

function impl_salers {
    cd $SCRIPTPATH
    $VIVADO -source salers$1/build.tcl -mode tcl
}

function build_impl_aubrac {

    echo "************************************************************************************************"
    echo "** Building and implementing ${1^^}"
    echo "************************************************************************************************"

    build_aubrac_verilog $1
    impl_aubrac $1
}

function build_impl_salers {

    echo "************************************************************************************************"
    echo "** Building and implementing ${1^^}"
    echo "************************************************************************************************"

    build_salers_verilog $1
    impl_salers $1
}

build_impl_aubrac "u"
build_impl_aubrac "unlp"
build_impl_aubrac "up"
build_impl_aubrac "upnlp"
build_impl_aubrac "p"
build_impl_aubrac "pnlp"
build_impl_aubrac "pp"
build_impl_aubrac "ppnlp"


build_impl_salers "u"
build_impl_salers "unlp"
build_impl_salers "up"
build_impl_salers "upnlp"
build_impl_salers "p"
build_impl_salers "pnlp"
build_impl_salers "pp"
build_impl_salers "ppnlp"
