Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
| Date              : Tue Oct  6 23:06:05 2020
| Host              : tyranocif running 64-bit Ubuntu 20.04.1 LTS
| Command           : report_timing_summary -file salerspp/reports/impl_timing.txt
| Design            : Salers
| Device            : xczu7ev-ffvc1156
| Speed File        : -2  PRODUCTION 1.26 08-13-2019
| Temperature Grade : E
-----------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  No
  Borrow Time for Max Delay Exceptions       :  Yes
  Merge Timing Exceptions                    :  Yes

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There are 265 input ports with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There are 2609 ports with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WNS(ns)      TNS(ns)  TNS Failing Endpoints  TNS Total Endpoints      WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------      -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
     -0.275      -39.115                    434               118320        0.042        0.000                      0               118320        3.168        0.000                       0                 32882  


Timing constraints are not met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock  Waveform(ns)         Period(ns)      Frequency(MHz)
-----  ------------         ----------      --------------
clock  {0.000 3.700}        7.400           135.135         


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WNS(ns)      TNS(ns)  TNS Failing Endpoints  TNS Total Endpoints      WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------      -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clock              -0.275      -39.115                    434               118320        0.042        0.000                      0               118320        3.168        0.000                       0                 32882  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WNS(ns)      TNS(ns)  TNS Failing Endpoints  TNS Total Endpoints      WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------      -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WNS(ns)      TNS(ns)  TNS Failing Endpoints  TNS Total Endpoints      WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------      -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clock
  To Clock:  clock

Setup :          434  Failing Endpoints,  Worst Slack       -0.275ns,  Total Violation      -39.115ns
Hold  :            0  Failing Endpoints,  Worst Slack        0.042ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        3.168ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Max Delay Paths
--------------------------------------------------------------------------------------
Slack (VIOLATED) :        -0.275ns  (required time - arrival time)
  Source:                 pipe/back/ex/reg_br_new_0_valid_reg/C
                            (rising edge-triggered cell FDRE clocked by clock  {rise@0.000ns fall@3.700ns period=7.400ns})
  Destination:            pipe/front/unit_0/if2/Fifo/reg_fifo_1_instr_reg[8]/CE
                            (rising edge-triggered cell FDRE clocked by clock  {rise@0.000ns fall@3.700ns period=7.400ns})
  Path Group:             clock
  Path Type:              Setup (Max at Slow Process Corner)
  Requirement:            7.400ns  (clock rise@7.400ns - clock rise@0.000ns)
  Data Path Delay:        7.570ns  (logic 1.756ns (23.197%)  route 5.814ns (76.803%))
  Logic Levels:           26  (CARRY8=3 LUT4=3 LUT6=20)
  Clock Path Skew:        -0.009ns (DCD - SCD + CPR)
    Destination Clock Delay (DCD):    0.021ns = ( 7.421 - 7.400 ) 
    Source Clock Delay      (SCD):    0.030ns
    Clock Pessimism Removal (CPR):    0.000ns
  Clock Uncertainty:      0.035ns  ((TSJ^2 + TIJ^2)^1/2 + DJ) / 2 + PE
    Total System Jitter     (TSJ):    0.071ns
    Total Input Jitter      (TIJ):    0.000ns
    Discrete Jitter          (DJ):    0.000ns
    Phase Error              (PE):    0.000ns

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clock rise edge)      0.000     0.000 r  
                                                      0.000     0.000 r  clock (IN)
                         net (fo=32881, unset)        0.030     0.030    pipe/back/ex/clock
    SLICE_X79Y171        FDRE                                         r  pipe/back/ex/reg_br_new_0_valid_reg/C
  -------------------------------------------------------------------    -------------------
    SLICE_X79Y171        FDRE (Prop_AFF2_SLICEM_C_Q)
                                                      0.080     0.110 r  pipe/back/ex/reg_br_new_0_valid_reg/Q
                         net (fo=16, routed)          0.117     0.227    pipe/back/ex/muldiv_0/divss/reg_bus_1_data_res_reg[0]
    SLICE_X79Y170        LUT4 (Prop_B6LUT_SLICEM_I0_O)
                                                      0.125     0.352 r  pipe/back/ex/muldiv_0/divss/reg_bus_1_data_res[31]_i_3/O
                         net (fo=21, routed)          0.117     0.469    pipe/back/iss/reg_bus_1_rel_reg
    SLICE_X79Y168        LUT4 (Prop_C6LUT_SLICEM_I1_O)
                                                      0.051     0.520 r  pipe/back/iss/reg_br_new_0_pc[31]_i_7/O
                         net (fo=98, routed)          0.375     0.895    pipe/back/iss/reg_br_new_0_pc[31]_i_7_n_0
    SLICE_X78Y172        LUT6 (Prop_G6LUT_SLICEM_I1_O)
                                                      0.037     0.932 r  pipe/back/iss/reg_br_new_1_pc[31]_i_235/O
                         net (fo=1, routed)           0.153     1.085    pipe/back/iss/reg_br_new_1_pc[31]_i_235_n_0
    SLICE_X78Y171        LUT6 (Prop_C6LUT_SLICEM_I0_O)
                                                      0.051     1.136 r  pipe/back/iss/reg_br_new_1_pc[31]_i_198/O
                         net (fo=1, routed)           0.136     1.272    pipe/back/iss/reg_br_new_1_pc[31]_i_198_n_0
    SLICE_X78Y168        LUT6 (Prop_C6LUT_SLICEM_I5_O)
                                                      0.037     1.309 f  pipe/back/iss/reg_br_new_1_pc[31]_i_111/O
                         net (fo=1, routed)           0.489     1.798    pipe/back/iss/reg_br_new_1_pc[31]_i_111_n_0
    SLICE_X78Y165        CARRY8 (Prop_CARRY8_SLICEM_DI[6]_CO[7])
                                                      0.067     1.865 f  pipe/back/iss/reg_br_new_1_pc_reg[31]_i_61/CO[7]
                         net (fo=1, routed)           0.298     2.163    pipe/back/iss/ex/bru_1/_T_25
    SLICE_X75Y162        LUT6 (Prop_A6LUT_SLICEL_I2_O)
                                                      0.035     2.198 r  pipe/back/iss/reg_br_new_1_pc[31]_i_29/O
                         net (fo=1, routed)           0.161     2.359    pipe/back/iss/reg_br_new_1_pc[31]_i_29_n_0
    SLICE_X72Y162        LUT6 (Prop_C6LUT_SLICEL_I0_O)
                                                      0.035     2.394 r  pipe/back/iss/reg_br_new_1_pc[31]_i_12/O
                         net (fo=1, routed)           0.041     2.435    pipe/back/iss/reg_br_new_1_pc[31]_i_12_n_0
    SLICE_X72Y162        LUT6 (Prop_D6LUT_SLICEL_I2_O)
                                                      0.036     2.471 r  pipe/back/iss/reg_br_new_1_pc[31]_i_5_comp/O
                         net (fo=80, routed)          0.235     2.706    pipe/back/iss/reg_br_new_1_pc[31]_i_5_n_0
    SLICE_X71Y165        LUT6 (Prop_H6LUT_SLICEM_I4_O)
                                                      0.053     2.759 f  pipe/back/iss/reg_br_new_1_valid_i_121/O
                         net (fo=1, routed)           0.110     2.869    pipe/back/iss/reg_br_new_1_valid_i_121_n_0
    SLICE_X71Y164        LUT6 (Prop_B6LUT_SLICEM_I0_O)
                                                      0.149     3.018 r  pipe/back/iss/reg_br_new_1_valid_i_53/O
                         net (fo=1, routed)           0.016     3.034    pipe/back/iss/reg_br_new_1_valid_i_53_n_0
    SLICE_X71Y164        CARRY8 (Prop_CARRY8_SLICEM_S[1]_CO[7])
                                                      0.190     3.224 f  pipe/back/iss/reg_br_new_1_valid_reg_i_24/CO[7]
                         net (fo=1, routed)           0.026     3.250    pipe/back/iss/reg_br_new_1_valid_reg_i_24_n_0
    SLICE_X71Y165        CARRY8 (Prop_CARRY8_SLICEM_CI_CO[2])
                                                      0.057     3.307 f  pipe/back/iss/reg_br_new_1_valid_reg_i_18/CO[2]
                         net (fo=1, routed)           0.132     3.439    pipe/back/iss/ex/bru_1/_T_39
    SLICE_X71Y168        LUT6 (Prop_B6LUT_SLICEM_I5_O)
                                                      0.037     3.476 r  pipe/back/iss/reg_br_new_1_valid_i_12/O
                         net (fo=2, routed)           0.100     3.576    pipe/back/iss/reg_br_new_1_valid_i_12_n_0
    SLICE_X71Y170        LUT6 (Prop_C6LUT_SLICEM_I4_O)
                                                      0.037     3.613 f  pipe/back/iss/reg_valid_0_i_36/O
                         net (fo=1, routed)           0.218     3.831    pipe/back/iss/reg_valid_0_i_36_n_0
    SLICE_X70Y170        LUT6 (Prop_G6LUT_SLICEM_I1_O)
                                                      0.037     3.868 f  pipe/back/iss/reg_valid_0_i_35/O
                         net (fo=1, routed)           0.092     3.960    pipe/back/iss/reg_valid_0_i_35_n_0
    SLICE_X69Y170        LUT6 (Prop_G6LUT_SLICEL_I1_O)
                                                      0.050     4.010 r  pipe/back/iss/reg_valid_0_i_28/O
                         net (fo=4, routed)           0.142     4.152    pipe/back/bypass/ex_io_o_bypass_1_en
    SLICE_X69Y171        LUT6 (Prop_A6LUT_SLICEL_I4_O)
                                                      0.050     4.202 f  pipe/back/bypass/i_/reg_valid_0_i_16/O
                         net (fo=2, routed)           0.183     4.385    pipe/back/iss/reg_done_1_i_12_6
    SLICE_X68Y170        LUT6 (Prop_A6LUT_SLICEL_I5_O)
                                                      0.035     4.420 f  pipe/back/iss/reg_done_1_i_29/O
                         net (fo=1, routed)           0.087     4.507    pipe/back/iss/reg_done_1_i_29_n_0
    SLICE_X67Y170        LUT4 (Prop_A6LUT_SLICEM_I2_O)
                                                      0.038     4.545 r  pipe/back/iss/reg_done_1_i_12_comp_1/O
                         net (fo=1, routed)           0.089     4.634    pipe/back/id/unit_1/reg_bus_info_hart_i_3_0
    SLICE_X67Y169        LUT6 (Prop_A6LUT_SLICEM_I3_O)
                                                      0.090     4.724 f  pipe/back/id/unit_1/reg_done_1_i_5_comp/O
                         net (fo=5, routed)           0.105     4.829    pipe/back/id/unit_0/reg_bus_data_s2_reg[11]_1
    SLICE_X67Y167        LUT6 (Prop_A6LUT_SLICEM_I2_O)
                                                      0.090     4.919 r  pipe/back/id/unit_0/reg_bus_info_hart_i_3_comp/O
                         net (fo=158, routed)         0.220     5.139    pipe/rmu/hart/unit_0_io_o_lock
    SLICE_X66Y164        LUT6 (Prop_H6LUT_SLICEL_I3_O)
                                                      0.051     5.190 r  pipe/rmu/hart/reg_pt[2]_i_13/O
                         net (fo=72, routed)          0.588     5.778    pipe/rmu/hart/reg_state_0_valid_reg_18
    SLICE_X65Y160        LUT6 (Prop_D6LUT_SLICEL_I2_O)
                                                      0.089     5.867 r  pipe/rmu/hart/reg_fifo_1_pc[31]_i_7_comp/O
                         net (fo=132, routed)         0.246     6.113    pipe/front/unit_0/if0/reg_fifo_3_instr_reg[0]_2
    SLICE_X65Y157        LUT6 (Prop_E6LUT_SLICEL_I3_O)
                                                      0.099     6.212 f  pipe/front/unit_0/if0/reg_pt[2]_i_7/O
                         net (fo=109, routed)         0.621     6.833    pipe/front/unit_0/if2/Fifo/reg_fifo_1_instr_reg[0]_0
    SLICE_X66Y156        LUT6 (Prop_C6LUT_SLICEL_I2_O)
                                                      0.050     6.883 r  pipe/front/unit_0/if2/Fifo/reg_fifo_1_pc[31]_i_1/O
                         net (fo=62, routed)          0.717     7.600    pipe/front/unit_0/if2/Fifo/reg_fifo_1_pc[31]_i_1_n_0
    SLICE_X63Y153        FDRE                                         r  pipe/front/unit_0/if2/Fifo/reg_fifo_1_instr_reg[8]/CE
  -------------------------------------------------------------------    -------------------

                         (clock clock rise edge)      7.400     7.400 r  
                                                      0.000     7.400 r  clock (IN)
                         net (fo=32881, unset)        0.021     7.421    pipe/front/unit_0/if2/Fifo/clock
    SLICE_X63Y153        FDRE                                         r  pipe/front/unit_0/if2/Fifo/reg_fifo_1_instr_reg[8]/C
                         clock pessimism              0.000     7.421    
                         clock uncertainty           -0.035     7.386    
    SLICE_X63Y153        FDRE (Setup_DFF_SLICEM_C_CE)
                                                     -0.061     7.325    pipe/front/unit_0/if2/Fifo/reg_fifo_1_instr_reg[8]
  -------------------------------------------------------------------
                         required time                          7.325    
                         arrival time                          -7.600    
  -------------------------------------------------------------------
                         slack                                 -0.275    





Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.042ns  (arrival time - required time)
  Source:                 l1dcache/next/init/reg_rep_offset_0_reg[5]/C
                            (rising edge-triggered cell FDRE clocked by clock  {rise@0.000ns fall@3.700ns period=7.400ns})
  Destination:            l1dcache/next/init/reg_rep_offset_0_reg[5]/D
                            (rising edge-triggered cell FDRE clocked by clock  {rise@0.000ns fall@3.700ns period=7.400ns})
  Path Group:             clock
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clock rise@0.000ns - clock rise@0.000ns)
  Data Path Delay:        0.094ns  (logic 0.053ns (56.383%)  route 0.041ns (43.617%))
  Logic Levels:           1  (LUT5=1)
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    0.018ns
    Source Clock Delay      (SCD):    0.012ns
    Clock Pessimism Removal (CPR):    -0.000ns

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clock rise edge)      0.000     0.000 r  
                                                      0.000     0.000 r  clock (IN)
                         net (fo=32881, unset)        0.012     0.012    l1dcache/next/init/clock
    SLICE_X72Y222        FDRE                                         r  l1dcache/next/init/reg_rep_offset_0_reg[5]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X72Y222        FDRE (Prop_DFF_SLICEL_C_Q)
                                                      0.039     0.051 r  l1dcache/next/init/reg_rep_offset_0_reg[5]/Q
                         net (fo=3, routed)           0.025     0.076    l1dcache/next/init/reg_rep_offset_0[5]
    SLICE_X72Y222        LUT5 (Prop_D6LUT_SLICEL_I4_O)
                                                      0.014     0.090 r  l1dcache/next/init/reg_rep_offset_0[5]_i_2__0/O
                         net (fo=1, routed)           0.016     0.106    l1dcache/next/init/reg_rep_offset_0[5]_i_2__0_n_0
    SLICE_X72Y222        FDRE                                         r  l1dcache/next/init/reg_rep_offset_0_reg[5]/D
  -------------------------------------------------------------------    -------------------

                         (clock clock rise edge)      0.000     0.000 r  
                                                      0.000     0.000 r  clock (IN)
                         net (fo=32881, unset)        0.018     0.018    l1dcache/next/init/clock
    SLICE_X72Y222        FDRE                                         r  l1dcache/next/init/reg_rep_offset_0_reg[5]/C
                         clock pessimism              0.000     0.018    
    SLICE_X72Y222        FDRE (Hold_DFF_SLICEL_C_D)
                                                      0.046     0.064    l1dcache/next/init/reg_rep_offset_0_reg[5]
  -------------------------------------------------------------------
                         required time                         -0.064    
                         arrival time                           0.106    
  -------------------------------------------------------------------
                         slack                                  0.042    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clock
Waveform(ns):       { 0.000 3.700 }
Period(ns):         7.400
Sources:            { clock }

Check Type        Corner  Lib Pin     Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location       Pin
Min Period        n/a     RAMD32/CLK  n/a            1.064         7.400       6.336      SLICE_X94Y274  l1dcache/cache/subcache_0/set_10/data/m_data_14_reg_r1_0_15_0_7/RAMC/CLK
Low Pulse Width   Slow    RAMD32/CLK  n/a            0.532         3.700       3.168      SLICE_X94Y274  l1dcache/cache/subcache_0/set_10/data/m_data_14_reg_r1_0_15_0_7/RAMC/CLK
High Pulse Width  Slow    RAMD32/CLK  n/a            0.532         3.700       3.168      SLICE_X94Y274  l1dcache/cache/subcache_0/set_10/data/m_data_14_reg_r1_0_15_0_7/RAMC/CLK



