## ##############################
##          VARIABLES
## ##############################
set design_name "aubracu"
set project_path "."
set top_name "Aubrac"
set fpga_name "xczu7ev-ffvc1156-2-e"
set board_name "xilinx.com:zcu104:part0:1.1"
set design_files [list "${design_name}/src/AubracTop.v"]
set xdc_file "${design_name}/src/timing.xdc"

## ##############################
##        CREATE PROJECT
## ##############################
create_project ${design_name} ${project_path}/${design_name} -part ${fpga_name} -force

set_property board_part ${board_name} [current_project]
set_property simulator_language Verilog [current_project]

import_files -fileset sources_1 -force -norecurse ${design_files}
import_files -fileset constrs_1 -force -norecurse ${xdc_file}
update_compile_order -fileset sources_1

file mkdir ${design_name}/reports

## ##############################
##           SYNTHESIS
## ##############################
set_property -name {STEPS.SYNTH_DESIGN.ARGS.MORE OPTIONS} -value {-mode out_of_context} -objects [get_runs synth_1]

launch_runs synth_1
wait_on_run synth_1

open_run synth_1 -name synth_1

report_timing_summary -file ${design_name}/reports/synth_timing.txt
report_utilization -file ${design_name}/reports/synth_use.txt
report_utilization -hierarchical -file ${design_name}/reports/synth_use_hier.txt

## ##############################
##         IMPLEMENTATION
## ##############################
set_property STEPS.POST_ROUTE_PHYS_OPT_DESIGN.IS_ENABLED true [get_runs impl_1]

launch_runs impl_1
wait_on_run impl_1

open_run impl_1 -name impl_1

report_timing_summary -file ${design_name}/reports/impl_timing.txt
report_utilization -file ${design_name}/reports/impl_use.txt
report_utilization -hierarchical -file ${design_name}/reports/impl_use_hier.txt

exit
