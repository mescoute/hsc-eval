/*
 * File: verilator4036 copy.nix
 * Project: verilator
 * Created Date: Tuesday September 29th 2020
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 29th September 2020 11:07:22 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2020 INRIA
 */
with import <nixpkgs> {};

let
     mypkgs = import (builtins.fetchGit {
         # Descriptive name to make the store path easier to identify                
         name = "my-old-revision";                                                 
         url = "https://github.com/nixos/nixpkgs-channels/";                       
         ref = "refs/heads/nixpkgs-unstable";                     
         rev = "daaa0e33505082716beb52efefe3064f0332b521";                                           
     }) {};                                                                           

     myverilator = mypkgs.verilator;

in

# Make a new "derivation" that represents our shell
stdenv.mkDerivation {
  name = "verilator";

  # The packages in the `buildInputs` list will be added to the PATH in our shell
  buildInputs = [
    myverilator
  ];
}