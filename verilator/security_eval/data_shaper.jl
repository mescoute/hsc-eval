# Our experiments gives all the outputs for the same input on one line
# contrary to the classic expectation that input is on the x-axis and output is on the y-axis

# This function will flip matrices to switch the axes

using CSV
using DataFrames

function switch_axes(path::String)
    m = convert(Array{Int64, 2}, CSV.File(path, header=false) |> DataFrame)
    CSV.write(path, DataFrame(m'), writeheader=false)
    println("$path axes switched.")
end


switch_axes("results/pbht_matrix.csv")
switch_axes("results/pbtb_matrix.csv")
switch_axes("results/pcl1d_matrix.csv")
switch_axes("results/pl1d_matrix.csv")
switch_axes("results/pl1i_matrix.csv")
switch_axes("results/ppc_matrix.csv")

switch_axes("results/ubht_matrix.csv")
switch_axes("results/ubtb_matrix.csv")
switch_axes("results/ucl1d_matrix.csv")
switch_axes("results/ul1d_matrix.csv")
switch_axes("results/ul1i_matrix.csv")
switch_axes("results/upc_matrix.csv")