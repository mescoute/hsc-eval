using CSV

function byte_probability_vector(vector::Array{Int64,1})::Array{Float64,1}
    map(i -> 
        count(x->x==i,
            vector),
        0:255
    ) ./ size(vector,1)
end


function shannon_entropy(vector::Array{Int64,1})::Float64

    p = byte_probability_vector(vector)

    -sum(i-> p[i] == 0 ? 0 : p[i]*log(2,p[i]), 1:256)

end

function shannon_entropy(vector::Array{Int64,2})::Float64
    d = Dict()
    (nrows, ncols) = size(vector)

    for i in 1:nrows
        current_count = get(d, vector[i,:], 0)
        d[vector[i,:]] = current_count + 1
    end

    p::Float64 = 0.0

    for (k, v) in d
        thisp = v/nrows
        p += thisp * log(2, thisp)
    end

    return -p
end

function tm2pm_threshold(tm::Array{Int64, 2}, threshold::Int64)
    pm::Array{Float64, 2} = zeros(size(tm))

    pm[tm.<=threshold] .= 1.0
    pm[tm.>threshold] .= 0.0

    #normalize by column

    (rows, cols) = size(pm)
    s = sum(pm)

    if s != 0.0
        pm /= s
    end

    return pm
end

function tm2pm_min(tm::Array{Int64, 2})
    pm::Array{Float64, 2} = zeros(size(tm))
    mins = minimum(tm, dims=2)

    (nrows, ncols) = size(tm)

    for c in 1:ncols
        for r in 1:nrows
            if tm[r,c] <= mins[r]
                pm[r,c] = 1.0
            else
                pm[r,c] = 0.0
            end

            # pm[tm.<=mins[c],c] .= 1.0
            # pm[tm.>mins[c],c] .= 0.0
        end
    end


    #normalize by column

    (rows, cols) = size(pm)
    s = sum(pm)

    if s != 0.0
        pm /= s
    end

    return pm
end

function pm2mi(pm::Array{Float64,2})
    (rows, cols) = size(pm)
    @assert rows == cols

    pr::Array{Float64, 1} = sum(pm, dims=2)[:]
    pc::Array{Float64, 1} = sum(pm, dims=1)[:]
    
    mi = 0.0

    for r in 1:rows
        for c in 1:cols
            if pm[r, c] == 0.0
                continue
            else
                mi += pm[r, c] * (log2(pm[r, c]) - log2(pc[c]) - log2(pr[r]))
            end
        end
    end

    return mi
end

function mi_eval(path::String)
    tm = convert(Array{Int64, 2}, CSV.read(path, header=false))
    pm = tm2pm_min(tm)
    mi = pm2mi(pm)

    max_mi = log2(size(pm,1))
    ratio = round(mi/max_mi, digits=2)
    mi = round(mi, digits=2)
    max_mi = round(max_mi, digits=2)

    println("*** $path ***")
    println("MI = $mi/$max_mi ($ratio)")
    println()
end

mi_eval("results/ul1d_matrix.csv")
mi_eval("results/pl1d_matrix.csv")

mi_eval("results/ul1i_matrix.csv")
mi_eval("results/pl1i_matrix.csv")

mi_eval("results/ubht_matrix.csv")
mi_eval("results/pbht_matrix.csv")

mi_eval("results/ubtb_matrix.csv")
mi_eval("results/pbtb_matrix.csv")

mi_eval("results/ucl1d_matrix.csv")
mi_eval("results/pcl1d_matrix.csv")

mi_eval("results/upc_matrix.csv")