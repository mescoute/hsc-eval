#!/usr/bin/env nix-shell
#!nix-shell --pure ../verilator4036.nix -i bash

##########################################################################
# This script build the security tests from the cores and the testbenches
##########################################################################


SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
APPS_PATH="$(realpath "$SCRIPTPATH/apps")"
TESTBENCHES_DIR="$(realpath "$SCRIPTPATH/testbenches")"

# In case of tracing
VCD_DIR="$SCRIPTPATH/vcd"
# mkdir -p $VCD_DIR

RESULT_DIR="$SCRIPTPATH/results"
mkdir -p $RESULT_DIR

FIGURE_DIR="$SCRIPTPATH/figures"
mkdir -p $FIGURE_DIR

HEX_DIR="$SCRIPTPATH/hex"
AUBRAC_BINARY_PATH="obj_dir/VAubracTop"
SALERS_BINARY_PATH="obj_dir/VSalersTop"

function build_aubrac_verilator {
    verilator -Wno-WIDTH -cc $1/AubracTop.v $1/initram.sv --exe --build $TESTBENCHES_DIR/aubrac_testbench.cpp
}

function build_salers_verilator {
    verilator -Wno-WIDTH -cc $1/SalersTop.v $1/initram.sv --exe --build $TESTBENCHES_DIR/salers_testbench.cpp
}

function build_salers_verilator_tracing {
    verilator -Wno-WIDTH -cc $1/SalersTop.v $1/initram.sv --exe --build $TESTBENCHES_DIR/salers_testbench_tracing.cpp --trace
}

function aubrac_eval {
    $APPS_PATH/$1/$AUBRAC_BINARY_PATH $HEX_DIR/$2 $3 | tee "$RESULT_DIR/$1_matrix.csv"
    $APPS_PATH/$1/$AUBRAC_BINARY_PATH $HEX_DIR/$2 $3 timing | tee "$RESULT_DIR/$1_timing.csv"
}

function salers_eval {
    $APPS_PATH/$1/$SALERS_BINARY_PATH $HEX_DIR/$2 $3 | tee "$RESULT_DIR/$1_matrix.csv"
    $APPS_PATH/$1/$SALERS_BINARY_PATH $HEX_DIR/$2 $3 timing | tee "$RESULT_DIR/$1_timing.csv"
}

function salers_eval_tracing {
    mkdir -p $VCD_DIR
    $APPS_PATH/$1/$SALERS_BINARY_PATH $HEX_DIR/$2 $3 $4
}

function build_aubrac_eval {
    echo "************************************************************************************************"
    echo "** Evaluating $1 with Aubrac"
    echo "************************************************************************************************"

    TEST_DIR="$APPS_PATH/$1"
    cd $TEST_DIR
    build_aubrac_verilator $TEST_DIR
    aubrac_eval $1 $2 $3
}

function build_salers_eval {
    echo "************************************************************************************************"
    echo "** Evaluating $1 with Salers"
    echo "************************************************************************************************"

    TEST_DIR="$APPS_PATH/$1"
    cd $TEST_DIR
    build_salers_verilator $TEST_DIR
    salers_eval $1 $2 $3
}

function build_salers_eval_tracing {
    echo "************************************************************************************************"
    echo "** Evaluating $1 with Salers, tracing enabled"
    echo "************************************************************************************************"

    TEST_DIR="$APPS_PATH/$1"
    cd $TEST_DIR
    build_salers_verilator_tracing $TEST_DIR
    salers_eval_tracing $1 $2 $3 "$VCD_DIR/$1.vcd"
}

function pair_build_aubrac_eval {
    build_aubrac_eval "u$1" "$1.hex" $2 &
    build_aubrac_eval "p$1" "$1.hex" $2 &
    wait
}

function pair_build_salers_eval {
    build_salers_eval "u$1" "$1.hex" $2 &
    build_salers_eval "p$1" "$1.hex" $2 &
    wait
}

if [ -z $1 ]; then
    # pair_build_aubrac_eval "l1d"  8
    # pair_build_aubrac_eval "l1i"  8
    # pair_build_aubrac_eval "bht"  32
    # pair_build_aubrac_eval "btb"  16

    # pair_build_salers_eval "cl1d" 8
    pair_build_salers_eval "pc" 2

    # julia data_shaper.jl
else
    if [ $1 = "aubrac" ]; then
        pair_build_aubrac_eval $2 $3
    elif [ $1 = "salers" ]; then
        pair_build_salers_eval $2 $3
    elif [ $1 = "debug" ]; then
        build_salers_eval_tracing "u$2" "$2.hex" $3
        build_salers_eval_tracing "p$2" "$2.hex" $3
    fi
fi