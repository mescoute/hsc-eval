/*
 * File: aubrac-testbench.cpp
 * Project: security_eval
 * Created Date: Wednesday September 2nd 2020
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 3rd September 2020 3:13:36 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2020 INRIA
 */
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "VAubracTop.h"
#include "verilated.h"
#include "verilated_vcd_c.h"
#include "svdpi.h"
#include "VAubracTop__Dpi.h"
#include <time.h>

#define PICOSECONDS_PER_HALFCYCLE 500
#define PICOSECONDS_PER_CYCLE     (PICOSECONDS_PER_HALFCYCLE << 1)
#define TIMEOUT 5000

inline void step(VAubracTop *dut, int *cycle, int nb_steps) {
  for (int i = 0; i < nb_steps; i++) {
    //RE
    dut->clock = 1;
    dut->eval();
    *cycle = *cycle + 1;

    //FE
    dut->clock = 0;
    dut->eval();
  }
}

void reset(VAubracTop *dut, int *cycle) {
  // **********
  //   RESET
  // **********
  dut->reset = 1;
  step(dut, cycle, 5);
  dut->reset = 0;
}

void trigger_on_x29(VAubracTop *dut, int *cycle, uint32_t trig_val) {
    bool end = false;
    int start_cycle = *cycle;

    while (!Verilated::gotFinish()) {
      if(*cycle - start_cycle > TIMEOUT) {
        printf("Timeout! ");
        break;
      }
      
      step(dut, cycle, 1);

      if (dut->io_o_dbg_gpr_29 == trig_val) {
        // printf("TRIGGER ! x29 = %d\n", trig_val);
        break;
      }
    }
}

void channel_matrix(VAubracTop *dut, int *cycle, int alphabet_size, bool print) {

  trigger_on_x29(dut, cycle, 0xFFFFFFFF);


  for(int r = 0; r < alphabet_size; r++) {
    for(int c = 0; c < alphabet_size; c++) {
      int trig_val = r*alphabet_size + c;
      trigger_on_x29(dut, cycle, trig_val);

      uint32_t x28 = dut->io_o_dbg_gpr_28;
      // printf("x28 = %d\n", x28);

      if (print) {
        printf("%d", x28);

        if(c < alphabet_size-1) {
          printf(", ");
        }
        else {
          printf("\n");
        }
      }
    }
  }
}

int main(int argc, char **argv) {

  if(argc != 3 && argc != 4) {
    printf("Usage: ./[EXE_FILE] [HEX FILE] [SQUARE MATRIX SIZE] (timing)");
    return EXIT_FAILURE;
  }

  // Inputs
  char* hexfile = argv[1];
  
  bool timing = argc == 4;

  int alphabet_size;
  sscanf(argv[2], "%d", &alphabet_size);



	// Initialize Verilators variables
	Verilated::commandArgs(argc, argv);

  // Create an instance of our module under test
	VAubracTop *dut = new VAubracTop;

  // Generate VCD
  Verilated::traceEverOn(true);


  // Call task to initialize memory
  svSetScope(svGetScopeFromName("TOP.AubracTop.ram.ram"));
  dut->new_readmemh(hexfile);

	// Test variables
  int cycle = 0;      // Cycle

  reset(dut, &cycle);


  channel_matrix(dut, &cycle, alphabet_size, !timing);

  // // End report
  // printf("\n");
  // printf("End report: \n");
  // printf("N cycles: %d \n", cycle);

  if(timing) {
    printf("%d\n", cycle);
  }

  exit(EXIT_SUCCESS);
}
