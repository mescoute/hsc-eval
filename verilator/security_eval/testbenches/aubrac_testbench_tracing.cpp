/*
 * File: aubrac-testbench.cpp
 * Project: security_eval
 * Created Date: Wednesday September 2nd 2020
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 3rd September 2020 11:58:31 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2020 INRIA
 */
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "VAubracTop.h"
#include "verilated.h"
#include "verilated_vcd_c.h"
#include "svdpi.h"
#include "VAubracTop__Dpi.h"
#include <time.h>

#define PICOSECONDS_PER_HALFCYCLE 500
#define PICOSECONDS_PER_CYCLE     (PICOSECONDS_PER_HALFCYCLE << 1)
#define TIMEOUT 5000

inline void step(VAubracTop *dut, VerilatedVcdC *m_trace, int *cycle, int nb_steps) {
  for (int i = 0; i < nb_steps; i++) {
    //RE
    dut->clock = 1;
    dut->eval();
    *cycle = *cycle + 1;
    m_trace->dump(*cycle * PICOSECONDS_PER_CYCLE);
    

    //FE
    dut->clock = 0;
    dut->eval();
    m_trace->dump(*cycle * PICOSECONDS_PER_CYCLE + PICOSECONDS_PER_HALFCYCLE);
  }
}

void reset(VAubracTop *dut, VerilatedVcdC *m_trace, int *cycle) {
  // **********
  //   RESET
  // **********
  dut->reset = 1;
  step(dut, m_trace, cycle, 5);
  dut->reset = 0;
}

void trigger_on_x29(VAubracTop *dut, VerilatedVcdC *m_trace, int *cycle, uint32_t trig_val) {
    bool end = false;
    int start_cycle = *cycle;

    while (!Verilated::gotFinish()) {
      if(*cycle - start_cycle > TIMEOUT) {
        printf("Timeout! ");
        break;
      }
      
      step(dut, m_trace, cycle, 1);

      if (dut->io_o_dbg_gpr_29 == trig_val) {
        // printf("TRIGGER ! x29 = %d\n", trig_val);
        break;
      }
    }
}

void channel_matrix(VAubracTop *dut, VerilatedVcdC *m_trace, int *cycle, int alphabet_size) {

  trigger_on_x29(dut, m_trace, cycle, 0xFFFFFFFF);


  for(int r = 0; r < alphabet_size; r++) {
    for(int c = 0; c < alphabet_size; c++) {
      int trig_val = r*alphabet_size + c;
      trigger_on_x29(dut, m_trace, cycle, trig_val);

      uint32_t x28 = dut->io_o_dbg_gpr_28;
      // printf("x28 = %d\n", x28);
      printf("%d", x28);

      if(c < alphabet_size-1) {
        printf(", ");
      }
      else {
        printf("\n");
      }
    }
  }
}

int main(int argc, char **argv) {

  if(argc != 4) {
    printf("Usage: ./[EXE_FILE] [HEX FILE] [SQUARE MATRIX SIZE] [VCD FILE]");
    return EXIT_FAILURE;
  }

  // Inputs
  char* hexfile = argv[1];
  

  int alphabet_size;
  sscanf(argv[2], "%d", &alphabet_size);

  char* vcdfile = NULL;
  vcdfile = argv[3];
  

  time_t my_time = time(NULL);

	// Initialize Verilators variables
	Verilated::commandArgs(argc, argv);

  // Create an instance of our module under test
	VAubracTop *dut = new VAubracTop;

  // Generate VCD
  Verilated::traceEverOn(true);
  VerilatedVcdC* m_trace = new VerilatedVcdC;
  
  dut->trace(m_trace, 99);

  m_trace->open((vcdfile));
  

  // Call task to initialize memory
  svSetScope(svGetScopeFromName("TOP.AubracTop.ram.ram"));
  dut->new_readmemh(hexfile);

	// Test variables
  int cycle = 0;      // Cycle

  reset(dut, m_trace, &cycle);

  channel_matrix(dut, m_trace, &cycle, alphabet_size);

  // // End report
  // printf("\n");
  // printf("End report: \n");
  // printf("N cycles: %d \n", cycle);

  m_trace->close();

  exit(EXIT_SUCCESS);
}
