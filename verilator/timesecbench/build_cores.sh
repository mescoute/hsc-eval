#!/usr/bin/env nix-shell
#!nix-shell --pure ../../chisel.nix -i bash

####################################################################
# This script build all the cores for the security evaluation
####################################################################

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
ROOT_PATH="$(realpath "$SCRIPTPATH/../..")"
APPS_PATH="$(realpath "$SCRIPTPATH/apps")"

function build_aubrac_cores {
 
    U_DIR="$APPS_PATH/u$1"
    P_DIR="$APPS_PATH/p$1"
    mkdir -p $U_DIR
    mkdir -p $P_DIR

    cd $ROOT_PATH

    echo "************************************************************************************************"
    echo "** Building U${1^^} in $U_DIR"
    echo "************************************************************************************************"

    sbt "test:runMain eval.aubrac.AubracU${1^^} --target-dir $U_DIR --top-name AubracTop"

    echo "************************************************************************************************"
    echo "** Building P${1^^} in $P_DIR"
    echo "************************************************************************************************"

    sbt "test:runMain eval.aubrac.AubracP${1^^} --target-dir $P_DIR --top-name AubracTop"
}

function build_salers_cores {

    U_DIR="$APPS_PATH/u$1"
    P_DIR="$APPS_PATH/p$1"
    mkdir -p $U_DIR
    mkdir -p $P_DIR

    cd $ROOT_PATH

    echo "************************************************************************************************"
    echo "** Building U${1^^} in $U_DIR"
    echo "************************************************************************************************"

    sbt "test:runMain eval.salers.SalersU${1^^} --target-dir $U_DIR --top-name SalersTop"

    echo "************************************************************************************************"
    echo "** Building P${1^^} in $P_DIR"
    echo "************************************************************************************************"

    sbt "test:runMain eval.salers.SalersP${1^^} --target-dir $P_DIR --top-name SalersTop"
}

if [ -z $1 ]; then
    build_aubrac_cores "l1d"
    build_aubrac_cores "l1i"
    build_aubrac_cores "bht"
    build_aubrac_cores "btb"

    # build_salers_cores "cl1d"
    # build_salers_cores "pc"
else
    if [ $1 = "aubrac" ]; then
        build_aubrac_cores $2
    elif [ $1 = "salers" ]; then
        build_salers_cores $2
    fi
fi