/*
 * File: aubrac-testbench.cpp
 * Project: security_eval
 * Created Date: Wednesday September 2nd 2020
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 1st March 2021 4:42:46 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2020 INRIA
 */
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "VAubracTop.h"
#include "verilated.h"
#include "verilated_vcd_c.h"
#include "svdpi.h"
#include "VAubracTop__Dpi.h"
#include <time.h>

#define PICOSECONDS_PER_HALFCYCLE 500
#define PICOSECONDS_PER_CYCLE     (PICOSECONDS_PER_HALFCYCLE << 1)
#define TIMEOUT 50000

inline void step(VAubracTop *dut, int *cycle, int nb_steps) {
  for (int i = 0; i < nb_steps; i++) {
    //RE
    dut->clock = 1;
    dut->eval();
    *cycle = *cycle + 1;

    //FE
    dut->clock = 0;
    dut->eval();
  }
}

void reset(VAubracTop *dut, int *cycle) {
  // **********
  //   RESET
  // **********
  dut->reset = 1;
  step(dut, cycle, 5);
  dut->reset = 0;
}


void wait_for_transmit(VAubracTop *dut, int *cycle, uint32_t* input_val, uint32_t* output_val, uint32_t* timing_val) {
  bool end = false;
  int start_cycle = *cycle;

  while (!Verilated::gotFinish()) {
    if(*cycle - start_cycle > TIMEOUT) {
      printf("Timeout! ");
      break;
    }
    
    step(dut, cycle, 1);

    if (dut->io_o_dbg_gpr_31 == 0x101010) { // trigger signal
      // printf("TRIGGER ! x29 = %d\n", trig_val);
      *input_val = dut->io_o_dbg_gpr_30;
      *output_val = dut->io_o_dbg_gpr_29;
      *timing_val = dut->io_o_dbg_gpr_28;

      //wait for trigger down
      while(dut->io_o_dbg_gpr_31 != 0) {
        step(dut, cycle, 1);
      }
      
      break;
    }
  }
}

void channel_matrix(VAubracTop *dut, int *cycle, int alphabet_size, bool print) {

  uint32_t input_val;
  uint32_t output_val;
  uint32_t timing_val;

  for(int r = 0; r < 1; r++) {
    for(int r = 0; r < alphabet_size; r++) {
      for(int c = 0; c < alphabet_size; c++) {
        
        wait_for_transmit(dut, cycle, &input_val, &output_val, &timing_val);

        if (print) {
          printf("%d", timing_val);
          fflush(stdout);

          if(c < alphabet_size-1) {
            printf(", ");
          }
          else {
            printf("\n");
          }
        }
      }
    }
  }

  wait_for_transmit(dut, cycle, &input_val, &output_val, &timing_val);
  if(input_val != 0xffffffff || output_val != 0xffffffff || timing_val != 0xffffffff) {
    printf("End of transmission ERROR. input=%x, output=%x, timing=%d\n", input_val, output_val, timing_val);
  }
}

int main(int argc, char **argv) {

  if(argc != 3 && argc != 4) {
    printf("Usage: ./[EXE_FILE] [HEX FILE] [SQUARE MATRIX SIZE] (timing)");
    return EXIT_FAILURE;
  }

  // Inputs
  char* hexfile = argv[1];
  
  bool timing = argc == 4;

  int alphabet_size;
  sscanf(argv[2], "%d", &alphabet_size);



	// Initialize Verilators variables
	Verilated::commandArgs(argc, argv);

  // Create an instance of our module under test
	VAubracTop *dut = new VAubracTop;

  // Generate VCD
  // Verilated::traceEverOn(true);


  // Call task to initialize memory
  svSetScope(svGetScopeFromName("TOP.AubracTop.ram.ram"));
  dut->new_readmemh(hexfile);

	// Test variables
  int cycle = 0;      // Cycle

  reset(dut, &cycle);


  channel_matrix(dut, &cycle, alphabet_size, !timing);

  // // End report
  // printf("\n");
  // printf("End report: \n");
  // printf("N cycles: %d \n", cycle);

  if(timing) {
    printf("%d\n", cycle);
  }

  exit(EXIT_SUCCESS);
}
