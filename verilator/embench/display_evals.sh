#!/usr/bin/env nix-shell
#!nix-shell --pure -p gnuplot -i bash

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
RESULT_DIR="$SCRIPTPATH/results"
FIGURE_DIR="$SCRIPTPATH/figures"

mkdir -p $FIGURE_DIR

function plot {
    gnuplot -c "$SCRIPTPATH/display_benchmarks.gnuplot" "$RESULT_DIR/$1.csv" "$FIGURE_DIR/$1.eps" $2 $3
}


plot "aubrac_benchmarks_normalized" 0.6 1.15
plot "salers_benchmarks_normalized" 0.3 1.8