set terminal postscript enhanced eps size 6,3 color font 'Helvetica,18' linewidth 2
set output ARG2
set datafile missing '?'
set datafile separator ','
set style fill solid 2.0 border lt -1
set style data histograms
set yrange [ARG3:ARG4]
set ylabel "Normalized timing"
set auto x
set xtic rotate by -45 scale 0
set key autotitle columnheader
set key vertical maxrows 3
set key width -3
set key at graph 0.94, 0.98 spacing 1
#set arrow from graph 0,first 1.0 to graph 1,first 1.0 nohead lt 3 lc rgb "#000000" front

#plot 'results/synthetic_normalized.csv' \

plot ARG1 \
    using 2:xtic(1) ti col, \
    '' u 4 ti col, \
    '' u 6 ti col, \
    '' u 8 ti col, \
    '' u 3 ti col, \
    '' u 5 ti col, \
    '' u 7 ti col, \
    '' u 9 ti col, \
    1.0 lt 1 lc "black" dashtype 2

