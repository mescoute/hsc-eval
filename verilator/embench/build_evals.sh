#!/usr/bin/env nix-shell
#!nix-shell --pure ../verilator4036.nix -i bash

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
APPS_PATH="$(realpath "$SCRIPTPATH/apps")"
TESTBENCHES_DIR="$(realpath "$SCRIPTPATH/testbenches")"

# In case of tracing
VCD_DIR="$SCRIPTPATH/vcd"
mkdir -p $VCD_DIR

RESULT_DIR="$SCRIPTPATH/results"
# rm -r $RESULT_DIR
mkdir -p $RESULT_DIR

FIGURE_DIR="$SCRIPTPATH/figures"
mkdir -p $FIGURE_DIR

HEX_DIR="$SCRIPTPATH/hex"
AUBRAC_BINARY_PATH="obj_dir/VAubracTop"
SALERS_BINARY_PATH="obj_dir/VSalersTop"

# aha-mont64 \ # does not verify TODO
# nbody \ too long without Double support
TESTS="crc32 \
			  cubic \
        edn \
			  huffbench \
			 	matmult-int \
			  minver \
			  nettle-aes \
			  nettle-sha256 \
			  nsichneu \
			  picojpeg \
        qrduino \
        sglib-combined \
        slre \
        st \
        statemate \
        ud \
        wikisort"


TESTS_SMT="nettle-sha256-smt \
          nsichneu-smt \
          slre-smt"

# TESTS="sglib-combined"
# TESTS=""
# TESTS_SMT="nettle-sha256-smt"

CFG=$1



# call with app dir as $1
function build_aubrac_embench {
  verilator -Wno-WIDTH -cc $1/AubracTop.v $1/initram.sv --exe --build ${TESTBENCHES_DIR}/embench.cpp
}

# call with app dir as $1
function build_salers_embench {
  verilator -Wno-WIDTH -cc $1/SalersTop.v $1/initram.sv --exe --build ${TESTBENCHES_DIR}/embench_salers.cpp
}

function build_aubrac_embench_tracing {
  verilator -Wno-WIDTH -cc $1/AubracTop.v $1/initram.sv --exe --build ${TESTBENCHES_DIR}/embench_tracing.cpp --trace
}

function build_salers_embench_tracing {
  verilator -Wno-WIDTH -cc $1/SalersTop.v $1/initram.sv --exe --build ${TESTBENCHES_DIR}/embench_salers_tracing.cpp --trace
}


# call with app NAME as $1 (u, p, pp)
function aubrac_eval {
  OUT_CSV="$RESULT_DIR/aubrac_$1_timings.csv"
  echo -n "" | tee $OUT_CSV

  for test in $TESTS; do
    echo -ne "${test}, " | tee -a $OUT_CSV
    $APPS_PATH/aubrac_$1/$AUBRAC_BINARY_PATH $HEX_DIR/${test}.hex | tee -a $OUT_CSV
    echo -ne "\n" | tee -a $OUT_CSV
  done
}

# call with app NAME as $1 (u, p, pp)
function salers_eval {
  OUT_CSV="$RESULT_DIR/salers_$1_timings.csv"
  echo -n "" | tee $OUT_CSV

  for test in $TESTS; do
    echo -ne "${test}, " | tee -a $OUT_CSV
    $APPS_PATH/salers_$1/$SALERS_BINARY_PATH $HEX_DIR/${test}.hex | tee -a $OUT_CSV
    echo -ne "\n" | tee -a $OUT_CSV
  done

  for test in $TESTS_SMT; do
    echo -ne "${test}, " | tee -a $OUT_CSV
    $APPS_PATH/salers_$1/$SALERS_BINARY_PATH $HEX_DIR/${test}.hex | tee -a $OUT_CSV
    echo -ne "\n" | tee -a $OUT_CSV
  done
}

function aubrac_debug {
  OUT_CSV="$RESULT_DIR/aubrac_$1_timings.csv"
  echo -n "" | tee $OUT_CSV

  for test in $TESTS; do
    echo -ne "${test}, " | tee -a $OUT_CSV
    $APPS_PATH/aubrac_$1/$AUBRAC_BINARY_PATH $HEX_DIR/${test}.hex $VCD_DIR/aubrac_$1_${test}.vcd | tee -a $OUT_CSV
    echo -ne "\n" | tee -a $OUT_CSV
  done
}

function salers_debug {
  OUT_CSV="$RESULT_DIR/salers_$1_timings.csv"
  echo -n "" | tee $OUT_CSV

  for test in $TESTS; do
    echo -ne "${test}, " | tee -a $OUT_CSV
    $APPS_PATH/salers_$1/$SALERS_BINARY_PATH $HEX_DIR/${test}.hex $VCD_DIR/salers_$1_${test}.vcd | tee -a $OUT_CSV
    echo -ne "\n" | tee -a $OUT_CSV
  done

  for test in $TESTS_SMT; do
    echo -ne "${test}, " | tee -a $OUT_CSV
    $APPS_PATH/salers_$1/$SALERS_BINARY_PATH $HEX_DIR/${test}.hex $VCD_DIR/salers_$1_${test}.vcd | tee -a $OUT_CSV
    echo -ne "\n" | tee -a $OUT_CSV
  done
}

function build_aubrac_debug {
  echo "************************************************************************************************"
  echo "** Debugging embench $1 with Aubrac"
  echo "************************************************************************************************"

  TEST_DIR="$APPS_PATH/aubrac_$1"
  cd $TEST_DIR
  build_aubrac_embench_tracing $TEST_DIR
  aubrac_debug $1
}

function build_salers_debug {
  echo "************************************************************************************************"
  echo "** Debugging embench $1 with Salers"
  echo "************************************************************************************************"

  TEST_DIR="$APPS_PATH/salers_$1"
  cd $TEST_DIR
  build_salers_embench_tracing $TEST_DIR
  salers_debug $1
}

function build_aubrac_eval {
  echo "************************************************************************************************"
  echo "** Evaluating embench $1 with Aubrac"
  echo "************************************************************************************************"

  TEST_DIR="$APPS_PATH/aubrac_$1"
  cd $TEST_DIR
  build_aubrac_embench $TEST_DIR
  aubrac_eval $1
}

function build_salers_eval {
  echo "************************************************************************************************"
  echo "** Evaluating embench $1 with Salers"
  echo "************************************************************************************************"

  TEST_DIR="$APPS_PATH/salers_$1"
  cd $TEST_DIR
  build_salers_embench $TEST_DIR
  salers_eval $1
}

if [ -z $1 ]; then

build_aubrac_eval "p" &
build_aubrac_eval "pnlp" &
wait
build_aubrac_eval "u" &
build_aubrac_eval "unlp" &
wait
build_aubrac_eval "pp" &
build_aubrac_eval "ppnlp" &
wait
build_aubrac_eval "up" &
build_aubrac_eval "upnlp" &
wait



build_salers_eval "u" &
build_salers_eval "unlp" &
wait
build_salers_eval "up" &
build_salers_eval "upnlp" &
wait
build_salers_eval "p" &
build_salers_eval "pnlp" &
wait
build_salers_eval "pp" &
build_salers_eval "ppnlp" &
wait


else
  if [ $1 = "aubrac" ]; then
      build_aubrac_eval $2 $3
  elif [ $1 = "salers" ]; then
      build_salers_eval $2 $3
  elif [ $1 = "debug" ]; then
      build_salers_debug  $2
  fi
fi