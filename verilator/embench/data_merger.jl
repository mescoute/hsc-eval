# /*
#  * File: data_merger.jl
#  * Project: embench
#  * Created Date: Tuesday September 29th 2020
#  * Author: Ronan (ronan.lashermes@inria.fr)
#  * -----
#  * Last Modified: Tuesday, 29th September 2020 1:06:01 pm
#  * Modified By: Ronan (ronan.lashermes@inria.fr>)
#  * -----
#  * Copyright (c) 2020 INRIA
#  */

using CSV
using DataFrames
using StatsBase


function filter_data(x)
    if typeof(x) == String
        sx = strip(x)
        return sx != "." && sx != "?"
    else
        return true
    end
end

function convert_if(x)::Float64
    if typeof(x) != Float64
        return parse(Float64, x)
    else
        return x
    end
end

function div_if_int(x, d)
    if typeof(x) == String
        sx = strip(x)
        if sx == "." || sx == "?"
            return x
        end
        i = parse(Int64, sx)
        i = round( i / d, digits=2)
        return string(i)
    else
        return round(x/d, digits=2)
    end
end

function benchmarks_results(merged::DataFrame, name::String, norm_vec)
    configs = names(merged)[2:end] #skip "benchmark"


    # write merged csv (in cycles and in seconds) 1MHz

    benchmarks_cycles = copy(merged)
    CSV.write("results/$(name)_benchmarks_cycles.csv", benchmarks_cycles)

    SPEED = 1000000

    benchmarks_seconds = copy(merged)
    for conf in configs
        benchmarks_seconds[!,conf] = div_if_int.(benchmarks_seconds[!,conf], SPEED)
    end

    CSV.write("results/$(name)_benchmarks_seconds.csv", benchmarks_seconds)

    # normalization

    benchmarks_normalized = copy(merged)

    # norm_vec = benchmarks_normalized[!,configs[1]]

    (rows, cols) = size(benchmarks_normalized)

    for conf in configs
        for r in 1:rows
            if r == 1
                if typeof(benchmarks_normalized[1,conf]) == Int64
                    benchmarks_normalized[!,conf] = convert.(Float64, benchmarks_normalized[!,conf])
                end
            end

            
            benchmarks_normalized[r,conf] = div_if_int(benchmarks_normalized[r,conf], norm_vec[r])
        end
    end

    CSV.write("results/$(name)_benchmarks_normalized.csv", benchmarks_normalized)



    # compute geometric means in another file

    shart_benchmarks_normalized = copy(benchmarks_normalized)

    #delete smt benchmarks
    smtrows = findall( x -> occursin("smt", x), merged[:,:benchmark])
    delete!(shart_benchmarks_normalized, smtrows)

    gm_dict = Dict()
    push!(gm_dict, "benchmark"=>"geometric mean")


    for conf in configs
        gm = geomean(map(convert_if, filter(filter_data, shart_benchmarks_normalized[!,conf])))
        push!(gm_dict, conf=>round(gm, digits=2))
        
    end

    gm_df = DataFrame(gm_dict)
    println(name)
    println(gm_df)
    CSV.write("results/$(name)_geomean_normalized.csv", gm_df)

    #smt geomean
    if length(smtrows) > 0

        smt_benchmarks_normalized = copy(benchmarks_normalized)
        smtrows_single = map(x-> replace(x, "-smt" => ""), smt_benchmarks_normalized[smtrows,1])
        smtrows_single = map(x -> findfirst(y -> y==x, smt_benchmarks_normalized[:,1]),smtrows_single)

        gm_smt_dict = Dict()
        push!(gm_smt_dict, "benchmark"=>"geometric mean (smt)")
        gm_dict = Dict()
        push!(gm_dict, "benchmark"=>"geometric mean (single)")

        for conf in configs
            gm = geomean(map(convert_if, filter(filter_data, smt_benchmarks_normalized[smtrows_single,conf])))
            gm_smt = geomean(map(convert_if, filter(filter_data, smt_benchmarks_normalized[smtrows,conf])))
            push!(gm_dict, conf=>round(gm, digits=2))
            push!(gm_smt_dict, conf=>round(gm_smt, digits=2))
        end

        gm_smt_df = DataFrame(gm_dict)
        push!(gm_smt_df, gm_smt_dict)
        println(gm_smt_df)
        CSV.write("results/$(name)_smt_geomean_normalized.csv", gm_smt_df)
    end

end

# u unprotected p protected
# s small b big (cache size)  (in file name: second p -> big)
# nlp (branch prediction)

# read data

#aubrac 
u = CSV.read("results/aubrac_u_timings.csv", header=["benchmark", "small"])
unlp = CSV.read("results/aubrac_unlp_timings.csv", header=["benchmark", "small-nlp"])

p = CSV.read("results/aubrac_p_timings.csv", header=["benchmark", "protected-small"])
pnlp = CSV.read("results/aubrac_pnlp_timings.csv", header=["benchmark", "protected-small-nlp"])

up = CSV.read("results/aubrac_up_timings.csv", header=["benchmark", "big"])
upnlp = CSV.read("results/aubrac_upnlp_timings.csv", header=["benchmark", "big-nlp"])

pp = CSV.read("results/aubrac_pp_timings.csv", header=["benchmark", "protected-big"])
ppnlp = CSV.read("results/aubrac_ppnlp_timings.csv", header=["benchmark", "protected-big-nlp"])

# merge in same DataFrame
merged = innerjoin(u, unlp, p, pnlp, pp, ppnlp, up, upnlp, on=:benchmark)

configs = names(merged)[2:end]

#ref is aubrac_u
norm_vec = copy(merged[!,configs[1]])

#add for smt
smt_bench = ["cubic", "nettle-aes", "nettle-sha256", "nsichneu", "slre"]

for bench in smt_bench
    ind = findfirst(x -> x == bench, merged[:,1])
    push!(norm_vec, merged[ind, configs[1]])
end


benchmarks_results(merged, "aubrac", norm_vec)


# salers 
u = CSV.read("results/salers_u_timings.csv", header=["benchmark", "small"])
unlp = CSV.read("results/salers_unlp_timings.csv", header=["benchmark", "small-nlp"])

p = CSV.read("results/salers_p_timings.csv", header=["benchmark", "protected-small"])
pnlp = CSV.read("results/salers_pnlp_timings.csv", header=["benchmark", "protected-small-nlp"])

up = CSV.read("results/salers_up_timings.csv", header=["benchmark", "big"])
upnlp = CSV.read("results/salers_upnlp_timings.csv", header=["benchmark", "big-nlp"])

pp = CSV.read("results/salers_pp_timings.csv", header=["benchmark", "protected-big"])
ppnlp = CSV.read("results/salers_ppnlp_timings.csv", header=["benchmark", "protected-big-nlp"])

# merge in same DataFrame
merged = innerjoin(u, unlp, p, pnlp, pp, ppnlp, up, upnlp, on=:benchmark)

benchmarks_results(merged, "salers", norm_vec)