#include <stdlib.h>
#include "VAubracTop.h"
#include "verilated.h"
#include "verilated_vcd_c.h"
#include "svdpi.h"
#include "VAubracTop__Dpi.h"
#include <time.h>


int main(int argc, char **argv) {
  // Inputs
  char* hexfile = argv[1];
  char* vcdfile = argv[2];
  // int end_trigger = atoi(argv[3]);

  time_t my_time = time(NULL);

	// Initialize Verilators variables
	Verilated::commandArgs(argc, argv);

	// Create an instance of our module under test
	VAubracTop *dut = new VAubracTop;

  // Generate VCD
  Verilated::traceEverOn(true);
  VerilatedVcdC* m_trace = new VerilatedVcdC;
  dut->trace(m_trace, 99);
  m_trace->open((vcdfile));

  // Call task to initialize memory
  svSetScope(svGetScopeFromName("TOP.AubracTop.ram.ram"));
  //Verilated::scopesDump();
  dut->new_readmemh(hexfile);

	// Test variables
  int cycle = 0;        // Cycle
  int point = 50000;   // Trigger to regularly print cycle number
  //int verify = 0;       // Trigger for the test verification phase

  int trigger = 0;      // Detected triggers
  int start = 0;        // Start test cycle
  int stop = 0;         // End test cycle
  bool end = false;     // Test end

  // **********
  //   RESET
  // **********
  for (int i = 0; i < 5; i++) {
    dut->clock = 1;
  	dut->eval();
    cycle = cycle + 1;
    m_trace->dump(cycle * 10);

		dut->clock = 0;
    dut->reset = 1;
		dut->eval();
    m_trace->dump(cycle * 10 + 5);
  }
  dut->reset = 0;

  printf("Starting...\n");
  // **********
  // TEST LOOP
  // **********
	while ((!Verilated::gotFinish()) && (end == false)) {
    my_time = time(NULL);

		dut->clock = 1;
		dut->eval();
    cycle = cycle + 1;
    m_trace->dump(cycle * 10);

		dut->clock = 0;
		dut->eval();
    m_trace->dump(cycle * 10 + 5);

    // If trigger detected
    if ((dut->io_o_dbg_gpr_29 == 0x101010) && (dut->io_o_dbg_gpr_30 == 0) && (trigger == 0)) {
      printf("Time: %s \n", ctime(&my_time));
      printf("Trigger detected! \n");
      printf("Cycle: %d \n", cycle);
      printf("Start benchmark! \n");
      start = dut->io_o_dbg_gpr_31;
      trigger = 1;
    } else if ((dut->io_o_dbg_gpr_29 == 0x101010) && (dut->io_o_dbg_gpr_30 == 1) && (trigger == 1)) {
      printf("Time: %s \n", ctime(&my_time));
      printf("Trigger detected! \n");
      printf("Cycle: %d \n", cycle);
      printf("Stop benchmark! \n");
      stop = dut->io_o_dbg_gpr_31;
      trigger = 2;
    } else if ((dut->io_o_dbg_gpr_29 == 0x101010) && (dut->io_o_dbg_gpr_30 == 2) && (trigger == 2)) {
      printf("Time: %s \n", ctime(&my_time));
      printf("Trigger detected! \n");
      printf("Cycle: %d \n", cycle);
      printf("Verify benchmark! \n");
      printf("Result: %d", dut->io_o_dbg_gpr_10);
      trigger = trigger + 1;
    } else if (cycle % point == 0) {
      printf("Time: %s \n", ctime(&my_time));
      printf("Cycle: %d \n", cycle);
    }

    // If start, stop and end detected
    if (trigger > 2) {
      end = true;
    }
    // if (cycle == end_trigger) {
    //   end = true;
    // }
	}

  //Final report
  printf("\n");
  printf("End report: \n");
  printf("N cycles: %d \n", stop - start);

  m_trace->close();

  exit(EXIT_SUCCESS);
}
