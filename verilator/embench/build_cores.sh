#!/usr/bin/env nix-shell
#!nix-shell --pure ../../chisel.nix -i bash

####################################################################
# This script build all the cores for the embench evaluation
####################################################################

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
ROOT_PATH="$(realpath "$SCRIPTPATH/../..")"
APPS_PATH="$(realpath "$SCRIPTPATH/apps")"

function build_aubrac_core {
 
    DIR="$APPS_PATH/aubrac_$1"
    mkdir -p $DIR

    cd $ROOT_PATH

    echo "************************************************************************************************"
    echo "** Building ${1^^} in $DIR"
    echo "************************************************************************************************"

    sbt "test:runMain eval.aubrac.AubracEmbench${1^^} --target-dir $DIR --top-name AubracTop"
}

function build_salers_core {
 
    DIR="$APPS_PATH/salers_$1"
    mkdir -p $DIR

    cd $ROOT_PATH

    echo "************************************************************************************************"
    echo "** Building ${1^^} in $DIR"
    echo "************************************************************************************************"

    sbt "test:runMain eval.salers.SalersEmbench${1^^} --target-dir $DIR --top-name SalersTop"
}

if [ -z $1 ]; then

build_aubrac_core "u"
build_aubrac_core "unlp"
build_aubrac_core "p"
build_aubrac_core "pnlp"
build_aubrac_core "pp"
build_aubrac_core "ppnlp"
build_aubrac_core "up"
build_aubrac_core "upnlp"

build_salers_core "u"
build_salers_core "unlp"
build_salers_core "p"
build_salers_core "pnlp"
build_salers_core "up"
build_salers_core "upnlp"
build_salers_core "pp"
build_salers_core "ppnlp"

else
    if [ $1 = "aubrac" ]; then
        build_aubrac_core $2
    elif [ $1 = "salers" ]; then
        build_salers_core $2
    fi

fi