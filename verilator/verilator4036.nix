with import <nixpkgs> {};

let
     pkgs_v4036 = import (builtins.fetchGit {
         # Descriptive name to make the store path easier to identify                
         name = "my-old-revision";                                                 
         url = "https://github.com/nixos/nixpkgs-channels/";                       
         ref = "refs/heads/nixpkgs-unstable";                     
         rev = "c83e13315caadef275a5d074243c67503c558b3b";                                           
     }) {};                                                                           

     verilator4036 = pkgs_v4036.verilator;

in

# Make a new "derivation" that represents our shell
stdenv.mkDerivation {
  name = "verilator4036";

  # The packages in the `buildInputs` list will be added to the PATH in our shell
  buildInputs = [
    verilator4036
  ];
}